xvoice=load('voice.mat');
x3=xvoice.voice;
Fs=xvoice.fs;
tiempo=size(x3,2)/Fs;
t=(0:1/Fs:tiempo-1/Fs);
X3 =abs(fftshift(fft(x3)))./(tiempo*Fs);
f=((-tiempo*Fs)/2:(tiempo*Fs)/2-1).*(1/tiempo);

figure;
subplot(2,1,1);
plt_fs=plot(f,X3);
title ("Señal Voice.mat"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot(2,1,2);
plt_fmax=plot(f((-5000*2)+round((size(t,2)/2),0):(5000*2)+round((size(t,2)/2),0)),abs(X3((-5000*2)+round((size(t,2)/2),0):(5000*2)+round((size(t,2)/2),0))));
title ("Señal Voice.mat"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
set(plt_fs,'Color','green');set(plt_fmax,'Color','green');


%% Modulacion
fmax = 5000;
Am = 4e-3;
Beta = 0.01;
Kp = Beta/Am;
%Kp = 1;
carrier = cos(2*pi*8*fmax*t);
Carrier = abs(fftshift(fft(carrier)))./(tiempo*Fs);
x_pm = cos((2*pi*8*fmax*t)+(Kp*x3));
X_pm = abs(fftshift(fft(x_pm)))./(tiempo*Fs);

X3_fase = angle(fftshift(fft(x3)));
X_pm_fase = angle(fftshift(fft(x_pm)));

figure;
subplot(2,2,1);
plt_carrier=plot(f,Carrier);
title ("Señal portadora"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot(2,2,2);
plt_mensaje=plot(f,X3);
axis([-0.5e4 0.5e4 0 5e-3])
title ("Señal Voice.mat"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot(2,2,3);
plt_PM=plot(f,X_pm);
title ("Señal modulada en PM"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot(2,2,4);
plt_PM_zoom=plot(f,X_pm);
axis([1.9e4 2.9e4 0 5e-3])
title ("Señal modulada en PM"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
set(plt_carrier,'Color','magenta');set(plt_mensaje,'Color','green');set(plt_PM,'Color','cyan');set(plt_PM_zoom,'Color','cyan')

%% Demodulacion

df = f(1:length(f)-1);
dt = t(1:length(t)-1);

dx3 = diff(x_pm);
dx3 = envelope(dx3);
dx3 = dx3 - mean(dx3);
dx3 = cumtrapz(df,dx3);
dx3 = highpass(dx3,25,Fs);

demod_frec = abs(fftshift(fft(dx3)))./(Fs);
uplim = 5000;
low_lim = -uplim;

figure
subplot(2,1,1)
plt_fx3 = plot(f((low_lim*2)+round((size(f,2)/2),0):(uplim*2)+round((size(f,2)/2),0)),abs(X3((low_lim*2)+round((size(t,2)/2),0):(uplim*2)+round((size(f,2)/2),0))));
title ("Señal original x3"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot(2,1,2)
plt_fdemod =plot(df((low_lim*2)+round((size(df,2)/2),0):(uplim*2)+round((size(df,2)/2),0)),abs(demod_frec((low_lim*2)+round((size(df,2)/2),0):(uplim*2)+round((size(df,2)/2),0))));
title ("Señal x3 demodulada"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
set(plt_fx3,'Color','green');set(plt_fdemod,'Color','red');

soundsc(dx3,Fs)
%soundsc(x3,Fs)
