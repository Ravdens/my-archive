tiempo = 5;
Fs = 10^5;
t = 0:(1/Fs):tiempo;
m = (0.4*cos(2*pi*1000*t));
p = (1*cos(2*pi*10000*t));
AM = (0.4+m).*p;
DSB = m.*p;
LSB = m.*p + imag(hilbert(m)).*imag(hilbert(p));
USB = m.*p - imag(hilbert(m)).*imag(hilbert(p));

freq=((-tiempo*Fs)/2:(tiempo*Fs)/2).*(1/tiempo);%crea un vector de frecuencias para el vetor de la transformada
                                                 %(1/tiempo) es para reescalar la frecuencia

tfxAM =fftshift(fft(AM))./(tiempo*Fs); %vector de la traformada de la señal

tfxDSB =fftshift(fft(DSB))./(tiempo*Fs); 

tfxLSB =fftshift(fft(LSB))./(tiempo*Fs);

tfxUSB =fftshift(fft(USB))./(tiempo*Fs);

subplot(2,2,1);
plot(freq,abs(tfxAM));
title('magnitud AM');

subplot(2,2,2);
plot(freq,abs(tfxDSB));
title('magnitud DSB');

subplot(2,2,3);
plot(freq,abs(tfxLSB));
title('magnitud LSB');

subplot(2,2,4);
plot(freq,abs(tfxUSB));
title('magnitud USB');

