tiempo = 5;
Fs = 10^5;
t = 0:(1/Fs):tiempo;
x1 = cos(2*pi*100*t);
xc = cos(2*pi*5000*t);
xFM = cos(2*pi*5000*t+3.8*100*2*pi*integral(@X1,-Inf,tiempo));
freq=((-tiempo*Fs)/2:(tiempo*Fs)/2).*(1/tiempo);
tfxFM =fftshift(fft(xFM))./(tiempo*Fs);
plot(freq((4500*tiempo)+round((size(t,2)/2),0):(5500*tiempo)+round((size(t,2)/2),0)),abs(tfxFM((4500*tiempo)+round((size(t,2)/2),0):(5500*tiempo)+round((size(t,2)/2),0))));

function x = X1(t)
    x=cos(2*pi*100*t);
end
