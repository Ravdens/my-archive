function x = square(t,wp)
    T = 2*pi/(wp);
    t = mod(t,T);
    x = 1-(sign(t-(T/4))-sign(t-(3*T/4))>0);
end

