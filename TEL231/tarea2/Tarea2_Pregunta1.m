Am = 0.4;
Ac = 1;
Fs = 100000;
%Esto es para graficar las funciones en el tiempo y que se vean bien
cap = ceil(0.01.*Fs); 
t = 0:(1/Fs):1;
f = (-Fs/2:Fs/2);
wm = 2*pi*1000;
wp = 2*pi*10000;
m = Am*cos(wm*t);
M = abs(fftshift(fft(m)))./(Fs);
p = Ac*cos(wp*t);
P = abs(fftshift(fft(p)))./(Fs);

%% Modulacion
% AM / DSB
xAM = (m+Ac).*cos(wp*t);
XAM = abs(fftshift(fft(xAM)))./(Fs);

xDSB = m.*p;
XDSB = abs(fftshift(fft(xDSB)))./(Fs);

%USB / LSB
%Nota: Para esta seccion, Si representa la señal en la etapa i del diagrama
%del circuito de modulacion SSB visto en clase

S1 = m;
S2 = imag(hilbert(S1));
S3 = p;
S4 = imag(hilbert(p));
S5 = S1.*S3;
S6 = S2.*S4;
xLSB = S5+S6;
xUSB = S5-S6;

XLSB = abs(fftshift(fft(xLSB)))./(Fs);
XUSB = abs(fftshift(fft(xUSB)))./(Fs);

figure;
subplot (5,1,1);
pltM = plot(f,M);
title ("Señal original"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot (5,1,2);
pltAM = plot(f,XAM);
title ("Señal AM"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot (5,1,3);
pltDSB = plot(f,XDSB);
title ("Señal DSB"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot (5,1,4);
pltUSB = plot(f,XUSB);
title ("Señal SSB-USB"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot (5,1,5);
pltLSB = plot(f,XLSB);
title ("Señal SSB-LSB"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
set(pltM,'Color','green');set(pltAM,'Color','blue');set(pltDSB,'Color','cyan');set(pltUSB,'Color','red');set(pltLSB,'Color','magenta');

%% Demodulacion

Fcorte = 5000;
[b,a] = butter(6,Fcorte/(Fs/2));

% Demodulacion sincronica

m_AM_sinc = xAM.*cos(wp*t);
m_DSB_sinc = xDSB.*cos(wp*t);

m_AM_sinc = filter(b,a,m_AM_sinc);
m_DSB_sinc = filter(b,a,m_DSB_sinc);

M_AM_sinc = abs(fftshift(fft(m_AM_sinc)))./(Fs);
M_DSB_sinc = abs(fftshift(fft(m_DSB_sinc)))./(Fs);

% Demodulacion asincronica

m_AM_asinc = xAM.*square(t,wp);
m_DSB_asinc = xDSB.*square(t,wp);

Fcorte = 5000;
[b,a] = butter(6,Fcorte/(Fs/2));
m_AM_asinc = filter(b,a,m_AM_asinc);
m_DSB_asinc = filter(b,a,m_DSB_asinc);

M_AM_asinc = abs(fftshift(fft(m_AM_asinc)))./(Fs);
M_DSB_asinc = abs(fftshift(fft(m_DSB_asinc)))./(Fs);

% para demodulacion asincronica, se sustituye el diodo y capacitor por un
% pulso cuadrado equivalente.

figure;
subplot (2,2,1);
b_pltAMa = plot(f,M_AM_asinc);
title ("Señal AM demoulada asincronicamente"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot (2,2,2);
b_pltDSBa = plot(f,M_DSB_asinc);
title ("Señal DSB demoulada asincronicamente"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot (2,2,3);
b_pltAMs = plot(f,M_AM_sinc);
title ("Señal AM demoulada sincronicamente"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot (2,2,4);
b_pltDSBs = plot(f,M_DSB_sinc);
title ("Señal DSB demoulada sincronicamente"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
set(b_pltAMa,'Color','red');set(b_pltDSBa,'Color','blue');set(b_pltAMs,'Color','red');set(b_pltDSBs,'Color','blue');
