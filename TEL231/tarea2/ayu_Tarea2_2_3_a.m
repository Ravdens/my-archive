xvoice=load('voice.mat');
x3=xvoice.voice;
Fs=xvoice.fs;
tiempo=size(x3,2)/Fs;
t=(0:1/Fs:tiempo-1/Fs);
tfx3 =fftshift(fft(x3))./(tiempo*Fs);
freq=((-tiempo*Fs)/2:(tiempo*Fs)/2-1).*(1/tiempo);
subplot(2,1,1);
plot(freq,abs(tfx3));

subplot(2,1,2);
plot(freq((-5000*2)+round((size(t,2)/2),0):(5000*2)+round((size(t,2)/2),0)),abs(tfx3((-5000*2)+round((size(t,2)/2),0):(5000*2)+round((size(t,2)/2),0))));