tiempo = 5;
Fs = 10^5;
t = 0:(1/Fs):tiempo;
x2 = cos(2*pi*100*t)+cos(2*pi*120*t);
xc = cos(2*pi*5000*t);
xFM = cos(2*pi*5000*t+75*integral(@X2,-Inf,tiempo));
freq=((-tiempo*Fs)/2:(tiempo*Fs)/2).*(1/tiempo);
tfxFM =fftshift(fft(xFM))./(tiempo*Fs);

subplot(3,1,1);
plot(freq((4500*tiempo)+round((size(t,2)/2),0):(5500*tiempo)+round((size(t,2)/2),0)),abs(tfxFM((4500*tiempo)+round((size(t,2)/2),0):(5500*tiempo)+round((size(t,2)/2),0))));
title('Amplitud x2 modulada FM');

fc = 80;
grado_filtro=6;
[b,a] = butter(grado_filtro,fc/(Fs/2));
x2_2 = filter(b,a,x2);
tfxx2 =fftshift(fft(x2))./(tiempo*Fs);
tfxx2_2 =fftshift(fft(x2_2))./(tiempo*Fs);

subplot(3,1,2);
plot(freq((80*5)+round((size(t,2)/2),0):(140*5)+round((size(t,2)/2),0)),abs(tfxx2((80*5)+round((size(t,2)/2),0):(140*5)+round((size(t,2)/2),0))));
title('x2 sin filtrar');

subplot(3,1,3);
plot(freq((80*5)+round((size(t,2)/2),0):(140*5)+round((size(t,2)/2),0)),abs(tfxx2_2((80*5)+round((size(t,2)/2),0):(140*5)+round((size(t,2)/2),0))));
title('x2 filtrada');

function x = X2(t)
    x=cos(2*pi*100*t)+cos(2*pi*120*t);
end