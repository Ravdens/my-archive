%% Pregunta 1
Fs = 100000;
cap = ceil(0.01.*Fs); 
t = 0:(1/Fs):1;
f = (-Fs/2:Fs/2);
w1 = 2*pi*100;
wc = 2*pi*5000;
Beta = 3.8317;
xFM = cos(wc*t+Beta*sin(w1*t));
XFM = abs(fftshift(fft(xFM)))./(Fs);
plot(f(4000+(Fs/2)+1:6000+(Fs/2)+1),XFM(4000+(Fs/2)+1:6000+(Fs/2)+1));
title ("x1 modulada en FM con B = 3.8317"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");

%% Pregunta 2.a

w2 = 2*pi*120;
Kf = 75;
x2 = cos(w1*t)+cos(w2*t);
xFM = cos(wc*t+Kf*((sin(w1*t)/w1)+(sin(w2*t)/w2)));
XFM = abs(fftshift(fft(xFM)))./(Fs);
XFM_DBm = 10*log(XFM/1e-3);
figure
subplot(2,1,1)
plot(f(4500+(Fs/2)+1:5500+(Fs/2)+1),XFM(4500+(Fs/2)+1:5500+(Fs/2)+1));
title ("x2 modulada en FM con Kf = 75"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot(2,1,2)
plot(f(4500+(Fs/2)+1:5500+(Fs/2)+1),XFM_DBm(4500+(Fs/2)+1:5500+(Fs/2)+1));
title ("Espectro x2FM en dBm"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud logaritmica [dBm]");

%% Pregunta 2.b

Fcorte = 120;
[b,a] = butter(6,Fcorte/(Fs/2),"high");
x2_filt1 = filter(b,a,x2);

X2 = abs(fftshift(fft(x2)))./(Fs);
X2_filt1 = abs(fftshift(fft(x2_filt1)))./(Fs);

[b,a] = butter(7,Fcorte/(Fs/2),"high");
x2_filt2 = filter(b,a,x2);
X2_filt2 = abs(fftshift(fft(x2_filt2)))./(Fs);

figure
subplot(3,1,1)
plt_x2 = plot(f(-200+(Fs/2)+1:200+(Fs/2)+1),X2(-200+(Fs/2)+1:200+(Fs/2)+1));
title ("Espectro x2 original"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot(3,1,2)
plt_x2_filt1 = plot(f(-200+(Fs/2)+1:200+(Fs/2)+1),X2_filt1(-200+(Fs/2)+1:200+(Fs/2)+1));
title ("x2 bajo filtro de orden 6"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot(3,1,3)
plt_x2_filt2 = plot(f(-200+(Fs/2)+1:200+(Fs/2)+1),X2_filt2(-200+(Fs/2)+1:200+(Fs/2)+1));
title ("x2 bajo filtro de orden 7"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
set(plt_x2,'Color','blue');set(plt_x2_filt1,'Color','green');set(plt_x2_filt2,'Color','red')


%% Pregunta 2.c Demodulacion

df = f(1:length(f)-1);
dt = t(1:length(t)-1);

dx2 = diff(xFM);
dx2 = envelope(dx2);
dx2 = dx2 - mean(dx2);

demod_frec = abs(fftshift(fft(dx2)))./(Fs);

figure
subplot(2,1,1)
plt_fx2 = plot(f(-200+(Fs/2)+1:200+(Fs/2)+1),X2(-200+(Fs/2)+1:200+(Fs/2)+1));
title ("Señal original X2"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot(2,1,2)
plt_fdemod = plot(df(-200+(Fs/2)+1:200+(Fs/2)+1),demod_frec(-200+(Fs/2)+1:200+(Fs/2)+1));
title ("Señal X2 demodulada"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
set(plt_fx2,'Color','blue');set(plt_fdemod,'Color','red');

