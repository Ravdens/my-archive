function x = triang(t)
   t = mod(t,0.05);
   x=-0.75 + ((1-abs((t-0.025)*20)).*(t>=-0.05).*(t<0.05));
   x = x*2
end