function x = saw(t)
    t = mod(t,0.05);
    x = -0.5+20*t.*(t<0.05).*(t>-0.05);
end

