function  x = rect(t)
    t = mod(t,0.05);
    x = -0.5+((sign(t+0.025)-sign(t-0.025))>0);
end