tiempo = 5;
Fs = 100; 
t = 0:(1/Fs):tiempo;
x = saw(t)

plot(t,x);
title('señal original');
tfx =fftshift(fft(x))./(tiempo*Fs); 
freq=((-tiempo*Fs)/2:(tiempo*Fs)/2).*(1/tiempo);
h=(-5*5)+251:(5*5)+251;


subplot(2,1,1);
m = plot(freq(h),abs(tfx(h)));
title("Señal triang")
set(m,'Linewidth',2,'Color','red');
ylabel('Magnitud'); xlabel('Frecuencia /100[hz]');


subplot(2,1,2)
a = plot(freq(h),angle(tfx(h)));
set(a,'Linewidth',2,'Color','blue');
ylabel('Fase'); xlabel('Frecuencia /100[hz]')
%title('fase');
