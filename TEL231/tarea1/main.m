%% Pregunta 1
tiempo = 0.1;
Fs = 1000;
t = 0:(1/Fs):tiempo;
f = ((-tiempo*Fs)/2:(tiempo*Fs)/2).*(1/tiempo);
x1 = triang(t);
X1 = fftshift(fft(x1))./(tiempo*Fs);
x2 = rect(t);
X2 = fftshift(fft(x2))./(tiempo*Fs);
x3 = saw(t);
X3 = fftshift(fft(x3))./(tiempo*Fs);

figure;
subplot (2,1,1);
pl1a = plot(f,abs(X1));
title ("Señal triangular"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot (2,1,2);
pl1b = plot(f,angle(X1));
xlabel ("Frecuencia [Hz]"); ylabel ("Fase");
set(pl1a,'Color','green');set(pl1b,'Color','green');

figure;
subplot (2,1,1);
pl1a = plot(f,abs(X2));
title ("Señal cuadrada"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot (2,1,2);
pl1b = plot(f,angle(X2));
xlabel ("Frecuencia [Hz]"); ylabel ("Fase");
set(pl1a,'Color','cyan');set(pl1b,'Color','cyan');

figure;
subplot (2,1,1);
pl1a = plot(f,abs(X3));
title ("Señal diente de sierra"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
subplot (2,1,2);
pl1b = plot(f,angle(X3));
xlabel ("Frecuencia [Hz]"); ylabel ("Fase");
set(pl1a,'Color','magenta');set(pl1b,'Color','magenta');


%% Pregunta 2

tiempo = 0.5;
Fs = 1000;
t = 0:(1/Fs):tiempo;
f = ((-tiempo*Fs)/2:(tiempo*Fs)/2).*(1/tiempo);
x1 = triang(t);
X1 = fftshift(fft(x1))./(tiempo*Fs);
x2 = rect(t);
X2 = fftshift(fft(x2))./(tiempo*Fs);
x3 = saw(t);
X3 = fftshift(fft(x3))./(tiempo*Fs);

x1R=0;
armonicos = 5;
for i = 1:armonicos
    x1R = x1R + 0.025*((sin(pi*20*0.025*i)/(pi*20*0.025*i)).^2)*cos(2*pi*t*i*20);
    %x1R = x1R + real(X1(i*20+250))*cos(2*pi*t*20*i);
    %x1R = x1R + imag(X1(i*20+250))*sin(2*pi*t*20*i);
end

figure;
subplot (2,1,1);
pl1a = plot(t,x1R);
title ("Señal triangular reconstruida"); xlabel ("Tiempo [s]"); ylabel ("Magnitud");
subplot (2,1,2);
pl1b = plot(t,x1);
title("Señal triangular original"); xlabel ("Tiempo [s]"); ylabel ("Magnitud");
set(pl1a,'Color','green');set(pl1b,'Color','green');


x2R=0;
armonicos = 5;
for i = 1:armonicos;
    x2R = x2R  + 0.025*(sin(pi*20*0.025*i)/(pi*20*0.025*i))*cos(2*pi*t*20*i);
end
x2R = real(x2R);

figure;
subplot (2,1,1);
pl2a = plot(t,x2R);
title ("Señal rectangular reconstruida"); xlabel ("Tiempo [s]"); ylabel ("Magnitud");
subplot (2,1,2);
pl2b = plot(t,x2);
title("Señal rectangular original"); xlabel ("Tiempo [s]"); ylabel ("Magnitud");
set(pl2a,'Color','cyan');set(pl2b,'Color','cyan');

x3R=0;
armonicos = 5;
for i = 1:armonicos;
   x3R = x3R + (((-1).^(2i+1))/(pi*i))*sin(2*pi*t*i*20);
end
x3R = real(x3R);

figure;
subplot (2,1,1);
pl3a = plot(t,x3R);
title ("Señal diente de sierra reconstruida"); xlabel ("Tiempo [s]"); ylabel ("Magnitud");
subplot (2,1,2);
pl3b = plot(t,x3);
title("Señal diente de sierra original"); xlabel ("Tiempo [s]"); ylabel ("Magnitud");
set(pl3a,'Color','magenta');set(pl3b,'Color','magenta');

%% Parte 3
tiempo = 0.5;
Fs = 1000;
t = 0:(1/Fs):tiempo;
f = ((-tiempo*Fs)/2:(tiempo*Fs)/2).*(1/tiempo);
x2 = rect(t);

figure;
subplot (5,1,1);
pl2a = plot(t,x2);
title("Señal rectangular original"); xlabel ("Tiempo [s]"); ylabel ("Magnitud");

for Nh = 1:4

    x2R=0;
    armonicos = 3+2*Nh;
    for i = 1:armonicos
        x2R = x2R  + 0.025*(sin(pi*20*0.025*i)/(pi*20*0.025*i))*cos(2*pi*t*20*i);
    end
    x5R = real(x2R);
    
    subplot (5,1,Nh+1);
    pl2b = plot(t,x2R);
    title ("Señal reconstruida con "+armonicos+" armonicos"); xlabel ("Tiempo [s]"); ylabel ("Magnitud");
end