t = -0.1:0.001:0.1;
%x = ustep(t);
y = rect(t);
z = triang(t);
x = saw(t);
%plot(t,x);

subplot(1,3,1);
title('saw');
xlabel('t'); ylabel('función');
plot(t,x);

subplot(1,3,2);
title('rectangulo');
xlabel('t'); ylabel('función');
plot(t,y);

subplot(1,3,3);
c = plot(t,z);
title('triangulo');
xlabel('t'); ylabel('función');


