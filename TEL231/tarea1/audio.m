[x,Fs]=audioread('C.wav');
xT = x.';
tiempo = size(x,1)/Fs;
t = 0:(1/Fs):tiempo;
f = ((-Fs*tiempo)/2:(Fs*tiempo)/2-1).*(1/tiempo);
Sp = fftshift(fft(x.'))./(tiempo*Fs);

%% Grafico a
plA = plot(f,abs(Sp));
title ("C.wav"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud");
set(plA,'Color','red');


%% Grafico b
newT = 0:1/Fs:3;

F = 0.005368*cos(2*pi*newT*1568.37);
F = F + 0.007918*cos(2*pi*newT*2091.04);
F = F + 0.009635*cos(2*pi*newT*3136.62);
F = F + 0.000856*cos(2*pi*newT*4182.2);
F = F + 0.001135*cos(2*pi*newT*4705.11);
F = F + 0.001550*cos(2*pi*newT*6273.36);
plotT = 0:(1/Fs):0.01;

from = 1;
to = 1.01;

figure;
subplot (2,1,1);
pl2a = plot(newT(from*Fs:to*Fs) , xT(from*Fs:to*Fs));
title ("Señal original C.wav"); xlabel ("Tiempo [s]"); ylabel ("Amplitud");
subplot (2,1,2);
pl2b = plot(newT(from*Fs:to*Fs),F(from*Fs:to*Fs));
title ("Señal C.wav reconstruida con los primeros 10 armonicos"); xlabel ("Tiempo [s]"); ylabel ("Amplitud");
set(pl2a,'Color','red');set(pl2b,'Color','blue');
%soundsc(x,Fs)
%soundsc(F,Fs)


%% Graficos C
[x,Fs]=audioread('G.wav');
xT = x.';
tiempo = size(x,1)/Fs;
t = 0:(1/Fs):tiempo;
f = ((-Fs*tiempo)/2:(Fs*tiempo)/2-1).*(1/tiempo);
Sp = fftshift(fft(x.'))./(tiempo*Fs);

F = 0.005368*cos(2*pi*newT*1568.37);
F = F + 0.007918*cos(2*pi*newT*2091.04);
F = F + 0.009635*cos(2*pi*newT*3136.62);
F = F + 0.000856*cos(2*pi*newT*4182.2);
F = F + 0.001135*cos(2*pi*newT*4705.11);
F = F + 0.001550*cos(2*pi*newT*6273.36);
F = F .*cos(2*pi*258*newT);

figure;
from = 1;
to = 1.01;
subplot (2,1,1);
pl2a = plot(newT(from*Fs:to*Fs) , xT(from*Fs:to*Fs));
title ("Señal original G.wav"); xlabel ("Tiempo [s]"); ylabel ("Amplitud");
subplot (2,1,2);
pl2b = plot(newT(from*Fs:to*Fs),F(from*Fs:to*Fs));
title ("Señal C.wav reconstruida con los primeros 10 armonicos modulada en coseno"); xlabel ("Tiempo [s]"); ylabel ("Amplitud");
set(pl2a,'Color','magenta');set(pl2b,'Color','green');
soundsc(x,Fs);
soundsc(F,Fs);
