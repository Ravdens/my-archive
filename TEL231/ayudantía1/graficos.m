t = -3:0.01:3;
x = ustep(t);
y = rect(t);
z = triang(t);
%plot(t,x);

subplot(1,3,1);
plot(t,x);

subplot(1,3,2);
plot(t,y);

subplot(1,3,3);
c = plot(t,z);
set(c,'Linewidth',2,'Color','green','Linestyle','-.');
title('triangulo');
xlabel('t'); ylabel('función');


