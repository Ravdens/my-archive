function  x = rect(t)
    x = (sign(t+0.5)-sign(t-0.5)>0);
end