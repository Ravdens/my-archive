function x = triang(t)
    x= (1-abs(t)).*(t>=-1).*(t<1);
end