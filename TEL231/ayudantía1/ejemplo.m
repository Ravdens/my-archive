tiempo = 5; %tiempo de la señal 
Fs = 100; %frecuancia de sampleo
t = 0:(1/Fs):tiempo; %vector del tiempo
x = rect(t)

subplot(2,2,1);
plot(t,x);
title('señal original');
tfx =fftshift(fft(x))./(tiempo*Fs); %vector de la traformada de la señal
freq=((-tiempo*Fs)/2:(tiempo*Fs)/2).*(1/tiempo); %crea un vector de frecuencias para el vetor de la transformada
                                                 %(1/tiempo) es para reescalar la frecuencia
                                                 %luego toma muestras cada 1/5 Hz por el reescalado
                                               
x4 = 0;
armonicos = 4;
for i = -armonicos:armonicos
    x4=x4+tfx((i*5)+251)*exp(1i*2*pi*t*i); %reconstrucción de la señal con los armonicos
    %1i es imaginario
    %(i*5)+251 es para centrar los armonicos en 0 y 5 es para reescarlar,
    %ya que los armonicos estan cada 5 muestras por el tiempo
end
x4 = real(x4); %la reconstrucción con exp deja elemento iumaginarios que hay que eliminar

subplot(2,2,2);
plot(t,x4);
title('señal reconstruida');

subplot(2,2,3);
plot(freq((-5*5)+251:(5*5)+251),abs(tfx((-5*5)+251:(5*5)+251))); %abs es para la magnitud
title('magnitud');

subplot(2,2,4);
plot(freq((-5*5)+251:(5*5)+251),angle(tfx((-5*5)+251:(5*5)+251))); %abs es para la magnitud
title('fase');
