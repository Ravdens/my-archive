from matplotlib import pyplot as plt

num = input(":_ ")




fhead = "tcp"+num
file = open(fhead+"paquetes.csv","r")


tiempo = []
numero = []
flag = True

for line in file:
    if flag == True:
        flag = False
        continue

    line=line.strip().split(",")
    newstring = ""
    for letra in line[0]:
        if letra!='"':
            newstring = newstring + letra
    line[0] = newstring


    newstring = ""
    for letra in line[1]:
        if letra!='"':
            newstring = newstring + letra
    line[1] = newstring

    if float(line[1])<=60 and float(line[1]) >= 0:

        print(line)        
        numero.append(float(line[0]))
        tiempo.append(float(line[1]))

file.close()

plt.xlabel("Tiempo transcurrido en segundos [s]")
plt.ylabel("Numero identificador de paquetes en wireshark")
if num == "1":
    plt.title("Paquetes entrantes a la aplicación (World of Warcraft) : Perdida = 10%")
if num == "2":
    plt.title("Paquetes entrantes a la aplicación (World of Warcraft) : Perdida = 80%")
plt.grid(True)
plt.plot(tiempo,numero,"r.")
plt.show()




fhead = "udp"+num
file = open(fhead+"paquetes.csv","r")


tiempo = []
numero = []
flag = True

for line in file:
    if flag == True:
        flag = False
        continue

    line=line.strip().split(",")

    newstring = ""
    for letra in line[0]:
        if letra!='"':
            newstring = newstring + letra
    line[0] = newstring


    newstring = ""
    for letra in line[1]:
        if letra!='"':
            newstring = newstring + letra
    line[1] = newstring

    if float(line[1])<=60 and float(line[1]) >= 0:

        print(line)
        numero.append(float(line[0]))
        tiempo.append(float(line[1]))
        

file.close()

plt.xlabel("Tiempo transcurrido en segundos [s]")
plt.ylabel("Numero identificador de datagramas en wireshark")
if num == "1":
    plt.title("Paquetes entrantes a la aplicación (Counter Strike: Global Offensive) : Perdida = 10%")
if num == "2":
    plt.title("Paquetes entrantes a la aplicación (Counter Strike: Global Offensive) : Perdida = 80%")
plt.grid(True)
plt.plot(tiempo,numero,"b.")
plt.show()