from socket import *
#serverName = 'localhost'
serverName = "aragorn.elo.utfsm.cl"  
serverPort = 47200
clientSocket = socket(AF_INET,SOCK_DGRAM)
message = input("Mensaje a enviar: ")


clientSocket.sendto(message.encode(),(serverName, serverPort))
modifiedMessage, serverAddress = clientSocket.recvfrom(2048)
print(f"Respuesta recibida: {modifiedMessage.decode()}")
        
clientSocket.close()
