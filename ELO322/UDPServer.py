
from socket import *
serverPort = 47204  
serverSocket = socket(AF_INET, SOCK_DGRAM)  # AF_INET is used for IPv4 and SOCK_DRAM for UDP.
serverSocket.bind(('', serverPort))
print ("The server is ready to receive")
while True:
	message, clientAddress = serverSocket.recvfrom(2048)
	modifiedMessage = message.decode().upper()
	serverSocket.sendto(modifiedMessage.encode(), clientAddress)
