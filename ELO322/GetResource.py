from socket import *
serverName ='profesores.elo.utfsm.cl'
serverPort = 80
clientSocket = socket(AF_INET, SOCK_STREAM)

#Ejemplo: /~agv/elo322/HTTP/prueba.html
recurso = input("Item a pedir: ")
request = "GET "+ recurso +" HTTP/1.1\r\nHOST: profesores.elo.utfsm.cl\r\n\r\n"
try:
    clientSocket.connect((serverName,serverPort))
    clientSocket.send(request.encode())
    response = clientSocket.recv(1024).decode()
    print("Respuesta: "+response)
except:
    print("Hubo un problema. Revise su conexión a internet e intente nuevamente.")

clientSocket.close()