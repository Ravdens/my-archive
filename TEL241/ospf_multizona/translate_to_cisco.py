
from cgitb import enable

cisco = open("RouterconfigCISCO_python.txt","w")
huawei = open("RouterConfig(HUAWEI).txt","r")
area = "NILL"
lastline = "NILL"

for line in huawei:
    try:
        linesplit = line.strip("\n").split(" ")

        print(line)
        print(linesplit)


        if linesplit[0]=="system":
            cisco.write("enable\nconfigure terminal\n")

        elif linesplit[0]=="undo":
            cisco.write("no shutdown\n")

        elif linesplit[0]=="y":
            continue

        elif linesplit[0]=="quit":
            if lastline=="quit\n":
                continue
            else:
                cisco.write("exit\n")

        elif linesplit[0]=="interface":
            newIF = linesplit[2][0]+"/"+linesplit[2][4]
            cisco.write("interface FastEthernet "+newIF+"\n")


        elif linesplit[0]=="ospf":
            cisco.write("router ospf "+linesplit[1]+"\nrouter-id "+linesplit[3]+"\n")

        elif linesplit[0]=="area":
            area = linesplit[1]

        elif linesplit[0]=="network":
            cisco.write("network "+linesplit[1]+" "+linesplit[2]+" area "+area+"\n")

        elif linesplit[0]=="save":
            cisco.write("copy running-config startup-config\n")

        else:
            cisco.write(line);
    except:
        cisco.write("ERRORLINE: "+line);

    lastline = line;
