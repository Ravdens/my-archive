# Este es mi MD

# CISCO

____________________________
### Configurar interfaz router:


enable

configure terminal

interface < *Tipointerfaz* > < *ID interfaz* >

ip address < *direccion* > < *mascara* >

no shutdown

exit


___________________________
### Configurar dirección fija:


enable

configure terminal

ip route < *ip_red_dest* > < *mascara* > < *ip_nexthop* >


___________________________
### Default gateway:

enable

configure terminal

ip route 0.0.0.0 0.0.0.0 < *ip_nexthop* >

___________________________
### Guardar: 	

enable

configure terminal

copy running-config startup-config

_or_

wr

___________________________
### Mostrar direcciones:

show ip route

show ip interface brief

___________________________
### Borrar startup config:

erase startup-config


___________________________
### RipV2:

router rip
passive-inteface < *Tipointerfaz* > < *ID interfaz* >
network < *ip_zona* >
exit


___________________________
### OSPF:

router ospf < *n* >

router-id < *id* >

network < *ip_zona* > < *mascara_formato_wildcard* > area < *nA* >

exit

___________________________
### ACL:

enable

configure terminal

ip access-list < *nombre* >

< *permite/deny* > < *protocolo* > < *snet + mask / host + sip* > < *dnet + mask / host + dip* > < *id / eq+port* >

interface < *Tipointerfaz* > < *ID interfaz* >

ip access-group < *nombre* >

exit


## Switch

___________________________
### make password:

enable

configure terminal

line console < *n* >

password < *password de entrada al switch* >

login

___________________________
### make admin password:

enable

configure terminal

enable password < *password para admins* >

_or_

enable secret < *password encriptada* >

___________________________
### encriptar passwords:

enable

configure terminal

service password-encryption

___________________________
### vtp:

enable

configure terminal

vtp mode < *server / client / transparent* >

___________________________
### create vlans:

enable

configure terminal

vlan < *n* >

name < *name* >

exit

___________________________
### bind vlan:

enable

configure terminal

interface < *Tipointerfaz* > < *ID interfaz* >

switchport mode < *acces / trunk* >

switchport access vlan < *n* >



# HUAWEI

____________________
### Configurar interfaz router:

system

interface < *Tipointerfaz* > < *ID interfaz* >

ip address < *direccion* > < *mascara* >

undo shutdown

quit

___________________________
### Configurar dirección fija:

system

ip route-static < *ip_red_dest* > < *mascara* > < *ip_nexthop* >

___________________________
### Default gateway:

system

ip route-static	 0.0.0.0 0.0.0.0 < *ip_nexthop* >

___________________________
### Guardar: 	

save

___________________________
### Mostrar direcciones:

display ip routing-table

display ospf peer

___________________________
### Borrar startup config:

reset saved-configuration

___________________________
### RipV2:

Rip

silent-interface < *Tipointerfaz* > < *ID interfaz* >

network < *ip_zona* >

quit

___________________________
### OSPF:

ospf < *n* > router-id < *routerid* >

area < *nA* >

network < *ip_zona* > < *mascara_formato_wildcard* >

quit


