Router config txt file
Sergio Ehlen Montero * 202130016-3 * 30/03/2023

*****
CISCO
*****



/-RIPV2-/------------------------------------------------
_______________________________________
Router Ra

enable
configure terminal

interface FastEthernet 0/0
ip address 10.0.0.1 255.0.0.0
no shutdown
exit

interface Serial 0/0/0
ip address 200.0.1.1 255.255.255.252
no shutdown
exit

interface Serial 0/0/1
ip address 200.0.2.1 255.255.255.252
no shutdown
exit

router rip
version 2
no auto-summary
passive-interface FastEthernet 0/0
network 10.0.0.0
network 200.0.1.0
network 200.0.2.0
exit

exit
copy running-config startup-config
y


_______________________________________
Router Rb

enable
configure terminal

interface FastEthernet 0/0
ip address 172.16.1.1 255.255.255.0
no shutdown
exit

interface FastEthernet 0/1
ip address 172.16.2.1 255.255.255.0
no shutdown
exit

interface Serial 0/0/0
ip address 200.0.2.2 255.255.255.252
no shutdown
exit

interface Serial 0/0/1
ip address 200.0.3.1 255.255.255.252
no shutdown
exit

router rip
version 2
no auto-summary
passive-interface FastEthernet 0/0
passive-interface FastEthernet 0/1
network 172.16.1.0
network 172.16.2.0
network 200.0.2.0
network 200.0.3.0
exit

exit
copy running-config startup-config
y


_______________________________________
Router Rc

enable
configure terminal

interface FastEthernet 0/0
ip address 192.168.1.1 255.255.255.0
no shutdown
exit

interface Serial 0/0/0
ip address 200.0.1.2 255.255.255.252
no shutdown
exit

interface Serial 0/0/1
ip address 200.0.3.2 255.255.255.252
no shutdown
exit

router rip
version 2
no auto-summary
passive-interface FastEthernet 0/0
network 192.168.1.0
network 200.0.1.0
network 200.0.3.0
exit

exit
copy running-config startup-config
y




/-OSPF-/------------------------------------------------
_______________________________________
Router Ra

enable
configure terminal

interface FastEthernet 0/0
ip address 10.0.0.1 255.0.0.0
no shutdown
exit

interface Serial 0/0/0
ip address 200.0.1.1 255.255.255.252
no shutdown
exit

interface Serial 0/0/1
ip address 200.0.2.1 255.255.255.252
no shutdown
exit

router ospf 1
router-id 1.1.1.1
network 10.0.0.0 0.255.255.255 area 0
network 200.0.1.1 0.0.0.3 area 0
network 200.0.2.1 0.0.0.3 area 0
exit

exit
copy running-config startup-config
y


______________________________________
Router Rb

enable
configure terminal

interface FastEthernet 0/0
ip address 172.16.1.1 255.255.255.0
no shutdown
exit

interface FastEthernet 0/1
ip address 172.16.2.1 255.255.255.0
no shutdown
exit

interface Serial 0/0/0
ip address 200.0.2.2 255.255.255.252
no shutdown
exit

interface Serial 0/0/1
ip address 200.0.3.1 255.255.255.252
no shutdown
exit

router ospf 1
router-id 2.2.2.2
network 172.16.1.0 0.0.0.255 area 0
network 172.16.2.0 0.0.0.255 area 0
network 200.0.2.2 0.0.0.3 area 0
network 200.0.3.1 0.0.0.3 area 0
exit

exit
copy running-config startup-config
y


_______________________________________
Router Rc

enable
configure terminal

interface FastEthernet 0/0
ip address 192.168.1.1 255.255.255.0
no shutdown
exit

interface Serial 0/0/0
ip address 200.0.1.2 255.255.255.252
no shutdown
exit

interface Serial 0/0/1
ip address 200.0.3.2 255.255.255.252
no shutdown
exit

router ospf 1
router-id 3.3.3.3
network 192.168.1.0 0.0.0.255 area 0
network 200.0.1.0 0.0.0.3 area 0
network 200.0.3.0 0.0.0.3 area 0
exit

exit
copy running-config startup-config
y

