Router config txt file
Sergio Ehlen Montero * 202130016-3 * 30/03/2023

******
HUWAEI
******



/-RIPV2-/------------------------------------------------
_______________________________________
Router Ra

system

interface Ethernet 0/0/0
ip address 10.0.0.1 255.0.0.0
quit

interface Serial 0/0/0
ip address 200.0.1.1 255.255.255.252
quit

interface Serial 0/0/1
ip address 200.0.2.1 255.255.255.252
quit

Rip
version 2
silent-interface Ethernet 0/0/0
network 10.0.0.0
network 200.0.1.0
network 200.0.2.0
quit

quit
save
y

_______________________________________
Router Rb

system

interface Ethernet 0/0/0
ip address 172.16.1.1 255.255.255.0
quit

interface Ethernet 0/0/1
ip address 172.16.2.1 255.255.255.0
quit

interface Serial 0/0/0
ip address 200.0.2.2 255.255.255.252
quit

interface Serial 0/0/1
ip address 200.0.3.1 255.255.255.252
quit

Rip
version 2
silent-interface Ethernet 0/0/0
silent-interface Ethernet 0/0/1
network 172.16.1.0
network 172.16.2.0
network 200.0.2.0
network 200.0.3.0
quit

quit
save
y

_______________________________________
Router Rc

system

interface Ethernet 0/0/0
ip address 192.168.1.1 255.255.255.0
quit

interface Serial 0/0/0
ip address 200.0.1.2 255.255.255.252
quit

interface Serial 0/0/1
ip address 200.0.3.2 255.255.255.252
quit

Rip
version 2
silent-interface FastEthernet 0/0
network 192.168.1.0
network 200.0.1.0
network 200.0.3.0
quit

quit
save
y



/-OSPF-/------------------------------------------------
_______________________________________
Router Ra

system

interface Ethernet 0/0/0
ip address 10.0.0.1 255.0.0.0
quit

interface Serial 0/0/0
ip address 200.0.1.1 255.255.255.252
quit

interface Serial 0/0/1
ip address 200.0.2.1 255.255.255.252
quit

ospf 1 router-id 1.1.1.1
area 0
network 10.0.0.0 0.255.255.255
network 200.0.1.1 0.0.0.3
network 200.0.2.1 0.0.0.3
quit
quit

quit
save
y


_______________________________________
Router Rb

system

interface Ethernet 0/0/0
ip address 172.16.1.1 255.255.255.0
quit

interface Ethernet 0/0/1
ip address 172.16.2.1 255.255.255.0
quit

interface Serial 0/0/0
ip address 200.0.2.2 255.255.255.252
quit

interface Serial 0/0/1
ip address 200.0.3.1 255.255.255.252
quit

ospf 1 router-id 2.2.2.2
area 0
network 172.16.1.0 0.0.0.255
network 172.16.2.0 0.0.0.255
network 200.0.2.2 0.0.0.3
network 200.0.3.1 0.0.0.3
quit
quit

quit
save
y


_______________________________________
Router Rc

system

interface Ethernet 0/0/0
ip address 192.168.1.1 255.255.255.0
quit

interface Serial 0/0/0
ip address 200.0.1.2 255.255.255.252
quit

interface Serial 0/0/1
ip address 200.0.3.2 255.255.255.252
quit

Rip
version 2
silent-interface FastEthernet 0/0
network 192.168.1.0
network 200.0.1.0
network 200.0.3.0
quit

quit
save
y
