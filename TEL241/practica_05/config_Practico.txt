Switch Central
--------------

enable
vlan database
vtp domain colocolo
vtp server
vlan 210
vlan 210 name Servers
vlan 220
vlan 220 name Users
vlan 225
vlan 225 name Voz
vlan 258
vlan 258 name Gestion

apply
exit

configure terminal
banner motd “ Este es el switch de la central. YIPEE”

line vty 0 15
password cisco
login
exit

spanning-tree mode rapid-pvst

spanning-tree vlan 210
spanning-tree vlan 220
spanning-tree vlan 225
spanning-tree vlan 258

interface FastEthernet 0/1
switchport mode trunk
no shutdown
exit

interface FastEthernet 0/2
switchport mode trunk
no shutdown
exit

interface FastEthernet 0/3
switchport mode trunk
no shutdown
exit


interface FastEthernet 0/4
switchport mode trunk
no shutdown
exit


interface FastEthernet 0/5
spanning-tree port-priority 0
switchport mode trunk
no shutdown
exit

interface FastEthernet 0/15
switchport mode access
switchport acces vlan 225
no shutdown
exit


interface vlan 258
ip address 192.168.222.12 255.255.255.0
no shutdown
ip default-gateway 192.168.222.254
exit