#ifndef ARTIST_H
#define ARTIST_H

class Artist{
    public:
        Artist(int, int);
        int getBirthday();
        int getAlbums();

        void setBirth(int);
    private:
        int birthday;
        int album;
};

#endif