#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(&scene);
    ui->graphicsView->setBackgroundBrush(Qt::white);

    this->rectangle = new QGraphicsRectItem(0,0,40,20);
    turnRectangleOff();
    scene.addItem(rectangle);
    connect(ui->pushButton,SIGNAL(pressed()),
            this, SLOT(turnRectangleOn()));

    this->timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),
            this ,SLOT(turnRectangleOff()));
}

MainWindow::~MainWindow()
{
    delete rectangle;
    delete ui;
}


void MainWindow::turnRectangleOn(){
    rectangle->setBrush(Qt::yellow);
    timer->start(2000);
}

void MainWindow::turnRectangleOff(){
    rectangle->setBrush(Qt::black);
}


