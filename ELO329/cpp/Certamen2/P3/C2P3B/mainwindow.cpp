#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(&scene);
    ui->graphicsView->setBackgroundBrush(Qt::white);

    this->rectangle = new QGraphicsRectItem(0,0,40,20);
    turnRectangleOff();
    scene.addItem(rectangle);
    connect(ui->pushButton,SIGNAL(pressed()),
            this, SLOT(turnRectangleOn()));

    //Tras revisar los ejemplos pedidos, esto no era necesario :P
    //connect(ui->pushButton,SIGNAL(released()),
    //        this, SLOT(turnRectangleOff()));

}

MainWindow::~MainWindow()
{
    delete rectangle;
    delete ui;
}


void MainWindow::turnRectangleOn(){
    rectangle->setBrush(Qt::yellow);
}

void MainWindow::turnRectangleOff(){
    rectangle->setBrush(Qt::black);
}
