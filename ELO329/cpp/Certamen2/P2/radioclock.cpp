#include "radioclock.h"
RadioClock::RadioClock(int tHour, int tMinute, int tSecond, bool isOn, bool isArmed)
{
    this->hour=tHour;
    this->minute=tMinute;
    this->second=tSecond;
    this->isOn = isOn;
    this->status = isArmed;
}
void RadioClock::setAlarm(int pHour, int pMinute, int pSecond){
    this->a_hour = pHour;
    this->a_minute = pMinute;
    this->a_second = pSecond;
}
void RadioClock::switch_Alarm(){
    if (status){
        status=false;
    } else {
        status=true;
    }
}
bool RadioClock::verify_Alarm(){
    return(a_hour == hour
            && a_minute == minute
            && a_second == second);
}
void RadioClock::activate_Alarm(){
    if (status&&verify_Alarm()){
        std::cout<<"Beep"<<std::endl;
    }
}
void RadioClock::showTime(){
	activate_Alarm();
    std::cout<<hour<<"/"<<minute<<"/"<<second<<std::endl;
}
