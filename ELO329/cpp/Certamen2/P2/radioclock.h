#ifndef RADIOCLOCK_H
#define RADIOCLOCK_H

#include "radio.h"
#include "clock.h"
#include <iostream>

class RadioClock : public Radio , public Clock
{
public:
    RadioClock(int tHour, int tMinute, int tSecond, bool isOn, bool isArmed);
    void setAlarm(int pHour, int pMinute, int pSecond);
    void switch_Alarm();
    bool verify_Alarm();
    void activate_Alarm();
    void showTime();
private:
    int a_hour;
    int a_minute;
    int a_second;
    bool status;
};

#endif // RADIOCLOCK_H
