TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        clock.cpp \
        main.cpp \
        radio.cpp \
        radioclock.cpp

HEADERS += \
    clock.h \
    radio.h \
    radioclock.h
