#include "clock.h"

Clock::Clock()
{
    this->hour=0;
    this->minute=0;
    this->second=0;
}
void Clock::tick(){
    second += 1;
    if (second>=60){ // En estricto rigor debería ser == pero uso >= por si acaso)
        second = 0;
        minute+= 1;
        if (minute>=60){
            minute = 0;
            hour +=1;
            if (hour>=24){
                hour = 0;
            }
        }
    }
}
