#ifndef CLOCK_H
#define CLOCK_H


class Clock
{
public:
    Clock();
    void tick();
protected:
    int hour;
    int minute;
    int second;
};

#endif // CLOCK_H
