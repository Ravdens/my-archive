#include "magnetic_sensor.h"

MagneticSensor::MagneticSensor(int z)
	:Sensor(z,true){
}
void MagneticSensor::setSensorOpen() {
    if(this->isClose()==true){
        this->setClose(false);
    }
}
void MagneticSensor::setSensorClose() {
    if(this->isClose()==false){
        this->setClose(true);
    }
}
