#include "sensor.h"
#include <iostream>
using namespace std;
Sensor::Sensor(int z, bool close){
    zone = z;
    this->close=close;
}
bool Sensor::isClose() const {
    if(close==true){
        return 1;
    }
    else{
        return 0;
    }
}
int Sensor::getZone() const {
    return this->zone;
}
void Sensor::setClose(bool isClose) {
    this->close = isClose;
    cout << "Sensor is "<< (close?"close.":"open.") << endl;
}
void Sensor::setZone(int _zone) {
    this->zone = _zone;
}
