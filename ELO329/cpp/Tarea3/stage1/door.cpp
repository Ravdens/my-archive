#include "door.h"

Door::Door(MagneticSensor sensor): magneticSensor(sensor){
}
void Door::changeState() {
    if (magneticSensor.isClose()==true){
        magneticSensor.setSensorOpen();
    }
    else{
        magneticSensor.setSensorClose();
    }
}

