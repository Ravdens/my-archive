#include "central.h"
#include <iostream>

Central::Central(QObject *parent)
    : QObject(parent), timer(new QTimer(this)) {
    connect(timer, SIGNAL(timeout()), this, SLOT(checkZones()));
    timer ->start(200);
    armed = false;
}
void Central::addNewSensor(Sensor * ps){
    zones.push_back(ps);
}
void Central::addSiren(Siren *siren){
    this->siren = siren;
}
Siren * Central::getSiren(){
    return siren;
}
void Central::arm(){
    bool closeZones[2];
    checkCloseZones(closeZones);
    if (closeZones[0]&&closeZones[1]){
        emit changeLabel(QString("Armed"));
        armed=true;
    }
    if (!closeZones[0]&&!closeZones[1])
        emit changeLabel(QString("ERROR: Zona 0 y 1 están abiertas!"));
    if (!closeZones[0]&&closeZones[1])
        emit changeLabel(QString("ERROR: Zona 0 abierta!"));
    if (closeZones[0]&&!closeZones[1])
        emit changeLabel(QString("ERROR: Zona 1 abierta!"));
}
void Central::dissarm(){
    armed = false;
    emit changeLabel(QString("Disarmed"));
}
void Central::checkZones() {
    bool closeZones[2];
    checkCloseZones(closeZones);
    if (!closeZones[0]||!closeZones[1]){
        cout << "Alguna zona está abierta." << endl;
        if(siren!=NULL && armed){
            siren->turnOn();
        }
    }
    if(!armed){
        siren->turnOff();
    }
}
void Central::checkCloseZones(bool closeZones[]) {
    closeZones[0]= closeZones[1] = true;
    for (uint i=0; i< zones.size(); i++)
        closeZones[zones[i]->getZone()]&=zones[i]->isClose();
}
Central::~Central(){
    delete timer;
    //delete siren;
    for (uint i = 0; i < zones.size(); ++i)
        delete zones[i];
}
