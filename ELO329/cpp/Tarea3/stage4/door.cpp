#include "door.h"
#include "doorview.h"

Door::Door()
{}
//Door::Door(MagneticSensor * sensor, DoorView * v): ?? {
Door::Door(MagneticSensor * sensor, DoorView * v){
    this->magneticSensor = sensor;
    this->view = v;
    isClose=true;
    view->setDoorModel(this);
}
void Door::changeState() {
    if (isClose) {
        isClose = false;
        view->setOpen();
        magneticSensor->setSensorOpen();
    } else {
        isClose = true;
        view->setClose();
        magneticSensor->setSensorClose();
    }
}
Door::~Door(){
    delete view;
}

