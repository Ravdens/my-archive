#ifndef CENTRAL_H
#define CENTRAL_H
#include <QObject>
#include <QVBoxLayout>
#include <QLabel>
#include <vector>
#include <QTimer>
#include "sensor.h"
#include "siren.h"

using namespace std; //needed by vector<>

class Central : public QObject
{
    Q_OBJECT
public:
    explicit Central(QObject *parent = nullptr);
    void addNewSensor(Sensor * ps);
    void addSiren(Siren * siren);
    Siren* getSiren();
    void arm();
    void dissarm();
    ~Central();
signals:
    void changeLabel(QString s);
private slots:
    void checkZones();
private:
    void checkCloseZones(bool closeZones[]);
    vector<Sensor *> zones;  // keep references to all sensors already installed in doors and windows.
    QTimer * timer;
    Siren * siren;
    bool armed;
};

#endif // CENTRAL_H
