#ifndef HOUSEWINDOW_H
#define HOUSEWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QGraphicsScene>
#include <QGraphicsItemGroup>
#include "QVBoxLayout"
#include "sirenview.h"
#include "central.h"

QT_BEGIN_NAMESPACE
namespace Ui { class HouseWindow; }
QT_END_NAMESPACE

class HouseWindow : public QMainWindow
{
    Q_OBJECT

public:
    HouseWindow(QWidget *parent = nullptr);
    void addHouseHollow(QGraphicsItemGroup * ); // doors and windows
    void setCentral(Central * c);
    ~HouseWindow();

private slots:
    void on_ArmButton_clicked();
    void on_DissarmButton_clicked();
    void labelChange(QString s);

private:
    Ui::HouseWindow *ui;
    QGraphicsScene interiorScene;
    QGraphicsScene sirenScene;
    Central * central;
    QLabel * display;
};
#endif // HOUSEWINDOW_H
