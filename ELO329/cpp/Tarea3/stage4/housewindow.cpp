#include "housewindow.h"
#include "ui_housewindow.h"

HouseWindow::HouseWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::HouseWindow)
{
    ui->setupUi(this);
    ui->houseRegion->setScene(&interiorScene);
    ui->houseRegion->setBackgroundBrush(Qt::white);
    ui->houseRegion->setScene(&interiorScene);
    ui->houseRegion->setBackgroundBrush(Qt::white);
    ui->alarmRegion->setScene(&sirenScene);
    ui->alarmRegion->setBackgroundBrush(Qt::white);
    display = ui->Display;
}
void HouseWindow::addHouseHollow(QGraphicsItemGroup * compoundItem){
    interiorScene.addItem(compoundItem);
}
void HouseWindow::setCentral(Central * c){
    central = c;
    sirenScene.addItem(c->getSiren()->getSirenView());
    connect(c,SIGNAL(changeLabel(QString)),this,SLOT(labelChange(QString)));
}
void HouseWindow::on_ArmButton_clicked()
{
    central->arm();
}
void HouseWindow::on_DissarmButton_clicked()
{
    central->dissarm();
}
void HouseWindow::labelChange(QString s){
    display->setText(s);
}
HouseWindow::~HouseWindow()
{
    delete ui;
}
