#ifndef SIRENVIEW_H
#define SIRENVIEW_H
#include <QObject>
#include <QTimer>
#include <QBrush>
#include <QGraphicsItemGroup>
#include "siren.h"

class SirenView:public QObject, public QGraphicsItemGroup
{
    Q_OBJECT
public:
    explicit SirenView(QObject *parent = nullptr);
    void setSirenModel(Siren * model);
    void startPlaying();
    void stopPlaying();
    ~SirenView();
private slots:
    void changeCycleStep();
private:
    void makeSirenDrawing();
    Siren* model;
    QGraphicsPolygonItem* mainbody;
    QGraphicsPolygonItem* subbody;
    QTimer * timer;
    bool playingCycleStep;
    bool isplaying;
};

#endif // SIRENVIEW_H
