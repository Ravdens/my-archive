#include "sirenview.h"

SirenView::SirenView(QObject *parent)
    : QObject(parent), timer(new QTimer(this))
{
    connect(timer, SIGNAL(timeout()),this,SLOT(changeCycleStep()));
    timer->start(100);
    makeSirenDrawing();
    isplaying = false;
    playingCycleStep = false;
}
void SirenView::startPlaying(){
    isplaying = true;
}
void SirenView::stopPlaying(){
    isplaying = false;
    subbody->setBrush(QColorConstants::Svg::lightblue);
}
void SirenView::changeCycleStep(){
    if (!isplaying) return;
    if (playingCycleStep){
        subbody->setBrush(Qt::red);
    }
    else{
        subbody->setBrush(Qt::green);
    }
    playingCycleStep = !playingCycleStep;
}
void SirenView::makeSirenDrawing(){
    mainbody = new QGraphicsPolygonItem(this);
    QPolygonF m;
    m.append(QPointF(0,0));
    m.append(QPointF(30,0));
    m.append(QPointF(20,-30));
    m.append(QPointF(10,-30));
    mainbody->setPolygon(m);
    mainbody->setBrush(QColorConstants::Svg::lightblue);
    subbody = new QGraphicsPolygonItem(this);
    subbody->setPolygon(m);
    subbody->setScale(0.75);
    subbody->setX(mainbody->x()+4);
    addToGroup(mainbody);
    addToGroup(subbody);
    this->setScale(3);
}
SirenView::~SirenView(){
    delete timer;
}
