#ifndef WINDOW_H
#define WINDOW_H
#include"magneticsensor.h"

class WindowView;

class Window
{
private:
    Window();
public:
    Window(MagneticSensor * sensor, WindowView * v);
    void changeState();
    ~Window();
private:
    MagneticSensor * magneticSensor;
    WindowView * view;
    bool isClose;
};

#include "windowview.h"
#endif // WINDOW_H
