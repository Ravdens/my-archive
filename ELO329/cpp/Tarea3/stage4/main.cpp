#include "housewindow.h"
#include <QApplication>
#include <iostream>
#include <fstream>
#include <vector>
#include "door.h"
#include "doorview.h"
#include "window.h"
#include "windowview.h"
#include "central.h"
#include "siren.h"
#include "sirenview.h"
using namespace std;
#include <QtWidgets>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    HouseWindow gui;  // gui: Graphical User Interface
    Central central;
    ifstream fin;
    vector<Window*> windows;
    vector<Door*> doors;
    int nDoors, nWindows;
    if (argc != 2) {
        cout << "Usage: "<<argv[0]<<" <configuration_file>" << endl;
        return -1;
    }
    fin.open(argv[1]);
    if (fin.fail()){
        cout << "Could not read file" << endl;
        return -2;
    }
    cout << "Argument:" << argv[1] << endl;
    fin >> nDoors;
    fin >> nWindows;
    cout << "nDoors:" << nDoors << " nWindows: " << nWindows << endl;
    for( int i=0; i<nDoors; i++) {
        int x, y, angle, zone;
        fin >> x >> y >> angle >> zone;
        cout <<"("<<x<<","<<y<<") "<<angle<<endl;
        MagneticSensor * sensor = new MagneticSensor(zone);
        DoorView * doorView = new DoorView(x,y,angle, sensor->getView());
        doors.push_back(new Door(sensor, doorView));
        central.addNewSensor(sensor);
        gui.addHouseHollow(doorView);
    }
    for( int i=0; i<nWindows; i++) {
        int x, y, angle, zone;
        fin >> x >> y >> angle >> zone;
        cout <<"("<<x<<","<<y<<") "<<angle<<endl;
        MagneticSensor * sensor = new MagneticSensor(zone);
        WindowView * windowView = new WindowView(x,y,angle, sensor->getView());
        windows.push_back(new Window(sensor, windowView));
        central.addNewSensor(sensor);
        gui.addHouseHollow(windowView);
    }

    SirenView sirenV(&central);
    Siren siren(&central,&sirenV);
    central.addSiren(&siren);
    gui.setCentral(&central);
    gui.show();
    a.exec();

    for (uint i = 0; i < doors.size(); ++i) delete doors[i];
    for (uint i = 0; i < windows.size(); ++i) delete windows[i];

    return 0;
}
