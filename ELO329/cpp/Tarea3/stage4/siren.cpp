#include "siren.h"
#include "sirenview.h"
#include "central.h"

Siren::Siren(Central* central, SirenView * v){
    this->central = central;
    this->sirenview = v;
    isPlaying = false;
    sirenview->stopPlaying();
}
void Siren::turnOn(){
    if(!isPlaying){
        this->isPlaying = true;
        sirenview->startPlaying();
    }
}
void Siren::turnOff(){
    if(isPlaying){
        this->isPlaying = false;
        sirenview->stopPlaying();
    }
}
SirenView * Siren::getSirenView(){
    return sirenview;
}
