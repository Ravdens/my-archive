#include "windowview.h"
WindowView::WindowView(int x, int y, int angle, MagneticSensorView * mv)
{
    makeWindowView();
    mv->setParentItem(this);
    installMagneticSensor(*mv);
    magnet = &(mv->getMagnetView());
    QTransform transform;
    transform.translate(x,y);
    transform.rotate(angle);
    setTransform(transform);
}
void WindowView::makeWindowView(){
    // x,y,width,height
    QGraphicsRectItem * origenPillar= new QGraphicsRectItem(0,0,20,20);
    origenPillar->setBrush(QColorConstants::Svg::darkred);
    switchPillar= new QGraphicsRectItem(200,0,20,20);
    switchPillar->setBrush(QColorConstants::Svg::darkred);
    QGraphicsRectItem * fixedPane= new QGraphicsRectItem(20,0,90,10);
    fixedPane->setBrush(QColorConstants::Svg::lightblue);
    slidingPane= new QGraphicsRectItem(110,10,90,10);
    slidingPane->setBrush(QColorConstants::Svg::lightblue);
    addToGroup(origenPillar);
    addToGroup(switchPillar);
    addToGroup(fixedPane);
    addToGroup(slidingPane);
}
void WindowView::setWindowModel(Window *m){
    model=m;
}
void WindowView::installMagneticSensor(MagneticSensorView & mv){
    mv.getMagnetView().setRect(slidingPane->rect().right()-mv.getMagnetView().rect().width(),
                               slidingPane->rect().bottom(),
                               mv.getMagnetView().rect().width(),
                               mv.getMagnetView().rect().height());
    mv.getSwitchView().setRect(switchPillar->boundingRect().x(),
                               switchPillar->boundingRect().height(),
                               mv.getSwitchView().rect().width(),
                               mv.getSwitchView().rect().height());
    mv.getMagnetView().setTransformOriginPoint(15,15);
    addToGroup(&mv.getMagnetView());
    addToGroup(&mv.getSwitchView());
}
void WindowView::setOpen(){
    slidingPane->setX(slidingPane->x()-90);
    magnet->setX(slidingPane->x());
}
void WindowView::setClose(){
    slidingPane->setX(slidingPane->x()+90);
    magnet->setX(slidingPane->x());
}
void WindowView::mousePressEvent(QGraphicsSceneMouseEvent * event){
    if (model!= NULL && event->button()==Qt::LeftButton)
        model->changeState();
}
WindowView::~WindowView(){
    delete slidingPane;
    delete switchPillar;
}
