#ifndef SIREN_H
#define SIREN_H

class SirenView;
class Central;

class Siren
{
private:
    Siren();
public:
    Siren(Central* central, SirenView * v);
    void turnOn();
    void turnOff();
    SirenView * getSirenView();
private:
    Central* central;
    SirenView* sirenview;
    bool isPlaying;
};

#endif // SIREN_H
