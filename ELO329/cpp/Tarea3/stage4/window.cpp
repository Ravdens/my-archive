#include "window.h"

Window::Window()
{

}
Window::Window(MagneticSensor * sensor, WindowView * v){
    this->magneticSensor = sensor;
    this->view = v;
    isClose=true;
    view->setWindowModel(this);
}
void Window::changeState(){
    if(isClose){
        isClose = false;
        view->setOpen();
        magneticSensor->setSensorOpen();
    }
    else{
        isClose = true;
        view->setClose();
        magneticSensor->setSensorClose();
    }
}
Window::~Window(){
    delete view;
}
