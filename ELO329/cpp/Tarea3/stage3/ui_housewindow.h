/********************************************************************************
** Form generated from reading UI file 'housewindow.ui'
**
** Created by: Qt User Interface Compiler version 6.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HOUSEWINDOW_H
#define UI_HOUSEWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_HouseWindow
{
public:
    QWidget *centralwidget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QGraphicsView *houseRegion;
    QGraphicsView *alarmRegion;
    QFrame *line;
    QMenuBar *menubar;
    QMenu *menuAntenaSimulator;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *HouseWindow)
    {
        if (HouseWindow->objectName().isEmpty())
            HouseWindow->setObjectName("HouseWindow");
        HouseWindow->resize(823, 499);
        centralwidget = new QWidget(HouseWindow);
        centralwidget->setObjectName("centralwidget");
        horizontalLayoutWidget = new QWidget(centralwidget);
        horizontalLayoutWidget->setObjectName("horizontalLayoutWidget");
        horizontalLayoutWidget->setGeometry(QRect(9, 10, 801, 431));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName("horizontalLayout");
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        houseRegion = new QGraphicsView(horizontalLayoutWidget);
        houseRegion->setObjectName("houseRegion");

        horizontalLayout->addWidget(houseRegion);

        alarmRegion = new QGraphicsView(horizontalLayoutWidget);
        alarmRegion->setObjectName("alarmRegion");
        alarmRegion->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);

        horizontalLayout->addWidget(alarmRegion);

        line = new QFrame(horizontalLayoutWidget);
        line->setObjectName("line");
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        horizontalLayout->addWidget(line);

        HouseWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(HouseWindow);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 823, 22));
        menuAntenaSimulator = new QMenu(menubar);
        menuAntenaSimulator->setObjectName("menuAntenaSimulator");
        HouseWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(HouseWindow);
        statusbar->setObjectName("statusbar");
        HouseWindow->setStatusBar(statusbar);

        menubar->addAction(menuAntenaSimulator->menuAction());

        retranslateUi(HouseWindow);

        QMetaObject::connectSlotsByName(HouseWindow);
    } // setupUi

    void retranslateUi(QMainWindow *HouseWindow)
    {
        HouseWindow->setWindowTitle(QCoreApplication::translate("HouseWindow", "MainWindow", nullptr));
        menuAntenaSimulator->setTitle(QCoreApplication::translate("HouseWindow", "AntenaSimulator", nullptr));
    } // retranslateUi

};

namespace Ui {
    class HouseWindow: public Ui_HouseWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HOUSEWINDOW_H
