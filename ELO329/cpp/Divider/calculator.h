#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <exception>

class calculator : public Exception
{
public:
    calculator(char *);
    what();
private:
    char * message;
};

#endif // CALCULATOR_H
