/* TrafficLight.java  */
import java.awt.Graphics2D;
import javax.swing.JPanel;
public class TrafficLight {
    public TrafficLight (int ft, int tt){
        followTime = ft;
        transitionTime = tt;
        state = TrafficLightState.STOP;
    }
    public void turnStop() {
        state = TrafficLightState.STOP;
        panel.repaint();
    }
    public void turnTransition() {
        state = TrafficLightState.TRANSITION;
        panel.repaint();
    }
    public void turnFollow() {
        state = TrafficLightState.FOLLOW;
        panel.repaint();
    }
    public TrafficLightState getState() {
        return state;
    }
    public void setPanel(JPanel p) {
        panel = p;
    }
    protected JPanel panel;
    protected int followTime;
    protected int transitionTime;
    private TrafficLightState state;
}