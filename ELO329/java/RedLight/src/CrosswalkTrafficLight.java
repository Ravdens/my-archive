/* CrosswalkTrafficLight.java   */
import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
public class CrosswalkTrafficLight extends TrafficLight implements ActionListener{
    public CrosswalkTrafficLight (int ft, int tt) {
        super(ft, tt);
        red_view = new Ellipse2D.Double(origen_x, origen_y, DIAMETER, DIAMETER);
        green_view = new Ellipse2D.Double(origen_x, origen_y+DIAMETER, DIAMETER, DIAMETER);
        timer = new Timer(ON_OFF_TIME, this);
    }
    public void paint_view (Graphics2D g2d) {
        switch (getState()) {
            case STOP: if (timer.isRunning()) timer.stop();
                g2d.setColor(Color.RED);
                g2d.fill(red_view);
                g2d.setColor(Color.GRAY);
                g2d.fill(green_view);
                break;
            case TRANSITION: g2d.setColor(Color.GRAY);
                g2d.fill(red_view);
                if(toggle)
                    g2d.setColor(Color.GREEN);
                g2d.fill(green_view);
                break;
            case FOLLOW: if (!timer.isRunning()) {
                timer.start();
                time=followTime+transitionTime;
            }
                g2d.setColor(Color.GRAY);
                g2d.fill(red_view);
                g2d.setColor(Color.GREEN);
                g2d.fill(green_view);
        }
    }
    public void actionPerformed (ActionEvent event) {
        toggle = !toggle;
        if (toggle) {
            time--;
            if (time==transitionTime) turnTransition();
            if (time == 0) turnStop();
        }
        panel.repaint();
    }
    private Ellipse2D red_view, green_view;
    private Timer timer;
    private int time;
    private boolean toggle=false;
    private int origen_x=50;
    private int origen_y=10;
    private static int DIAMETER=50;
    private static int ON_OFF_TIME = 500; //[ms]
}