/* MyButton.java  */
import javax.swing.JButton;
import java.awt.event.*;
public class MyButton extends JButton implements ActionListener {
    public MyButton (CrosswalkTrafficLight light) {
        super("Request Green light");
        tl = light;
        addActionListener(this);
    }
    public void actionPerformed (ActionEvent event) {
        if (tl.getState() == TrafficLightState.STOP)
            tl.turnFollow();
    }
    private CrosswalkTrafficLight tl;
}

