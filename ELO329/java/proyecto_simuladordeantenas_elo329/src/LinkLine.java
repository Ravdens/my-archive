import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.Random;

///////   LinkLine.java    ///////

/**
 * Elemento visual que conecta dos antenas para mostrar transferencia. Conecta dos antenas
 * y utiliza un timers con valores random para generar un patrón de parpadeo.
 */

public class LinkLine extends Line
{
    /**
     * Constructor de LinkLine. Se piden dos AntenaView como parametro, independiente de cual es
     * transmisor y cual es receptor.
     * @param A     Primera antena a conectar
     * @param B     Segunda antena a conectar
     */
    LinkLine(AntenaView A, AntenaView B){
        home = (Sim) A.getParent();
        setStartX(A.getCenterX());
        setStartY(A.getCenterY());
        setEndX(B.getCenterX());
        setEndY(B.getCenterY());
        setStroke(Color.GREY);
        getStrokeDashArray().addAll(50d, 10d);
        setStrokeWidth(3);

        ActionListener updater = actionEvent -> {
            phase = new Random().nextInt(100);
            //setStrokeDashOffset(phase*10);
            if (phase%6==0)setStroke(Color.RED);
            else setStroke(Color.GREEN);
        };
        timer = new Timer(300,updater);
        timer.start();

        displaying = false;
        double dX = A.getCenterX()- B.getCenterX();
        double dY = A.getCenterY()- B.getCenterY();
        double length = Math.sqrt(Math.pow(dX,2)+ Math.pow(dY,2))*Sim.getScale().getValScaled();

        Label label = new Label("Length: "+String.format("%.0f",length));
        label.setFont(new Font(15));
        label.setMinWidth(Region.USE_PREF_SIZE);
        label.setMaxWidth(Region.USE_PREF_SIZE);
        labelbox = new HBox();
        labelbox.getChildren().add(label);

        setOnMouseClicked(e->{
            if (displaying){
                home.getChildren().remove(labelbox);
                displaying = false;
            }
            else {
                home.getChildren().add(labelbox);
                labelbox.relocate(B.getCenterX()+dX/2,B.getCenterY()+dY/2);
                displaying = true;
            }
        });
        label.setOnMouseClicked(e->{
            home.getChildren().remove(labelbox);
            displaying = false;
        });
    }

    /**
     * Detiene el timer que maneja el parpadeo de la LinkLine. ES OBLIGATORIO al remover la linkline
     * de los objetos mostrados en pantalla, o el thread seguirá corriendo.
     */
    public void stop(){
        home.getChildren().remove(labelbox);
        displaying = false; //por si acaso
        timer.stop();
    }

    private boolean displaying;
    private Sim home;
    private HBox labelbox;
    private Timer timer;
    private int phase;

}
