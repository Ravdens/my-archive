import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;

///////   ParabolicAntenaView.java    ///////

/**
 * Vista de una antena de tipo parabólica. En su implementación solo realiza el dibujo de la parabólica y
 * llama el constructor de AntenaView. Extiende la clase AntenaView y puede actuar como vista para
 * una ParabolicAntena.
 */

public class ParabolicAntenaView extends AntenaView
{
    /**
     * Constructor de la vista de una antena parabólica. Llama al constructor de AntenaView y luego
     * construye el dibujo de forma apropiada.
     * @param home          Instancia de Sim a la cual agregar la vista
     * @param movable       Booleano, true si es movible y funciona, false si es sólo un dibujo
     */
    public ParabolicAntenaView(Sim home, Boolean movable){
        super(home,movable);
        width = 90;
        height = width;

        Arc reflector = new Arc();
        reflector.setCenterX(0); reflector.setCenterY(0);
        reflector.setLength(180);
        reflector.setStartAngle(90);
        reflector.setRadiusX(width/2); reflector.setRadiusY(height/2);
        reflector.setFill(Color.GREY);
        Rectangle waveGuide = new Rectangle(width/3,height/20);
        waveGuide.relocate(0,-height/40);
        Polygon feed = new Polygon();
        feed.getPoints().addAll(0d,0d,
                20d, 5d,
                20d, 15d,
                0d, 20d
                );
        feed.setFill(Color.GREY);
        feed.relocate((width/2)-20,-10);

        getChildren().addAll(waveGuide,reflector,feed);
    }
}
