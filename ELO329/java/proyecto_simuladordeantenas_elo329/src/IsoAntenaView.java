import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

///////   IsoAntenaView.java    ///////

/**
 * Vista de una antena de tipo isotrópica. En su implementación solo realiza el dibujo de la isotrópica y
 * llama el constructor de AntenaView. Extiende la clase AntenaView y puede actuar como vista para
 * una IsoAntena.
 */

public class IsoAntenaView extends AntenaView
{
    /**
     * Constructor de la vista de una antena isotrópica. Llama al constructor de AntenaView y luego
     * construye el dibujo de forma apropiada.
     * @param home          Instancia de Sim a la cual agregar la vista
     * @param movable       Booleano, true si es movible y funciona, false si es sólo un dibujo
     */
    public IsoAntenaView(Sim home, Boolean movable){
        super(home,movable);
        width = 90;
        height = width;

        Circle antena = new Circle(0,0,width/6);
        antena.setFill(Color.BLACK);
        antena.setStroke(Color.GREY);
        Circle ring2 = new Circle(0,0,width/3);
        ring2.setFill(Color.TRANSPARENT);
        ring2.setStroke(Color.LIGHTBLUE);
        Circle ring1 = new Circle(0,0,width/2);
        ring1.setFill(Color.TRANSPARENT);
        ring1.setStroke(Color.LIGHTBLUE);

        getChildren().addAll(ring1,ring2,antena);
    }
}
