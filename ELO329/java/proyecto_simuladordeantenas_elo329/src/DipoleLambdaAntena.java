import java.util.Arrays;
import java.util.PriorityQueue;

///////   DipoleLambdaAntena.java    ///////

/**
 * Modelo de una antena de tipo lambda. Extiende la clase Antena, y posee metodos propios
 * para crear sus propios nombres. Puede ser creada como receptor o transmisor.
 */
public class DipoleLambdaAntena extends Antena
{
    /**
     * Constructor de una antena dipolo lambda transmisora.
     * @param efficiency        Valor de eficiencia de la antena en rango [1,0]
     * @param potencia          Potencia siendo transmitida en Watts
     * @param view              Vista de la antena
     */
    public DipoleLambdaAntena(double efficiency, double potencia,DipoleAntenaView view)
    {
        angle = new Angle(0,false);
        isTransmiting = true;
        this.potencia = potencia;
        this.efficiency = efficiency;
        this.view = view;
        drawName();
    }

    /**
     * Constructor de una antena dipolo lambda medio receptora
     * @param efficiency        Valor de la eficiencia de la antena en rango [0,1]
     * @param view              Vista de la antena
     */
    public DipoleLambdaAntena(double efficiency,DipoleAntenaView view)
    {
        angle = new Angle(0,false);
        isTransmiting = false;
        this.potencia = 0;
        this.efficiency = efficiency;
        this.view = view;
        drawName();
    }

    /**
     * Ganancia directiva de la antena con respecto a otra antena con una inclinación de alpha. Este
     * no es necesariamente el ángulo de radiación, pues internamente se hace la suma con la inclinación de
     * la antena misma.
     * @param alpha         Ángulo entre las posiciónes de las antenas
     * @return gain         Ganancia directiva
     */
    @Override
    public double directionGain(Angle alpha)
    {
        double theta = alpha.plus(angle).plus(Angle.PiHalf()).getRad();
        double g = Math.cos(Math.cos(theta)*Math.PI)+1;
        g /= Math.sin(theta);
        g  = Math.pow(g,2);
        g *= 2.41/4;

        return g*efficiency;
    }

    /**
     * La antena obtiene un id del stack y lo vuelve no-disponble a otras antenas de el mismo tipo.
     */
    public void drawName(){
        id = nameList.remove();
    }

    /**
     * La antena retorna su id al stack y lo vuelve disponible a otras antenas del mismo tipo.
     */
    public void returnName(){
        nameList.add(id);
    }

    /**
     * Retorna el nombre de la antena con el formato "Tipo de antena + id"
     * @return name     String con el nombre de la antena con el formato "Tipo de antena + id"
     */
    public String getName() {
        return "Dipolo L "+id;
    }
    private static PriorityQueue<Integer> nameList;
    static {
        nameList = new PriorityQueue<>(Arrays.asList(10,9,8,7,6,5,4,3,2,1));
    }

}

