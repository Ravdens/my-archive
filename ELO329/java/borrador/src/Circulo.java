import java.lang.Math.*;
public class Circulo {
    int radio;

    public Circulo(int radio){
        this.radio=radio;
    }
    public int getRadio() {
        return radio;
    }

    public void setRadio(int radio) {
        this.radio = radio;
    }

    public double Area(){
        return radio*Math.PI;
    }
}
