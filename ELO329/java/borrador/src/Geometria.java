public class Geometria {
    Circulo c;
    Rectangulo r;

    public Geometria(Circulo c, Rectangulo r){
        this.c = c;
        this.r = r;
    }

    public Circulo getC() {
        return c;
    }

    public void setC(Circulo c) {
        this.c = c;
    }

    public Rectangulo getR() {
        return r;
    }

    public void setR(Rectangulo r) {
        this.r = r;
    }
}
