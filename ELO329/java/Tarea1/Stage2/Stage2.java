import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Stage2 {
    public Stage2() {
        doors = new ArrayList<Door>();
        windows = new ArrayList<Window>();
    }
    public void readConfiguration(Scanner in) {
        // reading <#_doors> <#_windows> <#_PIRs>
        central = new Central();

        int numDoors = in.nextInt();
        for (int i = 0; i < numDoors; i++)
        {
            Door di = new Door();
            doors.add(di);
            central.addNewSensor(di.getSensor());
        }
        int numWindows = in.nextInt();
        for (int i = 0; i < numWindows; i++)
        {
            Window wi = new Window();
            windows.add(wi);
            central.addNewSensor(wi.getSensor());
        }

        // DETECCION DE PIRS AQUI!!! (para stage 3-4)

        in.nextLine();
        String soundFile = in.next();
        siren = new Siren(soundFile);
        central.setSiren(siren);
        in.close();
    }
    public void executeUserInteraction (Scanner in, PrintStream out){
        String command;
        char parameter;
        int i;
        int step=0;
        printHeader(out);
        while (true) {
            try {
                printState(step++, out);
                command = in.next();
                //System.out.println("comando: "+command);
                switch (command.charAt(0)) {
                    case 'x':
                        central.getSiren().stop();
                        return;
                    case 'd':
                        i = Integer.parseInt(command.substring(1));
                        parameter = in.next().charAt(0);
                        if (parameter == 'o')
                            doors.get(i).open();
                        else
                            doors.get(i).close();
                        break;
                    case 'w':
                        i = Integer.parseInt(command.substring(1));
                        parameter = in.next().charAt(0);
                        if (parameter == 'o')
                            windows.get(i).open();
                        else
                            windows.get(i).close();
                        break;
                    case 'k':
                        parameter = in.next().charAt(0);
                        //System.out.println(parameter);
                        switch (parameter) {
                            case 'a':
                                central.arm();
                                break;
                            case 'p':
                                //En el futuro, tiene que funcionar distinto de 'a'
                                central.arm();
                                break;
                            case 'd':
                                central.disarm();
                                break;
                        }
                } //end switch
                central.checkZone();
            } //end try
            catch (Exception A){System.out.println("Invalid entry, try again");}
        } // end while
    }

    //Escribe header del archivo csv
    public void printHeader(PrintStream out){
        out.print("Step");
        for (int i=0; i < doors.size(); i++)
            out.print("\t"+doors.get(i).getHeader());
        for (int i=0 ; i< windows.size() ; i++)
            out.print("\t"+windows.get(i).getHeader());
        out.print("\t"+central.getHeader());
        out.print("\t"+siren.getHeader());
        out.println();
    }

    //Imprime linea de estado al archivo csv
    public void printState(int step, PrintStream out){
        out.print(step);
        for (int i=0; i < doors.size(); i++)
            out.print("\t"+doors.get(i).getState());
        for (int i=0 ; i< windows.size() ; i++)
            out.print("\t"+windows.get(i).getState());
        out.print("\t"+central.getState());
        out.print("\t"+siren.getState());
        out.println();
    }

    public static void main(String [] args) throws IOException {
        if (args.length != 1) {
            System.out.println("Usage: java Stage1 <configurationFile.txt>");
            System.exit(-1);
        }
        Scanner in = new Scanner(new File(args[0]));
        //System.out.println("File: " + args[0]);
        Stage2 stage = new Stage2();
        stage.readConfiguration(in);
        System.out.println("------------");
        stage.executeUserInteraction(new Scanner(System.in), new PrintStream(new File("output.csv")));
    }

    private ArrayList<Door> doors;
    private ArrayList<Window> windows;
    private Central central;
    private Siren siren;
}
