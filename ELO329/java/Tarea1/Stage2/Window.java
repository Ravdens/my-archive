public class Window {
    public Window() {
        magneticSensor = new MagneticSensor();
        state = State.CLOSE;
        //Empieza como cerrado porque ESO DICE LA TAREA :P
    }
    {
        id = nextId++;
    }
    public void open() {
        state = State.OPEN;
        magneticSensor.moveMagnetAwayFromSwitch();
    }
    public void close() {
        state = State.CLOSE;
        magneticSensor.putMagnetNearSwitch();
    }
    public String getHeader(){
        return "w"+id;
    }
    public int getState(){ //Retorna 1 como cerrado y 0 como abierto
        if (state == State.CLOSE) return 1;
        else return 0;
    }
    public MagneticSensor getSensor() { return magneticSensor; }
    private MagneticSensor magneticSensor;
    private State state;
    private final int id;
    private static int nextId=0;
}
