import java.io.File;
import java.net.URL;

public class Siren {
    public Siren (String soundFileName){
        try {
            dir = new File(soundFileName).toURI().toURL();
        }
        catch (Exception exc){
            exc.printStackTrace(System.out);
        }
        isSounding = false;
        aWave = null;
    }
    public void play()
    {
        if (isSounding) return;
        try
        {
            isSounding = true;
            aWave = new AePlayWave(dir);
            aWave.start();
        }
        catch(Exception A) {System.out.println("||ENCOUNTERED ERROR TRYING TO PLAY AUDIO FILE||");}
    }

    public void stop()
    {
        if(!isSounding) return;
        try
        {
            isSounding = false;
            aWave.stopSounding();
        }
        catch (Exception A) {System.out.println("||ENCOUNTERED ERROR TRYING TO STOP AUDIO FILE||");}
    }

    public String getHeader() {
        return "Siren";
    }
    public int getState() {
        if (isSounding){return 1;}
        else return 0;
    }
    private URL dir;
    private boolean isSounding;
    private AePlayWave aWave;
}
