import java.util.ArrayList;

public class Central {
    public Central(){
        zone0 = new ArrayList<Sensor>();
        isArmed = false;
        siren = null;
    }
    public void arm() {
        isArmed=true;
    }
    public void disarm(){
        isArmed=false;
        if (siren.getState()==1) siren.stop();
    }
    public void setSiren(Siren s) {
        siren =s;
    }
    public void addNewSensor(Sensor s){
        zone0.add(s);
    }
    public void checkZone(){
        if (!isArmed) return;
        //System.out.println("Checking");
        for (int i=0; i < zone0.size(); i++){
            if (zone0.get(i).getState() == SwitchState.OPEN)
            {
                siren.play();
                return;
            }
        }
        //System.out.println("ALL'S GOOD, NO ALARMS HERE!");
    }
    public String getHeader(){
        return "Central";
    }
    public int getState(){
        return isArmed?1:0;
    }
    public Siren getSiren() {
        return siren;
    }

    private ArrayList<Sensor> zone0;
    private boolean isArmed;
    private Siren siren;
}
