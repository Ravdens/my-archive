TAREA 1 - ELO329 - STAGE 3
Grupo 6:
Maximo Flores Suarez
Camilo Troncoso Hormazabal
Sergio Ehlen Montero
Cristian Gonzales Bustos

Notas de uso:
- Para indicar los caracteres de flechas, usar:
	UpArrow = ^
	DownArrow = V
	LeftArrow = <
	RightArrow = >
Ej: p0 > 
- El resto del funcionamiento es igual al indicado en el pdf de la tarea

