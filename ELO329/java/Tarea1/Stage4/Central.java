import java.util.ArrayList;

public class Central {
    public Central(){
        zones = new ArrayList<Zone>();
        //Zona 0 = puerta principal
        zones.add(new Zone());
        //Zona 1 = otras puertas y ventanas
        zones.add(new Zone());
        //Zona 2 = sensores PIR
        zones.add(new Zone());
        siren = null;
    }
    public void arm(int zoneID) {
        zones.get(zoneID).isArmed = Boolean.TRUE;
        isArmed = true;
    }
    public void disarm(){
        for (int i = 0; i < zones.size(); i++) {zones.get(i).isArmed=Boolean.FALSE;}
        isArmed=false;
        if (siren.getState()==1) siren.stop();
    }
    public void setSiren(Siren s) {
        siren =s;
    }
    public void addNewSensor(Sensor s, int zoneID){
        //Nos aseguramos que la primera puerta vaya en la zona 0
        if (zoneID == 1 && zones.get(0).sensors.size()==0) zoneID = 0;
        zones.get(zoneID).sensors.add(s);
    }
    public void checkZones(){
        for (int i = 0; i <zones.size() ; i++)
        {
            if (zones.get(i).isArmed == Boolean.FALSE) continue;
            //System.out.println("Checking");
            for (int j=0; j < zones.get(i).sensors.size(); j++){
                if (zones.get(i).sensors.get(j).getState() == SwitchState.OPEN)
                {
                    siren.play();
                    return;
                }
            }
        }

        //System.out.println("ALL'S GOOD, NO ALARMS HERE!");
    }
    public String getHeader(){
        return "Central";
    }
    public int getState(){
        return isArmed?1:0;
    }
    public Siren getSiren() {
        return siren;
    }
    private ArrayList<Zone> zones;
    private boolean isArmed;
    private Siren siren;
}
