import java.lang.Math.*;
public class Vector
{
    public Vector() {x = 0 ; y = 0 ; r = 0 ; theta= 0;}
    public Vector(float x, float y)
    {
        this.x = x ;
        this.y = y;
        r = makeR();
        theta = makeTheta();
    }

    //Retorna el vector suma v+u entre v (este vector) y u (argumento)
    public Vector VSum(Vector u){
      return new Vector(x+u.getX(),y+u.getY());
    }

    //Retorna el vector resta v-u entre v (este vector) y u (argumento)
    public Vector VDif(Vector u){
        return new Vector(x-u.getX(),y-u.getY());
    }

    private float makeR(){return (float)Math.sqrt(x*x+y*y);}

    private float makeTheta(){return (int)(Math.atan(y/x)*360/Math.PI);}


    public float getX() {return x;}
    public void setX(float x)
    {
        this.x = x;
        r = makeR();
        theta = makeTheta();
    }
    public float getY() {return y;}
    public void setY(float y)
    {
        this.y = y;
        r = makeR();
        theta = makeTheta();
    }
    public float getR() {return r;}
    public float getTheta() {return theta;}

    protected float x , y , r , theta;
}
