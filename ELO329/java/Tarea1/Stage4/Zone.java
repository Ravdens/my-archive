import java.util.ArrayList;

public class Zone
{
    public Zone(ArrayList<Sensor> sensors, Boolean isArmed)
    {
        this.sensors = sensors;
        this.isArmed = isArmed;
    }
    public Zone()
    {
        this.sensors = new ArrayList<Sensor>();
        this.isArmed = Boolean.FALSE;
    }

    public ArrayList<Sensor> getSensors()
    {
        return sensors;
    }

    public void setSensors(ArrayList<Sensor> sensors)
    {
        this.sensors = sensors;
    }

    public Boolean getArmed()
    {
        return isArmed;
    }

    public void setArmed(Boolean armed)
    {
        isArmed = armed;
    }

    public ArrayList<Sensor> sensors;
    public Boolean isArmed;
}
