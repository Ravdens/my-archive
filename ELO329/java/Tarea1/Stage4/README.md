# TAREA 1 - ELO329 - STAGE 4
## Grupo 6
* Maximo Flores Suarez / 202130019-8
* Camilo Troncoso Hormazabal / 202130004-k
* Sergio Ehlen Montero / 202130016-3
* Cristian Gonzales Bustos / 201704003-3

## Uso del programa

#### config.txt
Debe contener la configuración del sistema de seguridad, siguiendo el siguiente formato rigido:

><#_doors> <#_windows> <#_PIRs>
>
>< x > < y > <direction_angle> <sensing_angle> <sensing_range>
>
>...
>
>< siren_sound_file >

#### Correr el programa
Primero el programa se debe compilar usando *$ make* y luego ejecutar con *$ make run*

Los comandos que recibe el programa son:
> k ( a | p | d )

Ordena a la central a: armar todo , p: armar solo perimetro , d: desarmar todo
>di ( o | c ) 

Cambia el estado de la puerta i a o: abierto , c: cerrado
>wi ( o | c )

Cambia el estado de la ventana i a o: abierto , c: cerrado
>c < x > < y >

Crear una persona en las coordenadas cartesianas (x,y)
>pi ( < | V | > | ^ )

Desplaza a la persona i 0.5[m] en la dirección indicada 

#### Salida del programa
Ademas de reproducir un sonido cuando se activa la sirena, el programa genera un archivo
en formato .csv con el estado de cada puerta, ventana, sensor pir, central, sirena, y posicion de personas,
tras cada acción del usuario, en el siguiente formato:

| Step | d0  | ..di | w0 | ..wi | pir0 | ..piri | Siren | Central |     |     |
|------|-----|------|----|------|------|--------|-------|---------|-----|-----|
| 0    | 1   | ..1  | 1  | ..1  | 1    | ..1    | 0     | 0       |     |     |
| 1    | 1   | ..1  | 1  | ..1  | 1    | ..1    | 0     | 1       |     |     |
| 2    | 1   | ..1  | 1  | ..1  | 1    | ..1    | 0     | 1       | 4.0 | 4.0 |
| 3    | ... |      |    |      |      |        |       |         |     |     |

## Archivos incluidos

#### Archivos de clase:
- ._AePlayWave.java
- AePLayWave.java
- Central.java
- Door.java
- MagneticSensor.java
- Persona.java
- PirSensor.java
- Sensor.java
- Siren.java
- Vector.java
- Window.java
- Zone.java

#### Archivos Enum:
- State.java
- SwitchState.java

#### Archivo Principal:

- Stage4.java

_Contiene el ciclo de funcionamiento principal del programa y llama a los otros 
archivos de clase. Se ejecuta al usar make run_ 

#### Archivos extra: 

- Carpeta out
- Carpeta .idea
- stage4.iml

_Contienen información del proyecto en intellij, no son necesarios para la ejecución del programa._

- config.txt
- output.csv

_Archivos de entrada y salida sobre la configuración del sistema a simular, y de los resultados 
de la simulación_

- siren.wave

_Archivo de sonido reproducido cuando se activa la sirena en la simulación_

- Stage4_structure.puml
- stage4_structure.png

_Contienen el diagrama UML del programa_

### Puntaje extra Stage 5
No se opta a puntaje extra por stage 5.

_Para más información, consultar el archivo pdf de documentación_

![Stage4_structure.png](Stage4_structure.png)

