import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Stage1 {
    public Stage1() {
        doors = new ArrayList<Door>();
        windows = new ArrayList<Window>();
    }
    public void readConfiguration(Scanner in){
        // reading <#_doors> <#_windows> <#_PIRs>

        int numDoors = in.nextInt();
        for (int i = 0; i < numDoors; i++)
            doors.add(new Door());

        int numWindows = in.nextInt();
        for (int i =0 ; i<numWindows ; i++)
            windows.add(new Window());

        //To be implemented
        //int numPirs = in.nextInt();
        //for (int i =0 ; i< numPirs ; i++)
            //
        in.close();
    }
    public void executeUserInteraction (Scanner in, PrintStream out){
        String command;
        char parameter;
        int i;
        int step=0;
        boolean done =false;

        printHeader(out);
        System.out.println("");
        while (!done) {
            try
            {
            printState(step++, out);
            command = in.next();
            switch (command.charAt(0)) {
                case 'd':
                    i = Integer.parseInt(command.substring(1));
                    parameter = in.next().charAt(0);
                    if (parameter == 'o')
                        //Para usar el char recibido como index lo pasamos a int
                        doors.get(i).open();
                    else
                        doors.get(i).close();

                    break;

                case 'w':
                    i = Integer.parseInt(command.substring(1));
                    parameter = in.next().charAt(0);
                    if (parameter == 'o')
                        windows.get(i).open();
                    else
                        windows.get(i).close();

                    break;

                case 'x':
                    done = true;   // Added to finish the program

            }
            }
            catch (Exception A){System.out.println("Invalid entry, try again");}
        }
    }
    //Escribe header del archivo csv
    public void printHeader(PrintStream out){
        out.print("Step");
        for (int i=0; i < doors.size(); i++)
            out.print("\t"+doors.get(i).getHeader());
        for (int i=0 ; i< windows.size() ; i++)
            out.print("\t"+windows.get(i).getHeader());
        out.println();
    }

    //Imprime linea de estado al archivo csv
    public void printState(int step, PrintStream out){
        out.print(step);
        for (int i=0; i < doors.size(); i++)
            out.print("\t"+doors.get(i).getState());
        for (int i=0 ; i< windows.size() ; i++)
            out.print("\t"+windows.get(i).getState());
        out.println();
    }
    public static void main(String [] args) throws IOException {
        if (args.length != 1) {
            System.out.println("Usage: java Stage1 <configurationFile.txt>");
            System.exit(-1);
        }
        Scanner in = new Scanner(new File(args[0]));
        //System.out.println("File: " + args[0]);
        Stage1 stage = new Stage1();
        stage.readConfiguration(in);
        stage.executeUserInteraction(new Scanner(System.in), new PrintStream(new File("output.csv")));
    }

    private ArrayList<Door> doors;
    private ArrayList<Window> windows;
}
