public class MagneticSensor extends Sensor {
    public MagneticSensor(SwitchState s){
        super(s);
    }
    public void moveMagnetAwayFromSwitch() {
        setState(SwitchState.OPEN);
    }
    public void putMagnetNearSwitch() {
        setState(SwitchState.CLOSE);
    }
}
