import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Stage3
{
    public Stage3() {
        doors = new ArrayList<Door>();
        windows = new ArrayList<Window>();
        personas = new ArrayList<Persona>();
        pirs = new ArrayList<PirSensor>();
    }

    public void readConfiguration(Scanner in) {
        // reading <#_doors> <#_windows> <#_PIRs>
        central = new Central();

        int numDoors = in.nextInt();
        for (int i = 0; i < numDoors; i++)
        {
            Door di = new Door();
            doors.add(di);
            central.addNewSensor(di.getSensor());
        }
        int numWindows = in.nextInt();
        for (int i = 0; i < numWindows; i++)
        {
            Window wi = new Window();
            windows.add(wi);
            central.addNewSensor(wi.getSensor());
        }

        int numPirs = in.nextInt();
        //Esto es muy ineficiente y debería ser mejorado, pero funciona
        float x=in.nextFloat();float y=in.nextFloat();float a1=in.nextInt();float a2=in.nextInt();float r=in.nextInt();
        PirSensor p0 = new PirSensor(x,y,a1,a2,r);
        pirs.add(p0);
        central.addNewSensor(p0);

        in.nextLine();
        String soundFile = in.next();
        siren = new Siren(soundFile);
        central.setSiren(siren);
        in.close();
    }

    public void executeUserInteraction (Scanner in, PrintStream out){
        String command;
        char parameter;
        int i;
        int step=0;
        printHeader(out);
        while (true) {
            try{
            printState(step++, out);
            command = in.next();
            switch (command.charAt(0)) {
                case 'x': central.getSiren().stop(); return;
                case 'd':
                    i = Integer.parseInt(command.substring(1));
                    parameter = in.next().charAt(0);
                    if (parameter == 'o') doors.get(i).open();
                    else doors.get(i).close();
                    break;
                case 'w':
                    i = Integer.parseInt(command.substring(1));
                    parameter = in.next().charAt(0);
                    if (parameter == 'o') windows.get(i).open();
                    else windows.get(i).close();
                    break;
                case 'k':
                    parameter = in.next().charAt(0);
                    switch (parameter) {
                        case 'a': central.arm(); break;
                        case 'p': central.arm(); break;
                        case 'd': central.disarm(); break;
                    }
                    break;
                case 'c':
                    float x = Character.getNumericValue(in.next().charAt(0));
                    float y = Character.getNumericValue(in.next().charAt(0));
                    personas.add(new Persona(x,y));
                    break;
                case 'p':
                    i = Integer.parseInt(command.substring(1));
                    parameter = in.next().charAt(0);
                    switch (parameter){
                        case '<': personas.get(i).moveWest();break;
                        case '>': personas.get(i).moveEast();break;
                        case 'V': personas.get(i).moveSouth();break;
                        case '^': personas.get(i).moveNorth();break;
                    }
                    break;
            } //end switch
            for (int a = 0; a < pirs.size(); a++) {pirs.get(a).update(personas);};
            central.checkZone();
            } //end try
            catch (Exception A){System.out.println("Invalid entry, try again");}
        } // end while
    }

    //Escribe header del archivo csv
    public void printHeader(PrintStream out){
        out.print("Step");
        for (int i=0; i < doors.size(); i++)
            out.print("\t"+doors.get(i).getHeader());
        for (int i=0 ; i< windows.size() ; i++)
            out.print("\t"+windows.get(i).getHeader());
        for (int i=0 ; i< pirs.size() ; i++)
            out.print("\t"+pirs.get(i).getHeader());
        out.print("\t"+central.getHeader());
        out.print("\t"+siren.getHeader());
        for (int i = 0; i < personas.size(); i++)
            out.print("\t"+personas.get(i).getX()+"\t"+personas.get(i).getY());
        out.println();
    }

    //Imprime linea de estado al archivo csv
    public void printState(int step, PrintStream out){
        out.print(step);
        for (int i=0; i < doors.size(); i++)
            out.print("\t"+doors.get(i).getState());
        for (int i=0 ; i< windows.size() ; i++)
            out.print("\t"+windows.get(i).getState());
        for (int i=0 ; i< pirs.size() ; i++)
            out.print("\t"+pirs.get(i).getStateInt());
        out.print("\t"+central.getState());
        out.print("\t"+siren.getState());
        out.println();
    }

    public static void main(String [] args) throws IOException {
        if (args.length != 1) {
            System.out.println("Usage: java Stage1 <configurationFile.txt>");
            System.exit(-1);
        }
        Scanner in = new Scanner(new File(args[0]));
        //System.out.println("File: " + args[0]);
        Stage3 stage = new Stage3();
        stage.readConfiguration(in);
        System.out.println("------------");
        stage.executeUserInteraction(new Scanner(System.in), new PrintStream(new File("output.csv")));
    }

    private ArrayList<Door> doors;
    private ArrayList<Window> windows;
    private ArrayList<Persona> personas;
    private ArrayList<PirSensor> pirs;
    private Central central;
    private Siren siren;
}