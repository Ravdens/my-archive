public class Persona extends Vector
{
    public Persona(float x, float y) {super(x, y);}

    public void moveNorth(){setY((float)(y+0.5));}
    public void moveSouth(){setY((float)(y-0.5));}
    public void moveEast(){setX((float)(x+0.5));}
    public void moveWest(){setX((float)(x-0.5));}
}
