import java.util.ArrayList;

public class PirSensor extends Sensor
{
    public PirSensor()
    {
        setState(SwitchState.CLOSE);
        minAngulo = 0 ; maxAngulo = 0;
        posc = new Vector(0,0);
    }
    public PirSensor(float x, float y, float aOrientacion, float aConoDeteccion , float range)
    {
        setState(SwitchState.CLOSE);
        posc = new Vector(x,y);
        this.range = range;
        minAngulo = aOrientacion-(aConoDeteccion/2);
        while(minAngulo>360){minAngulo -= 360;}
        maxAngulo = aOrientacion+(aConoDeteccion/2);
        while(maxAngulo>360){maxAngulo -= 360;}
        //System.out.println("PIR SENSOR: "+posc.getX()+" "+posc.getY()+" "+minAngulo+" "+maxAngulo+ " "+range);
    }
    {
        id = nextId++;
    }

    //EL retorn Boolean no es estrictamente necesario, pero está ahí por si se necesita a futuro.
    public Boolean update(ArrayList<Persona> personas)
    {
        Vector dV;
        for (int i = 0; i < personas.size(); i++)
        {
            dV = personas.get(i).VDif(posc);
            if(dV.theta>minAngulo && dV.theta<maxAngulo && dV.r<=range){
               setState(SwitchState.OPEN);
               return Boolean.TRUE;
            }
        }
        setState(SwitchState.CLOSE);
        return Boolean.FALSE;
    }
    //METODOS PARA ACTUALIZAR Y RETORNAR ESTADOS VAN AQUI

    public int getStateInt(){if (getState() == SwitchState.CLOSE) return 1 ; else return 0 ;}
    public String getHeader(){
        return "Pir"+id;
    }
    private Vector posc;
    private float minAngulo;
    private float maxAngulo;
    private float range;
    private final int id;
    private static int nextId;
    static {
        nextId = 0;
    }
}
