public class Door {
    public Door () {
        magneticSensor = new MagneticSensor(SwitchState.CLOSE);
        state = State.CLOSE;
    }
    {
        id = nextId++;
    }
    public void open() {
        state = State.OPEN;
        magneticSensor.moveMagnetAwayFromSwitch();
    }
    public void close() {
        state = State.CLOSE;
        magneticSensor.putMagnetNearSwitch();
    }
    public String getHeader(){
        return "d"+id;
    }
    public int getState(){ //Retorna 1 como cerrado y 0 como abierto
        if (state == State.CLOSE) return 1;
        else return 0;
    }
    public MagneticSensor getSensor() { return magneticSensor;}

    private MagneticSensor magneticSensor;
    private State state;
    private final int id;
    private static int nextId;
    static {
        nextId = 0;
    }
}
