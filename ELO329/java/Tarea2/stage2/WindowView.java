import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class WindowView extends Group {
    public WindowView(int x, int y, int angle){
        makeWindowViewWithoutSensor();
        setRotate(angle);  // to rotate at the geometric center.
        relocate(x,y); //reubicar en el plano
        prepareOpen_CloseTransition();
    }
    private void makeWindowViewWithoutSensor(){
    //                                x  , y    ,  width  , height
        origenPillar = new Rectangle(0, 0, 20, 20);
        origenPillar.setFill(Color.BLUE);
        origenPillar.setStroke(Color.BLUE);

        switchPillar = new Rectangle(180, 0, 20, 20);
        switchPillar.setFill(Color.BLUE);
        switchPillar.setStroke(Color.BLUE);

        fixedGlas = new Rectangle(21, 4, 82, 6);
        fixedGlas.setFill(Color.LIGHTBLUE);

        slidingGlas = new Rectangle(97,11,82,6);
        slidingGlas.setFill(Color.LIGHTBLUE);

        Rectangle border = new Rectangle(0, 0, 200, 20);
        border.setFill(Color.WHITE);
        border.setStroke(Color.BLACK);
        border.setStrokeWidth(1);
        border.getStrokeDashArray().addAll(4d,4d );
        getChildren().add(border);
        getChildren().addAll(origenPillar, switchPillar, fixedGlas,slidingGlas);

    }
    public void setWindowModel(Window model) {
        this.winModel = model;
        //Esto es una funcion lambda para cambiar el estado de la ventana cuando la view sea clickeada
        setOnMouseClicked(click ->{
            model.changeState();
        });
    }
    public void addMagneticSensorView(MagneticSensorView msView){
        placeMagneticSensor(msView);
        getChildren().add(msView);
    }
    private void placeMagneticSensor( MagneticSensorView mv){
        Rectangle magView = mv.getMagnetView();
        Rectangle swtView = mv.getSwitchView();
        magView.setX(slidingGlas.getX()+slidingGlas.getWidth()-magView.getWidth());
        magView.setY(slidingGlas.getY()+slidingGlas.getHeight()); //seteo en 'y' para el sensor magnetico
        magView.translateXProperty().bind(slidingGlas.translateXProperty()); // so it moves along with window

        swtView.setX(switchPillar.getX());
        swtView.setY(switchPillar.getY()+switchPillar.getHeight());
    }
    private void prepareOpen_CloseTransition(){
        transition = new TranslateTransition(Duration.millis(2000), slidingGlas);
        transition.setCycleCount(1);
        //utilizamos clase anonima para invocar un evento para decir que el movimiento ha finalizado
        transition.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e){winModel.finishMovement();}
        });
    }
    public void startOpening(){
        transition.stop();
        transition.setFromX(slidingGlas.getTranslateX());// in case the user decides to close before it opens.
        transition.setToX(slidingGlas.getTranslateX()+fixedGlas.getX()-slidingGlas.getX());
        transition.play();
    }
    public void startClosing(){
        transition.stop();
        transition.setFromX(slidingGlas.getTranslateX());  // in case the user decides to open before it closes.
        transition.setToX(switchPillar.getLayoutX()); // original position
        transition.play();
    }
    private TranslateTransition transition;
    private Window winModel;
    private Rectangle origenPillar;
    private Rectangle switchPillar;
    private Rectangle slidingGlas;
    private Rectangle fixedGlas;
}
