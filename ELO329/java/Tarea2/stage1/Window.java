/**
 * A window with its magnetic sensor.
 */
public class Window {
    public Window(int zone, WindowView view) {
        magneticSensor = new MagneticSensor(zone);
        state = State.CLOSE;
        wView = view;
        wView.addMagneticSensorView(magneticSensor.getView());
        wView.setWindowModel(this);
    }
    public void changeState() {  // is called when this window's view is clicked
        switch (state)
        {
            case OPEN:
                state = State.CLOSING;
                wView.startClosing();
                break;
            case CLOSE:
                magneticSensor.setSensorOpen();
                state = State.OPENING;
                wView.startOpening();
                break;
        }
    }
    public void finishMovement() { // is called when this window ends closing or opening
        switch (state) {
            case OPENING:
                state = State.OPEN;
                break;
            case CLOSING:
                state = State.CLOSE;
                magneticSensor.setSensorClose();
                break;
        }
    }

    public WindowView getView(){
        return wView;
    }
    private final WindowView wView;
    private final MagneticSensor magneticSensor;
    private State state;
}
