import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

public class PersonView extends Group
{
    public PersonView(){
        Rectangle body = new Rectangle(-20,-10,40,20);
        body.setFill(Color.GREEN);
        body.setStroke(Color.GREY);
        Rectangle arm1 = new Rectangle(20,-10,10,20);
        arm1.setFill(Color.GREEN);
        arm1.setStroke(Color.GREY);
        Rectangle arm2 = new Rectangle(-30,-10,10,20);
        arm2.setFill(Color.GREEN);
        arm2.setStroke(Color.GREY);
        Circle head = new Circle(0,0,10);
        head.setFill(Color.BROWN);
        head.setStroke(Color.GREY);
        getChildren().addAll(body,arm1,arm2,head);
    }

    public void setModel(Person pmodel)
    {
        model = pmodel;
        setOnMousePressed(e->{
            for (int i=0; i<getChildren().size();i++)
            ((Shape) getChildren().get(i)).setStroke(Color.YELLOW);
        });
        setOnMouseDragged(e->{
            setLayoutX(getLayoutX()+e.getX());
            setLayoutY(getLayoutY()+e.getY());
            //System.out.println(e.getX()+","+e.getY());
        });
        setOnMouseReleased(e->{
            //System.out.println("Now at: "+getLayoutX()+" "+getLayoutY());
            for (int i=0; i<getChildren().size();i++)
                ((Shape) getChildren().get(i)).setStroke(Color.GREY);
            model.move((int)getLayoutX(),(int)getLayoutY());
        });
    }

    private Person model;
}
