import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.ArcType;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

public class Pir_DetectorView extends Group
{
    public Pir_DetectorView()
    {
        Rectangle center = new Rectangle(0, 0, 20, 20);
        center.setFill(Color.GOLD);
        light = new Rectangle(5,5,6,6);
        light.setFill(Color.GREEN);
        Circle bleep = new Circle(10,0,5);
        bleep.setFill(Color.WHITE);
        bleep.setStroke(Color.LIGHTBLUE);
        getChildren().addAll(bleep,center,light);
    }
    private void makeCone(int angCono, int radio)
    {
        Arc cono = new Arc();
        cono.setType(ArcType.ROUND);
        cono.setCenterX(10);
        cono.setCenterY(10);
        cono.setLength(angCono);
        cono.setStartAngle(90-angCono/2);
        cono.setRadiusX(radio);
        cono.setRadiusY(radio);
        cono.setFill(Color.LIGHTGREEN);
        getChildren().add(0,cono);
    }
    public void makeOpen(){light.setFill(Color.RED);}
    public void makeClosed(){light.setFill(Color.GREEN);}
    public void setModel(Pir_Detector model)
    {
        this.model=model;
        model.setView(this);
        makeCone(model.getAngDeteccion(),model.getRadio());
        relocate(model.getX(),model.getY());
        setRotate(model.getPoscAngle());
        //this.setOnMouseEntered(e-> System.out.println("Entered"));
    }
    private Rectangle light;
    private Pir_Detector model;
}
