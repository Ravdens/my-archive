import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;
import java.util.Scanner;

public class House extends Pane {
    public House(Scanner in, Central central) {

        // reading <#_doors> <#_windows> <#_PIRs>
        int numDoors = in.nextInt();
        int numWindows = in.nextInt();
        int numPirs = in.nextInt();

        //Make Doors loop
        for (int i = 0; i < numDoors; i++){
            int x = in.nextInt();
            int y = in.nextInt();
            int angle = in.nextInt();
            int zone = in.nextInt();
            DoorView doorView = new DoorView(x,y,angle);
            MagneticSensor sensor = new MagneticSensor(zone);
            central.addNewSensor(sensor);
            new Door(sensor,doorView);
            getChildren().add(doorView);
        }
        //Make Windows loop
        for (int i = 0; i < numWindows; i++){
            int x = in.nextInt();
            int y = in.nextInt();
            int angle = in.nextInt();
            int zone = in.nextInt();
            WindowView windowView= new WindowView(x, y, angle);
            MagneticSensor sensor = new MagneticSensor(zone);
            central.addNewSensor(sensor);
            new Window(sensor, windowView);
            getChildren().add(windowView);
        }
        //Make Pis loop
        for (int i = 0; i < numPirs; i++){
            int x = in.nextInt();
            int y = in.nextInt();
            int angle = in.nextInt();
            int sensAngle = in.nextInt();
            int sensRad = in.nextInt();
            int zone = in.nextInt();
            Pir_DetectorView pirdetectorView = new Pir_DetectorView();
            Pir_Detector pirdetector = new Pir_Detector(x,y,angle,sensAngle,sensRad,zone);
            pirdetectorView.setModel(pirdetector);
            central.addNewSensor(pirdetector);
            getChildren().add(pirdetectorView);
        }
        //Make person (STAGE 3 ONLY)
        PersonView personaview = new PersonView();
        Person persona = new Person(personaview,300,300,central.getZones());
        getChildren().add(personaview);



        setMinWidth(800);
    }
}
