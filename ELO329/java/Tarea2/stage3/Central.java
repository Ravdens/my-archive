import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import java.util.ArrayList;

public class Central {
    public Central(Siren siren){
        view = new CentralView(this);
        zones = new ArrayList<Sensor>();
        state = CentralState.DISARMED;
        this.siren = siren;
        periodicCheck = new Timeline(new KeyFrame(Duration.millis(200),
                e-> checkZones()));
        periodicCheck.setCycleCount(Animation.INDEFINITE);
        periodicCheck.play();
    }
    public VBox getView (){
        return view;
    }
    public void armAll() {
        boolean[] close = checkCloseZones();
        String msg="Open zone(s): ";
        msg+=(!close[0]?"0":"-") + (!close[1]?",1":"-") + (!close[2]?",2":"-");
        if(close[0] && close[1] && close[2]) {
            state = CentralState.ALL_ARMED;
            view.setDisplay("All armed");
        }
        else {
            view.setDisplay(msg);
        }
    }
    public void armPerimeter() {
        boolean[] close = checkCloseZones();
        String msg="Open zone(s): ";
        msg+=(!close[0]?"0":"-") + (!close[1]?",1":"");
        if(close[0] && close[1]) {
            state = CentralState.PERIMETER_ARMED;
            view.setDisplay("Perimeter armed");
        }
        else
            view.setDisplay(msg);
    }
    public void disarm() {
        state = CentralState.DISARMED;
        view.setDisplay("Disarmed");
        siren.stop();
    }
    private boolean[] checkCloseZones() {
        boolean[] close = new boolean[3];
        for (int i = 0; i < 3; i++) {close[i]= true;}
        for (Sensor sensor : zones)
        {
            close[sensor.getZone()] &= sensor.isClose();
        }
        return close;
    }
    public void addNewSensor(Sensor s){
        zones.add(s);
    }
    private void checkZones(){
        boolean[] close = checkCloseZones();
        switch (state) {
            case ALL_ARMED -> {
                if(!close[0] || !close[1] || !close[2]){
                    siren.play();
                }
            }
            case PERIMETER_ARMED -> {
                if(!close[0] || !close[1]){
                    siren.play();
                }
            }
        }
    }

    public ArrayList<Sensor> getZones()
    {
        return zones;
    }

    enum CentralState {
        ALL_ARMED, PERIMETER_ARMED, DISARMED
    }
    private final ArrayList<Sensor> zones;
    private CentralState state;
    private final Siren siren;
    private final Timeline periodicCheck;
    private final CentralView view;
}
