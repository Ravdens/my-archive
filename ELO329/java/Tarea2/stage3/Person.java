import java.util.ArrayList;

public class Person
{
    public Person(PersonView view, int x, int y, ArrayList<Sensor> zones)
    {
        this.view = view;
        this.zones = zones;
        view.setModel(this);
        view.relocate(x,y);
    }
    public void move(int x, int y){
        this.x = x;
        this.y = y;
        for (int i = 0; i < zones.size(); i++)
        {
            if (zones.get(i) instanceof Pir_Detector)
            {
                ((Pir_Detector) zones.get(i)).check(this);
            }
        }
    }

    public int getX() {return x;}
    public int getY() {return y;}

    private PersonView view;
    private ArrayList<Sensor> zones;
    private int x;
    private int y;
    private State state;
}
