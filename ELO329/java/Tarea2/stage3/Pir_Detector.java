import javafx.scene.shape.Arc;

import java.util.ArrayList;

public class Pir_Detector extends Sensor
{
    public Pir_Detector(int x, int y, int angle, int angDeteccion, int radio, int zona)
    {
        super(zona);
        this.angDeteccion = angDeteccion;
        this.poscAngle = angle;
        this.radio = radio;
        this.x = x;
        this.y = y;
    }

    public void check(Person person)
    {
        Arc cono = (Arc) view.getChildren().get(0);
        if (cono.contains(cono.sceneToLocal(person.getX(),person.getY()))==true)
        {
            setState(SwitchState.OPEN);
            view.makeOpen();
            setClose(false);
        }
        else
        {
            setState(SwitchState.CLOSE);
            view.makeClosed();
            setClose(true);
        }
    }

    public void setView(Pir_DetectorView v){this.view = v;}
    public int getAngDeteccion() {return angDeteccion;}
    public int getRadio() {return radio;}
    public int getX() {return x;}
    public int getY() {return y;}
    public int getPoscAngle(){return poscAngle;}

    private ArrayList<Person> personas;
    private Pir_DetectorView view;
    private int poscAngle;
    private int angDeteccion;
    private int radio;
    private int x;
    private int y;
}
