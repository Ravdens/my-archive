public class Sensor {
    public Sensor(int z){
        this(z, true);
    }
    public Sensor(int z, boolean close){
        zone = z;
        isClose = close;
    }
    public SwitchState getState(){
        return state;
    }
    protected void setState(SwitchState s) {
        state = s;
    }
    public int getZone() {
        return zone;
    }

    public boolean isClose(){
        return isClose;
    }
    protected void setClose(boolean close) {
        isClose = close;
    }

    private boolean isClose;
    private SwitchState state;
    private int zone;
}
