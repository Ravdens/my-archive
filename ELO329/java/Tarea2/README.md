# TAREA 2 - ELO329
## Grupo 5
* Maximo Flores Suarez / 202130019-8
* Camilo Troncoso Hormazabal / 202130004-k
* Sergio Ehlen Montero / 202130016-3
* Cristian Gonzales Bustos / 201704003-3

## Uso del programa

#### config.txt
Debe contener la configuración del sistema de seguridad, siguiendo el siguiente formato rigido:

><#_doors> <#_windows> <#_PIRs>
>
>< x > < y > <direction_angle> <zone>
>
>...
>
>< x > < y > <direction_angle> <sensing_angle> <sensing_range> <zone>
>
>...
>
>< siren_sound_file >

#### Compilación
Se debe editar el archivo makefile, e ingresar la ruta donde se encuentra la carpeta
/lib de JavaFX en la variable JFX_OPTIONS.

Despues de esto, usar *make* para compilación, *make run* para ejecución, y *make clean* para remover
archivos innecesarios.

#### Uso del programa

*-> Abrir/Cerrar una puerta*:

Click izquierdo sobre la puerta la abrirá, o si ya está abierta, la cerrará

*-> Abrir/Cerrar una ventana*:

Click izquierdo sobre la ventana la abrirá, o si ya está abierta, la cerrará

*-> Armado completo*:

Apretar el botón [A] en la central. En modo armado completo, la sirena
se activará si se activa cualquier sensor magnetico o PIR. La antena se apagará cuando la central sea desarmada.
El armado fallará si un sensor magnetico o PIR ya se encuentra activado.

*-> Armado periferico*:

Apretar el botón [P] en la central. En modo armado perimetral, la sirena
se activará si se activa cualquier sensor magnetico. La antena se apagará cuando la central sea desarmada.
El armado fallará si un sensor magnetico ya se encuentra activado.

*-> Desarmado*:

Apretar el botón [D] en la central. Esto apagará la alarma y esta no se activará con el cambio
de los sensores hasta que se realize un armado nuevo.

*-> Crear nueva persona*:

En el menu de la barra superior, se encuentran las opciones crear nueva persona (por default en el centro de la ventana)
, y crear nueva persona en el origen. Alternativamente, puede usar CTRL+N y CTRL+SHIFT+N respectivamente.

*-> Mover una persona*:

Simplemente haga click, y lleve la persona a su nueva posición. Una vez listo, suelte el mouse
para dejarlo caer.
 
*-> Borrar una persona*:

Click derecho sobre la persona abrirá el menu de contexto de esta,
donde estará la opción para borrar.


## Archivos incluidos

#### Archivos de clase:
- Central.java
- CentralView.java
- Door.java
- DoorView.java
- House.java
- MagneticSensor.java
- MagneticSensorView.java
- Person.java
- PersonView.java
- Pir_Detector.java
- Pir_DetectorView.java
- Sensor.java
- Siren.java
- SirenView.java
- Stage4.java
- Window.java
- WindowView.java

#### Archivos Enum:
- State.java

#### Archivo Principal:

- Stage4.java

_Contiene la configuración inicial de la interfaz, y permite lanzar la aplicación._ 

#### Archivos extra:

- makefile

_Archivo con las instrucciones de compilación. Su uso se explica en la sección Compilación._

- config.txt

_Archivo de configuración utilizado como prueba durante el desarrollo. Puede ser editar para cambiar
el layout._

- Documentacion_Tarea2_Grupo6_ELO329.pdf

_Archivo de documentación con información relevante sobre el desarrollo y estructura interna del código._

- Central_structure.puml
- Central_structure.png

_Contienen el diagrama UML del programa_

### Puntaje extra Stage 5
No se opta a puntaje extra por stage 5.

_Para más información, consultar el archivo pdf de documentación_

