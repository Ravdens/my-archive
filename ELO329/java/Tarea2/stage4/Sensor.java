///////    Sensor.java    ///////

/**
 *Superclase de sensor para el programa de GraphicAlarmTest
 * Diseñado para ser heradado, pero aun así puede funcionar como
 * clase propia (incluido en código base de stage 2).
 * @see <a href="http://profesores.elo.utfsm.cl/~agv/elo329/1s23/Assignments/T2/stage2.tar">...</a>
 */
public class Sensor {
    /**
     * Constructor Sensor (sin estado). Crea un sensor sin especificar su estado.
     * @param z     Id de zona a la que pertenece el sensor.
     */
    public Sensor(int z){
        this(z, true);
    }

    /**
     * Constructor Sensor (con estado). Crea un sensor especificando su estado.
     * @param z     Id de zona a la que pertenece el sensor.
     * @param close indica si el sensor esta cerrado o abierto.
     */
    public Sensor(int z, boolean close){
        zone = z;
        isClose = close;
    }

    /**
     *Get Zone
     * @return z    Id de zona a la que pertenece el sensor.
     */
    public int getZone() {
        return zone;
    }

    /**
     * Get isClose
     * @return isClose  Valor booleano del estado del sensor.
     */

    public boolean isClose(){
        return isClose;
    }

    /**
     * Set isClose
     * @param close     Nuevo valor booleano del estado del sensor.
     */
    protected void setClose(boolean close) {
        isClose = close;
    }

    /**
     * Valoor booleano del estado del sensor (true = está cerrado).
     */
    protected boolean isClose;
    /**
     * Numero identificador de la zona a la que pertenece el sensor.
     */
    private int zone;
}
