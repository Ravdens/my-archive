public class Person
{
    public Person(PersonView view, int x, int y, House inHouse)
    {
        this.view = view;
        this.inHouse = inHouse;
        view.setModel(this);
        view.relocate(x,y);
    }
    public void move(int x, int y){
        this.x = x;
        this.y = y;
        inHouse.getCentral().updatePirs();
    }

    public int getX() {return x;}
    public int getY() {return y;}

    private PersonView view;
    private House inHouse;
    private int x;
    private int y;
    private State state;
}
