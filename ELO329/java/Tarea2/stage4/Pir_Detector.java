
///////    Pir_Detector.java    ///////

/**
 * Subclase que hereda de Sensor. Cumple la funcion de modelo en el patrón MVC para el objeto
 * sensor pir. Este es un sensor que detecta personas dentro de un cierto angulo de detección.
 * @author Grupo 05
 */
public class Pir_Detector extends Sensor
{
    /**
     * Constructor Pir_Detector
     * @param x             Valor del eje horizontal de la posición (de izquierda a derecha).
     * @param y             Valor del eje vertical de la posición (de arriba a abajo).
     * @param angle         Angulo al que está ubicado el sensor.
     * @param angDeteccion  Angulo dentro del cual es capaz de detectar.
     * @param radio         Distancia máxima dentro de la cual puede detectar.
     * @param zona          Id de zona a la que pertenece el sensor.
     */
    public Pir_Detector(int x, int y, int angle, int angDeteccion, int radio, int zona)
    {
        super(zona);
        this.angDeteccion = angDeteccion;
        this.poscAngle = angle;
        this.radio = radio;
        this.x = x;
        this.y = y;
    }
    /**
     * Confirma detección en un punto dentro del plano.
     * @param x             Valor del eje horizontal de la posición (de izquierda a derecha).
     * @param y             Valor del eje vertical de la posición (de arriba a abajo).
     * @return              true si puede detectar el punto (x,y), false de lo contrario.
     */
    public boolean detects(int x, int y){
        return view.getCone().contains(view.getCone().sceneToLocal(x,y));
    }

    /**
     * Set isClose
     * @param close     Nuevo valor booleano del estado del sensor.
     */
    @Override
    protected void setClose(boolean close) {
        if (close) {
            super.setClose(true);
            view.makeClosed();
        }
        else {
            super.setClose(false);
            view.makeOpen();
        }
    }

    /**
     * Set Pir_DetectorView. Es necesario para poder utilizar detects()
     * @param v     Vista del sensor pir en el programa.
     */
    public void setView(Pir_DetectorView v){
        this.view = v;
        view.setModel(this);
        view.makeCone(angDeteccion,radio);
        view.relocate(x,y);
        view.setRotate(poscAngle);
    }

    /**
     * Get Pir_DetectorView
     * @return v     Vista del sensor pir en el programa.
     */
    public Pir_DetectorView getView() {return view;}

    /**
     * Retorna angulo de detección
     * @return angDeteccion  Angulo dentro del cual es capaz de detectar.
     */
    public int getAngDeteccion() {return angDeteccion;}

    /**
     * Retorna radio
     * @return  Distancia máxima dentro de la cual puede detectar.
     */
    public int getRadio() {return radio;}

    /**
     * Retorna X
     * @return x             Valor del eje horizontal de la posición (de izquierda a derecha).
     */
    public int getX() {return x;}

    /**
     * Retorna Y
     * @return y             Valor del eje vertical de la posición (de arriba a abajo).
     */
    public int getY() {return y;}

    /**
     * Retorna poscAngle
     * @return poscAngle         Angulo al que está ubicado el sensor.
     */
    public int getPoscAngle(){return poscAngle;}

    /**
     * Vista del sensor PIR de acuerdo al patrón de diseño MVC
     */
    private Pir_DetectorView view;
    /**
     * Angulo al que se encuentra posicionado el sensor (0-360)
     */
    private int poscAngle;
    /**
     * Amplitud del angulo dentro del que es capas de detectar el sensor (0-360)
     */
    private int angDeteccion;
    /**
     * Distancia dentro de la cual es capas de detectar el sensor
     */
    private int radio;
    /**
     * Distancia desde el borde izquierdo de la interfaz
     */
    private int x;
    /**
     * Distancia desde el borde superior de la interfaz
     */
    private int y;
}
