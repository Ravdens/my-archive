import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.Scanner;

public class House extends Pane {
    public House(Scanner in, Central central) {
        this.central = central;
        // reading <#_doors> <#_windows> <#_PIRs>
        int numDoors = in.nextInt();
        int numWindows = in.nextInt();
        int numPirs = in.nextInt();

        //Make Doors loop
        for (int i = 0; i < numDoors; i++){
            int x = in.nextInt();
            int y = in.nextInt();
            int angle = in.nextInt();
            int zone = in.nextInt();
            DoorView doorView = new DoorView(x,y,angle);
            MagneticSensor sensor = new MagneticSensor(zone);
            central.addNewSensor(sensor);
            new Door(sensor,doorView);
            getChildren().add(doorView);
        }
        //Make Windows loop
        for (int i = 0; i < numWindows; i++){
            int x = in.nextInt();
            int y = in.nextInt();
            int angle = in.nextInt();
            int zone = in.nextInt();
            WindowView windowView= new WindowView(x, y, angle);
            MagneticSensor sensor = new MagneticSensor(zone);
            central.addNewSensor(sensor);
            new Window(sensor, windowView);
            getChildren().add(windowView);
        }
        //Make Pis loop
        for (int i = 0; i < numPirs; i++){
            int x = in.nextInt();
            int y = in.nextInt();
            int angle = in.nextInt();
            int sensAngle = in.nextInt();
            int sensRad = in.nextInt();
            int zone = in.nextInt();
            Pir_DetectorView pirdetectorView = new Pir_DetectorView();
            Pir_Detector pirdetector = new Pir_Detector(x,y,angle,sensAngle,sensRad,zone);
            pirdetector.setView(pirdetectorView);
            central.addNewSensor(pirdetector);
            getChildren().add(pirdetectorView);
        }
        setMinWidth(800);
    }

    public void setPersonas(ArrayList<Person> personas){this.personas = personas;}

    public Central getCentral() {return central;}

    public void makeNewPersona(int x, int y)
    {
        // context menu
        ContextMenu contextMenu = new ContextMenu();
        MenuItem menuItemRemove = new MenuItem("Eliminar");
        contextMenu.getItems().add(menuItemRemove);

        //Make persona
        PersonView personaview = new PersonView();
        Person persona = new Person(personaview,0,0,this);
        personaview.setModel(persona);
        getChildren().add(personaview);
        personas.add(persona);

        personaview.relocate(x,y);
        persona.move(x,y);

        personaview.setOnMouseClicked(e->
        {
            if (e.getButton() == MouseButton.SECONDARY)
                contextMenu.show(personaview, e.getScreenX(), e.getScreenY());
        });
        menuItemRemove.setOnAction(e->{
            getChildren().remove(personaview);
            personas.remove(persona);
            persona.move(-1,-1);
        });
    }
    private Central central;
    private ArrayList<Person> personas;
}
