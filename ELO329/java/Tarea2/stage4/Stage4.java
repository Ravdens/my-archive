import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Stage4 extends Application {
    @Override
    public void start(Stage primaryStage) {
        List<String> args = getParameters().getRaw();
        if (args.size() != 1) {
            System.out.println("Usage: java Stage4 <configurationFile.txt>");
            System.exit(-1);
        }
        Scanner in = null;
        try {
            in = new Scanner(new File(args.get(0)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        //Siren siren = new Siren(Stage2.class.getResource("siren.wav").toExternalForm());
        Siren siren = new Siren("http://profesores.elo.utfsm.cl/~agv/elo329/JavaProg/JavaFX/AudioMediaDemo/siren.wav");
        Central central = new Central(siren);
        House house = new House(in, central);
        house.setPadding(new Insets(10,10,10,10));
        in.close();

        // Central de control
        VBox vBox = new VBox(20);
        Separator hSeparator = new Separator(Orientation.HORIZONTAL);
        vBox.getChildren().addAll(siren.getView(),hSeparator, central.getView());
        vBox.setPadding(new Insets(10, 10,10,10));
        vBox.setAlignment(Pos.CENTER);

        // agregando barra de menú
        MenuBar menuBar = new MenuBar();
        Menu menuInsert = new Menu("Insertar");
        menuBar.getMenus().add(menuInsert);

        MenuItem menuAgregarPersona = new MenuItem("Agregar persona");
        menuInsert.getItems().add(menuAgregarPersona);
        menuAgregarPersona.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));
        menuAgregarPersona.setOnAction(e->house.makeNewPersona((int) (house.getWidth()/2), (int) (house.getHeight()/2)));

        MenuItem menuAgregarPersonaCero = new MenuItem("Agregar persona (Corner)");
        menuInsert.getItems().add(menuAgregarPersonaCero);
        menuAgregarPersonaCero.setAccelerator(KeyCombination.keyCombination("Ctrl+SHIFT+N"));
        menuAgregarPersonaCero.setOnAction(e-> house.makeNewPersona(0, 0));

        VBox vBoxHouse = new VBox(20);
        vBoxHouse.getChildren().add(menuBar);
        vBoxHouse.setAlignment(Pos.TOP_CENTER);

        //Ligaremos arreglo persons a House y a Siren, pues Siren necesita revisarlas, y House necesita poder agregarles
        ArrayList<Person> personas = new ArrayList<>();
        central.setPersonas(personas);
        house.setPersonas(personas);



        // Adición de pane a la escena
        BorderPane borderPane = new BorderPane();
        borderPane.setLeft(house);
        borderPane.setCenter(new Separator(Orientation.VERTICAL));
        borderPane.setTop(vBoxHouse);
        borderPane.setRight(vBox);
        Scene scene = new Scene(borderPane);

        // Levantar escenario
        primaryStage.setScene(scene);
        primaryStage.setTitle("GraphicAlarmTest!");
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}

