
public class Door {
    public Door(MagneticSensor sensor, DoorView view) {
        magneticSensor = sensor;
        state = State.CLOSE;
        dView = view;
        dView.addMagneticSensorView(magneticSensor.getView());
        dView.setDoorModel(this);
    }
    public void changeState() {
        switch (state) {  // this is the enhanced version of switch statement. It does not require breaks.
            case OPEN -> {
                state = State.CLOSING;
                dView.startClosing();
            }
            case CLOSE -> {
                state = State.OPENING;
                dView.startOpening();
                magneticSensor.setSensorOpen();

            }
        }
    }
    public void finishMovement() { // is called when this window ends closing or opening
        switch (state)
        {
            case OPENING ->
            {
                state = State.OPEN;
            }
            case CLOSING ->
            {
                state = State.CLOSE;
                magneticSensor.setSensorClose();
            }
        }
    }

    private final DoorView dView;
    private final MagneticSensor magneticSensor;
    private State state;
 }