import javafx.animation.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;
import javafx.util.Duration;
public class fDoorView extends Group {
    public DoorView(int x, int y, int angle){
        this.angle = angle;
        makeDoorViewWithoutSensor();
        setRotate(angle);  // to rotate at the geometric center.
        prepareOpen_CloseTransition();
        relocate(x,y);
    }
    private void makeDoorViewWithoutSensor(){
        origenPillar = new Polygon();
        origenPillar.getPoints().addAll(0d,0d,
                0d,20d,
                10d, 20d,
                10d, 10d,
                20d, 10d,
                20d, 0d);
        origenPillar.setFill(Color.BLUE);
        origenPillar.setStroke(Color.BLUE);
        switchPillar = new Polygon(
                160d,0d,
                160d, 10d,
                170d, 10d,
                170d, 20d,
                180d, 20d,
                180d, 0d );
        switchPillar.setFill(Color.BLUE);
        switchPillar.setStroke(Color.BLUE);
        slidingSheet = new Rectangle(10,10,160,10);
        slidingSheet.setFill(Color.BURLYWOOD);
        border = new Rectangle(0,0 ,180, 180);
        border.setFill(Color.TRANSPARENT);
        border.setStroke(Color.GRAY);
        border.getStrokeDashArray().addAll(4d,4d );
        getChildren().addAll(border);
        getChildren().addAll(origenPillar, switchPillar,slidingSheet);
    }
    public void setDoorModel(Door model) {
        this.doorModel = model;
        setOnMouseClicked(e->{
            model.changeState();
        });
    }
    public void addMagneticSensorView(MagneticSensorView msView){
        placeMagneticSensor(msView);
        getChildren().add(msView);
    }
    private void placeMagneticSensor( MagneticSensorView mv){
        Rectangle sw = mv.getSwitchView();
        Rectangle mg = mv.getMagnetView();

        mg.setX(origenPillar.getLayoutX()+2*mg.getWidth());
        mg.setY(origenPillar.getLayoutY()+slidingSheet.getHeight());

        mg.getTransforms().add(new Translate(-slidingSheet.getWidth(),slidingSheet.getHeight()));
        mg.setTranslateX(slidingSheet.getWidth()-mg.getWidth());
        //mg.setTranslateY(slidingSheet.getHeight());
        mgtransition = new RotateTransition(Duration.millis(2000),mg);
        transition.setCycleCount(1);

        sw.setX(switchPillar.getLayoutX());
        sw.setY(switchPillar.getLayoutY()+3*sw.getHeight());
    }

    private void prepareOpen_CloseTransition(){

        slidingSheet.getTransforms().add(new Translate(-slidingSheet.getWidth()/2,0));
        slidingSheet.setTranslateX(slidingSheet.getWidth()/2);

        transition = new RotateTransition(Duration.millis(2000),slidingSheet);
        transition.setCycleCount(1);
        transition.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e){doorModel.finishMovement();}
        });
    }

    public void startOpening(){
        //Door movement
        transition.stop();
        transition.setFromAngle(slidingSheet.getRotate());
        transition.setToAngle(slidingSheet.getRotate()-90);
        transition.play();

        //Magnet movement
        if (mgtransition!=null)
        {
            mgtransition.stop();
            mgtransition.setFromAngle(slidingSheet.getRotate());
            mgtransition.setToAngle(slidingSheet.getRotate()-90);
            mgtransition.play();
        }
    }
    public void startClosing(){
        //Door movement
        transition.stop();
        transition.setFromAngle(slidingSheet.getRotate());
        transition.setToAngle(slidingSheet.getRotate()+90);
        transition.play();

        //Magnet movement
        if (mgtransition!=null)
        {
            mgtransition.stop();
            mgtransition.setFromAngle(slidingSheet.getRotate());
            mgtransition.setToAngle(slidingSheet.getRotate()+90);
            mgtransition.play();
        }
    }

    private int angle;
    private RotateTransition mgtransition;
    private RotateTransition transition;
    private Door doorModel;
    private Polygon switchPillar;
    private Polygon origenPillar;
    private Rectangle slidingSheet;
    private Rectangle border;
}
