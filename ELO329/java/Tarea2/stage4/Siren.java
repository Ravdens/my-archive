import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.shape.Polygon;

public class Siren {
    public Siren (String mediaURL){
        Media media = new Media(mediaURL);
        mediaPlayer = new MediaPlayer(media);
        mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);
        view = new SirenView();
    }
    public void play(){
        mediaPlayer.play(); 
        view.startBlinking();
    }
    public void stop(){
        mediaPlayer.stop();
        view.stopBlinking();
    }
    public Polygon getView() {
        return view;
    }
    private SirenView view;
    private MediaPlayer mediaPlayer;
}
