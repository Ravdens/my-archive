public class Fly
{
    private double x;
    private double y;

    public Fly(double x, double y)
    {
        this.x = x;
        this.y = y;
    }
    public void moveup(){
        y = y+5;
    }
    public void movedown(){
        y = y-5;
    }
    public void moveleft(){
        x = x-5;
    }
    public void moveright(){
        x = x+5;
    }

    public double getX()
    {return x;}

    public void setX(double x)
    {this.x = x;}

    public double getY()
    {return y;}

    public void setY(double y)
    {this.y = y;}
}
