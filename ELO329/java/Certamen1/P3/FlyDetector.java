public class FlyDetector
{
    private double rango;

    public FlyDetector(double rango)
    {
        this.rango = rango;
    }

    public boolean isinrange(Fly mosca){
        double dist = Math.sqrt((mosca.getX()* mosca.getX())+(mosca.getY()*mosca.getY()));
        return (dist<rango);
    }
}
