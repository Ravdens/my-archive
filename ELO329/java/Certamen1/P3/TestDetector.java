import java.util.Scanner;

public class TestDetector
{
    public static void main(String[] args)
    {
        System.out.println(args[0]);

        if (args.length < 3) {
            System.out.println("Error en la entrada ingresada");
            System.exit(-1);
        }

        Fly mosca = new Fly(Double.parseDouble(args[0]),Double.parseDouble(args[1]));
        FlyDetector detector = new FlyDetector(Double.parseDouble(args[2]));
        Scanner scan = new Scanner(System.in);
        char dir;

        Boolean done = false;

        while (!done){
            try{dir = scan.next().charAt(0);}
            catch (Exception e){dir = 'e';}

            switch (dir)
            {
                case 'w': mosca.moveup(); break;
                case 'a': mosca.moveleft(); break;
                case 's': mosca.moveright(); break;
                case 'z': mosca.movedown(); break;
                case 'x': done = true; break;
                default:
                    System.out.println("Oops, entrada invalida!!");
            }

            if(detector.isinrange(mosca)){
                System.out.println("Detectada");
            }
        }




    }
}
