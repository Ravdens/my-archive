import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.paint.Color;

import java.awt.event.MouseEvent;
import java.beans.EventHandler;

public class JavaFxDetector extends Application
{
    @Override
    public void start(Stage primaryStage)
    {
        //From ShowCircleCentered.java
        Pane pane = new Pane();
        Circle circle = new Circle();
        circle.centerXProperty().bind(pane.widthProperty().divide(2));
        circle.centerYProperty().bind(pane.heightProperty().divide(2));
        circle.setRadius(30);
        circle.setStroke(Color.BLACK);
        circle.setFill(Color.LIGHTGREEN);
        pane.getChildren().add(circle);

        //Codigo especifo a la resolucion del problema
        circle.setOnMouseEntered( In ->{
            System.out.println("Detectada");
        });



        Scene scene = new Scene(pane, 300, 300);
        primaryStage.setTitle("JavaFxDetector!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args){
        launch(args);
    }
}