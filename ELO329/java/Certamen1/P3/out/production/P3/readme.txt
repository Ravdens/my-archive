# Para compilar TestDetector.java

javac TestDetector.java

# Para compilar JavaFxDetector.java

javac -g --module-path /path/to/jfx/lib --add-modules=javafx.controls,javafx.fxml JavaFxDetector.java
