public class Enemigo extends Personaje
{
    private int fuerza;
    public Enemigo(String nombre,int vida,int fuerza)
    {
        this.nombre = nombre;
        this.fuerza = fuerza;
        this.vida = vida;
    }

    @Override
    public void atacar(Personaje objetivo)
    {
        objetivo.recibirDano(fuerza);
    }

    @Override
    public boolean estaVivo()
    {
        return (vida>0);
    }

    public int getFuerza()
    {
        return fuerza;
    }

    public void setFuerza(int fuerza)
    {
        this.fuerza = fuerza;
    }
}
