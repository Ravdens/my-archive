import java.util.Random;

public class Mago extends Personaje
{
    private int poder;

    public Mago(String nombre,int vida,int poder)
    {
        this.nombre= nombre;
        this.vida = vida;
        this.poder = poder;
    }

    @Override
    public void atacar(Personaje objetivo)
    {
        objetivo.recibirDano(poder);
    }

    @Override
    public boolean estaVivo()
    {
        return (vida>0);
    }

    public void aumentarVida()
    {
        Random random = new Random();
        random.nextInt(5);
    }
}
