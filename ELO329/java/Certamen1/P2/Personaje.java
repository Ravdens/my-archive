public abstract class Personaje implements Metodos
{
    protected String nombre;
    protected int vida;

    public abstract void atacar(Personaje objetivo);

    @Override
    public void recibirDano(int cantidad)
    {
        vida = vida-cantidad;
    }

    public String getNombre()
    {return nombre;}

    public void setNombre(String nombre)
    {this.nombre = nombre;}

    public int getVida()
    {return vida;}

    public void setVida(int vida)
    {this.vida = vida;}
}
