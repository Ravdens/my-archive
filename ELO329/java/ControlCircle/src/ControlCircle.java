import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.scene.Scene;

class CirclePane extends StackPane
{
    private Circle circle = new Circle(220);
    public CirclePane() {
        getChildren().add(circle);
        circle.setStroke(Color.BLACK);
        circle.setFill(Color.WHITE);
    }
    public void enlarge() {
        circle.setRadius(circle.getRadius() + 5);
    }
    public void shrink() {
        circle.setRadius(circle.getRadius() > 5
                ? circle.getRadius() - 5 :
                circle.getRadius());
    }
    public void newRadius(double r){
        circle.setRadius(r);
    }
    public void makePink(){
        circle.setFill(Color.GREEN);
    }
    public void makeWhite(){
        circle.setFill(Color.WHITE);
    }
}


public class ControlCircle extends Application {
    private CirclePane circlePane = new CirclePane();
    @Override
    public void start(Stage primaryStage) {
        HBox hBox1 = new HBox();

        Button btEnlarge = new Button("Enlarge");
        Button btShrink = new Button("Shrink");
        Button btMakePink = new Button("Make pink");
        Button btNOMOREPINK = new Button("NO MORE PINK");


        hBox1.getChildren().add(btEnlarge);
        hBox1.getChildren().add(btShrink);
        hBox1.getChildren().add(btMakePink);
        hBox1.getChildren().add(btNOMOREPINK);

        btEnlarge.setOnAction(e -> circlePane.enlarge());
        btShrink.setOnAction(e -> circlePane.shrink());
        btMakePink.setOnAction(e -> circlePane.makePink());
        btNOMOREPINK.setOnAction(e-> circlePane.makeWhite());

        HBox hBox2 = new HBox();

        Label label = new Label("Set radius");
        TextField textfield = new TextField();
        textfield.setPrefWidth(200);

        hBox2.getChildren().add(label);
        hBox2.getChildren().add(textfield);

        textfield.setOnKeyReleased(e->{if (e.getCode() == KeyCode.ENTER)
            circlePane.newRadius(Double.parseDouble(textfield.getText()));
        });



        VBox bigVbox = new VBox();
        bigVbox.getChildren().add(hBox1);
        bigVbox.getChildren().add(hBox2);

        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(circlePane);
        borderPane.setBottom(bigVbox);
        bigVbox.setAlignment(Pos.CENTER);
        Scene scene = new Scene(borderPane, 700, 700);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

}