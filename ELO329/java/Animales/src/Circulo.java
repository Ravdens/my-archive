public class Circulo extends Figura implements Redimensionable
{
    public double r;
    public double area(){
        return 3*r*r;
    }
    public void escalar(double factor){
        r = r*factor;
    }

}
