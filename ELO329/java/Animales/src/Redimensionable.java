public interface Redimensionable
{
    public abstract void escalar(double factor);
}
