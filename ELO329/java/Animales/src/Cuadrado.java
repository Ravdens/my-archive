public class Cuadrado extends Figura implements Redimensionable
{
    public double lado;
    public double area(){
        return lado*lado;
    }
    public void escalar(double factor){
        lado = lado*factor;
    }
}
