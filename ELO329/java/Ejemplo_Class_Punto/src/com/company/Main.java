package com.company;

public class Main {

    public static void main(String[] args) {
	        //This is the cetral method to execute commands and call the objects, OK!
            //Now, we use the class Punto through several objects

        Punto p1 = new Punto(); /* Name: Default constructor or Null Constructor, please look in the class the status*/
        /*Test*/
        System.out.println( p1.getX() );
        System.out.println( p1.getY() );

        /*Test another different object from class Punto*/
        Punto p2 = new Punto(8,7); /*This time we use the constructor with parameters*/
        System.out.println( p2.getX() );
        System.out.println( p2.getY() );

        /*So, we have two different points*/
        /*Now, we can calculate de Euclidean Distance between them, How come?*/
       /*First for all, we need to check if the class Point presents a behavior (comportamiento o metodo) for that purpose, letsg!*/
       /*No!, then implements!*/
        System.out.println( p1.euclidean_distance(p2) );
        System.out.println( p2.euclidean_distance(p1) );

        Punto p3 = clonar(p1);
        p3.printpunto();
        Punto p4 = clonar(p2);
        p4.printpunto();

    }

    public static Punto clonar(Punto p3){
        Punto aux = new Punto(p3.getX(), p3.getY());
        return aux;
    }
}
