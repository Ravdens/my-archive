package com.company;
import java.lang.Math;

class Punto { // nombre de la clase

    /*Atributos*/
    private int x,y; // atributos para almacenar el estado.

    /*Constructores*/
    public Punto(){ // método, define estado inicial, al momento de
        x=y=0; // ser creado, lo llamamos método constructor.
    } // fin de constructor

    public Punto(int _x, int _y){ // otro constructor
        x=_x;
        y=_y;
    }

    /*Metodos o comportamientos generales de un objeto Punto!*/

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean equals(Punto p){
        if (p==null) return false; // Objeto p no creado aún.
        return ((x==p.getX()) && (y==p.getY()));
    }

    /**/
    public double euclidean_distance(Punto p2) {
        double d1 = (x - p2.getX()) * (x - p2.getX());
        double d2 = (y - p2.getY()) * (y - p2.getY());
        return Math.sqrt(d1 + d2);
    }

    public void printpunto(){
        System.out.println( "(" + x + "," + y +")");

    }

}
