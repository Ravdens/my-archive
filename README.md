# Hola!
Este repo solo es para juntar en un único lugar todo mi código que he escrito para distintos ramos, porque a veces necesito hecharle un luke y está todo desparramado :P

## Ramos:
- TEL101 : Iniciación a la programación : Python
- TEL102 : Seminario de programación : Cpp+Qt
- ELO320 : Estructura de datos y algoritmos : C
- ELO322 : Redes de computadores : Python
- INF239 : Bases de datos : Python+SQL
- INF236 : Análisis y diseño de software : JS+React+Sequelize / Dockerfile
- ELO329 : Diseño y programación orientada a objetos : Java+JavaFx / Cpp+Qt
- TEL241 : Laboratorio de redes de computadores : Cisco / Huawei
- INF225 : Ingeniería de software : Python+Reflex / JS+Sequelize / Dockerfile
- TEL231 : Sistemas de telecomunicaciones : Matlab
- ELO241 : Laboratorio de comunicaciones : Matlab
- MAT270 : Análisis numérico : Matlab
- TEL354 : Minería de datos : Python+Pandas+ScikitLearn
- ELO212 : Laboratorio de sistemas digitales : SystemVerilog