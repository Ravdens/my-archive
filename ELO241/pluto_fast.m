sampleRate = 20000e3; %tasa de muestreo
centerFreq = 2.4e9; %Frecuencia Central
numMuestras=2000000; %cantidad de datos por medicion
rx = sdrrx('Pluto','RadioID','usb:0','CenterFrequency',centerFreq); %inicializacion ..del dispositivo
rx.GainSource='Manual' %Tipo de ganancia manual
rx.Gain=10; %configuracion de ganancia
rx.SamplesPerFrame=numMuestras; %Configuracion de numero de muestras por medicion
rx.OutputDataType='double'; %configuracion de tipo de datos como double
rx.BasebandSampleRate=sampleRate; %configuracion tasa de muestreo
rx.ShowAdvancedProperties=true; %ver configuracion avanzada
dataA=real(rx());

sa = dsp.SpectrumAnalyzer('SampleRate', sampleRate, 'SpectralAverages', 1);
sa.PlotAsTwoSidedSpectrum=false;
sa.RBWSource = 'Property';
sa.RBW =1000;
sa.FrequencySpan="start and stop frequencies"; %''Full
sa.StartFrequency=0 ; % frecuencia de inicio
sa.StopFrequency=30e3; % frecuencia final
sa(dataA); %vizualizar los datos