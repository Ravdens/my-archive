
%Tiempo maximo a graficar
mx = 10;
%Numero de funciones a graficar
n = 4;


z = 0:0.1:mx;
J = zeros(n+1,1+mx*10);
for i = 0:n
    J(i+1,:) = besselj(i,z);
end

hold on
plot(z,J)
plot(z,zeros(size(z)))
hold off
axis([0,mx,-1,1])
grid on
%legend('J_0','J_1','J_2','J_3','J_4','Location','Best')
ti = ['Bessel Functions of the First Kind for $\nu \in [0, ',num2str(n),']$'];
title(ti,'interpreter','latex')
xlabel('z','interpreter','latex')
ylabel('$J_\nu(z)$','interpreter','latex')