root="/home/ravdens/Desktop/2023-2/ELO241/lab241.3.5/distancia/";

load(root+"1_10.mat");
subplot(1,3,1);
title("Potencia[dBm](x,y) separacion 1[m]");
xlabel('x[m]'); ylabel('y[m]'); zlabel('Potencia[dBm]')
surf(Dim_x,Dim_y,Pr)

load(root+"2_10.mat");
subplot(1,3,2);
title("Potencia[dBm](x,y) separacion 10[m]");
xlabel('x[m]'); ylabel('y[m]'); zlabel('Potencia[dBm]')
surf(Dim_x,Dim_y,Pr)

load(root+"3_10.mat");
subplot(1,3,3);
title("Potencia[dBm](x,y) separacion 100[m]");
xlabel('x[m]'); ylabel('y[m]'); zlabel('Potencia[dBm]')
surf(Dim_x,Dim_y,Pr)
