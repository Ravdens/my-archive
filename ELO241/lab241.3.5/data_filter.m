prueba = "distancia/3_"

for n = 1:10
    root="/home/ravdens/Desktop/2023-2/ELO241/lab241.3.5/";
    filename = root+prueba+string(5*n)+".mat";
    load(filename);
    tot = 0;
    count = 0;
    for i = 1:size(Dim_x)
        for j = 1:size(Dim_y)
            tot = tot + Pr(i,j);
            count = count+1;
        end
    end
    disp("D: "+5*n+" P: "+string(tot/count))
end