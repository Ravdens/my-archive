%% recepción de datos mediante dispositivo adalm-pluto
% A continuación se presenta el codigo de Matlab que permite la recepción
% de datos mediante el dispositivo Adalm Pluto, considerando la
% configuración de frecuencia central, frecuencia de muestreo, ganancia de
% recepción, entre otros parametros del dispositivo.

%Configuración del equipo
sampleRate = 20000e3;      %Tasa de muestreo
centerFreq = 2.4e9;        %Frecuencia Central
numMuestras=2000000;       %Cantidad de datos por medicion
rx = sdrrx('Pluto','RadioID','usb:0','CenterFrequency',centerFreq); %Inicializacion del dispositivo
rx.GainSource='Manual'     %Tipo de ganancia manual
rx.Gain=40;                % configuracion de ganancia en 40 dB
rx.SamplesPerFrame=numMuestras;   %Configuracion de numero de muestras por medicion
rx.OutputDataType='double';       %configuracion de tipo de datos como double
rx.BasebandSampleRate=sampleRate; %configuracion tasa de muestreo
rx.ShowAdvancedProperties=true;   %ver configuracion avanzada del dispositivo

%Recepción de datos
data1=real(rx());          %Datos recibidos

%% Osciloscopio
% A continuación se presenta el codigo Matlab que permite generar y
% utilizar un Osciloscopio digital para la visualización temporal de los 
% datos recibidos por el dispositivo Adalm Pluto

scope = timescope('SampleRate', sampleRate);  %inicializacion del osciloscopio
scope.MeasurementChannel=1;   %numero de canales de medicion

%scope(data1);  %Vizualizar los datos medidos

%% Analizador de espectros
% A continuación se presenta el codigo Matlab que permite generar y
% utilizar un Analizador de Espectros digital para la visualización en
% frecuencia de los datos recibidos por el dispositivo Adalm Pluto para
% Matlab version 2021 hacia atrás

sa = dsp.SpectrumAnalyzer('SampleRate', sampleRate, 'SpectralAverages', 1); %Inicialización del AE
sa.PlotAsTwoSidedSpectrum=false; %Visualizar frecuencias negativas
sa.RBWSource = 'Property'; % Ancho de banda segun propiedad
sa.RBW =50;  % Ancho de banda 100 Hz
sa.FrequencySpan='start and stop frequencies'; %Configuración de frecuencia según Start y Stop
sa.StartFrequency=25e3 ; % frecuencia inicial de vizualización
sa.StopFrequency=35e3; % frecuencia final de viazualización

%sa(data1);  %vizualizar los datos recibidos