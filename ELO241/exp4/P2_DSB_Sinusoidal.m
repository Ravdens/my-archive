clear all
%% Configuración de transmisor dispositivo adalm-pluto
% A continuación se presenta el código de Matlab que permite configurar la 
% transmición de datos mediante el dispositivo Adalm Pluto, considerando la
% frecuencia central, frecuencia de muestreo, ganancia de
% transmisión, entre otros parametros del dispositivo.

sampleRate = 20000e3;             % Tasa de muestreo
centerFreq = 2.4e9;               % Frecuencia Central
numMuestras=2000000;              % Numero de muestras a transmitir
tx = sdrtx('Pluto','RadioID','usb:0','CenterFrequency',centerFreq) %Inicializacion del dispositivo
tx.Gain=-10                         % Configuracion de ganancia
tx.BasebandSampleRate=sampleRate; % Configuracion tasa de muestreo
tx.ShowAdvancedProperties=true;   % Ver configuracion avanzada
tx.SamplesPerFrame=numMuestras;   % Configuracion de numero de muestras por medicion
tx.OutputDataType='double';       % Configuracion de tipo de datos como double
tx.BasebandSampleRate=sampleRate; % Configuracion tasa de muestreo
tx.ShowAdvancedProperties=true;   % Ver configuracion avanzada del dispositivo

%% Generación del mensaje, portadora y señal modulada DSB-SC
% A continuación se presenta el código de Matlab que permite generar las
% señales modulante (mensaje) sinusoidal, portadora sinusoidal y señal 
% modulada segun el esquema DSB-SC
t=[0:numMuestras-1]'/sampleRate;    % Base temporal de las señales
fc=100e3;                           % Frecuencia Portadora
fm=10e3;                            % Frecuencia mensaje
Am=1;                               % Amplitud Modulante
Ac=1;                               % Amplitud Portadora
modulante=Am*cos(2*pi*fm*t);        % Señal modulante
portadora=Ac*cos(2*pi*fc*t);        % Señal Portadora
modulada=modulante.*portadora;       % Señal Modulada DSB-SC

%% Osciloscopio
% A continuación se presenta el codigo Matlab que permite generar y
% utilizar un Osciloscopio digital para la visualización temporal de los 
% datos a transmitir por el dispositivo Adalm Pluto

scope = timescope('SampleRate', sampleRate);  %inicializacion del osciloscopio
scope.MeasurementChannel=1;   %numero de canales de medicion

scope(modulada);  %Vizualizar los datos medidos

%% Analizador de espectros
% A continuación se presenta el codigo Matlab que permite generar y
% utilizar un Analizador de Espectros digital para la visualización en
% frecuencia de los datos a transmitir por el dispositivo Adalm Pluto para
% Matlab versión 2022 en adelante

sa = dsp.SpectrumAnalyzer('SampleRate', sampleRate, 'SpectralAverages', 1); %Inicialización del AE
sa.PlotAsTwoSidedSpectrum=false; %Visualizar frecuencias negativas
sa.RBWSource = 'Property'; % Ancho de banda segun propiedad
sa.RBW =50;  % Ancho de banda 100 Hz
sa.FrequencySpan='start and stop frequencies'; %Configuración de frecuencia según Start y Stop
sa.StartFrequency=80e3 ; % frecuencia inicial de vizualización
sa.StopFrequency=120e3; % frecuencia final de viazualización

sa(modulada);  %vizualizar los datos recibidos

%% Transmisión de datos mediante dispositivo adalm-pluto
% A continuación se presenta el código de Matlab que permite realizar la
% transmisión de datos de manera repetida mediante el dispositivo adalm 
% pluto.

Transmitir_AM(tx,modulada);    %Función para la transmición de datos


