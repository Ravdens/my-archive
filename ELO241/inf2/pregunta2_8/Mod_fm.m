function vco= Mod_fm(data,t,sampleRate)
    %data: información a modular en frecuencia
    %t: vector de tiempo
    % sampleRate: tasa de muestreo
    deltaF=-50000;  %desfase de frecuencia
    kf = deltaF/2*pi;
    kf_mod=kf/sampleRate;
    vco= cos(2*pi*190000*t + kf_mod*cumsum(data));
end