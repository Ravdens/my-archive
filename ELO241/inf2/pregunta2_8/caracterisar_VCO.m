Fs = 10000;
t = 0:(1/Fs):1;
f = (-Fs/2:Fs/2);
DC = 0.5;
cap = 0.01*Fs;
m = DC + zeros(1,Fs+1);

xFM = Mod_fm(m,t,Fs);
XFM = abs(fftshift(fft(xFM)));

plt1 = plot(f,XFM);
title ("Salida VCO para DC = 0.5"); xlabel ("Frecuencia [Hz]"); ylabel ("Magnitud")

V = 0:0.01:2;
F = -2500.*V+5000;

plt2 = plot(V,F);
title ("Grafico fout vs Vin del VCO"); xlabel ("Voltaje DC[V]"); ylabel ("Frecuencia [Hz]")
set(plt2,'Color','red');