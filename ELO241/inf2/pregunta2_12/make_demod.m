sampleRate = 20000e3;
centerFreq = 2.4e9;
numMuestras=2000000; 

demod = Demod_fm(data1,sampleRate);


%% Osciloscopio
% A continuación se presenta el codigo Matlab que permite generar y
% utilizar un Osciloscopio digital para la visualización temporal de los 
% datos recibidos por el dispositivo Adalm Pluto

scope = timescope('SampleRate', sampleRate);  %inicializacion del osciloscopio
scope.MeasurementChannel=1;   %numero de canales de medicion

scope(demod);  %Vizualizar los datos medidos

%% Analizador de espectros
% A continuación se presenta el codigo Matlab que permite generar y
% utilizar un Analizador de Espectros digital para la visualización en
% frecuencia de los datos recibidos por el dispositivo Adalm Pluto para
% Matlab version 2021 hacia atrás

sa = dsp.SpectrumAnalyzer('SampleRate', sampleRate, 'SpectralAverages', 1); %Inicialización del AE
sa.PlotAsTwoSidedSpectrum=false; %Visualizar frecuencias negativas
sa.RBWSource = 'Property'; % Ancho de banda segun propiedad
sa.RBW =50;  % Ancho de banda 100 Hz
sa.FrequencySpan='start and stop frequencies'; %Configuración de frecuencia según Start y Stop
sa.StartFrequency=0 ; % frecuencia inicial de vizualización
sa.StopFrequency=20e3; % frecuencia final de viazualización

sa(demod);  %vizualizar los datos recibidos