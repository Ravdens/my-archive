function pll = Demod_fm(y,sampleRate)  
%y: datos modulados
%sampleRate: Tasa de muestreo de datos modulados
ini_phase = 0;
freqdev=10000;
Fs=sampleRate;
Fc=190000;
len = size(y,1);
if(len==1)
    y = y(:);
end
t = (0:1/Fs:((size(y,1)-1)/Fs))';
t = t(:,ones(1,size(y,2)));
yq = hilbert(y).*exp(-1i*2*pi*Fc*t);
pll =-1*((1/(2*pi*freqdev))*[zeros(1,size(yq,2)); diff(unwrap(angle(yq)))*Fs]);

if(len == 1)
    pll = pll';
end




   

