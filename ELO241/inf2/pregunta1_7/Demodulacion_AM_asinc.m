%% Demodulación en AM
%codigos asociados a la demodulación en amplitud, se presentan 3 formas de
%demodulacion, demodulación con detector de envolvente de matlab,
%demodulacion con un detector de envolvente diseñado a partir de la funcion
%valor absoluto y un filtro pasa bajos y finalmente un demodulador a traves
%de demodulación sincronica.

%% Demodulación con detector de envolvente (Función valor absoluto) y filtro
%data1: Señal modulada en amplitud
%demod: Señal demodulada

%demodulación
demod_abs=abs(data1);                      % Se aplica la funcion valor absoluto a la data recibida

%diseño del filtro
fc=100000;                                 % Frecuencia de corte del filtro pasa bajos
fs=sampleRate;                             % Frecuencia de muestreo de la señal recibida
[b,a] = butter(3,fc/(fs/2));               % Diseño del filtro butterworth de orden3

%filtrado de la señal
demod = filter(b,a,demod_abs);             % Se aplica el filtro pasa bajos a la señal luego del valor absoluto.



%% Osciloscopio
% A continuación se presenta el codigo Matlab que permite generar y
% utilizar un Osciloscopio digital para la visualización temporal de los 
% datos recibidos por el dispositivo Adalm Pluto

scope = timescope('SampleRate', sampleRate);  %inicializacion del osciloscopio
scope.MeasurementChannel=1;   %numero de canales de medicion

scope(demod);  %Vizualizar los datos medidos

%% Analizador de espectros
% A continuación se presenta el codigo Matlab que permite generar y
% utilizar un Analizador de Espectros digital para la visualización en
% frecuencia de los datos recibidos por el dispositivo Adalm Pluto para
% Matlab version 2021 hacia atrás

sa = dsp.SpectrumAnalyzer('SampleRate', sampleRate, 'SpectralAverages', 1); %Inicialización del AE
sa.PlotAsTwoSidedSpectrum=false; %Visualizar frecuencias negativas
sa.RBWSource = 'Property'; % Ancho de banda segun propiedad
sa.RBW =50;  % Ancho de banda 100 Hz
sa.FrequencySpan='start and stop frequencies'; %Configuración de frecuencia según Start y Stop
sa.StartFrequency=1e3 ; % frecuencia inicial de vizualización
sa.StopFrequency=25e3; % frecuencia final de viazualización

sa(demod);  %vizualizar los datos recibidos