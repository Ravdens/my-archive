sampleRate = 20000e3;
numMuestras=2000000; 

demod = Demod_fm2(data1,sampleRate);

Fcorte = 11e3;
[b,a] = butter(6,Fcorte/(sampleRate/2));
demod = filter(b,a,demod);


%demod = data1;

%% Osciloscopio
% A continuación se presenta el codigo Matlab que permite generar y
% utilizar un Osciloscopio digital para la visualización temporal de los 
% datos recibidos por el dispositivo Adalm Pluto

scope = timescope('SampleRate', sampleRate);  %inicializacion del osciloscopio
scope.MeasurementChannel=1;   %numero de canales de medicion

scope(demod);  %Vizualizar los datos medidos

%% Analizador de espectros
sa = dsp.SpectrumAnalyzer('SampleRate', sampleRate, 'SpectralAverages', 1); %Inicialización del AE
sa.PlotAsTwoSidedSpectrum=false; %Visualizar frecuencias negativas
sa.RBWSource = 'Property'; % Ancho de banda segun propiedad
sa.RBW =50;  % Ancho de banda 100 Hz
sa.FrequencySpan='start and stop frequencies'; %Configuración de frecuencia según Start y Stop
sa.StartFrequency=0e3 ; % frecuencia inicial de vizualización
sa.StopFrequency=200e3; % frecuencia final de viazualización

sa(demod);  %vizualizar los datos recibidos