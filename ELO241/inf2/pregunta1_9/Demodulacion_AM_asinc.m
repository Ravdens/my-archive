
load("P4_T.mat")

demod=envelope(data1);

%scope = timescope('SampleRate', sampleRate);  
%scope.MeasurementChannel=1; 
%scope(data1);  


sa = dsp.SpectrumAnalyzer('SampleRate', sampleRate, 'SpectralAverages', 1); 
sa.PlotAsTwoSidedSpectrum=false;
sa.RBWSource = 'Property'; 
sa.RBW =50;  
sa.FrequencySpan='start and stop frequencies'; 
sa.StartFrequency=0 ; 
sa.StopFrequency=2e3;

sa(demod);