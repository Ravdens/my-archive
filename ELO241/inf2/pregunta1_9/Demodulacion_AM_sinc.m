%% Demodulación en AM
%codigos asociados a la demodulación en amplitud, se presentan 3 formas de
%demodulacion, demodulación con detector de envolvente de matlab,
%demodulacion con un detector de envolvente diseñado a partir de la funcion
%valor absoluto y un filtro pasa bajos y finalmente un demodulador a traves
%de demodulación sincronica.

%% Demodulación con demodulación sincrónica
%data1: Señal modulada en amplitud
%demod: Señal demodulada

%Diseño del oscilador local, identico a la portadora de la mosulación
sampleRate = 20000e3;                      % Frecuencia de muestreo (igual a la fs de data1)
numMuestras=2000000;                       % Numero de muestras (igual al numero de muestras de data1)
t=[0:numMuestras-1]'/sampleRate;           % Vector temporal
fc=31e3;                                  % Frecuencia de la señal portadora en Hz
port=cos(2*pi*fc*t);                       % Generación de la portadora

%diseño del filtro
fc=100000;                                 % Frecuencia de corte del filtro pasa bajos
fs=sampleRate;                             % Frecuencia de muestreo de la señal recibida
[b,a] = butter(3,fc/(fs/2));               % Diseño del filtro butterworth de orden3

%Señal demodulada sin filtrar
demod_sinc=data1.*port;                    % Demodulación sincronica

%Filtrado de la señal
demod = filter(b,a,demod_sinc);            % Señal demodulada filtrada



%% Osciloscopio
% A continuación se presenta el codigo Matlab que permite generar y
% utilizar un Osciloscopio digital para la visualización temporal de los 
% datos recibidos por el dispositivo Adalm Pluto

scope = timescope('SampleRate', sampleRate);  %inicializacion del osciloscopio
scope.MeasurementChannel=1;   %numero de canales de medicion

scope(demod);  %Vizualizar los datos medidos

%% Analizador de espectros
% A continuación se presenta el codigo Matlab que permite generar y
% utilizar un Analizador de Espectros digital para la visualización en
% frecuencia de los datos recibidos por el dispositivo Adalm Pluto para
% Matlab version 2021 hacia atrás

sa = dsp.SpectrumAnalyzer('SampleRate', sampleRate, 'SpectralAverages', 1); %Inicialización del AE
sa.PlotAsTwoSidedSpectrum=false; %Visualizar frecuencias negativas
sa.RBWSource = 'Property'; % Ancho de banda segun propiedad
sa.RBW =50;  % Ancho de banda 100 Hz
sa.FrequencySpan='start and stop frequencies'; %Configuración de frecuencia según Start y Stop
sa.StartFrequency=0 ; % frecuencia inicial de vizualización
sa.StopFrequency=5e3; % frecuencia final de viazualización

sa(demod);  %vizualizar los datos recibidos