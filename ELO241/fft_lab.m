%% Senal 1: f = 100 Hz

tiempo = 0.1;
Fs = 1000;
t = 0:(1/Fs):tiempo;
f = ((-tiempo*Fs)/2:(tiempo*Fs)/2).*(1/tiempo);
A=1.5;
f0 = 100;
x1 = A*cos(2*pi*f0*t);

X1 =fftshift(fft(x1))./(tiempo*Fs);
P1 = 10*log10(abs(X1)./1e-3);

figure;
subplot (2,1,1);
plot (t,x1);
title ("x1(t)"); xlabel ("Tiempo (s)"); ylabel ("Amplitud[V]");

subplot (2,1,2);
plot (f,P1);
title ("X1(f)"); xlabel ("Frecuencia (Hz)"); ylabel ("Magnitud (dBm )");

%% Senal 2: f = 10000 Hz

tiempo = 0.001;
Fs = 100000;
t = 0:(1/Fs):tiempo;
f = ((-tiempo*Fs)/2:(tiempo*Fs)/2).*(1/tiempo);
A=1.5;
f0 = 10000;
x2 = A*cos(2*pi*f0*t);

X2 =fftshift(fft(x2))./(tiempo*Fs);
P2 = 10*log10(abs(X2)./1e-3);

figure;
subplot (2,1,1);
plot (t,x2);
title ("x2(t)"); xlabel ("Tiempo (s)"); ylabel ("Amplitud[V]");

subplot (2,1,2);
plot (f,P2);
title ("X2(f)"); xlabel ("Frecuencia (Hz)"); ylabel ("Magnitud (dBm )");

%% Senal 3: f = 700MHz

tiempo = 0.0000001;
Fs = 700e7;
t = 0:(1/Fs):tiempo;
f = ((-tiempo*Fs)/2:(tiempo*Fs)/2).*(1/tiempo);
A=1.5;
f0 = 700e6;
x3 = A*cos(2*pi*f0*t);

X3 =fftshift(fft(x3))./(tiempo*Fs);
P3 = 10*log10(abs(X3)./1e-3);

figure;
subplot (2,1,1);
plot (t,x3);
title ("x3(t)"); xlabel ("Tiempo (s)"); ylabel ("Amplitud[V]");

subplot (2,1,2);
plot (f,P3);
title ("X3(f)"); xlabel ("Frecuencia (Hz)"); ylabel ("Magnitud (dBm )");

