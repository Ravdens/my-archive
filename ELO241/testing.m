Fs = 1e5;
t = 0:1/Fs:1;
f = -Fs/2:Fs/2;


wc = 2*pi*10000;
wm = 2*pi*10;
c = cos(wc*t);
m = cos(wm*t);
m = (m>0);

x1 = c.*m;
x2 = c.*(m+1);


M = abs(fftshift(fft(m)))./(Fs);
X1 = abs(fftshift(fft(x1)))./(Fs);
X2 = abs(fftshift(fft(x2)))./(Fs);


d = envelope(x2);
D = abs(fftshift(fft(d)))./(Fs);


plot(f,D)
axis([-100 100 0 0.6])

