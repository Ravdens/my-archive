from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

#Creamos tablas
class Usuarios(db.Model):
    __tablename__='usuarios'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), nullable=False)

    compras = db.relationship('Compras',cascade="all, delete-orphan")

    @classmethod
    def create(cls, username):
        usuario = Usuarios(username=username)
        return usuario.save()
    
    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return self
        except:
            return False
    
    def update(self):
        self.save()

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()

            return self
        except:
            return False

    
    def json(self):
        return {
            'id': self.id,
            'username': self.username,
        }


class Compras(db.Model):
    __tablename__ = 'compras'
    id_compra = db.Column(db.Integer, primary_key = True)
    producto = db.Column(db.String(50), nullable = False)
    comprador = db.Column(db.Integer, db.ForeignKey("usuarios.id"))

    @classmethod
    def create(cls, producto, comprador):
        compra = Compras(producto=producto, comprador=comprador)
        return compra.save()
    
    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return self
        except:
            return False
    
    def update(self):
        self.save()

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()

            return self
        except:
            return False
    
    def json(self):
        return {
            'id_compra': self.id_compra,
            'producto': self.producto,
            'id_comprador': self.comprador
        }