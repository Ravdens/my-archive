from flask import Flask, jsonify, request
from config import config
from models import db, Usuarios


def create_app(enviroment):
	app = Flask(__name__)
	app.config['JSON_AS_ASCII'] = False
	app.config.from_object(enviroment)
	with app.app_context():
		db.init_app(app)
		db.create_all()
	return app


# Accedemos a la clase config del archivo config.py
enviroment = config['development']
app = create_app(enviroment)

#CRUD API SIMPLE
#===============================================================================#
@app.route('/api/usuarios', methods=['GET'])
def get_usuarios():
    usuarios = [user.json() for user in Usuarios.query.all()]
    response = jsonify(usuarios)
    return response
#===============================================================================#


#===============================================================================#
@app.route('/api/usuarios', methods=['POST'])
def put_usuario():
    json = request.get_json()
    usuario = Usuarios.create(json['username'])
    response = jsonify(usuario.json())
    return response
#===============================================================================#



#===============================================================================#
@app.route('/api/usuarios/<id>', methods=['PUT'])
def editar_usuario(id):
    json = request.get_json()
    usuario = Usuarios.query.filter_by(id=id).first()
    usuario.username = json['username']
    usuario.update()
    return jsonify(usuario.json())
#===============================================================================#



#===============================================================================#
@app.route('/api/usuarios/<id>', methods=['DELETE'])
def delete_usuario(id):
    usuario = Usuarios.query.filter_by(id=id).first()
    usuario.delete()
    return jsonify({'mensaje':'Usuario borrado'})
#===============================================================================#


if __name__ == '__main__':
	app.run(debug=True)



