from flask_sqlalchemy import SQLAlchemy

# Creamos la instancia de la base de datos
db = SQLAlchemy()

#Crear modelos ORM
#====================================================================================================#

#---- Tabla Administradores ----
class Administradores(db.Model):
    __tablename__ = 'administradores'
    id_admin = db.Column(db.Integer, primary_key=True)
    usuario = db.Column(db.String(45), nullable=False)
    nombre = db.Column(db.String(45), nullable=False)
    fecha_ingreso = db.Column(db.DateTime(), nullable=False)
    Usuarios = db.relationship("Usuarios") #relacion con tabla Usuarios

    @classmethod
    def create(cls, usuario, nombre, fecha_ingreso):
        admin = Administradores(usuario=usuario,nombre=nombre,fecha_ingreso=fecha_ingreso)
        return admin.save()

    def json(self):
        return {
            'id_admin': self.id_admin,
            'usuario': self.usuario,
            "nombre": self.nombre,
            "fecha_ingreso": self.fecha_ingreso
        }

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self
    def update(self):
        self.save()
    def delete(self):
        db.session.delete(self)
        db.session.commit()
        return self

#---- Tabla Usuario_sigue_a_usuario ----
class Usuario_sigue_a_usuario(db.Model):
    __tablename__ = 'usuario_sigue_a_usuario'
    id_usuario = db.Column(db.Integer, db.ForeignKey('usuarios.id_usuario'), primary_key=True, autoincrement=False) #NOTA: Este ID no debe generarse automaticamente
    sigue_a_id_usuario = db.Column(db.Integer, db.ForeignKey('usuarios.id_usuario'), primary_key=True, autoincrement=False) #NOTA: Este ID no debe generarse automaticamente

    usuario_1 = db.relationship("Usuarios",primaryjoin="Usuario_sigue_a_usuario.id_usuario == Usuarios.id_usuario")
    usuario_2 = db.relationship("Usuarios",primaryjoin="Usuario_sigue_a_usuario.sigue_a_id_usuario == Usuarios.id_usuario")

    @classmethod
    def create(cls,id_usuario,sigue_a_id_usuario):
        follow = Usuario_sigue_a_usuario(id_usuario=id_usuario,sigue_a_id_usuario=sigue_a_id_usuario)
        return follow.save()

    def json(self):
        return {
            "id_usuario": self.id_usuario,
            "sigue_a_id_usuario": self.sigue_a_id_usuario
        }

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self
    def update(self):
        self.save()
    def delete(self):
        db.session.delete(self)
        db.session.commit()
        return self

#---- Tabla Usuarios ----
class Usuarios(db.Model):
    __tablename__ = 'usuarios'
    id_usuario = db.Column(db.Integer, primary_key=True)
    usuario = db.Column(db.String(45), nullable=False)
    password = db.Column(db.String(45), nullable=False)
    pagina_web = db.Column(db.String(60))
    url_foto = db.Column(db.String(60))
    verificado = db.Column(db.Boolean, nullable=False)
    verificado_por = db.Column(db.Integer, db.ForeignKey('administradores.id_admin')) #FK Admin
    Post = db.relationship("Post") #relacion con tabla Post
    #TODO: ESTA RELACIÓN NECESITA REVISIÓN Y ACTUALMENTE ESTÁ DANDO PROBLEMAS
    Usuario_sigue_a_usuario_s = db.relationship(Usuario_sigue_a_usuario, primaryjoin=db.or_(id_usuario==Usuario_sigue_a_usuario.id_usuario,id_usuario==Usuario_sigue_a_usuario.sigue_a_id_usuario),viewonly=True)


    @classmethod
    def create(cls, usuario, password, pagina_web, url_foto, verificado, verificado_por):
        user = Usuarios(usuario=usuario,password=password,pagina_web=pagina_web,url_foto=url_foto,verificado=verificado,verificado_por=verificado_por)
        return user.save()

    def json(self):
        return {
            'id_usuario': self.id_usuario,
            'usuario': self.usuario,
            "password": self.password,
            "pagina_web": self.pagina_web,
            "url_foto": self.url_foto,
            "verificado": self.verificado,
            "verificado_por": self.verificado_por
        }

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self
    def update(self):
        self.save()
    def delete(self):
        db.session.delete(self)
        db.session.commit()
        return self



#---- Tabla Post ----
class Post(db.Model):
    __tablename__ = 'post'
    id_post = db.Column(db.Integer, primary_key=True)
    mensaje = db.Column(db.String(120), nullable=False)
    fecha_envio = db.Column(db.DateTime(), nullable=False)
    id_usuario_emisor = db.Column(db.Integer, db.ForeignKey('usuarios.id_usuario'))
    cant_likes = db.Column(db.Integer, nullable=False)
    cant_repost = db.Column(db.Integer, nullable=False)
    Post_tiene_hashtag = db.relationship("Post_tiene_hashtag")

    @classmethod
    def create(cls, mensaje, fecha_envio, id_usuario_emisor, cant_likes, cant_repost):
        post = Post(mensaje=mensaje,fecha_envio=fecha_envio,id_usuario_emisor=id_usuario_emisor,cant_likes=cant_likes,cant_repost=cant_repost)
        return post.save()

    def json(self):
        return {
            "id_post": self.id_post,
            "mensaje": self.mensaje,
            "fecha_envio": self.fecha_envio,
            "id_usuario_emisor": self.id_usuario_emisor,
            "cant_likes": self.cant_likes,
            "cant_repost": self.cant_repost,
        }

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self
    def update(self):
        self.save()
    def delete(self):
        db.session.delete(self)
        db.session.commit()
        return self



#---- Tabla Hashtags ----
class Hashtags(db.Model):
    __tablename__ = 'hashtags'
    id_hashtag = db.Column(db.Integer, primary_key=True)
    texto_hashtag = db.Column(db.String(30), nullable=False)
    Post_tiene_hashtag = db.relationship("Post_tiene_hashtag")

    @classmethod
    def create(cls, texto_hashtag):
        hashtag = Hashtags(texto_hashtag=texto_hashtag)
        return hashtag.save()

    def json(self):
        return {
            "id_hashtag": self.id_hashtag,
            "texto_hashtag": self.texto_hashtag
        }

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self
    def update(self):
        self.save()
    def delete(self):
        db.session.delete(self)
        db.session.commit()
        return self


#Entidades o tablas con llaves compuestas:

#---- Tabla Post_tiene_hashtag ----
class Post_tiene_hashtag(db.Model):
    __tablename__ = 'post_tiene_hashtag'
    id_post = db.Column(db.Integer, db.ForeignKey('post.id_post'), primary_key=True, autoincrement=False) #NOTA: Este ID no debe generarse automaticamente
    id_hashtag = db.Column(db.Integer, db.ForeignKey('hashtags.id_hashtag'), primary_key=True, autoincrement=False) #NOTA: Este ID no debe generarse automaticamente
    
    @classmethod
    def create(cls, id_post,id_hashtag):
        rel = Post_tiene_hashtag(id_post=id_post,id_hashtag=id_hashtag)
        return rel.save()

    def json(self):
        return {
            "id_post": self.id_post,
            "id_hashtag": self.id_hashtag
        }

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self
    def update(self):
        self.save()
    def delete(self):
        db.session.delete(self)
        db.session.commit()
        return self

#====================================================================================================#
print("models.py bien\n")
