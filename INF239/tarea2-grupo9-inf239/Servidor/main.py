from flask import Flask, jsonify, request
from config import config
from models import db, Administradores, Usuarios, Post, Hashtags, Post_tiene_hashtag, Usuario_sigue_a_usuario 

# Creamos la función de creación de la aplicación
def create_app(enviroment):
	app = Flask(__name__)
	app.config['JSON_AS_ASCII'] = False
	app.config.from_object(enviroment)
	with app.app_context():
		db.init_app(app)
		db.create_all()
	return app


# Accedemos a la clase config del archivo config.py
enviroment = config['development']
app = create_app(enviroment)


# Rutas API y metodos
#====================================================================================================#

#---- Metodos tabla Administradores ----
@app.route('/api/Administradores', methods=["GET"])
def get_all_administradores():
	L=[]
	for admin in Administradores.query.all():
		L.append(admin.json())
	return jsonify(L)

@app.route('/api/Administradores/<id_admin>')
def get_administrador(id_admin):
	resultado = Administradores.query.filter_by(id_admin=id_admin).first()
	print(resultado.json())
	return jsonify(resultado.json())

@app.route('/api/Administradores', methods=["POST"])
def put_administrador():
	json = request.get_json()
	admin = Administradores.create(json['usuario'],json['nombre'],json['fecha_ingreso'])
	return jsonify(admin.json())

@app.route('/api/Administradores/<id_admin>',methods=["PUT"])
def edit_adminstrador(id_admin):
	json = request.get_json()
	admin = Administradores.query.filter_by(id_admin=id_admin).first()
	admin.usuario= json["usuario"]
	admin.nombre= json["nombre"]
	admin.fecha_ingreso= json["fecha_ingreso"]
	admin.update()
	return jsonify(admin.json())	

@app.route('/api/Administradores/<id_admin>',methods=["DELETE"]) 
#Administradores es entidad fuerte y solo se asocia a usuarios que tambien es entidad fuerte
def delete_administrador(id_admin):
	admin = Administradores.query.filter_by(id_admin=id_admin).first()
	admin.delete()
	return jsonify({"mensaje":"Administrador borrado exitosamente"})

#-----------------------------------


#---- Metodos tabla Usuarios ----
@app.route('/api/Usuarios', methods=["GET"])
def get_all_usuarios():
	L=[]
	for user in Usuarios.query.all():
		L.append(user.json())
	return jsonify(L)

@app.route('/api/Usuarios/<id_usuario>',methods=["GET"])
def get_usuario(id_usuario):
	resultado = Usuarios.query.filter_by(id_usuario=id_usuario).first()
	try:
		return jsonify(resultado.json())
	except:
		return "null"

@app.route('/api/Usuarios', methods=["POST"])
def put_usuario():
	json = request.get_json()
	print(json)
	user = Usuarios.create(json['usuario'],json['password'],json['pagina_web'],json['url_foto'],json['verificado'],json['verificado_por'])
	return jsonify(user.json())
	
@app.route('/api/Usuarios/<id_usuario>',methods=["PUT"])
def edit_usuario(id_usuario):
	json = request.get_json()
	user = Usuarios.query.filter_by(id_usuario=id_usuario).first()
	user.usuario= json["usuario"]
	user.password= json["password"]
	user.pagina_web= json["pagina_web"]
	user.url_foto= json["url_foto"]
	user.verificado= json["verificado"]
	user.verificado_por= json["verificado_por"]
	user.update()
	return jsonify(user.json())	

@app.route('/api/Usuarios/<id_usuario>',methods=["DELETE"]) 
def delete_usuario(id_usuario):
	user = Usuarios.query.filter_by(id_usuario=id_usuario).first()
	user.delete()

	#Usuario es una entidad fuerte pero está unida a usuario_sigue_a_usuario, que es entidad debil. Este metodo puede necesitar mayor desarrollo.

	return jsonify({"mensaje":"Usuario borrado exitosamente"})

#-----------------------------------


#---- Metodos tabla Post ----
@app.route('/api/Post', methods=["GET"])
def get_all_post():
	L=[]
	for post in Post.query.all():
		L.append(post.json())
	return jsonify(L)

@app.route('/api/Post/<id_post>',methods=["GET"])
def get_post(id_post):
	resultado = Post.query.filter_by(id_post=id_post).first()
	return jsonify(resultado.json())

@app.route('/api/Post', methods=["POST"])
def put_post():
	json = request.get_json()
	post = Post.create(json['mensaje'],json['fecha_envio'],json['id_usuario_emisor'],json['cant_likes'],json['cant_repost'])
	return jsonify(post.json())

@app.route('/api/Post/<id_post>',methods=["PUT"])
def edit_post(id_post):
	json = request.get_json()
	post = Post.query.filter_by(id_post=id_post).first()
	post.mensaje= json["mensaje"]
	post.fecha_envio= json["fecha_envio"]
	post.id_usuario_emisor= json["id_usuario_emisor"]
	post.cant_likes= json["cant_likes"]
	post.cant_repost= json["cant_repost"]
	post.update()
	return jsonify(post.json())	

@app.route('/api/Post/<id_post>',methods=["DELETE"]) 
def delete_post(id_post):
	post = Post.query.filter_by(id_post=id_post).first()
	post.delete()

	#Post es una entidad fuerte pero está ligada a post_tiene_hashtag. Este metodo puede necesitar mayor desarrollo.

	return jsonify({"mensaje":"Post borrado exitosamente"})

#-----------------------------------


#---- Metodos tabla Hashtags ----
@app.route('/api/Hashtags', methods=["GET"])
def get_all_hashtags():
	L=[]
	for hashtag in Hashtags.query.all():
		L.append(hashtag.json())
	return jsonify(L)

@app.route('/api/Hashtags/<id_hashtag>',methods=["GET"])
def get_hashtag(id_hashtag):
	resultado = Hashtags.query.filter_by(id_hashtag=id_hashtag).first()
	return jsonify(resultado.json())

@app.route('/api/Hashtags', methods=["POST"])
def put_hashtag():
	json = request.get_json()
	hashtag = Hashtags.create(json['texto_hashtag'])
	return jsonify(hashtag.json())

@app.route('/api/Hashtags/<id_hashtag>',methods=["PUT"])
def edit_hashtag(id_hashtag):
	json = request.get_json()
	hashtag = Hashtags.query.filter_by(id_hashtag=id_hashtag).first()
	hashtag.texto_hashtag= json["texto_hashtag"]
	hashtag.update()
	return jsonify(hashtag.json())	

@app.route('/api/Hashtags/<id_hashtag>',methods=["DELETE"]) 
def delete_hashtag(id_hashtag):
	hashtag = Hashtags.query.filter_by(id_hashtag=id_hashtag).first()
	hashtag.delete()

	#Hashtag es una entidad fuerte pero está ligada a post_tiene_hashtag. Este metodo puede necesitar mayor desarrollo.

	return jsonify({"mensaje":"Post borrado exitosamente"})

#-----------------------------------


#---- Metodos tabla Usuario_sigue_a_usuario ----
@app.route('/api/Usuario_sigue_a_usuario', methods=["GET"])
def get_all_follows():
	L=[]
	for follow in Usuario_sigue_a_usuario.query.all():
		L.append(follow.json())
	return jsonify(L)

@app.route('/api/Usuario_sigue_a_usuario/<id_usuario>,<sigue_a_id_usuario>',methods=["GET"])
def get_follow(id_usuario,sigue_a_id_usuario):
	resultado = Usuario_sigue_a_usuario.query.filter_by(id_usuario=id_usuario,sigue_a_id_usuario=sigue_a_id_usuario).first()
	return jsonify(resultado.json())

@app.route('/api/Usuario_sigue_a_usuario', methods=["POST"])
def put_follow():
	json = request.get_json()
	follow = Usuario_sigue_a_usuario.create(json["id_usuario"],json["sigue_a_id_usuario"])
	return jsonify(follow.json())

@app.route('/api/Usuario_sigue_a_usuario/<id_usuario>,<sigue_a_id_usuario>',methods=["DELETE"]) 
def delete_follow(id_usuario,sigue_a_id_usuario):
	follow = Usuario_sigue_a_usuario.query.filter_by(id_usuario=id_usuario,sigue_a_id_usuario=sigue_a_id_usuario).first()
	follow.delete()
	return jsonify({"mensaje":"Enlace borrado exitosamente"})

#-----------------------------------


#---- Metodos tabla Post_tiene_hashtag ----
@app.route('/api/Post_tiene_hashtag', methods=["GET"])
def get_all_hashtag_link():
	L=[]
	for linked_hastag in Post_tiene_hashtag.query.all():
		L.append(linked_hastag.json())
	return jsonify(L)

@app.route('/api/Post_tiene_hashtag/<id_post>,<id_hashtag>',methods=["GET"])
def get_hashtag_link(id_post,id_hashtag):
	resultado = Post_tiene_hashtag.query.filter_by(id_hashtag=id_hashtag,id_post=id_post).first()
	return jsonify(resultado.json())

@app.route('/api/Post_tiene_hashtag', methods=["POST"])
def put_hashtag_link():
	json = request.get_json()
	link = Post_tiene_hashtag.create(json["id_post"],json["id_hashtag"])
	return jsonify(link.json())

@app.route('/api/Post_tiene_hashtag/<id_post>,<id_hashtag>',methods=["DELETE"]) 
def delete_hashtag_link(id_post,id_hashtag):
	link = Post_tiene_hashtag.query.filter_by(id_post=id_post,id_hashtag=id_hashtag).first()
	link.delete()
	return jsonify({"mensaje":"Enlace borrado exitosamente"})

#-----------------------------------

#Ejecutamos el servidor
if __name__ == '__main__':
	print("main.py bien\n")
	app.run(debug=True)
