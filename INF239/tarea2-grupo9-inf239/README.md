# GRUPO 9 - BASES DE DATOS - INF239 

## Integrantes:
* Sergio Ehlen - 202130016-3
* Camilo Troncoso - 202130004-K
* Máximo Flores - 202130019-8	

Link reposotorio tarea 2:
https://gitlab.inf.utfsm.cl/bases-de-datos/grupo-9/tarea2-grupo9-inf239
Link documentación de postman:
https://documenter.getpostman.com/view/23742751/2s8YeoNsp2

Para correr:

* Correr: pip install flask-sqlalchemy
* Correr: pip install requests
* Cambiar el SQLALCHEMY_DATABASE_URI en config.py a los datos de la base de datos propia
* Correr: tarea2-grupo9-inf239\Servidor> python main.py
