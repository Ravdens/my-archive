PGDMP                     
    z           tarea-2    14.5    14.5 ,                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            !           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            "           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            #           1262    17983    tarea-2    DATABASE     e   CREATE DATABASE "tarea-2" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Spanish_Spain.1252';
    DROP DATABASE "tarea-2";
                postgres    false            �            1259    17985    administradores    TABLE     �   CREATE TABLE public.administradores (
    id_admin integer NOT NULL,
    usuario character varying(45) NOT NULL,
    nombre character varying(45) NOT NULL,
    fecha_ingreso timestamp without time zone NOT NULL
);
 #   DROP TABLE public.administradores;
       public         heap    postgres    false            �            1259    17984    administradores_id_admin_seq    SEQUENCE     �   CREATE SEQUENCE public.administradores_id_admin_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.administradores_id_admin_seq;
       public          postgres    false    210            $           0    0    administradores_id_admin_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public.administradores_id_admin_seq OWNED BY public.administradores.id_admin;
          public          postgres    false    209            �            1259    17992    hashtags    TABLE     t   CREATE TABLE public.hashtags (
    id_hashtag integer NOT NULL,
    texto_hashtag character varying(30) NOT NULL
);
    DROP TABLE public.hashtags;
       public         heap    postgres    false            �            1259    17991    hashtags_id_hashtag_seq    SEQUENCE     �   CREATE SEQUENCE public.hashtags_id_hashtag_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.hashtags_id_hashtag_seq;
       public          postgres    false    212            %           0    0    hashtags_id_hashtag_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.hashtags_id_hashtag_seq OWNED BY public.hashtags.id_hashtag;
          public          postgres    false    211            �            1259    18026    post    TABLE        CREATE TABLE public.post (
    id_post integer NOT NULL,
    mensaje character varying(120) NOT NULL,
    fecha_envio timestamp without time zone NOT NULL,
    id_usuario_emisor integer,
    cant_likes integer NOT NULL,
    cant_repost integer NOT NULL
);
    DROP TABLE public.post;
       public         heap    postgres    false            �            1259    18025    post_id_post_seq    SEQUENCE     �   CREATE SEQUENCE public.post_id_post_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.post_id_post_seq;
       public          postgres    false    217            &           0    0    post_id_post_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.post_id_post_seq OWNED BY public.post.id_post;
          public          postgres    false    216            �            1259    18037    post_tiene_hashtag    TABLE     j   CREATE TABLE public.post_tiene_hashtag (
    id_post integer NOT NULL,
    id_hashtag integer NOT NULL
);
 &   DROP TABLE public.post_tiene_hashtag;
       public         heap    postgres    false            �            1259    18010    usuario_sigue_a_usuario    TABLE     z   CREATE TABLE public.usuario_sigue_a_usuario (
    id_usuario integer NOT NULL,
    sigue_a_id_usuario integer NOT NULL
);
 +   DROP TABLE public.usuario_sigue_a_usuario;
       public         heap    postgres    false            �            1259    17999    usuarios    TABLE     "  CREATE TABLE public.usuarios (
    id_usuario integer NOT NULL,
    usuario character varying(45) NOT NULL,
    password character varying(45) NOT NULL,
    pagina_web character varying(60),
    url_foto character varying(60),
    verificado boolean NOT NULL,
    verificado_por integer
);
    DROP TABLE public.usuarios;
       public         heap    postgres    false            �            1259    17998    usuarios_id_usuario_seq    SEQUENCE     �   CREATE SEQUENCE public.usuarios_id_usuario_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.usuarios_id_usuario_seq;
       public          postgres    false    214            '           0    0    usuarios_id_usuario_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.usuarios_id_usuario_seq OWNED BY public.usuarios.id_usuario;
          public          postgres    false    213            s           2604    17988    administradores id_admin    DEFAULT     �   ALTER TABLE ONLY public.administradores ALTER COLUMN id_admin SET DEFAULT nextval('public.administradores_id_admin_seq'::regclass);
 G   ALTER TABLE public.administradores ALTER COLUMN id_admin DROP DEFAULT;
       public          postgres    false    209    210    210            t           2604    17995    hashtags id_hashtag    DEFAULT     z   ALTER TABLE ONLY public.hashtags ALTER COLUMN id_hashtag SET DEFAULT nextval('public.hashtags_id_hashtag_seq'::regclass);
 B   ALTER TABLE public.hashtags ALTER COLUMN id_hashtag DROP DEFAULT;
       public          postgres    false    211    212    212            v           2604    18029    post id_post    DEFAULT     l   ALTER TABLE ONLY public.post ALTER COLUMN id_post SET DEFAULT nextval('public.post_id_post_seq'::regclass);
 ;   ALTER TABLE public.post ALTER COLUMN id_post DROP DEFAULT;
       public          postgres    false    217    216    217            u           2604    18002    usuarios id_usuario    DEFAULT     z   ALTER TABLE ONLY public.usuarios ALTER COLUMN id_usuario SET DEFAULT nextval('public.usuarios_id_usuario_seq'::regclass);
 B   ALTER TABLE public.usuarios ALTER COLUMN id_usuario DROP DEFAULT;
       public          postgres    false    213    214    214                      0    17985    administradores 
   TABLE DATA           S   COPY public.administradores (id_admin, usuario, nombre, fecha_ingreso) FROM stdin;
    public          postgres    false    210   �5                 0    17992    hashtags 
   TABLE DATA           =   COPY public.hashtags (id_hashtag, texto_hashtag) FROM stdin;
    public          postgres    false    212   ,6                 0    18026    post 
   TABLE DATA           i   COPY public.post (id_post, mensaje, fecha_envio, id_usuario_emisor, cant_likes, cant_repost) FROM stdin;
    public          postgres    false    217   t6                 0    18037    post_tiene_hashtag 
   TABLE DATA           A   COPY public.post_tiene_hashtag (id_post, id_hashtag) FROM stdin;
    public          postgres    false    218   �6                 0    18010    usuario_sigue_a_usuario 
   TABLE DATA           Q   COPY public.usuario_sigue_a_usuario (id_usuario, sigue_a_id_usuario) FROM stdin;
    public          postgres    false    215   %7                 0    17999    usuarios 
   TABLE DATA           s   COPY public.usuarios (id_usuario, usuario, password, pagina_web, url_foto, verificado, verificado_por) FROM stdin;
    public          postgres    false    214   L7       (           0    0    administradores_id_admin_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.administradores_id_admin_seq', 6, true);
          public          postgres    false    209            )           0    0    hashtags_id_hashtag_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.hashtags_id_hashtag_seq', 5, true);
          public          postgres    false    211            *           0    0    post_id_post_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.post_id_post_seq', 6, true);
          public          postgres    false    216            +           0    0    usuarios_id_usuario_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.usuarios_id_usuario_seq', 12, true);
          public          postgres    false    213            x           2606    17990 $   administradores administradores_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.administradores
    ADD CONSTRAINT administradores_pkey PRIMARY KEY (id_admin);
 N   ALTER TABLE ONLY public.administradores DROP CONSTRAINT administradores_pkey;
       public            postgres    false    210            z           2606    17997    hashtags hashtags_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.hashtags
    ADD CONSTRAINT hashtags_pkey PRIMARY KEY (id_hashtag);
 @   ALTER TABLE ONLY public.hashtags DROP CONSTRAINT hashtags_pkey;
       public            postgres    false    212            �           2606    18031    post post_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.post
    ADD CONSTRAINT post_pkey PRIMARY KEY (id_post);
 8   ALTER TABLE ONLY public.post DROP CONSTRAINT post_pkey;
       public            postgres    false    217            �           2606    18041 *   post_tiene_hashtag post_tiene_hashtag_pkey 
   CONSTRAINT     y   ALTER TABLE ONLY public.post_tiene_hashtag
    ADD CONSTRAINT post_tiene_hashtag_pkey PRIMARY KEY (id_post, id_hashtag);
 T   ALTER TABLE ONLY public.post_tiene_hashtag DROP CONSTRAINT post_tiene_hashtag_pkey;
       public            postgres    false    218    218            ~           2606    18014 4   usuario_sigue_a_usuario usuario_sigue_a_usuario_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY public.usuario_sigue_a_usuario
    ADD CONSTRAINT usuario_sigue_a_usuario_pkey PRIMARY KEY (id_usuario, sigue_a_id_usuario);
 ^   ALTER TABLE ONLY public.usuario_sigue_a_usuario DROP CONSTRAINT usuario_sigue_a_usuario_pkey;
       public            postgres    false    215    215            |           2606    18004    usuarios usuarios_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (id_usuario);
 @   ALTER TABLE ONLY public.usuarios DROP CONSTRAINT usuarios_pkey;
       public            postgres    false    214            �           2606    18032     post post_id_usuario_emisor_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.post
    ADD CONSTRAINT post_id_usuario_emisor_fkey FOREIGN KEY (id_usuario_emisor) REFERENCES public.usuarios(id_usuario);
 J   ALTER TABLE ONLY public.post DROP CONSTRAINT post_id_usuario_emisor_fkey;
       public          postgres    false    214    217    3196            �           2606    18047 5   post_tiene_hashtag post_tiene_hashtag_id_hashtag_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.post_tiene_hashtag
    ADD CONSTRAINT post_tiene_hashtag_id_hashtag_fkey FOREIGN KEY (id_hashtag) REFERENCES public.hashtags(id_hashtag);
 _   ALTER TABLE ONLY public.post_tiene_hashtag DROP CONSTRAINT post_tiene_hashtag_id_hashtag_fkey;
       public          postgres    false    218    3194    212            �           2606    18042 2   post_tiene_hashtag post_tiene_hashtag_id_post_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.post_tiene_hashtag
    ADD CONSTRAINT post_tiene_hashtag_id_post_fkey FOREIGN KEY (id_post) REFERENCES public.post(id_post);
 \   ALTER TABLE ONLY public.post_tiene_hashtag DROP CONSTRAINT post_tiene_hashtag_id_post_fkey;
       public          postgres    false    218    3200    217            �           2606    18015 ?   usuario_sigue_a_usuario usuario_sigue_a_usuario_id_usuario_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_sigue_a_usuario
    ADD CONSTRAINT usuario_sigue_a_usuario_id_usuario_fkey FOREIGN KEY (id_usuario) REFERENCES public.usuarios(id_usuario);
 i   ALTER TABLE ONLY public.usuario_sigue_a_usuario DROP CONSTRAINT usuario_sigue_a_usuario_id_usuario_fkey;
       public          postgres    false    215    3196    214            �           2606    18020 G   usuario_sigue_a_usuario usuario_sigue_a_usuario_sigue_a_id_usuario_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_sigue_a_usuario
    ADD CONSTRAINT usuario_sigue_a_usuario_sigue_a_id_usuario_fkey FOREIGN KEY (sigue_a_id_usuario) REFERENCES public.usuarios(id_usuario);
 q   ALTER TABLE ONLY public.usuario_sigue_a_usuario DROP CONSTRAINT usuario_sigue_a_usuario_sigue_a_id_usuario_fkey;
       public          postgres    false    215    3196    214            �           2606    18005 %   usuarios usuarios_verificado_por_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_verificado_por_fkey FOREIGN KEY (verificado_por) REFERENCES public.administradores(id_admin);
 O   ALTER TABLE ONLY public.usuarios DROP CONSTRAINT usuarios_verificado_por_fkey;
       public          postgres    false    214    3192    210               3   x�3�t�O���KH̫*��HM)��4200�50�54Q00�#�=... Mw�         8   x�3�T��O��)I-�2�TN�2�TNI-H,*I�M�+�OI�M,����b�=... ���         v   x�E�;�@ ��>�/@d;-JGII�$V��J�q|>MF����%�2=gO�;y�4z�)�-Vh|X�r�oe՝�1��� �_���[w�V(�j^�j����yah�� ��~$�            x�3�4�2�4�2�4b�=... �            x���4�2���=... �.         1  x�=��n�0Eד�c��;j��"D�H�!�	$v�-��5 E���̹�[�������+�&��m�%;'�Ʒ8V��w�6�T�7ʎ��b�o�+ʁ��8�>0��o&�Ehy�p3���y���K�J�НC=Mҧ����{;�c�hIKF)/˄hβ"%�$��<!%��G��r��ߺ�������q�T1���|)8z_�eL0�3��J������`m��چ���M3����:��
w��޵/�J�Ƣ�A�#�XM~4�� '�g��E�³��EZ���]_R�rB�af�=������     