# Archivo para el codigo del cliente, menu, consumir API, etc.
#importar librerias a trabajar:
import requests as re
import random
host = "http://127.0.0.1:5000/api/"
#codigo ayuda para consultas:
'''

print(host+"Usuarios") #reemplazar en "xxxxx" nombre de registro
response = re.get(host+"Usuarios")
print(response.json())
'''

#rellenar consultas donde dice 'code'
#------- interfaz -------

while True:
    print("=====Bienvenido a Sandbird=====\n")
    print("Por favor indique si es usuario o administrador:\n")
    print("1. Usuario\n2. Administrador\n")
    n = int(input(">>>>>> "))

    #Seccion usuario
    if(n==1):
        #-------------------------------------------
        id = int(input("Ingrese su id de Usuario:"))
        usuario = re.get(host+"Usuarios/"+str(id)).json()

        if( usuario is not None):
            password = input("Ingrese su contraseña:")
            if(password==usuario["password"]):
                print("\nIngresaste como Usuario\n")

                #Interfaz usuario
                while True:
                    print("Selecciona lo que deseas Hacer:\n")
                    print("1. Ver un Post\n2. Crear un Post\n3. Editar un Post\n4. Crear un Hashtag\n5. Seguir a un usuario\n6. Dejar de seguir a un usuario\n7. Cerrar sesión\n")
                    op = int(input(">>>>>> "))
                    if(op==1):
                        id_post = int(input("Ingrese el id del post a ver: "))
                        post = re.get(host+"Post/"+str(id_post)).json()
                        if post is not None:
                            print("Mensaje: "+post["mensaje"])
                            print("Fecha de envio: " +post["fecha_envio"])
                            print("Emitido por id: "+str(post["id_usuario_emisor"]))
                            print("Cantidad de likes: "+str(post["cant_likes"]))
                            print("Cantidad de Re-Post: "+str(post["cant_repost"]))
                        else:
                            print("Error, no existe un post con esa ID")

                    if(op==2):
                        msj = str(input("Ingrese el mensaje del post: "))
                        lista_id_hashtags = str(input("Ingrese los id de hashtags separados por -: "))
                        correcto = 1
                        if lista_id_hashtags != "":
                            #descomponer y crear lista con hashtags a partir del string a trabajar
                            hashtags = lista_id_hashtags.split("-")
                            for i in range(len(hashtags)):
                                hashtag = re.get(host+"Hashtags/"+hashtags[i]).json()
                                if hashtag is None:
                                    print("Error, el Hash de ID "+hashtags[i]+" no existe, cree el hashtag o vuelva a crear el post sin el hashtag inexistente")
                                    correcto = 0
                                    break
                        #code
                        if correcto == 1:
                            tmp = re.post(host+"Post", json={
                                    "mensaje": msj,
                                    "fecha_envio": "10/11/2020",
                                    "id_usuario_emisor": id,
                                    "cant_likes": random.randint(0, 1000),
                                    "cant_repost": random.randint(0, 1000)
                                }).json()
                            posts = re.get(host+"Post").json()
                            for j in posts:
                                if (j["id_post"]== tmp["id_post"]):
                                    if lista_id_hashtags != "":
                                        for i in range(len(hashtags)):
                                            re.post(host+"Post_tiene_hashtag", json={
                                                "id_post": j["id_post"],
                                                "id_hashtag": hashtags[i]
                                            })
                                    print("Post enviado!\n")
                    if(op==3):
                        id_post = int(input("Ingrese el id del post a editar: "))
                        post = re.get(host+"Post/"+str(id_post)).json()
                        if(post["id_usuario_emisor"]==id):
                            new_msj = str(input("Ingrese el nuevo mensaje: "))
                            re.put(host+"Post/"+str(id), 
                            json={"mensaje": new_msj,
                            "fecha_envio": post["fecha_envio"],
                            "id_usuario_emisor": post["id_usuario_emisor"],
                            "cant_likes": post["cant_likes"],
                            "cant_repost": post["cant_repost"]})

                            print("Post Editado Correctamente!\n")
                        else:
                            print("Lo sentimos, no puedes editar un post de otra persona!\n")
                    if(op==4):
                        hashtags = re.get(host+"Hashtags").json()
                        new_hashtag = str(input("Ingrese el Texto del Hashtag: "))
                        l=[]
                        for dicc in hashtags:
                            l.append(dicc["texto_hashtag"])
                        if(new_hashtag not in l):
                            re.post(host+"Hashtags", json={
                                "texto_hashtag": new_hashtag
                                })
                            print("Hashtag creado correctamente!\n")
                        else:
                            print("Este hashtag ya existe!")
                    if(op==5):
                        id_usuario = int(input("Ingrese la ID del usuario a seguir: "))
                        f_usuario = re.get(host+"Usuarios/"+str(id_usuario)).json()
                        if f_usuario is not None:
                            re.post(host+"Usuario_sigue_a_usuario",json={"id_usuario":str(usuario["id_usuario"]),"sigue_a_id_usuario":str(f_usuario["id_usuario"])})
                            print("Ahora sigues al usuario "+str(id_usuario)+"!\n")
                        else:
                            print("Error!\n")

                    if(op==6):
                        id_usuario = int(input("Ingrese la ID del usuario a dejar de seguir: "))
                        follow = re.get(host+"Usuario_sigue_a_usuario/"+str(usuario["id_usuario"])+","+str(id_usuario))
                        if follow is not None:
                            re.delete(host+"Usuario_sigue_a_usuario/"+str(usuario["id_usuario"])+","+str(id_usuario))
                            print("Dejaste de seguir al usuario "+str(id_usuario)+"!\n")
                        else:
                            print("Error!\n")
                        
                    if(op==7):
                        print("Sesión Cerrada Correctamente!\n")
                        break
            
            
            else:
                print("Error!\n")
        else:
            print("Error!\n")


    #Sección administrador
    if(n==2):
        id = int(input("Ingrese su id de Administrador:"))
        admin = re.get(host+"Administradores/"+str(id)).json()
        if(admin is not None):

            password = input("Ingrese su contraseña:")
            print("\n")
            if(password=="1234"):

                while True:
                    print("Ingresaste como Administrador\n")
                    print("Selecciona lo que deseas Hacer\n")
                    print("1. Verificar a un Usuario\n2. Cerrar sesion\n")
                    op = int(input(">>>>>> "))
                    if(op==1):
                        id = int(input("Ingrese el id del usuario a verificar: "))
                        usuario = re.get(host+"Usuarios/"+str(id)).json()
                        if usuario is not None and usuario["verificado"]== False :
                            re.put(host+"Usuarios/"+str(id), json={"pagina_web":usuario["pagina_web"],"password": usuario["password"],"url_foto": usuario["url_foto"], "usuario": usuario["usuario"],
                            "verificado": True, "verificado_por": admin["id_admin"]})
                            print("Usuario verificado correctamente!\n")
                        else:
                            print("Error!\n")

                    if(op==2):
                        print("Sesión Cerrada Correctamente!\n")
                        break


                    