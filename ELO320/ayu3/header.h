#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define default_coins 10

typedef struct wallet
{
    //No necesitamos ni decimales ni negativos
    //int = 4 bytes (-x a x) ; unsigned int = 4 bytes (0 a y) con y > x
    unsigned int id ;
    int doge ;
    int cat ;
    int ufo ;
    int clp ;
    struct wallet* next;
}Wallet;

typedef struct registro{
    struct wallet* comprador;
    Wallet* vendedor;

    int cant_comp ;
    int cant_vend ;
    char* nombre_coin_vendida;
    char* nombre_coin_comprada;    
};

int init_wallet(Wallet *A);