#include "clase4.h"

int main(int argc , char *argv[])
{
    int a = 4;
    int b = 3;

    printf("a=%d , b=%d \n",a,b);
    swap2(&a,&b);
    printf("a=%d , b=%d \n",a,b);

    printf("\n");

    int ar[10] = {1,3,4,2,4,6,67,2,2,5};
    int *pa = &ar;
    for (int i = 0; i < 10; i++)
    {
        printf("AR[i] = %d\n",*(pa+i));
    }

    printf("\n");

    //Defino strings como arreglos de characteres (estos se pueden intepretar como número
    //o letra a la hora de imprimir por printf %d o %c)
    char saludo[] = "saludo";
    
    //char saludo[] = "saludo" es lo mismo que
    //char *psaludo = "saludo"

    // saludo: [s][a][l][u][d][o][0]
    // psaludo: [psaludo]  ------------> [s][a][l][u][d][o][0]

    int largo;
    largo = strlen(saludo);
    printf("El largo de %s es %d\n",saludo,largo);
    

    //copiar strings
    char *t = "hola";
    char *s = "chao";

    s=t; //solo cambia la referencia



    char *name[]={"EDA","MAT","ELO"};
    //char *argv[] = char **argv

    printf("\n");

    int i = 0;

    printf("Se ingresaron %d parametros \n",argc);

    for (i=0; i<argc ; i++)
    {
        printf(argv[i]);
    }

}

//no funciona, es como el dick
void swap(int aa, int bb)
{
    int temp = aa ; 
    aa = bb;
    bb = temp;
}

//funciona, es la leche
void swap2(int *pa , int *pb)
{
    int temp;
    temp = *pa;
    *pa = *pb;
    *pb = temp;
}

int strlen(char *w) //La variable se pasa por valor
{
    int n;
    for (n = 0; *w!=0 ; w++)
    {
        n++;
    }
    return n ;
}