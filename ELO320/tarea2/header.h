#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define INT_AMMOUNT 1920*1080
//Todos los archivos tendrán esta cantidad de datos adentro
#define INF_INT 2147483647
//Máximo valor que puede tomar un entero

void sort1(int* array , int size);/*
@PARAMS: int* arreglo a ordenar , int tamaño del arreglo a ordenar
@RETURNS: NULL
@TIME: Θ(size²)
Recibe un arreglo desordenado y lo ordena directamente en memoria (sin retornar)
usando un algoritmo tipo insertion sort.
*/

void sort2(int* array , int start , int end);/*
@PARAMS: int* arreglo a ordenar , int indice del primer elemtno , int indice del último elemento
@RETURNS: NULL
@TIME: Θ((end-start)log(end-start))
Recibe un arreglo desordenado y lo ordena directamente en memoria (sin retornar)
usando un algoritmo tipo merge sort.
*/

void sort3(int* array , int size);/*
@PARAMS: int* arreglo de enteros a ordenar , int tamaño del arreglo 
@RETURNS: NULL
@TIME: Θ(size)
Recibe un arreglo desordenado y lo ordena directamente en memoria (sin retornar)
usando un algoritmo tipo counting sort. Notar que si los enteros dentro de la lista
varían mucho en su valor, este método puede requerir una gran cantidad de memoria
*/

void copyarreglo(int* array_source , int* array_destination , int size);/*
@PARAMS: int* arreglo fuente , int* arreglo al cual copiar , int tamaño
@RETURNS: NULL
@TIME: Θ(size)
Copia dato por dato los elementos del arreglo source a el arreglo destination.
Ambos arreglos deben ser del mismo tamaño
*/

float segundos(clock_t clock);/*
@PARAMS: clock_t tiempo
@RETURNS: float tiempo transcurrido
@TIME: Θ(1)
transforma la cantidad dentro de la estructura clock_t a un dato float en segundos
*/
