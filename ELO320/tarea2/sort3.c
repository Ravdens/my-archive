#include"header.h"

void sort3(int* array , int size)
{
    if (array==NULL || size<2)
    {
        return;
    }

    int max = 0;
    for (int i = 0; i < size; i++)
    {
        if (array[i]>max)
        {
            max = array[i];
        }
    }
    
    int* new_array = NULL;
    new_array = (int*)malloc(sizeof(int)*size);
    int* cuant_array = NULL; 
    cuant_array = (int*)malloc(sizeof(int)*(max+1));
    
    for (int i = 0; i < max; i++)
    {
        cuant_array[i] = 0 ;
    }

    for (int i = 0; i < size; i++)
    {
        (cuant_array[array[i]])++ ;
    }

    for (int i = 0; i < max; i++)
    {
        cuant_array[i] = cuant_array[i]+cuant_array[i-1] ;
    }
    
    for (int i = size-1; i >= 0; i--)
    {
        new_array[cuant_array[array[i]]] = array[i] ;
        (cuant_array[array[i]])-- ;
    }

    for (int i = 0; i < size; i++)
    {
        array[i]=new_array[i];
    }
    free(new_array);
    free(cuant_array);
    
    return;
}