#include"header.h"

void sort2(int* array , int start , int end)
{
    if (array == NULL || start>=end)
    {
        return;
    }

    int mid;

    mid = (end+start)/2;
    sort2(array , start , mid);
    sort2(array , mid+1 , end);
    
    int size1 , size2;
    size1 = mid-start+1;
    size2 = end-mid;


    int arr1[size1+1];
    for (int i = 0 ; i < size1 ; i++)
    {
        arr1[i]=array[start+i];
    }
    arr1[size1]=INF_INT;
    
    int arr2[size2+1];
    for (int i = 0 ; i < size2 ; i++)
    {
        arr2[i]=array[mid+i+1];
    }
    arr2[size2]=INF_INT;
    
 
    int i=0 , j=0;
    for (int k = start ; k <= end ; k++)
    {
        if (arr1[i]<=arr2[j])
        {
            array[k]=arr1[i];
            i++;
        }
        else
        {
            array[k]=arr2[j];
            j++;
        }
    }     
    return;  
}