Tarea 2 : ELO320 | Estructuras de datos y algoritmos
Discordia Server

Nombre: Sergio Nicolas Ehlen Montero
Rol: 202130016-3
Correo: sergio.ehlen@usm.cl


Contenidos:

main.c
Archivo de código fuente con la función main() del programa y la implementacion de 2
funciones usadas en este

header.h
Incluye definiciones numéricas y prototipos de todas las funciones usadas en el programa

sort1.c
Implementación de la función sort1 (Insertion sort)

sort2.c
Implementación de la función sort2 (Merge sort)

sort3.c
Implementación de la función sort3 (Counting sort)

Makefile
Contiene procesos de compilación y ejecución del programa. 


Compilación:

$ make compile  o  $gcc -std=gnu99 main.c sort1.c sort2.c sort3.c -o decrypt


Ejecución:

$ ./decrypt [binary file] [*sorting algorithm ID]

*sorting algorithm ID es un campo opcional que permite elegir el algoritmo de
ordenamiento a usar

0: todos
1: sólo insertion sort
2: sólo merge sort
3: sólo counting sort

Si el campo se deja vacío, se ejecutaran los 3 algoritmos en serie, registrando
los tiempos de los 3 por separado.

Notas:

-En su implementacion actual, el programa recibe un archivo binario (.bin), y retorna un
archivo de reporte (analisis.txt) y un archivo imagen (image.pbm)

-El programa está hecho para recibir exactamente 2073600 (1920*1080) enteros del
archivo binario. Por esta razón, el algoritmo de ordenamiento sort1 se demora una cantidad
considerable de tiempo (en experimentos alrededor de 1 hora). ilación y ejecución del programa. 

Compilación:

$ make compile  o  $gcc -std=gnu99 main.c sort1.c sort2.c sort3.c -o decrypt

Ejecución:

$ ./decrypt [binary file] [*sorting algorithm ID]

*sorting algorithm ID es un campo opcional que permite elegir el algoritmo de
ordenamiento a usar

1: Insertion sort
2: Merge sort
3: Counting sort

Si el campo se deja vacío, se ejecutaran los 3 algoritmos en serie, registrando
los tiempos de los 3 por separado.

Notas:

-En su implementacion actual, el programa recibe un archivo binario (.bin), y retorna un
archivo de reporte (analisis.txt) y un archivo imagen (image.pbm)

-El programa está hecho para recibir exactamente 2073600 (1920*1080) enteros del
archivo binario. Por esta razón, el algoritmo de ordenamiento sort1 se demora una cantidad
considerable de tiempo (en experimentos alrededor de 1 hora). 