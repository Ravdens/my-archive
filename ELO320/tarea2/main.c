#include"header.h"

int main(int argc, char const **argv)
{
    //Inicialización:
    if (argc<2){ printf("Ingrese nombre de archivo\n"); return -1; }
    
    //optn indica algoritmo a usar (sort1,2,3 o todos)
    int optn=0;
    if (argc>2) optn=atoi(argv[2]);

    FILE* file;
    file=fopen(argv[1],"r");
    
    if (file==NULL){ printf("Error de archivo\n"); return -1;}

    //NO modificar array_respaldo. Contiene los datos del archivo en su orden original
    int* array_respaldo = NULL;
    array_respaldo = (int*)malloc(sizeof(int)*INT_AMMOUNT);

    fread(array_respaldo,sizeof(int),INT_AMMOUNT,file);

    fclose(file);

    //Mediciones de tiempo
    clock_t time;
    FILE* report;
    report=fopen("analisis.txt","w");
    int* array = NULL;
    array = (int*)malloc(sizeof(int)*INT_AMMOUNT);

    if (optn==1 || optn==0)
    {
        //Sorting 1: Insertion sort | Θ(n²)
        //En experiencia, suele tomar app 1 hora. Solo usar para generar el reporte final.    
        printf("Iniciando sort 1\n");
        copyarreglo(array_respaldo,array,INT_AMMOUNT);
        time = clock();
        sort1(array,INT_AMMOUNT);
        time = clock() - time;
        printf("Terminado sort 1\n");
        fprintf(report,"Sorting 1 : %f seconds\n",segundos(time));
    }
    
    if (optn==2 || optn==0)
    {
        //Sorting 2: Merge sort | Θ(nlogn)
        printf("Iniciando sort 2\n");
        copyarreglo(array_respaldo,array,INT_AMMOUNT);
        time = clock();
        sort2(array,0,INT_AMMOUNT-1);
        time = clock() - time;
        printf("Terminado sort 2\n");
        fprintf(report,"Sorting 2 : %f seconds\n",segundos(time));
    }
    
    if (optn==3 || optn==0)
    {
        //Sorting 3: Counting sort | Θ(n)
        //Ocupa una cantidad considerable de memoria. Considerar antes de usar.
        printf("Iniciando sort 3\n");
        copyarreglo(array_respaldo,array,INT_AMMOUNT);
        time = clock();
        sort3(array,INT_AMMOUNT);
        time = clock() - time;
        printf("Terminado sort 3\n");
        fprintf(report,"Sorting 3 : %f seconds\n",segundos(time));
    }
    

    fclose(report);
    printf("Archivo reporte completado. Encontrar bajo el nombre analisis.txt\n");
    //Importante: array queda ordenado, y a este punto se puede descartar el respaldo
    free(array_respaldo);

    file = fopen("image.pbm","w");
    fprintf(file,"P1\n1920 1080\n");
    for (int i = 1; i < INT_AMMOUNT+1; i++) //Este se ve medio raro porque 0%1920==0, pero funciona
    {
        fprintf(file,"%d ",(array[i-1])%2);
        if (i%1920==0)
        {
            fprintf(file,"\n");
        }
    }
    fclose(file);
    printf("Archivo imagen completado. Encontrar bajo el nombre imagen.pbm\n");
    free(array);
    return 0;
}

void copyarreglo(int* array_source , int* array_destination , int size)
{
    for (int i = 0; i < size; i++)
    {
        array_destination[i]=array_source[i];
    }
    return;
}

float segundos(clock_t clock)
{
    return ((float)clock)/CLOCKS_PER_SEC;
}