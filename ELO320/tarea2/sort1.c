#include"header.h"

void sort1(int* array , int size)
{
    if (array==NULL || size<2)
    {
        return;
    }
    
    int val; int pos; 
    for (int i = 1; i < size; i++)
    {
        pos = i;
        val = array[i];
        
        while (array[pos]<array[pos-1] && pos>0)
        {
            val = array[pos];
            array[pos]=array[pos-1];
            array[pos-1]=val;

            pos--;
        }
    }
    return;
}