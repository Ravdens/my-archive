#include "stack.h"


int main(int argc, char **argv)
{
	if (argc < 2)
	{
		printf("ERROR: ninguna formula ingresada\n");
		return -1;
	}

	for (int i = 1; i < argc; i++)
	{
		char* ecuacion = argv[i];
		int eLen = strlen(ecuacion);

		for (int j = 0; j < eLen; j++)
		{
			switch (ecuacion[j])
			{
				case 43: // suma "+"
					printf("Push suma\n");
					STACKpush(STACKpop()+STACKpop());
					break;

				case 45: // resta "-"
					printf("Push resta\n");
					STACKpush(STACKpop()-STACKpop());
					break;

				case 42: // multiplicacion "*"
					printf("Push multiplicacion\n");
					STACKpush(STACKpop()*STACKpop());
					break;

				case 37: // division "%"
					printf("Push division\n");
					STACKpush(STACKpop()/STACKpop());
					break;

				default:
					printf("Push default - ");
					//                 "0"                  "9"
					if (ecuacion[j] >= 48 && ecuacion[j] <= 57)
					{
					//              ATOI PERO RAW
						STACKpush(ecuacion[j] - 48);
					}
					
					break;
			}
			STACKprint();
		}
		printf("Resultado de la ecuación %d: %d\n",i,STACKpop());
	}
	return 0;
}
