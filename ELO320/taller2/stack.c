#include "stack.h"

int STACKempty()
{
	return stack==NULL;
}

int STACKfull()
{
	return STACKsize()==STACKmaxsize;
}

void STACKinit()
{
	if (!STACKempty())
	{
		STACKclean();
	}

	stack = (STACK*)malloc(sizeof(STACK));
	
	return;

}

//standard a usar para este taller:
//@return: void
//@params: int dato a pushear
void STACKpush(int dato)
{
	if (STACKempty())
	{
		STACKinit();
	}
	
	if (STACKfull())
	{
		return;
	}
	

	struct stacknode* tag = malloc(sizeof(STACK));
	
	tag->val=dato;
	tag->next= stack;
	stack=tag;

	printf("Pushed %d\n",dato);
	return;
}

int STACKpop()
{
	struct stacknode* tag = stack;
	int dat = stack->val;
	
	stack = stack->next;
	free(stack);

	printf("Popped %d\n",dat);
	return dat;
}

/* STACKsize: entrega la cantidad de elementos almacenados */
int STACKsize()
{
	struct stacknode *runner = stack;
	int count = 0;

	if (STACKempty)
	{
		return 0;
	}
	
	while (runner==NULL)
	{
		runner=runner->next;
		count++;
	}
	return count;
}

/* STACKclean: vacia el stack liberando la memoria cuando sea necesario */
void STACKclean()  
{
	STACK *tag = stack;

	if (STACKempty())
	{
		return;
	}
		
	do
	{
		tag = stack;
		stack = stack->next;
		free(tag);
	} while (stack!=NULL);
	
	return;
}

void STACKprint()
{
	printf("\n");

	struct stacknode *runner = stack;

	if (STACKempty)
	{	
		printf("Empty\n");
		return;
	}
	
	while (runner==NULL)
	{
		printf("/%d/",runner->val);
		runner=runner->next;
	}

	printf("\n");
	return;
}