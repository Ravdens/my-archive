#ifndef __STACK_H__
#define __STACK_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct stacknode
{
    int val;
    struct stacknode *next;
}
STACK;


#define STACKmaxsize 100
static STACK* stack = NULL;


void STACKinit();
int STACKempty();
void STACKpush(int);
int STACKpop();

/* STACKsize: entrega la cantidad de elementos almacenados */
int STACKsize();

/* STACKclean: vacia el stack liberando la memoria cuando sea necesario */
void STACKclean();

void STACKprint();

#endif // __STACK_H__
