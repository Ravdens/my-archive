#include"head.h"

int main(int argc, char const **argv)
{
    char* a = argv[1];
    int size = sizeof(a);
    StackInnit(size);
    for (int i = 0; i < size ; i++)
    {
        if (a[i]=="(" || a[i]=="[")
        {
            StackPush(a[i]);
        }

        if (a[i]==")" || a[i]=="]")
        {
            if (a[i]!=(StackPop()+1))
            {
                printf("ERROR, desbalanse de ()\n");
            }   
        }
    }
    if (StackEmpty())
    {
        printf("ERROR, desbalanceo de ()\n");
    }
    return 0;
}

void StackInnit(int size)
{
    //Si Shead fuera aglo, abría que liberarlo antes
    Shead= NULL;
}

int StackEmpty()
{
    //Expresión lógica, retorna 0 o 1 dependiendo si la pila está vacía o no
    return (Shead==NULL);
}

void StackPush(int val)
{
    addNodo(Shead,val);
}

int StackPop()
{
    int val;
    Stack *l;
    val=Shead->item;
    l=Shead->next;

    free(Shead);
    Shead=l;
    return val;
}








//Listas

Nodo* dltNodo2(Nodo *h , int valor)
{
    //Resuelve recorrer h y perder h
    Nodo *temp, *hh;
    hh = h; temp = h;

    if (h==NULL)
    {
        return h;
    }
    
    while (h!=NULL)
    {
        if (h->info == valor)
        {
            temp -> next = h -> next;
            free(h);
        }
        temp = h;
        h=h->next;
    }
}


/* Esta no sirve, pues la memoria se libera al terminar y se pierde el nodo creado
Nodo *makeNodo(int info)
{
    Nodo p;
    p.info = info;
    p.next = NULL;
    p.prev = NULL;
    return p;
}
*/ 

Nodo *makeNodo(int info)
{
    Nodo *p= malloc(sizeof(Nodo));
    p->info = info;
    p->next = NULL;
    p->prev = NULL;
    
    return p;
}

int lenLista(Nodo* h)
{
    int largo = 0;
    while (h!=NULL)
    {
        largo++;
        h=h->next;
    }
    return largo;
}

Nodo* findNodo(Nodo *h , int info)
{
    while (h!=NULL)
    {
        if (h->info == info)
        {
            return h;
        }
        else
        {
            h=h->next;
        }
        
    }
    return NULL;   
}

Nodo* getMinN(Nodo *h)
{
    if (h==NULL)
    {
        return h;
    }
    
    Nodo* temp = h;
    while (h != NULL)
    {
        if (h->next->info < temp->info)
        {
            temp= h;
        }
        h=h->next;
    }
    return temp;
}

void insrtNodoInicio(Nodo** h , int info)
{
    Nodo *p = makeNodo(info);
    p->next = *h;
    *h = p;
    return;
}

void insrtrNodoFinal(Nodo**h , int info)
{
    Nodo* p = makeNodo(info);
    if ((*h)==NULL)
    {
        (*h)=p;
        return;
    }

    while ((*h)!=NULL)
    {
        if ((*h)->next==NULL)
        {
            (*h)->next=p;
            p->next=NULL;
        }
        (*h)=(*h)->next;   
    }
}

//Tiene multples puntos de fallo
//No revisa primer nodo
//Mueve h, por lo cual pierde la cabeza de la lista
//Si llego al último Nodo, h->next es NULL, y NULL->info no existe, se cae.
Nodo* dltNodo(Nodo *h, int valor)
{
    Nodo *temp;
    if (h==NULL)
    {
        return h;
    }
    
    while (h!=NULL)
    {
        if (h->next->info == valor)
        {
            temp=h->next;
            h->next= h->next->next;
            free(temp);
        }
        h= h->next;
    }
    return;
}




//QUEUE


static int *q = NULL;
static int N, head, tail, nelem;

void Queueinit(int size)
{
    q=malloc(size*sizeof(int));
    N = size;
    head = 0;
    tail = 0;
    nelem = 0;
}

int Queueempty()
{
    return(nelem==0);
}

void Queueput(int dato)
{
    q[tail]=dato;
    tail = (tail+1)%N;
    nelem++;
}

int Queuepull()
{
    int val = q[head];
    head=(head+1)%N;
    nelem--;
    return val;
}
