#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//Stacks

typedef struct STACKnodo
{
    int item;
    struct STACKnodo *next;
}Stack;

static Stack *Shead=NULL;

void StackInnit(int size);
void addNodo(int val , Stack** head);


//Listas 

typedef struct modelo
{
    int info;
    struct modelo *next;
    struct modelo *prev;
}Nodo;

Nodo *makeNodo(int info);
int lenLista(Nodo* h);
Nodo* findNodo(Nodo *h , int info);
void insrtNodoInicio(Nodo** h , int info);
void insrtrNodoFinal(Nodo**h , int info);
Nodo* dltNodo(Nodo *h, int valor);
