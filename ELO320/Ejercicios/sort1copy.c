#include<stdio.h>
#include<stdlib.h>
#include<time.h>

#define INT_AMMOUNT 1920*1080
//Todos los archivos tendrán esta cantidad de datos adentro
#define INF_INT 2147483647
//Máximo valor que puede tomar un entero

void printarray(int* arr , int size);

void sort1(int* array , int size)
{
    if (array==NULL || size<2)
    {
        return;
    }
    
    
    int val; int pos; 
    for (int i = 1; i < size; i++)
    {   
        printf("%d : ",i);
        printarray(array,size);
        pos = i;
        val = array[i];
        
        while (array[pos]<array[pos-1] && pos>0)
        {

            val = array[pos];
            array[pos]=array[pos-1];
            array[pos-1]=val;

            pos--;

            printf("%d : ",i);
            printarray(array,size);
        } 

    }
    return;
}

void printarray(int* arr , int size)
{
    printf("{");
    for (int i = 0; i < size; i++)
    {
        printf(" %d",arr[i]);
    }
    printf(" }\n");
    return;
}

int main(int argc, char const *argv[])
{
    int t_array[5] = {98,32,53,465,123};
    sort1(t_array,5);
    int menor = 0;
    for (int i = 0; i < 5; i++)
    {
        if (t_array[i]>=menor)
        {
            menor = t_array[i];
        }
        else
        {
            printf("El arreglo no está ordenado\n");
            return 0;
        }
        
    }
    printf("El arreglo está ordenado\n");
    return 0;
}
