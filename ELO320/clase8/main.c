#include"head.h"

int main(int argc, char const **argv)
{

    return 0;
}


int factorialNOOB(int N)
{
    int t;
    for (int i = 0; i <= N; i++)
    {
        t*=i;
    }
    return t;
}

int factorialPRO(int N)
{
    if (N==0)
    {
        return 1;
    }
    else
    {
        return N*factorialPRO(N-1);
    }
}

