#include "head.h"

int main(int argc, char const *argv[])
{
    int largo = 0;
    Nodo *head;

    largo = largolista(head);
    addNodo(21 , &head);
    // o alternativamente 
    head = addNodoperosindoblepuntero(21 , head);




    return 0;
}

//Sacar el largo de una lista de nodos
int largolista(Nodo *head)
{
    int largo = 0;
    Nodo *current = head;
    while (current != NULL)
    {
        largo++;
    }
    return largo;    
}

//agregar un nodo al inicio de la lista internamente
void addNodo(int val , Nodo** head)
{
    Nodo *nodoNuevo;
    nodoNuevo = (Nodo*)malloc(sizeof(Nodo));
    nodoNuevo->dataso = val;
    nodoNuevo->next = *head;
    *head = nodoNuevo;
    return;

}

//agregar un nodo al incio de la lista por retorno
Nodo *addNodoperosindoblepuntero(int valor, Nodo *head)
{
    Nodo *nodoNuevo;
    nodoNuevo = (Nodo*)malloc(sizeof(Nodo));
    nodoNuevo -> dataso = valor;
    nodoNuevo -> next = head;

    return nodoNuevo;
}

//Insertar nodo en un punto especifico de una lista simple
void insertNode(int valor , int pos , Nodo **head)
{
    int largo;
    Nodo *sig;
    Nodo *ant;

    Nodo *nnodo = (Nodo*)malloc(sizeof(Nodo));
    nnodo -> dataso = valor;
    largo = largolista(*head);
    if (pos<largo)
    {
        for (int cc = 0; cc < pos;cc++ )
        {
            ant = sig;
            sig = sig -> next;
        }
        nnodo -> next = sig;
        ant -> next = nnodo;
        
    }
    

}

//Inserta nodo al inicio de la lista
DNodo *push(DNodo **href , int a)
{
    DNodo *newnodo = (DNodo*)malloc(sizeof(DNodo));
    newnodo -> info = a;
    newnodo -> next = *href;
    newnodo -> prev = NULL;
    newnodo -> next -> prev = newnodo; // Alternativa: (*href)-> prev = newnodo;

    return newnodo; // Alternativa: *href = newnodo
}

//Quita un nodo del frente de la lista y retorna su valor
int pop(DNodo **href)
{
    DNodo * head;
    int res;

    head = *href;
    res = head ->info; //Alternativa: res = (*href) ->info
    *href = head -> next;

    head->prev = NULL;

    free(head);
    
    return res;
}



















Nodo *construyeTresNodos()
{
    Nodo *uno = NULL;
    Nodo *dos = NULL;
    Nodo *tres = NULL;

    uno = (Nodo*)malloc(sizeof(Nodo));
    dos = (Nodo*)malloc(sizeof(Nodo));
    tres = (Nodo*)malloc(sizeof(Nodo));

    uno->next = dos;
    dos->next = tres;
    tres->next = NULL;

    return uno;
}