#include<stdio.h>
//#include<stdlib.h>


typedef struct modelo
{
    int dataso;
    struct modelo *next;
}Nodo;

typedef struct mascomplejo
{
    int info;
    struct mascomplejo *next;
    struct mascomplejo *prev;
}DNodo;


Nodo *construyeTresNodos();

int largolista(Nodo *n);
void addNodo(int val , Nodo** head);
Nodo *addNodoperosindoblepuntero(int valor, Nodo *head);
void insertNode(int valor , int pos , Nodo **head);

DNodo *push(DNodo **href , int a);
int pop(DNodo **href);