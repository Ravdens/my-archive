#include"header.h"

int main(int argc, char const **argv)
{
    //Inicialización:

    if (argc<2){ printf("Ingrese nombre de archivo\n"); return 0; }

    //Root es la raíz del árbol a usar durante el resto del programa 
    TrieNode* Root = MakeRoot();

    FILE* file;
    file=fopen(argv[1],"r");
    
    if (file==NULL){ printf("Error de archivo\n"); return 0;}

    char nletter ; char* str = NULL ; unsigned int Wamount = 0 ;
    str=(char*)malloc(sizeof(char)*MAX_WORD_SIZE); str[0]=0;
    
    do
    {
        //Ciclo de lectura de archivo (caracter por caracter)
        nletter=fgetc(file);

        if (nletter>=Aasc && nletter<=Zasc) nletter+=MayToMin;
        if (nletter>=aasc && nletter<=zasc) 
        {
            //letras se pasan a miniscula y se agregan al string str
            str=strncat(str,&nletter,1);
        }
        else
        {
            //se encuentra algo que no es una letra
            if (strlen(str)>1 && wordExist(Root,str)==0)
            {
                //La palabra no está ya y es de largo mayor a 1
                insertWord(Root,str);    
                Wamount++;
            }
            cleanString(str);   
        }
    }
    while (nletter!=EOF);
    
    fclose(file);
    free(str);
    printf("Trie listo! Se cargaron %d palabras\n(Para salir del programa, por favor ingresar la tecla enter)\n",Wamount);
    //printPreOrden(Root,0);


    //Ciclo de uso:

    char* inStr = NULL ; TrieNode* runner = NULL ; int flag ;
    inStr = (char*)malloc(sizeof(char)*MAX_WORD_SIZE);

    while (1)
    {
        printf("\n-----------------------------\nIngrese cadena de caracteres: ");
        flag = 0;
        fgets(inStr,MAX_WORD_SIZE,stdin);

        //fgets() registra \n, asique esta linea significa que el usuario no ingresó nada y desea salir
        if (inStr[0]=='\n')
        {
            break;
        }
        //tras la comprobación, removemos el caracter \n
        inStr[strlen(inStr)-1]=0;
        
        //Recorrer el arbol hasta llegar al subarbol que interesa mostrar
        runner = Root; 
        for (int i = 0; i < strlen(inStr); i++)
        {
            nletter = inStr[i];
            if (nletter>=Aasc && nletter<=Zasc) nletter+=MayToMin;
            if (nletter>=aasc && nletter<=zasc) 
            {
                if (runner->child[nletter-aasc]!=NULL)
                {
                    //Se recorre el arbol de Root con la parte ya conocida de la palabra
                    runner = runner -> child[nletter-aasc];
                }
                else
                {
                    //Si esto ocurre, significa que la palabra ingresada no coincide con un camino en el trie (no recomendaciones)
                    printf("No se han encontrado palabras sugeridas");
                    flag = 1;
                    cleanString(inStr);
                    break;
                }
                
                
            }
            else
            {
                //Si esto ocurre, el input del usuario incluía un caracter que no era una letra, y por tanto no es válido
                printf("Palabra ingresada no es valida, intente denuevo");
                flag = 1;
                cleanString(inStr);
                break;
            }
        }
        if (flag==0) //Condisión que runner se haya posicionado en el subarbol de forma correcta
        {
            //printPreOrden(runner,0);
            printf("Palabras sugeridas: ");
            showWords(runner,inStr,1);
        }
        cleanString(inStr);
    }
    
    //Finalización y limpieza final
    free(inStr);
    deleteTrie(Root);
    printf("Cerrando programa. Adios!! \n");
    return 0;
}
