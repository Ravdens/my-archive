#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define ALPHABET_SIZE 29
#define MAX_WORD_SIZE 50

//Definicion de numeros importantes para cambios de valores ASCII frecuentes
#define Aasc 65 //"A"
#define Zasc 90 //"Z"
#define aasc 97 //"a"
#define zasc 122 //"z"
#define AstAsc 42 //"*"
#define MinAsc 45 //"-"
#define MayToMin 32 //"a - A"


typedef struct mtrie
{
    char letter;
    struct mtrie* child[ALPHABET_SIZE];
}TrieNode;
/*Arreglo child guardará los hijos siempre en el mismo orden basado en su letra siguiendo el formato:
    {a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,k,r,s,t,u,v,w,x,y,z,NULL,-,*}
Con los espacios 26 quedando libres por si se necesita expandir a otros idiomas el sistema*/


void insertWord(TrieNode* tr , char* word);/*
@PARAMS: TrieNode* raíz , char* palabra a agregar
@RETURNS: NULL
Recibe un string palabra y la agrega al nodo raíz siguiendo el formato TrieNode y terminando con un nodo *.
*/
void deleteWord(TrieNode* tr , char* word);/*
@PARAMS: TrieNode* raíz , char* palabra a remover
@RETURNS: NULL
Remueve del árbol saliente de la raíz la palabra word quitando los nodos que forman parte de sólo esta palabra.
*/
int wordExist(TrieNode* tr , char *word);/*
@PARAMS: TreNode* raíz , char* palabra a confirmar
@RETURNS: int 1 if existe int 0 if no existe
Retorna confirmación de si se encuentran en el árbol saliente de la raíz los nodos necesarios para formar la palabra.
*/
TrieNode *insertnode(TrieNode* tr , char c);/*
@PARAMS: TrieNode* padre , char caracter a asignar al nuevo nodo
@RETURNS: Trienode* hijo
Construye el de caracter C y lo asigna a al espacio correspondiente en el arreglo del padre. Luego, retorna un puntero
a el nodo hijo creado.
*/
int childAmount(TrieNode* tr);/*
@PARAMS: TrieNode* nodo
@RETURNS: int cantidad
Retorna la cantidad de punteros en el arreglo child del padre los cuales apuntan a un Nodo en vez de NULL.
*/
TrieNode* MakeRoot();/*
@PARAMS: NULL
@RETURNS: TrieNode* raíz
Construye un nodo de forma raíz con el caracter - y retorna un puntero a este.
*/
void deleteTrie(TrieNode* tr);/*
@PARAMS: TrieNode* raíz
@RETURNS: NULL
Libera el espacio de memoria alocado por todos los nodos en el árbol saliente de la raíz. La función sigue un formato
post-orden, donde cada nodo libera primero a todos sus hijos y luego a si mismo.
*/
void showWords(TrieNode* tr , char* word , int n);/*
@PARAMS: TrieNode* sub-árbol , char* palabra incompleta , int n marcador de llamada
@RETURNS: NULL
Imprime de forma recursiva todos los recorridos desde el sub-árbol entregado hasta una hoja. Si n es distinto de
0, la función no cuenta el caracter donde empieza.
*/
void cleanString(char* S);/*
@PARAMS: char* string
@RETURNS: NULL
Reemplaza todos los caracteres distintos de \0 dentro de el string por \0
*/
void printNode(TrieNode* tr);/*
@PARAMS: TrieNode* nodo
@RETURNS: NULL
Imprime por pantalla información sobre el nodo, su valor, el valor de sus hijos y la direcciónes de este
*/
void printPreOrden(TrieNode* tr, int tab);/*
@PARAMS: TriNode* árbol , int tabulador
@RETURNS: NULL
Imprime por pantalla de forma recursiva el sub-árbol saliente de tr, usando el formato pre-orden
(raíz primero y luego hijos). Tab indica la cantidad de tabuladores - a poner desde el borde de la pantalla
al caracter.
*/