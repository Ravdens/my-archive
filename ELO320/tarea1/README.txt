Tarea 1 : ELO320 | Estructuras de datos y algoritmos
Árboles Trie

Nombre: Sergio Nicolas Ehlen Montero
Rol: 202130016-3
Correo: sergio.ehlen@usm.cl


Contenidos:

main.c
Archivo de código fuente con solamente la función main() del programa. Contiene algoritmo de formación de trie
y ciclo de uso.

trie.c
Contiene la implementacion de las principales funciones para el manejo de la estructura trie. 

header.h
Incluye definición de la estructura TrieNode usada para los trie, ademas de todos los prototipos de las funciones
y las definiciones númericas usadas en el programa. 

func.c
Incluye implementacion de funciones utilizadas durante el testeo del programa y para el uso más comodo de la 
estructura Trie. No se utilizan durante el funcionamiento del programa.

Makefile
Contiene procesos de compilación y ejecución del programa. 

Compilación:

$ make Comp  o  $gcc -std=gnu99 main.c trie.c func.c -o Load

Ejecución:

$ ./Load <file.txt>

Código DaVinci:

$ make Davinci o $ ./Load davincicode.txt

Notas:
- El programa acepta palabras de largo máximo 50 caracteres. Entrar una palabra más larga causará que se separe 
tras los 50 caracteres y se ingresen como palabras separadas.
- No se registran en el arbol palabras de una sola letra.
- Todas las palabras guardadas en el arbol se encuentran en minúscula