#include"header.h"

void cleanString(char* S)
{
    int strSize = strlen(S);
    for (int i = 0; S[i] !=0 ; i++)
    {
        //Remplaza todos los valores del string por \0
        S[i]=0;
    }
    return;
}

void printNode(TrieNode* tr)
{
    if (tr!=NULL) return;
    
    printf("NODE: %p CHAR: %c",tr,tr->letter);
    for (int i = 0; i <= ALPHABET_SIZE; i++)
    {
        //CHAR C: corresponde al caracter de ese espacio en el arreglo
        //CHAR R: corresponde al verdadero valor del nodo en ese espacio ('!' para nodos NULL)
        if (tr->child[i]==NULL)
        {
            printf("  CHILD: %p CHAR C: %c CHAR R: %c\n",tr->child[i],i+aasc,33);
        }
        else
        {
            printf("  CHILD: %p CHAR C: %c CHARD R: %c\n",tr->child[i],i+aasc,tr->child[i]->letter);
        }
    }
    
    return;
}

void printPreOrden(TrieNode* tr, int tab)
{
    if (tr!=NULL)
    {
        for (int i = 0; i < tab; i++)
        {
            //Los - agregados permiten ver la profundidad de cada nodo (expresada numericamente en tab)
            printf("-");
        }
        //Siguiendo el formato pre-orden, se imprime el valor de tr y luego el de sus hijos si es que los tiene
        printf(":%c\n",tr->letter);
        if (tr->letter==AstAsc)
        {
            //tr es el termino de una palabra
            return;
        }
        
        for (int i = 0; i <= ALPHABET_SIZE; i++)
        {
            if (tr->child[i]!=NULL)
            {
                //tab+1 señala el aumento de la profundidad
                printPreOrden(tr->child[i],tab+1);
            }
        }
    }
}