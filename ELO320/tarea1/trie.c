#include"header.h"

void insertWord(TrieNode* tr , char* word)
{
    //Esto solo pasa si se llama incorrectamente inserWord()
    if (tr==NULL) exit(-1);
    
   
    if (word[0]==0) //Fin de la palabra
    {
        //Nodos * al final de cada palabra se guardan en el espacio 28 del arreglo de hijos
        tr->child[ALPHABET_SIZE-1]=insertnode(tr,AstAsc);
        return;
    }
    
    if (tr->child[word[0]-aasc]==NULL)
    {
        //Si no existe ya el hijo, este se inserta
        tr->child[word[0]-aasc]=insertnode(tr,word[0]);
    }
    //Se llama a la función con la siguiente letra de la palabra en el subarbol correspondiente
    insertWord(tr->child[word[0]-aasc],word+1);
    return; 
    
}

void deleteWord(TrieNode* tr , char* word)
{
    int children = childAmount(tr);
    if (children>=2)
    {
        //Si el nodo tiene 2 o más hijos, este no se debe borrar
        deleteWord(tr->child[word[0]-aasc],word+1);
        return;
    }
    else if (children==1)
    {
        //Si tiene solamente un hijo, se borra el resto de la palabra primero y luego a si mismo
        deleteWord(tr->child[word[0]-aasc],word+1);
        free(tr);
        return;
    }
    else if (children==0)
    {
        //Si el nodo no tiene hijos, solo se libera y cierra
        free(tr);
        return;
    }
}

int wordExist(TrieNode* tr , char *word)
{
    if (tr==NULL) return 0;
    
    if (word[0]==0) //Se llega al final de la palabra (punto crítico)
    {
        //El arbol tiene un hijo en el espacio 28 y este tiene como letra *
        if ((tr->child[ALPHABET_SIZE-1]!=NULL)&&(tr->child[ALPHABET_SIZE-1]->letter==AstAsc))
        {
            //la palabra está en el arbol registrada como tal
            return 1;
        }
        else
        {
            //aunque las letras de la palabra están en el arbol, la palabra por si sola no se encuentra en el texto entregado
            return 0;
        }
        
    }else if (tr->child[word[0]-aasc]!=NULL)
    {
        //el hijo correspondiente a la primera letra existe, se llama denuevo al siguiente nodo con la siguiente letra
        return wordExist(tr->child[word[0]-aasc],word+1);
    }else
    {
        return 0;
    }
}

TrieNode *insertnode(TrieNode* tr , char c)
{
    //Esto solo pasa si se llama incorrectamente insernode()
    if (tr==NULL) exit(-1);
    
    TrieNode* newp=NULL;
    newp = malloc(sizeof(TrieNode));
    newp->letter=c;
    for (int i = 0; i < ALPHABET_SIZE; i++)
    {
        newp->child[i]=NULL;
    }

    if (c==AstAsc)
    {
        //Los nodos * se guardan en el espacio 28 del arreglo del padre
        tr->child[ALPHABET_SIZE-1]=newp;
    }
    else
    {
        //Los nodos [a,z] se guardan en los espacios [0,26]
        tr->child[c-aasc]=newp;
    }
    return newp;
}

int childAmount(TrieNode* tr)
{
    //Cuenta cantidad de nodos hijo de tr que no dan a NULL
    int n = 0;
    for (int i = 0; i < ALPHABET_SIZE; i++)
    {
        if (tr->child[i]!=NULL)
        {
            n++;
        }
    }
    return n;
}

TrieNode* MakeRoot()
{
    //Se diferencia a insertnode pues no se crea conección a un nodo padre
    TrieNode* pnew = NULL;
    pnew=(TrieNode*)malloc(sizeof(TrieNode));
    pnew->letter=MinAsc;
    //Solo el nodo Root usa - como letra

    for (int i = 0; i < ALPHABET_SIZE; i++)
    {
        pnew->child[i]=NULL;
    }
    return pnew;
}


void showWords(TrieNode* tr , char* word, int n)
{
    if (tr->letter==AstAsc)
    {
        //Se llega a una palabra, se imprime todas las letras del camino que se tomó para llegar al nodo
        printf("%s ",word);
        return;
    }
    else
    {
        for (int i = 0; i < ALPHABET_SIZE; i++)
        {
            if (tr->child[i]!=NULL)
            {   
                if (n!=0)
                {
                    //n!=0 solo se usa para no re-agregar la letra con la que se empieza
                    //Esto solo se usa en el llamado desde main, y no en ninguna recursión
                    showWords(tr->child[i],word,0);
                }
                else
                {
                    //Se agrega la letra del nodo actual
                    strncat(word,&(tr->letter),1);
                    //Llamada al proceso para el nodo inferior
                    showWords(tr->child[i],word,0);
                    //Cuando el proceso vuelva de este nodo, se remueve la letra agregada y se sale de este nodo tambien
                    word[strlen(word)-1]=0;
                }
            }
        }
    }
}

void deleteTrie(TrieNode* tr)
{
    //Se borran primero todos los sub-árboles que no dan a NULL
    for (int i = 0; i < ALPHABET_SIZE; i++)
    {
        if (tr->child[i]!=NULL)
        {
            deleteTrie(tr->child[i]);
        }
    }

    //Se libera posteriormente raíz para no perder al sub-árbol
    free(tr);
    return;
}