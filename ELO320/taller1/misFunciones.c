#include "misFunciones.h"

//abrir archivo y copiar el interior al puntero *palabra
void leer_palabra_secreta(char *filename, char **palabra)
{

	FILE *parchivo = NULL;
	parchivo = fopen(filename,"r");

	if (parchivo == NULL)
	{
		//main tambien retorna -1 si no se entrega nombre de archivo
		exit(-1);
	}
	
	//El tamaño de tempS es alocado no-dinamicamente en acuerdo con la profe
	char tempS[TEXTLEN];
	fscanf(parchivo, "%s" , tempS);
	fclose(parchivo);

	//Aunque palabra tendrá el mismo largo de tempS, este si se aloca dinámicamente :P
	*palabra = (char*)malloc(strlen(tempS)*(sizeof(char)));

	//Copy tempS to *palabra
	strcpy(*palabra,tempS);

	return;	
}

//Encontrar cuantas veces se encuentra c en *secreta y reemplazarlo en *adivinada
int buscar_letra(char *secreta, char *adivinada, char c)
{
	int ocurrencias = 0;
	for (int i = 0; i < strlen(secreta); i++)
	{
		if (secreta[i]==c)
		{
			adivinada[i]=c;
			ocurrencias++;
		}
		
	}
	return ocurrencias;
}
