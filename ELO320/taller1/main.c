#include "misFunciones.h"

int main(int argc, char *argv[])
{
    // Formato esperado de argv: ./a.out <name>.txt
    printf("Hola Bienvenido!!\n");
    if (argc != 2)
    {
        printf("ERROR, falta indicar archivo con la palabra secreta! D :,' \n");
        return -1;
    }

    char* palabra_secreta = NULL;
    // argv[1] = "<name>.txt"
    leer_palabra_secreta(argv[1],&palabra_secreta);


    char palabra_adivinada[strlen(palabra_secreta)] ;
    for (int i = 0; i < strlen(palabra_secreta); i++)
    {
        palabra_adivinada[i] = 42 ; //valor ascii del caracter "*"
    }

    //palabra_secreta = secreto | palabra_adivinada= ******

    char letra ; int turn = 0 ; int atmpt = MAX_INTENTOS ;
    
    char* inlist = NULL; //inlist: cadena con todos los char entrados por el usuario
    inlist = (char*)malloc(sizeof(char)*(MAX_INTENTOS+strlen(palabra_secreta)));

    //     Sin intentos          Descubrio la palabra completa
    while ( (atmpt>0) && (strcmp(palabra_adivinada,palabra_secreta)!=0) )
    {
        //UI de la partida (mismo printf para cada ronda)
        printf("---------------------------\nTurno: %d\n\t\tLetras ingresadas: %s\n%s \tIntentos permitidos: %d\n\nAdivine una letra: "
        ,turn,inlist,palabra_adivinada,atmpt);

        scanf("%c",&letra);

        //   buscar_letra = 0  =>  la letra no está en la palabra
        if(buscar_letra(palabra_secreta, palabra_adivinada , letra)==0)
        {
            atmpt-=1;
        }
        inlist[turn]=letra;
        turn++;

    }
    // Fin del juego
    
    printf("\n0-------------------------0\n");
    if (atmpt==0)
    {
        printf("YOU LOOSE, BOO HOO\n");
    }
    else
    {
        printf("You win or smthn I guess...\n");
    }
    printf("0-------------------------0\n");
    

    //printf("\n%s : %s\n",palabra_adivinada, palabra_secreta);
    free(palabra_secreta); free(inlist);
    
    return 0;
}


