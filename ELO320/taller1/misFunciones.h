#ifndef _MIS_FUNCIONES_
#define _MIS_FUNCIONES_


#define TEXTLEN 8
#define MAX_INTENTOS 6

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void leer_palabra_secreta(char *filename, char **palabra);
/*Recive un string con el nombre del archivo donde se encuentra la palabra y un puntero a un string
dentro del cual guarda la palabra. No retorna nada, pero si no se puede abrir el archivo cierra
el programa con estado -1.*/

int buscar_letra(char *secreta, char *adivinada, char c);
/*Recibe 2 strings, uno con la palabra que se intenta descubrir (secreta) y uno con la palabra que 
se busca despejar. Ademas se recibe un caracter. La función modifica directamente la variable adivinada,
reemplazando todas las ocurrencias de * donde en secreta se encuentra el caracter C. Retorna la cantidad
de veces que se encuentra C en secreta como int.*/


#endif // _MIS_FUNCIONES_