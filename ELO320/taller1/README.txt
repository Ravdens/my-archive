Taller 1 : ELO320 | Estructuras de datos y algoritmos
Nombre: Sergio Nicolas Ehlen Montero
Rol: 202130016-3
Correo: sergio.ehlen@usm.cl

Contenidos:

main.c
Archivo de código fuente con solamente la función main() del programa

misFunciones.c
Archivo de código fuente con las funciones leer_palabra_secreta() y buscar_letra()
utilizadas en main.c

misFunciones.h
Archivo header con definiciones y prototipos de funciones para misFunciones.c y main.c

palabra.txt
Archivo de texto que contiene la palabra secreta. Usado obligatoriamente en main.c


Compilación:
$ gcc main.c misFunciones.c -std=gnu99 -o ejecutable

Ejecución
$ ./ejecutable palabra.txt

