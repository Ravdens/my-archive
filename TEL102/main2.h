#include <iostream>
#include <vector>
#include <cmath>

#define G 9.807
#define pi 3.14

class Track
{
public:
    Track(float coef_roce , float acc  , float masa_vehiculo)
    {
        this->coef_roce = coef_roce ; 
        this->acc = acc ;
        this->masa_vehiculo = masa_vehiculo;

        ir= 0 ; ic = 0 ;
    }

    void addCurva(float radio, float len_ang) //len_ang en grados
    {
        Curva newCurva(coef_roce , radio , masa_vehiculo, len_ang) ;
        Vec_Curvas.push_back(newCurva) ;
        ic +=1 ;

        if (Vec_Rectas.size()>0)
        {
            Vec_Rectas[ic].finishRecta(newCurva.getSpeed());
        }
    }

    void addRecta(float largo)
    {
        float v_inicial;
        if (Vec_Curvas.size()>0)
        {
            v_inicial = Vec_Curvas[ir-1].getSpeed() ;
        }
        else
        {
            v_inicial = 0 ;
        }
         
        Recta newRecta(v_inicial,largo,acc);
        Vec_Rectas.push_back(newRecta);
        ir+=1;
    }

private:
    float coef_roce ;
    float acc ;
    float masa_vehiculo ;

    int ic ;
    int ir ;
    std::vector<Recta> Vec_Rectas ;
    std::vector<Curva> Vec_Curvas ;
};

class Recta
{
public:
    Recta(float v_entrada , float largo, float acc)
    {
        this->v_entrada = v_entrada;
        this->largo = largo; 
        this->acc = acc;
    }

    void finishRecta(float v_salida)
    {
        this->v_salida = v_salida;
        tiempo = makeTiempo();
    }

    float getTiempo()
    {return tiempo;}

protected:
    float makeTiempo();

private:
    float v_entrada;
    float v_salida;
    float tiempo;
    float largo; 
    float acc;
};

class Curva
{
public:
    Curva(float k , float r , float m, float theta)
    {  
        radio = r;
        maxSpeed = sqrt(k*G*r);
        fCentripeta = m*(pow(maxSpeed,2)/r);
        tiempo = (2*pi*radio*theta/360) / maxSpeed;
    }

    float getSpeed()
    {return maxSpeed;}

    float getCentripeta()
    {return fCentripeta;}

    float getRadio()
    {return radio;}

    float gettiempo()
    {return tiempo;}

private:
    float tiempo;
    float radio;
    float maxSpeed;
    float fCentripeta;
};