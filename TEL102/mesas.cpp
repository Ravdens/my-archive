#include <iostream>
using namespace std;
const int M =5;

struct mesa 
{
    int personas ;
    int used ;
};

mesa * initMesas ()
{
    int i;
    mesa * local ;
    local = new mesa [M ];
    for (i =0; i < M;i ++)
    {
        local [i ]. personas =0;
        local [i ]. used =0;
    }
    return local ;
}

mesa * checkMesa(mesa *local)
{
    for (int i = 0; i < M; i++)
    {
        if ((local+i)->used == 0)
        {return local+i;} 
    }

    mesa *newpoint = NULL;
    return newpoint;
}

void printMesas(mesa *local)
{
   for (int i = 0; i < M; i++)
    {
        cout <<"| Mesa "<<i+1<<" - "<< local + i <<endl;
        cout <<"| Personas: "<<(local+i)->personas;
        cout <<" | Usada: "<<(local+i)->used <<endl;
    }

}

void assignMesa(mesa *local, int numMesa,int personas)
{

    mesa *newpoint = local + numMesa-1;

    newpoint->personas = personas;
    newpoint->used = 1;

}

int main ()
{
    mesa * local1 = initMesas () ;
    printMesas(local1);
    assignMesa(local1,3,4);
    cout<<endl<<endl;
    printMesas(local1);
    delete[] local1 ;
    return 0;
}