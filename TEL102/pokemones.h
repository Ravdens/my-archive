#include <iostream>
#include <string>
#include <vector>

class Monster{
    public:
        Monster(std::string nombre, int vida, int ataque, int tipo)
        {
            name = nombre;
            hp = vida;
            attk = ataque;
            type = tipo;
        }

        int getHP(){return hp;}
        int getAttk(){return attk;}
        int getType(){return type;}
        std::string getName(std::string n){return name;}


        virtual void attackedBy(Monster *m) = 0;

    private:
        std::string name;
        int attk;
        int type;
    protected:
        int hp;
};

class FireMonster : public Monster{ //type 0
public:
    FireMonster(std::string nombre, int vida, int ataque) : Monster(nombre,vida,ataque,0){}
    void attackedBy(Monster *m)
    {
        if (m->getType()==1)
        {
            hp = hp - 2*m->getAttk();
        }
        else
        {
            hp = hp - m->getAttk();
        }   
    }
};

class WaterMonster : public Monster{ //type 1
public:
    WaterMonster(std::string nombre, int vida, int ataque) : Monster(nombre,vida,ataque,1){}
    void attackedBy(Monster *m)
    {
        if (m->getType()==0)
        {
            hp = hp - 2*m->getAttk();
        }
        else
        {
            hp = hp - m->getAttk();
        }
    }
};
