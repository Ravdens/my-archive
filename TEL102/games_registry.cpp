#include <iostream>
#include <string.h>
#define charlength 80
using namespace std;

struct game
{
    
    char title[charlength];
    char system[charlength];

    bool amiibo_compatible;
    bool multiplayer;
    bool has_ngp;
    int price;
    int age_rating;
    float file_size;
};


game * innitGame()
{
    struct game * newGame = new struct game ; 
    char temporary[charlength];
    cout<<"Enter the game's name: "; cin>>temporary;
    strcpy(newGame->title,temporary);
    cout<<"What system is the game for: "; cin>>temporary;
    strcpy(newGame->system,temporary);

    cout<<"Enter the games price: "; cin>>newGame->price ;
    cout<<"Enter the games age rating: "; cin>>newGame->age_rating ;
    cout<<"Enter the games file size: "; cin>>newGame->file_size ;

    cout<<"The game is compatible with amiibo " ; cin>>newGame->amiibo_compatible;
    cout<<"The game has multiplayer " ; cin>>newGame->multiplayer;
    cout<<"The game has a new game plus feature " ; cin>>newGame->has_ngp;
    

    return newGame;
}



int main()
{
    struct game * pointerEnd;
    pointerEnd = innitGame();


    cout << pointerEnd->title <<endl; 
    cout << pointerEnd-> system <<endl;
    cout << pointerEnd->price <<endl; 
    cout << pointerEnd->age_rating <<endl; 
    cout << pointerEnd->file_size <<endl; 
    cout << pointerEnd->amiibo_compatible <<endl; 
    cout << pointerEnd->multiplayer<<endl; 
    cout << pointerEnd->has_ngp <<endl; 
    return 0;

}