#include<iostream>
#include<cmath>
#define G 9.807
using namespace std;

//Obtener radio en [m]
float getRadio(){
    int opt;float radio;
    while(true){
        cout<<"Elija unidad de medida de distancia:"<<endl
        <<"(1) Metros\n(2) Kilometros\n(3) Centimetros\n(4) Millas\n(5) Pies"<<endl;
        cin>>opt;
        
        switch(opt){
            case 1:
                cout<<"Ingrese radio de giro en [m]: ";
                cin>>radio;
                return radio;

            case 2:
                cout<<"Ingrese radio de giro en [km]: ";
                cin>>radio;
                return (radio*1000);

            case 3:
                cout<<"Ingrese radio de giro en [cm]: ";
                cin>>radio;
                return (radio/100);

                
            case 4:
                cout<<"Ingrese radio de giro en [mi]: ";
                cin>>radio;
                return (radio/1609.34);

            case 5:
                cout<<"Ingrese radio de giro en [ft]: ";
                cin>>radio;
                return (radio/3.28084);

            default:
                cout<<"Opcion invalida, intente denuevo.";
        }
    }
}

//Obtener masa en [kg]
float getMasa(){
    int opt;float masa;
    while(true){
        cout<<"Elija unidad de medida de masa:"<<endl
        <<"(1) Kilogramos\n(2) Gramos\n(3) Libras\n(4) Toneladas"<<endl;
        cin>>opt;
        
        switch(opt){
            case 1:
                cout<<"Ingrese masa del vehiculo en [kg]: ";
                cin>>masa;
                return masa;

            case 2:
                cout<<"Ingrese masa del vehiculo en [g]: ";
                cin>>masa;
                return (masa/1000);

            case 3:
                cout<<"Ingrese masa del vehiculo en [lb]: ";
                cin>>masa;
                return (masa/2.20462);
                
            case 4:
                cout<<"Ingrese masa del vehiculo en [tn]]: ";
                cin>>masa;
                return (masa*1000);

            default:
                cout<<"Opcion invalida, intente denuevo.";
        }
    }
}


//funciones de calculo:
float Velocidad(float constante, float radio){
    return sqrt(constante*G*radio);
}
float FuerzaCentripeta(float velocidad, float masa, float radio){
    return masa*(pow(velocidad,2)/radio);
}


int main(){

    // R= radio [m]  : kr = constante de roce [adimensional]
    // m= masa  [kg] : v  = velocidad lineal  [m/s]
    float R,kr,m,v;

    R = getRadio();
    m = getMasa() ;
    cout<<"Ingrese coeficiente de roce: "; cin>>kr;

    v=Velocidad(kr, R);
    cout<< "Velocidad maxima: " << v <<" [m/s]."<<endl; 
    cout<< "Fuerza centripeta: " << FuerzaCentripeta(v,m,R)<<" [N]."<<endl;
    return 0;
}
