#include <iostream>
#include <cmath>
//#include "funciones.h"
using namespace std;


//Calcular Capital final, valor de la cuota y ganancias del banco a partir del
//inter´es simple.
//Calcular Capital final, valor de la cuota y ganancias del banco a partir del
//inter´es compuesto.
//Calcular Capital final, valor de la cuota y ganancias del banco a partir del
//tipo de inter´es que genere mayor ganancia para el banco.
//Finalizar programa.

int main() {
    cout <<"(1) Calcular capital final a base de interes simple"<<endl;
    cout <<"(2) Calcular capital final a base de interes compuesto"<<endl;
    cout <<"(3) Calcular capital final a base de tipo de interes que genere mayor ganancia"<<endl;
    cout <<"(4) Cerrar programa"<<endl;

    int opt ; cout<< "Opcion: "; cin >> opt;
    



    return 0;
}

float interes_simple(float C_i , int n_Cuotas , float intrs){
    float C_final = (C_i*(1+n_Cuotas*intrs));
    return C_final;
}

float interes_compuesto(float C_i , int n_Cuotas , float intrs){
    float C_final = 1+intrs;
    C_final= pow(C_final,n_Cuotas);
    C_final*=C_i;
    return C_final;
}

float valor_cuota_simple(float C_i , int n_Cuotas, float intrs){
    float C_final = (C_i*(1+n_Cuotas*intrs));
    C_final/=n_Cuotas;
    return C_final;
}

float valor_cuota_compuesto(float C_i , int n_Cuotas, float intrs){
    float C_final = 1+intrs;
    C_final= pow(C_final,n_Cuotas);
    C_final*=C_i;
    C_final/=n_Cuotas;
    return C_final;
}