#include <iostream> 
using namespace std;

int main() {
    cout << "\nWelcome to calculator: \n(1)Sum\n(2)Subtraction\n(3)Multiplication\n(4)Division\n";
    int option;
    cout << "\nOption: ";
    cin >> option;

    int num1;
    cout << "\nPlease enter the first number: ";
    cin >> num1;
    int num2;
    cout << "Please enter the second number: ";
    cin >> num2;

    if (option==1){
        cout << "\nThe sum of both numbers is: " << num1+num2 << endl;
    }


    if (option==2){
        cout << "\nThe subtraction of both numbers is: " << num1-num2 << endl;
    }


    if (option==3){
        cout << "\nThe multiplication of both numbers is: " << num1*num2 << endl;
    }


    if (option==4){
        cout << "\nThe division of both numbers is: " << num1/num2 << endl;
    }
}