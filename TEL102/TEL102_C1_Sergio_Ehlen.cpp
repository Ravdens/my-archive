#include<iostream>
#include<cmath>
#define NF 3
#define PI 3.141592654
using namespace std; //Por conveniencia, obvio321

struct fuerza{
	float f_x;
	float f_y;
};

float grados2Radianes(float angle)
{
    angle=angle*PI/180;
    return angle;
}

fuerza getComponentes(float mag ,float angle)
{
    fuerza toReturn;
    toReturn.f_x=cos(angle)*mag;
    toReturn.f_y=sin(angle)*mag;
    
    return toReturn;
}

float magnitudFuerza(fuerza f)
{
    //Mulitplico las componentes por si mismas en vez de usar
    //Una funcion pow para no involucrar demaciadas funciones
    float magnitud=sqrt(f.f_x * f.f_x+f.f_y * f.f_y);
    return magnitud;
}

fuerza getFuerza()
{
    float magn_in ; 
    cout<<"Ingrese la magnitud del vector en Newton [N]: "<<endl;
    cin>>magn_in;

    float ang_in ; 
    cout<<"Ingrese el angulo del vector en grados: "<<endl;
    cin>>ang_in ; ang_in=grados2Radianes(ang_in);
    //Este endl esta solo para que se vea igual al ejemplo
    cout<<endl;

    return getComponentes(magn_in,ang_in);
}

int main(){

	fuerza arreglo[NF]; //LLame a mi arreglo de fuerzas arreglo por conveniencia
    float totalx = 0;
    float totaly = 0;


    for (int i = 0; i < NF; i++)
    {
        arreglo[i]=getFuerza();
    }

    //Utilise un for de guardado y uno de lectura/suma ya que tenia que
    //usar el arreglo aunque en mi opinion era inncesario

    for (int i = 0; i < NF; i++)
    {
        totalx+=arreglo[i].f_x;
        totaly+=arreglo[i].f_y;
    }

    //Genero aqui un structfuerza solo para poder llamar al final
    //a la funcion  magnitudFuerza()

    fuerza fuerzafinal;
    fuerzafinal.f_x=totalx;
    fuerzafinal.f_y=totaly;

    cout<<"La magnitud del vector fuerza total es "
    <<magnitudFuerza(fuerzafinal)<<"[N]"<<endl;

    return 0;
}