#include <iostream>
#include <cmath>
using namespace std;

float average(float arreglo[5] , int sizeArreglo){
    int total = 0;
    for (int i = 0; i < sizeArreglo; i++)
    {
        total+=arreglo[i];
    }
    total/=sizeArreglo;
    return total;
}

int main()
{
    float arreglo[5];
    for (int i = 0; i<5 ;i++)
    {
        cout << "Ingrese dato numero "<< i+1 <<" de la lista: ";
        cin >> arreglo[i];
    }

    int promedio = average(arreglo,5);
    cout << "El promedio de los datos es: "<< promedio<<endl;


    int adentro_raiz =0;
    for (int i = 0; i < 5; i++)
    {adentro_raiz+=(arreglo[i]-promedio)*(arreglo[i]-promedio);}
    
    adentro_raiz/=5;
    adentro_raiz= sqrt(adentro_raiz);

    cout<< "La desviacion estandar es: "<<adentro_raiz<<endl;

    return 0;
}

