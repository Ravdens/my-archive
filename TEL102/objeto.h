#include <iostream>

class High_templar
{
public:
    //constructor
    High_templar(float pos_x , float pos_y,int lvl_atk, int lvl_escudo)
    {
        unit_innit(lvl_atk,lvl_escudo);
        unit_spawn(pos_x,pos_y);
    }

    //Implementacion de metodos

    float cast_feedback(int ID_personaje_enemigo)
    {
        if (energia_actual>0)
        {
            std::cout<<"Se casteo feedback contra el personaje de ID "<<ID_personaje_enemigo<<std::endl;
            energia_actual = energia_actual - 20 ; 
        }
        else
        {
            std::cout<<"No se casteo feedback por falta de energia"<<std::endl;
        }
    }


protected:
    bool unit_spawn(float spawn_pos_x , float spawn_pos_y)
    {
        std::cout<<"Se genero la unidad en la posicion"<<spawn_pos_x<<" "<<spawn_pos_y<<std::endl;
    }

    int unit_innit(int lvl_atk, int lvl_escudo)
    {
        int ID = 8888888 ;
        std::cout<<"La unidad ahora esta registrada bajo el ID "<<ID<<std::endl;
        hp_actual = 120;
        escudo_actual = 200;
        energia_actual = 250;
    }


private:
    float pos_x , pos_y;
    float hp_actual , escudo_actual , energia_actual;
    int lvl_atk , lvl_escudo ;
    bool ps_storm;
    bool moving;
};