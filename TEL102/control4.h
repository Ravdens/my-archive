#include <iostream>
#include <string>
#include <vector>

const float tasa_descarga_4g = 1;  
const float tasa_descarga_5g = 20;

struct Archivo{
    std::string nombre;
    float tamano;
};

class Servidor{
    private:
        std::vector<Archivo> archivos;
    public:
        Servidor(std::vector<Archivo> &files);
        std::vector<Archivo> getArchivos();
};

class Dispositivo{
    public:
        Dispositivo();
        void virtual descargarArchivos(Servidor s) = 0;
        void estadoDescargas();
    protected:
        std::vector<Archivo> archivos;
        float tiempo_total;
};

//Todo arriba de esta linea no fue editado y sigue intacto
// Defina aqui sus clases

class CuatroG : public Dispositivo
{
public:
    CuatroG();
    void descargarArchivos(Servidor s);
};

class CincoG : public Dispositivo
{
public:
    CincoG();
    void descargarArchivos(Servidor s);
};