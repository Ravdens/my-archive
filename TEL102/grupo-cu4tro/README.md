# Grupo cu4tro: Simulación de movimiento circular de un vehiculo.

El repositorio aquí presente incluye el código fuente  e instrucciones de compilación correspondientes al proyecto de TEl102: Seminario de programación del grupo 4.

#### Nuestro grupo se compone de:

    -Sergio Ehlen Montero       | Rol USM : 202130016-3
    -Daniel Fernández Martínez  | Rol USM : 202003004-9
    -Máximo Flores Suarez       | Rol USM : 202130019-8

## Temática y objetivo del proyecto

Como grupo elejimos estudiar el movimiento circular. Especificamente, la fuerza centripeta de un vehiculo en una curva. El objetivo principal del proyecto es, usando la menor cantidad de datos posibles, poder extraer información sobre la situación dinámica del vehiculo, y poder realisar una simulación apropiada para propositos de seguridad automovilistica. Actualmente, el proyecto se encuentra orientado a la simulación de vehiculos en pistas de carreras predefinidas, calculando y ofreciendo una representación gráfica de las velocidades máximas que puede tomar el  vehiculo para recorrer esta en el menor tiempo posible.

#### Video presentación del proyecto:

https://drive.google.com/file/d/15m--fvcfq2ZQMwidZcvABRIhSgUSie1o/view?usp=sharing\

#### Presentación avance versión 2.x:

https://drive.google.com/file/d/1OTHPLBPWNGT5AwNtAybWRnPSKO-7D2PP/view?usp=sharing

## Diagrama de componentes:

![ScreenShot]( Diagrama_de_componentes.png)

## Construcción e inicialización del programa:

Los archivos contenidos dentro de este repositorio corresponden a un proyecto de QT. Por lo tanto, para poder utilizarlo, se requiere clonar el repositorio y luego agregarlo como proyecto a QTCreator. 

Para el desarrollo del proyecto, se utilizó la versión 5.0.3 de QTCreator. Para construcción y ejecución del prgrama, ocupamos el metodo "Desktop Qt 5.15.2 MinGW 64-bit". Aunque distintas versiones pueden funcionar, algunas pueden tener problemas de compatibilidad, por tanto se recomienda usar estas mismas.

Es importante asegurar que la instalación personal de QT tenga incluida la librería QTCharts.

Para importar el proyecto, primero se debe clonar el repositorio usando "git clone". Luego, en QtCreator, importar el proyecto usando la opción "Open".

Seleccionar el archivo .pro, y al elejir metodo de construcción, usar "Desktop Qt 5.15.2 MinGW 64-bit". Ahora Qt podrá automaticamente correr el programa.

## Uso del programa:

La interfaz del programa se tiene 3 principales componentes. La sección de registro, la lista de datos y las pistas predeterminadas.

#### Sección de registro

En los 3 espacios principales, el usuario debe ingresar sus datos utilizando un teclado, usando "." para separar la parte entera y decimal de números no-enteros. Cuando todos los datos estén, se debe utilizar el botón "Agregar". Luego de esto, la interfaz cambiará para pedir los datos necesarios para la siguiente parte de la pista. 

Cuando la pista tenga todos los componentes mínimos, un botón "Finalizar" se hará disponible. Apretar este registrará los datos ingresados como la recta final y terminará la pista apropiadamente, cerrando la interfaz y abriendo en una ventana separada los gráficos de velocidad y fuerzas G.

Importante: una pista no puede terminar en una curva, sólo una recta, y por tanto el botón desaparecerá mientras se registra una curva.

#### Lista de datos

En la parte derecha de la ventana principal, se agregaran simultaneamente a su registro, los datos de la pista, las rectas, y las curvas apropiadas. Esto se realizará independiente de si se construye una pista personalizada o se utiliza una predeterminada.

#### Pistas predeterminadas

En la sección inferior izquierda de la ventan principal, encontrará 3 botones, los cuales contienen datos sobre pistas ya previamente construidas. Apretar uno de ellos cargará esta pista, y esperará en el estado previo a finalisación. Esto registrará los datos de la pista predeterminada en la lista de datos, y permitirá al usuario analisarlos antes de pasar a la siguiente fase. Cuando se deseen ver los gráficos, sólamente se necesita apretar el botón "Finalizar", y la ventana se cerrará, abriendo una nueva para los gráficos.

Importante: una vez cargada una pista predeterminada, no se podrá agregar más datos a la pista. Así mismo, al empezar una pista personalizada, no se podrán cargar las pistas predeterminadas.


Cabe notar que el proposito de este programa es la simulación, por lo tanto, usar datos irrealistas o incongruentes puede llevar a resultados erroneos.

## Referencias:

Equaciones de cinematica: Apuntes personales de clases

Equaciones de fuerza de roce: https://www.fisicalab.com/apartado/fuerza-rozamiento

Documentación de QT: https://doc.qt.io/qt-5.15/index.html

Página oficial de QT: https://www.qt.io/
