#include <cmath>

class Recta
{
public:
    Recta(float v_entrada , float largo, float acc);
    void finishRecta(float v_salida);
    float getTiempo1();
    float getTfinal();
    float getVmax();
    float getVInicial();
    float getVSalida();

protected:
    void makeTiempo(int acc, int v0, int vf, int largo);

private:
    float v_entrada;
    float v_salida;
    float tiempo1;
    float tiempo;
    float vmax;
    float largo;
    float acc;
};
