#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->boton2->setVisible(false);
    ui->Error_text->setVisible(false);
    track = new Track(0,0,0);
    i = 1;
    status = 1;

    //Status significa estado de la ventana
    // 1 = Entrando track ; 2 = Entrando recta ; 3 = Entrando curva
}

MainWindow::~MainWindow()
{
    delete ui;
    delete track;
}


void MainWindow::on_boton1_clicked() //Boton ok, sirve para registrar track, recta o curva, dependiendo del status de la ventana
{

    switch(status)
    {
        case 1://Registro de track

            if (ui->input_1->text().toFloat() == 0 || ui->input_2->text().toFloat()==0 || ui->input_3->text().toFloat()==0 ){
                ui->Error_text->setVisible(true);
                return;
            }

            //Importante: Deshabilitar aqui todos los defaults agregados posteriormente
            ui->Default1->setVisible(false);
            ui->Default2->setVisible(false);
            ui->Default3->setVisible(false);

            track->setMasa(ui->input_1->text().toFloat());
            track->setAc(ui->input_2->text().toFloat());
            track->setCoef(ui->input_3->text().toFloat());

            ui->TablaPrincipal->addItem("My Track:");
            ui->TablaPrincipal->addItem("\tMasa: "+ui->input_1->text()+"[kg]");
            ui->TablaPrincipal->addItem("\tAceleración: "+ui->input_2->text()+"[m/s²]");
            ui->TablaPrincipal->addItem("\tCoef. de roce: "+ui->input_3->text());

            ui->input_1->clear();
            ui->input_2->clear();
            ui->input_3->clear();

            ui->Titulo->setText(QStringLiteral("Valores de la recta %1").arg(i));

            ui->texto_1->setText("Ingrese longitud de la recta en metros:");

            ui->texto_1->setVisible(true);
            ui->texto_2->setVisible(false);
            ui->texto_3->setVisible(false);

            ui->input_1->setVisible(true);
            ui->input_2->setVisible(false);
            ui->input_3->setVisible(false);

            ui->Error_text->setVisible(false);

            status = 2;
            break;


        case 2://Registro de recta

            if (ui->input_1->text().toFloat() == 0){
                ui->Error_text->setVisible(true);
                return;
            }

            track->addRecta(ui->input_1->text().toFloat());

            ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
            ui->TablaPrincipal->addItem("\tLargo: "+ui->input_1->text()+"[m]");

            ui->input_1->clear();

            ui->Titulo->setText(QStringLiteral("Valores de la curva %1").arg(i));

            ui->texto_1->setText("Ingrese el radio de la curva en metros:");
            ui->texto_2->setText("Ingrese angulo de la curva en grados sexagesimales:");

            ui->texto_1->setVisible(true);
            ui->texto_2->setVisible(true);
            ui->texto_3->setVisible(false);

            ui->input_1->setVisible(true);
            ui->input_2->setVisible(true);
            ui->input_3->setVisible(false);

            ui->Error_text->setVisible(false);

            status = 3;
            ui->boton2->setVisible(false);//deshabilitar finalisacion (no puede terminar en una recta)
            break;


        case 3://Registro de curva

            if (ui->input_1->text().toFloat() == 0 || ui->input_2->text().toFloat()==0){
                ui->Error_text->setVisible(true);
                return;
            }

            track->addCurva(ui->input_1->text().toFloat() , ui->input_2->text().toFloat());

            ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
            ui->TablaPrincipal->addItem("\tRadio: "+ui->input_1->text()+"[m]");
            ui->TablaPrincipal->addItem("\tÁngulo: "+ui->input_2->text()+"°");

            ui->input_1->clear();
            ui->input_2->clear();

            i+=1 ; //Contador de rectas y curvas (solo aumenta aqui ya que van de a pares)

            ui->Titulo->setText(QStringLiteral("Valores de la recta %1").arg(i));

            ui->texto_1->setText("Ingrese longitud de la recta en metros:");

            ui->texto_1->setVisible(true);
            ui->texto_2->setVisible(false);
            ui->texto_3->setVisible(false);

            ui->input_1->setVisible(true);
            ui->input_2->setVisible(false);
            ui->input_3->setVisible(false);

            ui->Error_text->setVisible(false);

            status = 2;
            ui->boton2->setVisible(true);//Habilitar finalisacion

            break;

    }
}


void MainWindow::on_boton2_clicked()//Finalizar track
{
    if (ui->input_1->text().toFloat() == 0){
        ui->Error_text->setVisible(true);
        return;
    }

    track->addRecta(ui->input_1->text().toFloat());
    track->FinalizarTrack();
    ui->input_1->clear();
    ui->Layout_Pista->setVisible(false);

    graphs(track)->show();
    hide();

    return;

}

//Para crear mas pistas default, copiar esta funciona en otro boton y cambiar los datos
void MainWindow::on_Default1_clicked()
{

    track->setAc(2);
    track->setCoef(0.5);
    track->setMasa(2000);

    ui->TablaPrincipal->addItem("My Track:");
    ui->TablaPrincipal->addItem("\tMasa: 2000[kg]");
    ui->TablaPrincipal->addItem("\tAceleración: 2[m/s²]");
    ui->TablaPrincipal->addItem("\tCoef. de roce: 0.5");

    track->addRecta(1000);
    ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
    ui->TablaPrincipal->addItem("\tLargo: 1000[m]");

    track->addCurva(300,90);
    ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
    ui->TablaPrincipal->addItem("\tRadio: 300[m]");
    ui->TablaPrincipal->addItem("\tÁngulo: 90°");

    i+=1;

    track->addRecta(1000);
    ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
    ui->TablaPrincipal->addItem("\tLargo: 1000[m]");

    track->addCurva(800,90);
    ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
    ui->TablaPrincipal->addItem("\tRadio: 800[m]");
    ui->TablaPrincipal->addItem("\tÁngulo: 90°");

    i+=1;

    track->addRecta(500);
    ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
    ui->TablaPrincipal->addItem("\tLargo: 500[m]");

    track->addCurva(400,180);
    ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
    ui->TablaPrincipal->addItem("\tRadio: 400[m]");
    ui->TablaPrincipal->addItem("\tÁngulo: 180°");

    i+=1;

    track->addRecta(600);
    ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
    ui->TablaPrincipal->addItem("\tLargo: 600[m]");

    track->addCurva(800,180);
    ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
    ui->TablaPrincipal->addItem("\tRadio: 800[m]");
    ui->TablaPrincipal->addItem("\tÁngulo: 180°");

    i+=1;

    ui->verticalLayout->setEnabled(false);
    ui->boton1->setVisible(false);
    ui->boton2->setVisible(true);
    ui->Default1->setVisible(false);
    ui->Default2->setVisible(false);
    ui->Default3->setVisible(false);




    ui->texto_1->setText("Ingrese largo de la recta final:");
    ui->texto_2->setVisible(false);
    ui->texto_3->setVisible(false);

    ui->input_1->setText("1000");
    ui->input_2->setVisible(false);
    ui->input_3->setVisible(false);

    return;
}

void MainWindow::on_Default2_clicked()
{

    track->setAc(12);
    track->setCoef(0.1);
    track->setMasa(3450);

    ui->TablaPrincipal->addItem("My Track:");
    ui->TablaPrincipal->addItem("\tMasa: 3450[kg]");
    ui->TablaPrincipal->addItem("\tAceleración: 12[m/s²]");
    ui->TablaPrincipal->addItem("\tCoef. de roce: 0.1");

    track->addRecta(1700);
    ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
    ui->TablaPrincipal->addItem("\tLargo: 1700[m]");

    track->addCurva(500,110);
    ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
    ui->TablaPrincipal->addItem("\tRadio: 500[m]");
    ui->TablaPrincipal->addItem("\tÁngulo: 110°");

    i+=1;

    track->addRecta(3000);
    ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
    ui->TablaPrincipal->addItem("\tLargo: 3000[m]");

    track->addCurva(200,90);
    ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
    ui->TablaPrincipal->addItem("\tRadio: 200[m]");
    ui->TablaPrincipal->addItem("\tÁngulo: 90°");

    i+=1;

    track->addRecta(700);
    ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
    ui->TablaPrincipal->addItem("\tLargo: 700[m]");

    track->addCurva(1000,150);
    ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
    ui->TablaPrincipal->addItem("\tRadio: 1000[m]");
    ui->TablaPrincipal->addItem("\tÁngulo: 150°");

    i+=1;

    track->addRecta(900);
    ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
    ui->TablaPrincipal->addItem("\tLargo: 900[m]");

    track->addCurva(890,100);
    ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
    ui->TablaPrincipal->addItem("\tRadio: 890[m]");
    ui->TablaPrincipal->addItem("\tÁngulo: 100°");

    i+=1;

    ui->verticalLayout->setEnabled(false);
    ui->boton1->setVisible(false);
    ui->boton2->setVisible(true);
    ui->Default1->setVisible(false);
    ui->Default2->setVisible(false);
    ui->Default3->setVisible(false);




    ui->texto_1->setText("Ingrese largo de la recta final");
    ui->texto_2->setVisible(false);
    ui->texto_3->setVisible(false);

    ui->input_1->setText("300");
    ui->input_2->setVisible(false);
    ui->input_3->setVisible(false);

    return;
}

void MainWindow::on_Default3_clicked()
{


    track->setAc(1.5);
    track->setCoef(0.5);
    track->setMasa(850);

    ui->TablaPrincipal->addItem("My Track:");
    ui->TablaPrincipal->addItem("\tMasa: 850[kg]");
    ui->TablaPrincipal->addItem("\tAceleración: 1.5[m/s²]");
    ui->TablaPrincipal->addItem("\tCoef. de roce: 0.8");

    track->addRecta(4500);
    ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
    ui->TablaPrincipal->addItem("\tLargo: 4500[m]");

    track->addCurva(950,180);
    ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
    ui->TablaPrincipal->addItem("\tRadio: 950[m]");
    ui->TablaPrincipal->addItem("\tÁngulo: 180°");

    i+=1;

    track->addRecta(500);
    ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
    ui->TablaPrincipal->addItem("\tLargo: 500[m]");

    track->addCurva(1000,90);
    ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
    ui->TablaPrincipal->addItem("\tRadio: 1000[m]");
    ui->TablaPrincipal->addItem("\tÁngulo: 90°");

    i+=1;

    track->addRecta(400);
    ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
    ui->TablaPrincipal->addItem("\tLargo: 400[m]");

    track->addCurva(1000,90);
    ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
    ui->TablaPrincipal->addItem("\tRadio: 1000[m]");
    ui->TablaPrincipal->addItem("\tÁngulo: 90°");

    i+=1;

    track->addRecta(2000);
    ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
    ui->TablaPrincipal->addItem("\tLargo: 2000[m]");

    track->addCurva(1000,45);
    ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
    ui->TablaPrincipal->addItem("\tRadio: 1000[m]");
    ui->TablaPrincipal->addItem("\tÁngulo: 45°");

    i+=1;

    track->addRecta(3000);
    ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
    ui->TablaPrincipal->addItem("\tLargo: 3000[m]");

    track->addCurva(800,90);
    ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
    ui->TablaPrincipal->addItem("\tRadio: 800[m]");
    ui->TablaPrincipal->addItem("\tÁngulo: 90°");

    i+=1;

    track->addRecta(5000);
    ui->TablaPrincipal->addItem(QStringLiteral("Recta %1:").arg(i));
    ui->TablaPrincipal->addItem("\tLargo: 5000[m]");

    track->addCurva(50,180);
    ui->TablaPrincipal->addItem(QStringLiteral("Curva %1:").arg(i));
    ui->TablaPrincipal->addItem("\tRadio: 50[m]");
    ui->TablaPrincipal->addItem("\tÁngulo: 190°");

    i+=1;

    ui->verticalLayout->setEnabled(false);
    ui->boton1->setVisible(false);
    ui->boton2->setVisible(true);
    ui->Default1->setVisible(false);
    ui->Default2->setVisible(false);
    ui->Default3->setVisible(false);


    ui->texto_1->setText("Ingrese largo de la recta final");
    ui->texto_2->setVisible(false);
    ui->texto_3->setVisible(false);

    ui->input_1->setText("2000");
    ui->input_2->setVisible(false);
    ui->input_3->setVisible(false);

    return;
}

QWidget* MainWindow::graphs(Track *track)
{
    //Serie velocidad vs tiempo
    //Cout son temporales para revisión de datos
    QLineSeries *tiempoVsVelocidad = new QLineSeries();
    tiempoVsVelocidad->setName("Velocidad lineal[m/s] vs Tiempo[s]");

    float topSpeed = 0 ,  t = 0;
    //Punto inicial 0,0
    tiempoVsVelocidad->append(0,0);

    { //Ambito gráfico de v / t (al cerrarse borra las variables t1 , t2 , vmax , vCurva , tCurva)

    float t1 , t2 , vmax , vCurva , tCurva;

    //Notar que cada vector tiene .size() = 5 , entonces usaremos -1 como la ronda final (4) y -2 como la última ronda completa (la ronda 3)
    for(int i = 0 ; i<(track->Vec_Rectas.size() - 1) ; i++ )
    {
        //Para hacer más legible el codigo se utilizaran estas variables en el proceso
        t1 =  track->Vec_Rectas[i].getTiempo1();
        t2 =  track->Vec_Rectas[i].getTfinal();
        vmax = track->Vec_Rectas[i].getVmax();
        vCurva = track->Vec_Curvas[i].getSpeed();
        tCurva = track->Vec_Curvas[i].gettiempo();

        //Primera mitad de la recta
        t+= t1;
        tiempoVsVelocidad->append( t , vmax);

        //Chequeo
        if (vmax > topSpeed)
        {topSpeed = vmax;}


        //Final de la recta / Inicio de la curva
        t+=(t2 - t1);
        tiempoVsVelocidad->append( t , vCurva );

        //Chequeo
        if  (vCurva > topSpeed)
        {topSpeed =  vCurva;}

        //Final de la curva
        t+= tCurva;
        tiempoVsVelocidad->append( t , vCurva) ;
    }

    //Ultima vuelta (Hecha aparte para utilizar solo la Recta y no la curva)
    //El i correspondiente a la recta final está descrito por  [track1->Vec_Rectas.size() - 1]

    t1 =  track->Vec_Rectas[track->Vec_Rectas.size() - 1].getTiempo1();
    t2 =  track->Vec_Rectas[track->Vec_Rectas.size() - 1].getTfinal();
    vmax = track->Vec_Rectas[track->Vec_Rectas.size() - 1].getVmax();

                               //En este caso es la 4
    t+= t1;
    tiempoVsVelocidad->append( t , vmax );

    //Chequeo final
    if (vmax > topSpeed)
    {topSpeed = vmax;}


    //Usar getVsalida() para asegurar completamente que se haya alcansado correctamente
    t+=(t2 - t1);
    tiempoVsVelocidad->append( t , track->Vec_Rectas[track->Vec_Rectas.size() - 1].getVSalida());

    }//Cierre ambito t1,t2,vmax,vCurva y tCurva.



    //Si es posible, implementar esta sección en un if else dentro del ciclo while
    //Funciona igualmente, solo es dificil de leer


    //chart1 tendrá el grafico v vs t, agregar aquí sus opciones
    QChart *chart1 = new QChart();

    //tiempoVsVelocidad->setPointLabelsVisible(true);
    tiempoVsVelocidad->setPointsVisible(true);
    chart1->legend()->isVisible();
    QPen pen1 (QColor(0, 0 , 255 , 255));
    pen1.setWidth(3);
    tiempoVsVelocidad->setPen(pen1);

    chart1->addSeries(tiempoVsVelocidad);

    chart1->createDefaultAxes();
    chart1->axisY()->setRange(0,round(topSpeed+(topSpeed/10)));
    chart1->axisX()->setRange(0,t);

    //Todo: encontrar un metodo no deprecado de definir los ejes X e Y


    QLineSeries *tiempoVsGs = new QLineSeries();
    tiempoVsGs->setName("Aceleración total[g] vs Tiempo[s]");

    float topG = track->getAc()/G;
    t=0;

    {//Ambito para la creación del gráfico G vs t (al terminarlo se limpian las variables t2, acc, centripeta y masa)
    float t2 , acc , centripeta , masa , tCurva ;

    masa = track->getMasa();
    acc = track->getAc() / G;
    //acc no será la aceleración en si, sino la aceleracion en Gs (calculando diviendo en aceleracion de gravedad)

    tiempoVsGs->append(0,acc); //Punto inicial (0,a) (el auto empieza acelerando)
    for (int i = 0 ; i<(track->Vec_Rectas.size()-1) ; i++ )
    {
        t2 = track->Vec_Rectas[i].getTfinal();
        tCurva = track->Vec_Curvas[i].gettiempo();
        centripeta = track->Vec_Curvas[i].getCentripeta() / (masa*G);

        //centripeta sera la aceleracion centripeta en Gs, pero getCentripeta() retorna fuerzac
        //entonces dividiremos lo retornado en la masa y luego en aceleracion de gravedad (G)

        t+=(t2);
        tiempoVsGs->append( t ,acc );
        tiempoVsGs->append( t ,centripeta);

        t+=tCurva;
        tiempoVsGs->append( t ,centripeta );
        tiempoVsGs->append(t , acc);

        //Importante: Registrar los puntos de a pares para generar las lineas rectas verticales


        //Chequeo
        if (centripeta > topG)
        {
           topG = centripeta;
        }

    }

    t+=(track->Vec_Rectas[track->Vec_Rectas.size() - 1].getTfinal());
    tiempoVsGs->append(t,acc);

    }//Fin ambito t2 , acc , centripeta , masa , tCurva



    //Chart 2 contiene grafico G vs t. Cambiar sus opciones aquí
    QChart *chart2 = new QChart();

    //tiempoVsGs->setPointLabelsVisible(true);
    tiempoVsGs->setPointsVisible(true);
    chart2->legend()->isVisible();
    QPen pen2 (QColor(255, 0, 0, 255));
    pen2.setWidth(3);
    tiempoVsGs->setPen(pen2);

    chart2->addSeries(tiempoVsGs);

    chart2->createDefaultAxes();
    chart2->axisY()->setRange(0,topG*2);
    chart2->axisX()->setRange(0,t);



    //Generación de views y ventanas, cambiar opciones de la ventana generada aquí
    QChartView *chartView1 = new QChartView(chart1);
    chartView1->setRenderHint(QPainter::Antialiasing);

    QChartView *chartView2 = new QChartView(chart2);
    chartView2->setRenderHint(QPainter::Antialiasing);

    QVBoxLayout* myLayout = new QVBoxLayout;
    myLayout->addWidget(chartView1);
    myLayout->addWidget(chartView2);


    QWidget *wdg = new QWidget;
    wdg->setLayout(myLayout);
    wdg->resize(1500,900);

    return wdg;
}
