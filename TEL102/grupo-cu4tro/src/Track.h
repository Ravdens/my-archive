#include <vector>
#include <cmath>
#include "Recta.h"
#include "Curva.h"

class Track
{
public:
    Track(float coef_roce , float acc  , float masa_vehiculo);
    void addCurva(float radio, float len_ang);
    void addRecta(float largo);
    float getAc();
    float getMasa();
    void setAc(float x);
    void setMasa(float x);
    void setCoef(float x);
    void FinalizarTrack();

    //Estos vectores quedan en public para poder acceder sus datos desde afuera
    std::vector<Recta> Vec_Rectas ;
    std::vector<Curva> Vec_Curvas ;

private:
    float coef_roce ;
    float acc ;
    float masa_vehiculo ;

    //Ic e Ir serviran como indices para ambos vectores durante la construccion de una pista
    int ic ;
    int ir ;
};

