#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts>
#include <QApplication>
#include "Track.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QWidget* graphs(Track *track);

private slots:
    void on_boton1_clicked();

    void on_boton2_clicked();

    void on_Default1_clicked();

    void on_Default2_clicked();

    void on_Default3_clicked();

private:
    Ui::MainWindow *ui;
    Track *track;
    int i;
    int status;
};
#endif // MAINWINDOW_H
