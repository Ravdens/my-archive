#include "control3.h"
#include <iostream>
using namespace std; 

//Funciones clase telefono

int Telefono::getSaldo()
{return (prepago - saliente);}

float Telefono::getPromedio()
{return (saliente+entrante)/llamadas;}

void Telefono::reset()
{
    ocupado = false;
    discando = false;
}


void Telefono::finalizar(int tiempo)
{
    ocupado = false;
    llamadas+=1;
    if (discando = true)
    {
        saliente+=tiempo;
        discando = false;
    }
    else
    {
        entrante+=tiempo;
    }
    
}


bool Telefono::recibir()
{
    if (ocupado==false)
    {
        ocupado = true;
        return true;
    }

    else
    {return false;}

}

bool Telefono::discar()
{
    if (getSaldo()>0 and ocupado==false)
    {
        discando = true;
        ocupado = true;
        return true;
    }
    else
    {return false;}
}



//Funciones clase llamada

Llamada::Llamada(int codigo,Telefono *origen, Telefono *destino)
{
    this->origen = origen ; 
    this->destino = destino ;
    this->codigo = codigo ; 
}

void Llamada::iniciar()
{
    if (origen->discar() == true)
    {
        if (destino->recibir()!=true)
        {
            origen->reset();
        }
    }
}

void Llamada::finalizar(int tiempo)
{
    origen->finalizar(tiempo);
    destino->finalizar(tiempo);
}



//funciones sin clase

void printStatus(Telefono *list[])
{
    for (int i = 0; i <TELEFONOS; i++)
    {
        cout
        <<"Telefono "<<i
        <<" saldo = "<<list[i]->getSaldo()
        <<" promedio = "<<list[i]->getPromedio() 
        <<endl;
    }
}




int main()
{

    Telefono * list [ TELEFONOS ];
    list [0]= new Telefono (100);
    list [1]= new Telefono (50);
    list [2]= new Telefono (35);
    list [3]= new Telefono (67);
    list [4]= new Telefono (32);
    Llamada call_1 (1 , list [0] , list [1]);
    Llamada call_2 (2 , list [1] , list [2]);
    Llamada call_3 (3 , list [2] , list [0]);
    Llamada call_4 (4 , list [4] , list [3]);
    Llamada call_5 (5 , list [4] , list [0]);
    Llamada call_6 (6 , list [0] , list [2]);
    Llamada call_7 (7 , list [3] , list [0]);
    Llamada call_8 (8 , list [2] , list [3]);
    Llamada call_9 (9 , list [1] , list [2]);
    call_1 . iniciar ();
    call_2 . iniciar ();
    call_1 . finalizar (50);
    call_2 . iniciar ();
    call_2 . finalizar (40);
    call_3 . iniciar ();
    call_3 . finalizar (40);
    call_4 . iniciar ();
    call_5 . iniciar ();
    call_4 . finalizar (30);
    call_5 . iniciar ();
    call_5 . finalizar (20);
    call_6 . iniciar ();
    call_7 . iniciar ();
    call_6 . finalizar (20);
    call_8 . iniciar ();
    call_9 . iniciar ();
    call_9 . finalizar (10);

    printStatus ( list );
}