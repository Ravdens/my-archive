#include "pokemones.h"

// Las tildes han sido omitidas intencionalmente

// Implemente aca los metodos solicitados

int main(){
    std::vector<Monster*> equipoA = {new WaterMonster("Fisher", 100, 10),
    new WaterMonster("Croac", 85, 15),
    new FireMonster("Flamer", 60, 30)};
    
    std::vector<Monster*> equipoB = {new FireMonster("Foxy", 95, 15),
    new WaterMonster("Fontaine", 80, 18),
    new FireMonster("Drag", 75, 10)};


    int victoryA = 0;
    int victoryB = 0;
    int tie = 0;

    int ia = 0 , ib = 0 ;
    while (ia<4 and ib<4)
    {
        equipoA[ia]->attackedBy(equipoB[ib]);
        equipoB[ib]->attackedBy(equipoA[ia]);
        if (equipoA[ia]->getHP()<=0 and equipoB[ib]->getHP()>0)
        {
            ia+=1;
            victoryB+=1;
        }
        if (equipoB[ib]->getHP()<=0 and equipoA[ia]->getHP()>0)
        {
            ib+=1;
            victoryA+=1;
        }
        if (equipoA[ia]->getHP()<=0 and equipoB[ib]->getHP()>0)
        {
            tie+=1;
            ia+=1;
            ib+=1;
        }
    }

    if (ia>ib)
    {
        std::cout<<"Gano el equipo a"<<std::endl;
    }
    else if (ib>ia)
    {
        std::cout<<"Gano el equipo b"<<std::endl;
    }
    else
    {
        std::cout<<"No gano nadie chao"<<std::endl;
    }

    return 0;
}