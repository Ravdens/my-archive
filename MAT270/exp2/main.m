clear
clc

%% Metodo de Jacobi
A = tridiag(10,4,1,1);
b = rand(10,1);
tol = 10^-6;
maxit = 1000;

n = length(A);
N = diag(diag(A)); %Jacobi
%N = tril(A) %Gauss-Seidel
P = N-A;
x = zeros(n,1);
corr = 1;
errest = 1;
iter = 1;
while errest > tol && iter < maxit
    iter = iter+1;
    x0 = x;
    corr0 = corr;
    x = N\(P*x0+b);
    corr = norm(x-x0,inf);
    normest = corr/corr0;
    if normest >=1
        error('norma de la matriz de iteración > 1')
    end
    errest = normest/(1-normest)*corr;
end
disp('numero de iteraciones ')
iter


%% Metodo Gauss-Seigel

A = tridiag(10,4,1,1);
b = rand(10,1);
tol = 10^-6;
maxit = 1000;

n = length(A);
%N = diag(diag(A)); %Jacobi
N = tril(A); %Gauss-Seidel
P = N-A;
x = zeros(n,1);
corr = 1;
errest = 1;
iter = 1;
while errest > tol && iter < maxit
    iter = iter+1;
    x0 = x;
    corr0 = corr;
    x = N\(P*x0+b);
    corr = norm(x-x0,inf);
    normest = corr/corr0;
    if normest >=1
        error('norma de la matriz de iteración > 1')
    end
    errest = normest/(1-normest)*corr;
end
disp('numero de iteraciones ')
iter


%% Ejercicio
d = 9;
A = [12 2 1/4 ; 3 -15 1 ; -2 5/d 17];
b = [3; -5 ;5];
x0 = 0;
[xk,r,M,k] = Gauss_Seidel(A,b,x0,15,10^-8);

%% Factorizacion LU

A = tridiag(10,4,1,1);
b = rand(10,1);
rank(A);
x = A\b;

[L,U,P] = lu(A);
xlu = U\(L\b);

%% alt LU

A= rand(10,10);
b = rand(10,1);
while rank(A) ~=10
    A = rand(10,10);
end

x = A\b;

[L,U,P] = lu(A);
xlu = U\(L\b);
