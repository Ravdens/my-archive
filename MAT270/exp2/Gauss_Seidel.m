function [xk,r,M,k] = Gauss_Seidel(A,b,x0,n,tol)
    D = tril(A);
    L = D-tril(A);
    U = D-triu(A);
    M = D\(L+U);
    C = D\b;
    xk = x0;
    xkk = C+M*xk;
    k = 1;
    while ((k<n)&&(norm(xk-xkk,inf)<tol))
        xk=xkk;
        xkk=C+M*xk;
        k=k+1;
    end
    xk=xkk;
    r=max(abs(eig(M)));
end

