function [s] = Cuadratura_Gauss_tres(a,b,f)
    syms x
    p= [-sqrt(3/5),0,sqrt(3/5)];
    w = [5/9,8/9,5/9];
    s=0;
    for i=1:3
        e=((b-a)/2)*p(i)+(a+b)/2;
        s=s+w(i)*vpa(subs(f,x,(e)));
    end
    s = s*(b-a)/2;
end