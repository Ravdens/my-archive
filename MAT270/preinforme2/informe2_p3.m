clear
clc
d=2+1+2;

%Declaracion de parametros
m=d;
k=0.5;
b=3;
t0=0;
tf=45.2;
y10=1+(1/d);    %=y0
y20=2/d;        %=v0

%Funciones derivadas Y'
syms y1 y2 t;
f1=@(y1,y2,t) y2;
f2=@(y1,y2,t) -((b/m)*y1)-((k/m)*y1);

%% Para h=0.1
h=0.1;
n=(tf-t0)/h;

tv(1)=t0;
yv(1)=y10;
vv(1)=y20;

for i=1:n
    tv(i+1)=tv(i)+h;
    yvmedio(i+1)=yv(i)+(h/2)*f1(yv(i),vv(i),tv(i));
    vvmedio(i+1)=vv(i)+(h/2)*f2(yv(i),vv(i),tv(i));
    yv(i+1)=yv(i)+h*f1(yvmedio(i),vvmedio(i),tv(i)+(h/2));
    vv(i+1)=vv(i)+h*f2(yvmedio(i),vvmedio(i),tv(i)+(h/2));
end

%t=45.2
n_t=size(tv,2);
display(yv(n_t))
display(vv(n_t))

hold on
plot(tv,yv,".r")
plot(tv,vv,".g")
legend("Posicion y(t)","Velocidad v(t)")
hold off

%% Para h=0.05
h=0.05;
n=(tf-t0)/h;

tv(1)=t0;
yv(1)=y10;
vv(1)=y20;

for i=1:n
    tv(i+1)=tv(i)+h;
    yvmedio(i+1)=yv(i)+(h/2)*f1(yv(i),vv(i),tv(i));
    vvmedio(i+1)=vv(i)+(h/2)*f2(yv(i),vv(i),tv(i));
    yv(i+1)=yv(i)+h*f1(yvmedio(i),vvmedio(i),tv(i)+(h/2));
    vv(i+1)=vv(i)+h*f2(yvmedio(i),vvmedio(i),tv(i)+(h/2));
end

n_t=size(tv,2);
display(yv(n_t))
display(vv(n_t))

hold on
plot(tv,yv,".r")
plot(tv,vv,".g")
legend("Posicion y(t)","Velocidad v(t)")
hold off


%% Para h=10^-5

h=10^-5;
n=(tf-t0)/h;

tv(1)=t0;
yv(1)=y10;
vv(1)=y20;

for i=1:n
    tv(i+1)=tv(i)+h;
    yvmedio(i+1)=yv(i)+(h/2)*f1(yv(i),vv(i),tv(i));
    vvmedio(i+1)=vv(i)+(h/2)*f2(yv(i),vv(i),tv(i));
    yv(i+1)=yv(i)+h*f1(yvmedio(i),vvmedio(i),tv(i)+(h/2));
    vv(i+1)=vv(i)+h*f2(yvmedio(i),vvmedio(i),tv(i)+(h/2));
end

n_t=size(tv,2);
display(yv(n_t))
display(vv(n_t))

hold on
plot(tv,yv,".r")
plot(tv,vv,".g")
legend("Posicion y(t)","Velocidad v(t)")
hold off