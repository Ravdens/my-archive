clear
clc

d = 2+1+2+1;

%% a)

syms x y;
f1=@(x,y) x*(y^3);
y1= 1/sqrt((85/49)-(x^2));

a=1;
h=0.03;
n=10;
x0=1;
y0=1+(1/d);


xv(1)=x0;
yv(1)=y0;
for i=1:n
    xv(i+1)=xv(i)+h;
    yv(i+1)=yv(i)+h*f1(xv(i),yv(i));
end

hold on
plot(xv,yv,".r")
fplot(y1,[0.9,1.3],"b-")
legend("Estimacion","Solucion analitica")
hold off
%% b)

A=[xv;yv]';
[p_N,s_N] = Newton(A,1.5);
display(vpa(p_N));

hold on
plot(xv,yv,".r")
fplot(p_N,[0.9,1.3],"--")
fplot(y1,[0.9,1.3],"b-")
legend("Estimacion","Polinomio interpolante","Solucion analitica")
hold off

%% c)

for i=1:n+1
    A(i,1)=sin((pi*xv(i))/4)^2 ;
    A(i,2)=cos((pi*xv(i))/4) ;
end

% m = (a,b)
m = (A'*A)\(A'*yv');
display(m)

p_m = (m(1)*(sin((pi*x)/4)^2))+(m(2)*cos((pi*xv(i))/4));

hold on
plot(xv,yv,".r")
fplot(p_N,[0.9,1.3],"--")
fplot(p_m,[0.9,1.3],"--")
fplot(y1,[0.9,1.3],"b-")
legend("Estimacion","Polinomio interpolante", ...
    "Estimacion por minimos cuadrados","Solucion analitica")
hold off