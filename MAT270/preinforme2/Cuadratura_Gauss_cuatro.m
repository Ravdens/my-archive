function [s] = Cuadratura_Gauss_cuatro(a,b,f)
    syms x
    fa=3/7;
    fb=(2/7)*sqrt(6/5);
    p= [-sqrt(fa+fb),-sqrt(fa-fb),sqrt(fa-fb),sqrt(fa+fb)];
    w = [(18-sqrt(30))/36,(18+sqrt(30))/36,(18+sqrt(30))/36,(18-sqrt(30))/36];
    s=0;
    for i=1:4
        e=((b-a)/2)*p(i)+(a+b)/2;
        s=s+w(i)*vpa(subs(f,x,(e)));
    end
    s = s*(b-a)/2;
end