\documentclass[letterpaper]{article}
\usepackage[utf8]{inputenc}
\usepackage[spanish,activeacute,es-tabla]{babel}
\usepackage{graphicx}
\usepackage{anysize}
\usepackage{subcaption}
\usepackage{tikz}
\usepackage{titlesec}
\setcounter{secnumdepth}{4}
\usepackage{titling}
\usepackage{enumitem}
\usepackage{amsmath}
\usepackage{float}
\usepackage{listings}
\usepackage{color}
\input{insbox}
\usepackage[hidelinks]{hyperref}
\usepackage{array}
\usepackage{tabularx}
\usepackage{cancel}
\usepackage{tocloft}
\usepackage{textcomp}
\usepackage{fancyhdr}
\usepackage{wrapfig} 
\usepackage{mathdots}
\usepackage{mathrsfs}
\usepackage{moreverb}
\usepackage{xcolor}
\usepackage{verbatim}
\usepackage{xfrac}

\usepackage{matlab-prettifier}

\DeclareUnicodeCharacter{2212}{-}
\DeclareUnicodeCharacter{2217}{*}
\setcounter{MaxMatrixCols}{35}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\titleformat{\paragraph}
{\normalfont\normalsize\bfseries}{\theparagraph}{1em}{}
\titlespacing*{\paragraph}
{0pt}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}

\marginsize{2cm}{2cm}{2cm}{2cm}
\graphicspath{ {imagenes/} }
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
\begin{document}

\date{\today}

\thispagestyle{empty}

{\small
\begin{tabular}{p{0.55\textwidth} p{0.48\textwidth} }
\includegraphics[scale=0.18]{img/usm.png} &  \includegraphics[scale=0.22]{img/elo.jpg}
\end{tabular}
}


\begin{center}

\rule{\linewidth}{0.5mm}\\[0.5cm]
\textsc{MAT270: Análisis numérico}\\[0.5cm]
\textbf{\Large Pre-informe 2} \\ [0.45cm]

\begin{tabular}{rl}
\textbf{Alumno:} & Sergio Ehlen Montero\\
\textbf{Rol:}       & 202130016-3\\
\textbf{Rut:}       & 21217062-3\\
\textbf{Profesor:}    &  Felipe Millar\\
\textbf{Ayudante:} & Benjamín Caniulao\\
\textbf{Fecha:}       & \thedate   \\
\end{tabular}\\[0.5cm]
\rule{\linewidth}{0.5mm}

\par\vspace{1cm}

\end{center}


\pagestyle{fancyplain}
\fancyhf{}
\lhead{ \fancyplain{}{MAT270: Análisis numérico}}
\rhead{ \fancyplain{}{\the\year} }
\lfoot{ \fancyplain{}{USM 2023 - 2} }
\rfoot{ \fancyplain{}{\thepage} }
\renewcommand{\footrulewidth}{0.5pt}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Problema 1}
\subsection{Aproximación método de Euler}

Antes de realizar el análisis numérico, ya que es posible, se resuelve de forma analítica el problema de valor inicial. Tomando la EDO propuesta como una ecuación diferencial de Bernoulli, se puede llegar a la solución

$$y=\frac{1}{\sqrt{C_1-x^2}}$$

Reemplazando en la condición de valor inicial, se llega a el resultado

$$y=\frac{1}{\sqrt{\frac{85}{49}-x^2}}$$

Podemos notar que esta ecuación se indefine en $$|x|>\sqrt{\frac{85}{49}}\approx 1.31707778$$

lo cual significa que no habrán problemas al aproximar en el intervalo $x\in [1,1.3]$

Para aproximar por método de Euler, se utiliza el siguiente código

\begin{lstlisting}[style=Matlab-editor]
d= 2 +1 +2 +1;
syms x y;
f1=@(x,y) x*(y^3);
y1= 1/sqrt((85/49)-(x^2));

a=1;
h=0.03;
n=10;
x0=1;
y0=1+(1/d);


xv(1)=x0;
yv(1)=y0;
for i=1:n-1
    xv(i+1)=xv(i)+h;
    yv(i+1)=yv(i)+h*f1(xv(i),yv(i));
end
\end{lstlisting}

El cual otorga estos resultados

\begin{center}
\begin{tabular}{||c c c||}
 \hline
 i &  $x_i$ & $y_i$ \\ [0.5ex] 
 \hline\hline
 0&1.0000&1.1667\\ 
 \hline
 1&1.0300&1.2143\\ 
 \hline
 2&1.0600&1.2696\\ 
 \hline
 3&1.0900&1.3347\\ 
 \hline
 4&1.1200&1.4125\\ 
 \hline
 5&1.1500&1.5072\\ 
 \hline
 6&1.1800&1.6253\\ 
 \hline
 7&1.2100&1.7772\\ 
 \hline
 8&1.2400&1.9810\\ 
 \hline
 9&1.2700&2.2702\\ 
 \hline
 10&1.3000&2.7160\\ 
 \hline
\end{tabular}
\end{center}

Graficando podemos comparar la aproximación a la función resultante del método analítico (Figura 1).

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{img/1_a.jpg}
    \caption{Resultado de aproximación por método de Euler con 10 iteraciones.}
    \label{fig:figura1}
\end{figure}

Podemos ver que el método se acerca bastante bien al resultado real, pero se empieza a alejar a medida que aumentan las iteraciones.

\subsection{Polinomio de interpolación}

Tomando los valores de la pregunta anterior, se utiliza la función de interpolación por método de Newton

\begin{lstlisting}[style=Matlab-editor]
A=[xv;yv]';
[p_N,s_N] = Newton(A,1.5);
display(vpa(p_N));
\end{lstlisting}

La cual se define como

\begin{lstlisting}[style=Matlab-editor]
function [p,s] = Newton(A,r)
    k=size(A,1);
    w=zeros(k,k+1);
    for i=1:k
        w(i,1)=A(i,1);
        w(i,2)=A(i,2);
    end
    for i=3:k+1
        for j=1:(k-i+2)
            w(j,i)=(w(j+1,i-1)-w(j,i-1))/(w(j+i-2,1)-w(j,1));
        end
    end
    syms x;
    p=w(1,2);
    for i=3:k+1
        m=w(1,i);
        for j=1:i-2
            m=m*(x-w(j,1));
        end
        p=p+m;
    end
    p=simplify(p);
    s=vpa(subs(p,x,r));
end
\end{lstlisting}

Y retorna el siguiente polinomio \\

$1478163.4289182818029075860977173*x^{10} - 16612305.371648463007295504212379*x^9 + \\ 83995679.47591702867296407930553*x^8 - 251613621.40682995769914778065868*x^7 + \\ 494496203.34030697042353769618785*x^6 -  666203382.54526736437306888103194*x^5 + \\ 623089815.48408842919546572707579*x^4 - 399476785.27703221516051254437296*x^3 + \\ 168014905.71936451890493512570552*x^2 -  41859995.293281113764053924383688*x + \\ 4691323.6121305516709350869539046$\\

Podemos visualizar esta aproximación en la Figura 2

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{img/1_b.jpg}
    \caption{Polinomio interpolante para la aproximación de Newton}
    \label{fig:figura2}
\end{figure}

Este polinomio da la estimación $f(1.5)=100.6964$. Dado que la solución analítica se indefine desde $x=1.3$ en adelante, no podemos comparar esta aproximación con ningún valor real.

\subsection{Aproximación por mínimos cuadrados}
Se propone aproximar los datos mediantes una función de tipo

$$f(x)=a\cdot sin^2(\frac{\pi x}{4})+b\cdot cos(\frac{\pi x_i}{4})$$

Esto se puede reescribir por método de mínimos cuadrados como un sistema de ecuaciones de la forma
$$
\begin{pmatrix}
    sin^2(\frac{\pi x_0}{4}) &  cos(\frac{\pi x_0}{4})    \\
    sin^2(\frac{\pi x_1}{4}) &  cos(\frac{\pi x_1}{4})    \\
    ...  &  ... \\
\end{pmatrix} *
\begin{pmatrix}
    a  \\
    b  \\
\end{pmatrix} =
\begin{pmatrix}
    y_0  \\
    y_1  \\
    ...  \\
\end{pmatrix}
$$

Mediante la factorización

$$A^TAx=A^Tb$$

se puede resolver el sistema de ecuaciones en matlab con el siguiente código

\begin{lstlisting}[style=Matlab-editor]
for i=1:n
    A(i,1)=sin((pi*xv(i))/4)^2 ;
    A(i,2)=cos((pi*xv(i))/4) ;
end

% m = (a,b)
m = (A'*A)\(A'*yv');
display(m)
\end{lstlisting}

Que nos otorga los resultados $a=4.5661$ y $b=-1.8613$

Podemos visualizar la aproximación en la Figura 3

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{img/1_c.jpg}
    \caption{Comparación de solución analítica, aproximación por método de Newton y ajuste por mínimos cuadrados}
    \label{fig:figura3}
\end{figure}

\section{Problema 2}
\subsection{Estimación por regla de Trapecio}

La función a integrar se comporta de la siguiente manera en el intervalo $[0,2\pi]$ (Figura 4)

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{img/2_a.jpg}
    \caption{función f(x) a integrar}
    \label{fig:figura4}
\end{figure}

Se utiliza el siguiente código para estimar la integral en n segmentos usando el método de Trapecio

\begin{lstlisting}[style=Matlab-editor]
function s=Trapecio_N(fn,a,b,N)
    syms x;
    h=(b-a)/N;
    s=vpa(subs(fn,x,a))+vpa(subs(fn,x,b));
    for k=1:(N-1)
        p=a+(h*k);
        s=s+2*vpa(subs(fn,x,p));
    end
    s=s*(h/2);
    s=simplify(s);
end
\end{lstlisting}

Evaluando en nuestra función

\begin{lstlisting}[style=Matlab-editor]
d = 2+1+2+1;
syms x
f = exp(sqrt(2*d)*cos(x))*cos(x)+exp(-d*(x^2));
a=0;
b=2*pi;
s_T=Trapecio_N(f,a,b,200);
\end{lstlisting}

Nos da como estimación $s_T=38.110407466833836660077938496297$

\subsection{Estimación por regla de Simpson}

Se define el código para estimar la integral por regla de Simpson

\begin{lstlisting}[style=Matlab-editor]
function s=Simpson(fn,a,b,N)
    syms x;
    h=(b-a)/N;
    s=4*vpa(subs(fn,x,a+(h/2)))+vpa(subs(fn,x,a))+vpa(subs(fn,x,b));
    for k=1:(N-1)
        p=a+(h*k);
        pp=p+(h/2);
        s=s+2*vpa(subs(fn,x,p))+4*vpa(subs(fn,x,pp));
    end
    s=s*(h/6);
end
\end{lstlisting}

Que otorga como estimación $S_s=38.110407466833836660077938496297$

\subsection{Estimación por cuadratura Gaussiana de 3 y 4 nodos}

Los códigos a usar para la estimación fueron

\begin{lstlisting}[style=Matlab-editor]
function [s] = Cuadratura_Gauss_tres(a,b,f)
    syms x
    p= [-sqrt(3/5),0,sqrt(3/5)];
    w = [5/9,8/9,5/9];
    s=0;
    for i=1:3
        e=((b-a)/2)*p(i)+(a+b)/2;
        s=s+w(i)*vpa(subs(f,x,(e)));
    end
    s = s*(b-a)/2;
end

function [s] = Cuadratura_Gauss_cuatro(a,b,f)
    syms x
    fa=3/7;
    fb=(2/7)*sqrt(6/5);
    p= [-sqrt(fa+fb),-sqrt(fa-fb),sqrt(fa-fb),sqrt(fa+fb)];
    w = [(18-sqrt(30))/36,(18+sqrt(30))/36,(18+sqrt(30))/36,(18-sqrt(30))/36];
    s=0;
    for i=1:4
        e=((b-a)/2)*p(i)+(a+b)/2;
        s=s+w(i)*vpa(subs(f,x,(e)));
    end
    s = s*(b-a)/2;
end
\end{lstlisting}

Que otorgan resultados $s_{C3}=36.831021008280114743647385614001$ y $s_{C4}=45.728393425328185980173805015934$

Para el método de Cuadratura de Gauss, se tiene que el grado de exactitud es $2n+1$ para n nodos, por lo tanto el grado de exactitud sería 7 y 9 para las estimaciones anteriores.

\section{Problema 3}
\subsection{Expresar como sistema de ecuaciones diferenciales}

Se define el problema como una ecuación diferencial ordinaria de segundo orden de la forma

$$\frac{d^2y}{dt^2}=
-\frac{b}{m}\frac{dy}{dt}-\frac{k}{m}y$$

Para convertir el problema en un sistema de ecuaciones, se utiliza el siguiente cambio de variables

$$\begin{cases}
y_1 = y&\implies\frac{dy_1}{dt}=y_2 \\ 
y_2 = \frac{dy}{dy}&\implies\frac{dy_2}{dt}=-\frac{b}{m}y_2-\frac{k}{m}y_1
\end{cases} $$

De esta manera tenemos el siguiente sistema de problemas de valor inicial

$$\begin{cases}
    Y'=
    \begin{pmatrix}
    y_1'  \\
    y_2'  \\
\end{pmatrix}=\begin{pmatrix}
    y_2  \\
    -\frac{b}{m}y_2-\frac{k}{m}y_1  \\
\end{pmatrix}\\
    Y(\begin{pmatrix}
    0  \\
    y_0  \\
\end{pmatrix})=
\begin{pmatrix}
    y_0  \\
    v_0  \\
\end{pmatrix}\end{cases}$$

\subsection{Método de Euler mejorado}

Se plantea el siguiente código para iterar las variables propuestas anteriormente con el método de Euler mejorado usando h=0.1

\begin{lstlisting}[style=Matlab-editor]
%Declaracion de parametros
d=2+1+2;
m=d;
k=0.5;
b=3;
t0=0;
tf=45.2;
y10=1+(1/d);    %=y0
y20=2/d;        %=v0

%Funciones derivadas Y'
syms y1 y2 t;
f1=@(y1,y2,t) y2;
f2=@(y1,y2,t) -((b/m)*y1)-((k/m)*y1);

%Metodo de Euler
h=0.1;
n=(tf-t0)/h;

tv(1)=t0;
yv(1)=y10;
vv(1)=y20;

for i=1:n
    tv(i+1)=tv(i)+h;
    yvmedio(i+1)=yv(i)+(h/2)*f1(yv(i),vv(i),tv(i));
    vvmedio(i+1)=vv(i)+(h/2)*f2(yv(i),vv(i),tv(i));
    yv(i+1)=yv(i)+h*f1(yvmedio(i),vvmedio(i),tv(i)+(h/2));
    vv(i+1)=vv(i)+h*f2(yvmedio(i),vvmedio(i),tv(i)+(h/2));
end

n_t=size(tv,2);
display(yv(n_t))
display(vv(n_t))

\end{lstlisting}

La estimación obtenida se puede ver en la Figura 5

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{img/3_b_1.jpg}
    \caption{Estimación por Método de Euler mejorado con h=0.1}
    \label{fig:figura5}
\end{figure}

Y para $t=45.2$ otorga $y(45.2)=19.0656$ , $v(45.2)=16.7395$. Utilizando el mismo código que antes, cambiamos el parámetro h a 0.05 para obtener la estimación vista en la Figura 6

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{img/3_b_2.jpg}
    \caption{Estimación por Método de Euler mejorado con h=0.05}
    \label{fig:figura6}
\end{figure}

Que retorna como nueva estimación $y(45.2)=5.6464$ y $v(45.2)=2.1259$. Notamos que en ambos casos, se presenta el comportamiento oscilatorio esperado de un oscilador armónico, pero el comportamiento de amortiguación no se ve presente. Se puede evidenciar sin embargo que la amplitud aumenta mucho más para h=0.1 (hasta aproximadamente A=20) que para h=0.05 (hasta aproximadamente A=5). Para corroborar que este comportamiento se debe a un problema de precisión, se realizó una tercera aproximación usando $h=10^{-5}$, que otorga los resultados presentes en la Figura 7.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{img/3_b_3.jpg}
    \caption{Estimación por Método de Euler mejorado con $h=10^-5$}
    \label{fig:figura7}
\end{figure}

$$y(45.2)=1.2483$$
$$v(45.2)=0.2792$$

Podemos ver entonces que para un h suficientemente alto, la Amplitud no crece en el tiempo.

\end{document}