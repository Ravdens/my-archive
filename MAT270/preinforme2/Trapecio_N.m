function s=Trapecio_N(fn,a,b,N)
    syms x;
    h=(b-a)/N;
    s=vpa(subs(fn,x,a))+vpa(subs(fn,x,b));
    for k=1:(N-1)
        p=a+(h*k);
        s=s+2*vpa(subs(fn,x,p));
    end
    s=s*(h/2);
    s=simplify(s);
end