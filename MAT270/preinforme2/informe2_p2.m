clear
clc
%% a)
d = 2+1+2+1;

syms x
f = exp(sqrt(2*d)*cos(x))*cos(x)+exp(-d*(x^2));
a=0;
b=2*pi;
s_T=Trapecio_N(f,a,b,200);

fplot(f,[a,b])

%% b)
s_S=Simpson(f,a,b,150);

%% c)
%Grado de precision 2n+1
s_C3=Cuadratura_Gauss_tres(a,b,f);
s_C4=Cuadratura_Gauss_cuatro(a,b,f);
