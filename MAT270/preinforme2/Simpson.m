function s=Simpson(fn,a,b,N)
    syms x;
    h=(b-a)/N;
    s=4*vpa(subs(fn,x,a+(h/2)))+vpa(subs(fn,x,a))+vpa(subs(fn,x,b));
    for k=1:(N-1)
        p=a+(h*k);
        pp=p+(h/2);
        s=s+2*vpa(subs(fn,x,p))+4*vpa(subs(fn,x,pp));
    end
    s=s*(h/6);
end