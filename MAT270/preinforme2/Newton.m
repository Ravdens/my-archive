function [p,s] = Newton(A,r)
    k=size(A,1);
    w=zeros(k,k+1);
    for i=1:k
        w(i,1)=A(i,1);
        w(i,2)=A(i,2);
    end
    for i=3:k+1
        for j=1:(k-i+2)
            w(j,i)=(w(j+1,i-1)-w(j,i-1))/(w(j+i-2,1)-w(j,1));
        end
    end
    syms x;
    p=w(1,2);
    for i=3:k+1
        m=w(1,i);
        for j=1:i-2
            m=m*(x-w(j,1));
        end
        p=p+m;
    end
    p=simplify(p);
    s=vpa(subs(p,x,r));
end

