function s=V_Medio(fn,a,b,N)
syms x;
h=(b-a)/N;
s=0;
for k=0:(N-1)
    p=(2*a+(h*k)+(h*(k+1)))/2;
    s=s+(vpa(subs(fn,x,p))*h);
end
end