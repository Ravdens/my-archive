function s=Trapecio_h(fn,a,b,h)
syms x;
N=(b-a)/h;
s=vpa(subs(fn,x,a))+vpa(subs(fn,x,b));
for k=1:(N-1)
    p=a+(h*k);
    s=s+2*vpa(subs(fn,x,p));
end
s=s*(h/2);
s=simplify(s);
end
