function s=Trapecio_P(A)
k=size(A,1);
s=0;
for i=1:(k-1)
    h=A(i+1,1)-A(i,1);
    ab=A(i,2)+A(i+1,2);
    s=s+(ab*(h/2));
end