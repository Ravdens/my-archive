%sesion 5 : interpolacion

%% Interpolar seno
x = 0:10;
y = sin(x);
p5 = polyfit(x,y,5);
p10 = polyfit(x,y,10);

c5=polyval(p5,x);
c10=polyval(p10,x);
hold on
plot(x,y,"-k",x,c5,"-ro",x,c10,'--bo')
legend("sin(x)","Grado 5","Grado 10")


%% Interpolar 1/(x+1^2)

x = -5:5;
y = 1./(1+x.^2);
p5 = polyfit(x,y,5);
p10 = polyfit(x,y,10);

c5=polyval(p5,x);
c10=polyval(p10,x);
hold on
plot(x,y,"-k",x,c5,"-ro",x,c10,'--bo')
legend("f(x)","Grado 5","Grado 10")

%% Spline


%% Polinomio interpolante de Lagrange

syms x;
x_value = [0 1 2.5 3 4 5 6 7 8 8.75 9 10 11 11.25 11.5];
y_value = [1 2 9 9.2 10 12 14.5 17 20 23 23.5 24 25.5 25.9 25.9];
px = 0;
a = 12;
for i = 1:length(x_value)
    x_xk = 1;
    x0_x1=1;
    for j=1:length(x_value)
        if j~=1
            x_xk=x_xk * (x-x_value(j));
            x0_x1=x0_x1*(x_value(i)-x_value(j));
        end
    end
    px=px+x_xk/x0_x1*y_value(i);
end
disp(px);
px(x)=px;

%% Otro ejercicio

syms x;
x_value = [1 3 5 7 13];
y_value = [800 2310 3090 3940 4755];
px = 0;
a = 12;
for i = 1:length(x_value)
    x_xk = 1;
    x0_x1=1;
    for j=1:length(x_value)
        if j~=1
            x_xk=x_xk * (x-x_value(j));
            x0_x1=x0_x1*(x_value(i)-x_value(j));
        end
    end
    px=px+x_xk/x0_x1*y_value(i);
end
disp(px);
px(x)=px;
plot(x_value,px(x_value))

%% Newton

x_value = [0 3 5 7 10 15 17 19 20 25];
y_value = [6.67 7.41 9.42 17.33 42.67 65.03 50.21 48.32 30.10 20.22];
n=length(x_value);
alpha = zeros(n);
alpha(:,1)=y_value(:);
for j=2:n
    for i=1:n-j+1
        alpha(i,j)=(alpha(i+1,j-1)-alpha(i,j-1))/(x_value(i+j-1)-x_value(i));
    end
end
a=12;
xt=1;
ya=alpha(1,1);
for j=1:n-1
    xt=xt*(a-x_value(j));
    ya=ya+alpha(1,j+i)*xt;
end
