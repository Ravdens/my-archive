function [p,s] = Hermite(A,r)
    k=size(A,1);
    w=zeros(2*k,2*k+1);
    for i=1:k
        w(2*i-1,1)=A(i,1);
        w(2*i-1,2)=A(i,2);
        w(2*i-1,1)=A(i,3);
        w(2*i,1)=A(i,1);
        w(2*i,2)=A(i,2);
    end
    for j=1:k-1
        w(2*j,3)=(w(2*j+1,2)-w(2*j-1,2))/(w(2*j+1,1)-w(j*2-1,1));
    end
    for i=4:2*k+1
        for j=1:(2*k-i+2)
            w(j,i)=(w(j+i,i-1)-w(j,i-1))/(w(j+i-2,1)-w(j,1));
        end
    end
    syms x;
    p=w(1,2);
    for i=3:2*k+1
        m=w(1,i);
        for j=1:i-2
            m=m*(x-w(j,1));
        end
        p=p+m;
    end
    p=simplify(p);
    s=vpa(subs(p,x,r));
end

