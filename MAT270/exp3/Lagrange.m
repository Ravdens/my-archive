function [p,s] = Lagrange(A,r)
    k=size(A,1);
    syms x
    p=0;
    for i=1:k
        L=1;
        for j=1:k
            if i~=j
                L=L*((x-A(j,1))/(A(i,1)-A(j,1)));
            end
        end
        p=p+A(i,2)*L;
    end
    p=simplify(p);
    s=vpa(subs(p,x,r));
end

