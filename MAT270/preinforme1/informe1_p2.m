clear;clc;

format shortG
R=intraclase(5,1/6);
L=chol(R)';
b = [12,-3,4,13,1]';

y=L\b;
x=L'\y;