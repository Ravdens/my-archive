function [xk,r,Tw,k] = iterativo(A,b,x0,n)
    D = diag(diag(A));
    L = tril(A)-D;
    U = triu(A)-D;
    Tj=-inv(D)*(L+U);
    r_i = max(abs(eig(Tj)));
    w = 2/(1+sqrt(1-r_i*r_i));
    Tw = (D+w*L)\((1-w)*D-w*U);
    C = w*(D+w*L)\b;
    xk = x0;
    xkk = C+Tw*xk;
    k = 1;
    while (k<n)
        xk=xkk;
        xkk=C+Tw*xk;
        k=k+1;
    end
    xk=xkk;
    r=max(abs(eig(Tw)));
end

