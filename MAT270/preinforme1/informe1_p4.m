%% Pregunta 1 
clear;clc;
format shortG
d=2+1+2+1;
A=[7,1/d,1,0,0;
    1/d,-12,2,0,0;
    0,2,8,1/3,0;
    0,0,1/3,8,1/2;
    0,0,0,1/2,d];
D = diag(diag(A));
L = tril(A)-D;
U = triu(A)-D;
Tj=-inv(D)*(L+U);
r = max(abs(eig(Tj)));
w = 2/(1+sqrt(1-r*r));
Tw = (D+w*L)\((1-w)*D-w*U);
r2 = max(abs(eig(Tw)));

%% Pregunta 2
format long
b=[12,-14,11,-3,2]';
x0=[0,0,0,0,0]';

[xk_w]=iterativo(A,b,x0,50);

%% Pregunta 3
format long
[xk_j]=Jacobi(A,b,x0,50);