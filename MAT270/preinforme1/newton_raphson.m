function [x_k,iter] = newton_raphson(f,x0,tol)
    iter= 0;
    x_k1 = x0;
    x_k = x_k1 + 2*tol;
    Df = diff(f);
    while(abs(x_k1 - x_k)>tol)
        x_k = x_k1;
        x_k1 = x_k-(f(x_k)/Df(x_k));
        iter = iter+1;
    end
end