function [A] = matriz_A(n,r)
    A = zeros(n,n);
    for i=1:n
       for j=1:n
            A(i,j)=r^(abs(i-j));
       end
    end
end