function [xk,r,M,k] = Gauss_Seidel(A,b,x0,n)
    D = diag(diag(A));
    L = tril(A)-D;
    U = triu(A)-D;
    M = -D\(L+U);
    C = D\b;
    xk = x0;
    xkk = C+M*xk;
    k = 1;
    while (k<n)
        xk=xkk;
        xkk=C+M*xk;
        k=k+1;
    end
    xk=xkk;
    r=max(abs(eig(M)));
end

