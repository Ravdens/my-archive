%% Pregunta 1
clear;clc;
format shortG
M = matriz_M(5,1/6);
D = diag(diag(M));
L = tril(M)-D;
U = triu(M)-D;
Tj=-inv(D)*(L+U);
eig(Tj);
Tg=-inv(L+D)*U;
eig(Tg);

b=[11,-3,4,1,1]';
x0=[0,0,0,0,0]';
[xk_j]= Jacobi(M,b,x0,50);
[xk_g]= Gauss_Seidel(M,b,x0,50);


%% Pregunta 2
clear;clc;
A = matriz_A(5,1/6);
D = diag(diag(A));
L = tril(A)-D;
U = triu(A)-D;
Tj=-inv(D)*(L+U);
eig(Tj);
Tg=-inv(L+D)*U;
eig(Tg);

b=[11,-3,4,1,1]';
x0=[0,0,0,0,0]';
[xk_j]= Jacobi(A,b,x0,50);
[xk_g]= Gauss_Seidel(A,b,x0,50);