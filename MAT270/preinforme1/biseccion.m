function [c] = biseccion(f,a,b,iter)
    c = (a+b)/2;
    while (iter>0)
        if (f(c)*f(b)<0)
           a = c;
        elseif (f(c)*f(a)<0)
          b = c;
        end
        c = (a+b)/2;
        iter = iter-1;
    end
end