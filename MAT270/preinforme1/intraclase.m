function [A] = intraclase(n,r)
    A = zeros(n,n);
    for i=1:n
       for j=1:n
            if (i==j)
                A(i,j) = 1;
            else
                A(i,j) = r;
            end
       end
    end
end

