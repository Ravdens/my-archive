function [M] = matriz_M(n,r)
    M = zeros(n,n);
    for i=1:n
       for j=1:n
            if (i==j)
                M(i,j) = 1;
            elseif (abs(i-j)==1)
                M(i,j) = r;
            else
                M(i,j) = 0;
            end
       end
    end
end