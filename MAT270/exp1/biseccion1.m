function [c,iter,t_end] = biseccion1(f,a,b,tol)
    t_start = cputime; % Da el tiempo actual de acuerdo a la CPU
    c = (a+b)/2;
    iter = 0;
    while (abs(f(c))>tol)
        if (f(c)*f(b)<0)
           a = c;
        elseif (f(c)*f(a)<0)
          b = c;
        end
        c = (a+b)/2;
        iter = iter+1;
    end
    t_end = cputime - t_start;
end