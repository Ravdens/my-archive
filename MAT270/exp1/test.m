
%% Bisección
clear;clc;

syms x
f(x)=(x^2)-4;
[c,iter,t_end]=biseccion1(f,0,3,0.1)

%% Punto fijo
clear;clc;

syms x
f(x)= 1/sqrt((x^2)+2);
[x_k,iter,t_end]=punto_fijo1(f,0,0.1,100)

%% Newton-Rap son
clear;clc;

syms x
f(x)= (x^5)+5*x+8;
[x_k,iter,t_end]=newton_raphson1(f,-2,0.1,1000)

%% EJ
clear;clc;

syms x
d = 2+ 1 + 2 + 1
f(x)= (exp(x))-(x^2)+(3*x)-d;
[x_k,iter,t_end]=newton_raphson1(f,1.5,1e-8,10000)
%Tiene que dar entre 0.9 y 1
