function [ x_k,iter,t_end] = newton_raphson1(f,x0,tol,maxIter)
    t_start = cputime;
    iter= 0;
    x_k1 = x0;
    x_k = x_k1 + 2*tol;
    Df = diff(f);
    while((abs(x_k1 - x_k)>tol)&&(iter<maxIter))
        x_k = x_k1;
        x_k1 = x_k-(f(x_k)/Df(x_k));
        iter = iter+1;
    end
    t_end = cputime - t_start;
end