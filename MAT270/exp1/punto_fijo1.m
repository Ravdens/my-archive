function [x_k, iter, t_end] = punto_fijo1( f,x0,tol,maxIter)
    syms x
    t_start = cputime;
    iter = 0;
    x_k1 = x0;
    x_k = x_k1+2*tol;
    while ((abs(x_k1-x_k)>tol)&&(iter<maxIter))
        x_k = x_k1;
        x_k1 = subs(f,x,x_k);
        iter = iter+1;
    end
    t_end = cputime - t_start;
end