function [x,y]=rungekutta2(f,x0,y0,a,b,n)
    h=(b-a)/n;
    x(1)=x0;
    y(1)=y0;
    for i=1:n
        x(i+1)=x(i)+h;
        k1=h*f(x(i),y(i));
        k2=h*f(x(i+1),y(i)+k1);
        y(i+1)=y(i)+(1/2)*(k1+k2);
    end
end