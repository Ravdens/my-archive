function [x,y] = eulerm(f,x0,y0,a,b,n)
    h=(b-a)/n;
    x(1)=x0;
    y(1)=y0;
    for i=1:n
        x(i+1)=x(i)+h;
        y(i+1)=y(i)+h*f(x(i),y(i));
    end
end

