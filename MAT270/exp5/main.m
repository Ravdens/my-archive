%% testin
syms x y;
f1=@(x,y) x+y;
y1=@(x) 3*exp(x)+1;

x0=0;
y0=2;
a=0;
b=1;
n=10;

[xe,ye]=eulerm(f1,x0,y0,a,b,n);
[xa,ya]=taylor2(f1,x0,y0,a,b,n);
[xr,yr]=rungekutta2(f1,x0,y0,a,b,n);

%% paracaidista
clear
clc
syms t v;

g=9.8;
cd=0.25;
m=90;

a=0;
b=100;
n = (b-a)/(0.1);

f1 = @(t,v) g - (cd/m)*v^2;


[xe,ye]=eulerm(f1,0,0,a,b,n);
