const { DataTypes } = require("sequelize");
const sequelize = require("../db/db.js");
const Task = require("./Tasks.js");
const User = require("./Users.js");

const Observation = sequelize.define(
    "Observations",
    {
        id:{
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        idAutor:{
            type: DataTypes.INTEGER,
        },
        idTask:{
            type: DataTypes.INTEGER,
        },
        content:{
            type: DataTypes.STRING,
        },
    },
    {
        timestamps: false,
    }
);

Observation.belongsTo(User,{
    foreignKey: "idAutor",
    sourceKey: "id",
});

Observation.belongsTo(Task,{
    foreignKey: "idTask",
    targetId: "id",
});

module.exports = Observation;