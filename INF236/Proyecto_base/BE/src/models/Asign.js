const {DataTypes}=require("sequelize");
const sequelize=require("../db/db.js");
const Task = require("./Tasks.js");
const User = require("./Users.js");

const Asign = sequelize.define(
    "Asign",
    {
        userId: {
            type: DataTypes.INTEGER,
        },
        taskId: {
            type: DataTypes.INTEGER,
        },
    },
    {timestamps: false}
);

User.hasMany(Asign,{
    foreignKey: "userId",
    sourceKey: "id",
});

Task.hasMany(Asign,{
    foreignKey: "taskId",
    sourceKey: "id",
});

module.exports = Asign;