const login = require("./routes/login.js");
const userType = require("./routes/userType.js");
const task = require("./routes/task.js");
const observation = require("./routes/observation.js");
const asign = require("./routes/asign.js");

module.exports = (app) => {
  app.use(login);
  app.use(userType);
  app.use(task);
  app.use(observation);
  app.use(asign);
};
