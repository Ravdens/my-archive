const Users = require("../models/Users.js");

const getUsers = async (req, res) => {
  const AllUser = await Users.findAll({});
  res.send(AllUser);
};

//Me imagino que esto es lo suficientemente seguro para un login
const loginUser = async (req, res) => {
  const {email,password} = req.body;
  
  const q = await Users.findOne({
    where :{email:email}
  });
  console.log(q);
  if (!q){
    res.status(200).send({"validated":false});
  }
  else if (q.password==password){
    res.status(200).send({"validated":true});
  }
  else{
    res.status(200).send({"validated":false});
  }
  //console.log(req.body)
  //console.log(q);
  //res.json(body);
};

const createUser = async (req, res) => {
  try {
    const { email, password, typeId } = req.body;
    console.log(typeId);
    if (!email || !password || !typeId) {
      return res.status(400).json({ error: "content missing" });
    }
    const userCreate = await Users.create({
      email,
      password,
      typeId,
    });
    res.json(userCreate).status(200);
  } catch (error) {
    console.log(error);
  }
};
module.exports = { getUsers, loginUser, createUser };
