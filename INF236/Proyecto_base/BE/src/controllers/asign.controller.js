const Asign = require("../models/Asign.js");

const getAsign=async(req,res)=>{
    const AllAsign = await Asign.findAll({});
    res.send(AllAsign);
};
const getAsignByTask=async(req,res)=>{
    const {id} = req.params
    const asign = await Asign.findAll({
        where:{taskId:id}
    });
    res.status(200).json(asign);
};
const getAsignByUser=async(req,res)=>{
    const {id}= req.params;
    const asign = await Asign.findAll({
        where:{userId:id}
    });
    res.status(200).json(asign);
};
const createAsign=async(req,res)=>{
    const {userId,taskId}=req.body;
    if(!userId||!taskId)
    return res.status(400).json({error:"content-missing"});

    const asign = await Asign.create({
        userId,
        taskId,
    });
    res.status(200).json(asign);
};
const deleteAsign=async(req,res)=>{
    const {quserId,qtaskId}=req.params;
    if(!userI||!taskId)
    return res.status(400).json({error:"params-missing"});
    try{
        const asign = await Asign.findOne({
            where:{
                userId:quserId,
                taskId:qtaskId,
            }
        });

        if(!asign)
        return res.status(404).send("Asign not found");

        await asign.destroy();
        res.status(204).json("deteled Asign");
    } catch(error){
        res.status(500);
    }
};

module.exports = {getAsign,getAsignByTask,getAsignByUser,createAsign,deleteAsign}