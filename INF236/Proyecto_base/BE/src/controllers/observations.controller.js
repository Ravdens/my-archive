const Observations = require("../models/Observations.js");

//obtener todas las observaciones
const getObservation = async(req,res)=>{
    const AllObs = await Observations.findAll({});
    res.send(AllObs).status(200);
};

//obtener una observacion especifica
const getOneObservation = async(req,res)=>{
    const qid = req.params;
    const obs = await Observations.findAll({
        where:{id:qid}
    });
    res.status(200).json(obs);
};

const getObservationByTask = async(req,res)=>{
    const {id} = req.params;
    const obs = await Observations.findAll({
        where: {idTask:id}
    });
    res.status(200).json(obs);
}

//crear una observacion
const createObservation = async(req,res)=>{
    const {idAutor, idTask, content} = req.body;

    if(!idAutor||!idTask||!content) 
    return res.status(400).json({error:"content-missing"});

    const obsCreate = await Observations.create({
        idAutor,
        idTask,
        content,
    });
    res.status(200).json(obsCreate);
};

//eliminar observacion
const deleteObeservation = async(req,res)=>{
    const {id} = req.params;
    try{
        const obs = await Observations.findOne({where:{id}});
        
        if(!obs)
        return res.status(404).send("Observation not found");

        await obs.destroy();
        res.status(204).json("deleted Observation");
    } catch(error){
        res.status(500);
    }
};

const putObservation = async(req,res)=>{
    const{id}=req.params;
    const{idAutor,idTask,content} = req.body;
    console.log(req.body);
    try{
        const obs = await Observations.findOne({where:{id}});
        if(!obs){
            return res.status(404).send("Observation not found");
        }
        obs.idAutor = idAutor;
        obs.idTask = idTask;
        obs.content = content;
        await obs.save();
        return res.json(obs);
    } catch(error){
        return res.status(500);
    }
};

module.exports = {getObservation,getOneObservation,getObservationByTask,createObservation,deleteObeservation,putObservation}