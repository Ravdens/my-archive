const express = require("express");
const{
    getAsign,
    getAsignByTask,
    getAsignByUser,
    createAsign,
    deleteAsign,
} = require("../controllers/asign.controller.js");
const router = express.Router();

router.get("/asign",getAsign);
router.get("/asign/task/:id",getAsignByTask);
router.get("/asign/user/:id",getAsignByUser);
router.post("/asign/",createAsign);
//Esto está medio trucho pero funcionaba en BD y creo que tambien aqui se puede
router.delete("/asign/:userId&:taskId",deleteAsign);

module.exports = router;