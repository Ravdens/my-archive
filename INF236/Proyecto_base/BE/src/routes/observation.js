const express = require("express");
const{
    getObservation,
    getOneObservation,
    getObservationByTask,
    createObservation,
    putObservation,
    deleteObeservation,
} = require("../controllers/observations.controller.js");
const router = express.Router();

router.get("/observation",getObservation);
router.get("/observation/:id",getOneObservation);
router.get("/observation/task/:id",getObservationByTask);
router.post("/observation",createObservation);
router.put("/observation",putObservation);
router.delete("/observation/:id",deleteObeservation);

module.exports = router;