import { publicInstance } from "../../axios-instance";

export const login = (data) => {
    return async (dispatch) =>{
        try{
        const res = await publicInstance.post(`login`,data);
        console.log(res.data);
        dispatch(POSTTASK(res.data));
        } catch (error) {
        dispatch({ type: "POST_ERROR_LOGIN", error });
        }
    };
};
const POSTTASK = (data) => ({
    type: "POST_TASK",
    payload: data,
});