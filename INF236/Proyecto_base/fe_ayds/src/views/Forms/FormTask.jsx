import { useState } from "react";
import { useDispatch } from "react-redux";
import { updatedTask } from "../../redux/actionRedux/task";

const FormTask = ({ ...props }) => {
  const dispatch = useDispatch();
  const [title, setTitle] = useState(props.task);
  const [description, setDescription] = useState(props.observation);
  //controlando el evento
  const handleSubmit = (event) => {
    event.preventDefault();
    //acá debemos mandar lo que editamos
    dispatch(
      updatedTask(props.id, { title: title, description: description })
    );
    props.funcion(false);
  };
  const handleTitle = (event) => {
    setTitle(event.target.value);
  };
  const handleDescription = (event) => {
    setDescription(event.target.value);
  };
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div>
          <label className="w-full">Titulo</label>
          <input
            className="border w-full"
            type="text"
            value={title}
            onChange={handleTitle}
          />
          <label className="w-full">Descripcion</label>
          <textarea
            className="border w-full"
            typte="text"
            value={description}
            onChange={handleDescription}
          />
        </div>
        <div className="flex justify-end pt-2">
          <button className="bg-black text-white rounded p-2" type="submit">
            Editar
          </button>
        </div>
      </form>
    </div>
  );
};

export default FormTask;
