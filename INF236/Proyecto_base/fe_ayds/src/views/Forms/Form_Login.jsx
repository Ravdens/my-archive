import React, { useEffect } from 'react';
import { useState } from 'react';
import { useDispatch } from "react-redux";
import { login } from '../../redux/actionRedux/login';
import Button from "../../components/Button";
import Input from "../../components/Input";
import { publicInstance } from '../../axios-instance';

const Form = ({ onIngresar }) => {
  const dispatch = useDispatch();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = (e) =>{
    e.preventDefault();
    dispatch(login({email:email,password:password}))
    .then((response)=>{
      console.log("Arrived");
      //Esto aun sigue retornando undefined y no se por qué
      console.log(response);
      onIngresar()
    });
    console.log("TESTEST");
    //const result = 
    //if (result.validated == true) {
    //  onIngresar();
    //}

    /*
    .then(function (response) {
      // handle success
      const rep = response.json;
      console.log(response)
      console.log(response.json);
      console.log(response.validated);
      onIngresar();
    })
    .catch(function (error){
      console.log(error);
    })
    */
  };

  /*
  async function getLogin(){
    const resp = await publicInstance.post('/login',{email:email,password:password});
    console.log(resp);
    console.log(resp.validated);
  }
  */

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <Input label="email" type="text" value={email} onChange={handleEmailChange} />
        <Input label="contraseña" type="password" value={password} onChange={handlePasswordChange} />
        <Button name="Ingresar" type="submit" />
      </form>
    </div>
  );
};

export default Form;