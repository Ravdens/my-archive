import { useEffect, memo, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { deleteTask, getTask } from "../../redux/actionRedux/task";
import AddTask from "../../components/AddTask";
import Layout from "../../components/Layout";
import Card from "../../components/Card_mec";
import Modal from "../../components/Modal";

//VISTA DE MECANICOS

const Home = memo(() => {
  const dispatch = useDispatch();
  const [openModal, setOpenModal] = useState(false);
  const { task, isLoading, error } = useSelector((state) => state.edit);
  useEffect(() => {
    dispatch(getTask());
  }, []);

  const deleteTaskk = (id) => {
    dispatch(deleteTask(id));
  };

  return (
    <div>
      <Layout>
        <div className="flex py-2 w-full justify-between">
          <h1>Tareas activas</h1>
        
        </div>
        <div className="flex justify-around h-12 m-4 shadow rounded bg-gray">
          <div className="flex items-center w-1/4 px-4">ID</div>
          <div className="w-1/4 p-5">TITULO</div>
          <div className="w-1/2 p-5">DESCRIPCIÓN</div>
          <div className="flex w-1/4 items-center justify-center p-7">
            ACCIONES
          </div>
        </div>
        <div>
          {isLoading ? (
            <p>Loading...</p>
          ) : (
            <>
              {error ? (
                <p>{error.message}</p>
              ) : (
                task?.map((t) => (
                  <div className="m-4" key={t.id}>
                    <Card data={t} dlteTask={deleteTaskk} />
                  </div>
                ))
              )}
            </>
          )}
        </div>
      </Layout>
      <Modal
        isValidate={openModal}
        title={"Agregar nueva tarea"}
        funcion={setOpenModal}
      >
        <AddTask funcion={setOpenModal} />
      </Modal>
    </div>
  );
});

export default Home;