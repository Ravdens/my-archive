import { useEffect, memo, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { deleteTask, getTask } from "../../redux/actionRedux/task";
import AddTask from "../../components/AddTask";
import Layout from "../../components/Layout";
import Card from "../../components/Card_jefe";
import Modal from "../../components/Modal";
import Incidencias from "../../components/incidencias";

//VISTA DE JEFE DE MECANICOS

const Home = memo(() => {
  const dispatch = useDispatch();
  const [openModal, setOpenModal] = useState(false);
  const [openIncidencias, setOpenIncidencias] = useState(false);
  const { task, isLoading, error } = useSelector((state) => state.edit);
  useEffect(() => {
    dispatch(getTask());
  }, []);

  const deleteTaskk = (id) => {
    dispatch(deleteTask(id));
  };

  const handleOpenIncidencias = () => { 
    //Logica para ver las incidencias
    setOpenIncidencias(true);
  };

  const handleCloseIncidencias = () => {
    //Logica para ver las incidencias
    setOpenIncidencias(false);
  };

  return (
    <div>
      <Layout>
        <div className="flex py-2 w-full justify-between">
          <h1>Tareas activas</h1>
          <button
            className="bg-white text-black hover:bg-overlay rounded p-4 ease-in duration-200 mr-4"
            onClick={() => setOpenModal(true)}
          >
            Tarea nueva
          </button>

          <button
            className="bg-white text-black hover:bg-overlay rounded p-4 ease-in duration-200"
            onClick={handleOpenIncidencias}
          >
            Incidencias
          </button>
        </div>
        <div className="flex justify-around h-12 m-4 shadow rounded bg-gray">
          <div className="flex items-center w-1/4 px-4">ID</div>
          <div className="w-1/4 p-5">TITULO</div>
          <div className="w-1/2 p-5">DESCRIPCIÓN</div>
          <div className="flex w-1/4 items-center justify-center p-7">
            ACCIONES
          </div>
        </div>
        <div>
          {isLoading ? (
            <p>Loading...</p>
          ) : (
            <>
              {error ? (
                <p>{error.message}</p>
              ) : (
                task?.map((t) => (
                  <div className="m-4" key={t.id}>
                    <Card data={t} dlteTask={deleteTaskk} />
                  </div>
                ))
              )}
            </>
          )}
        </div>
      </Layout>
      <Modal
        isValidate={openModal}
        title={"Agregar nueva tarea"}
        funcion={setOpenModal}
      >
        <AddTask funcion={setOpenModal} />

      </Modal>
      {openIncidencias && (
        <Modal
          isValidate={openIncidencias}
          title={"Incidencias"}
          funcion={handleCloseIncidencias}
        >
          <Incidencias />
        </Modal>
      )}
    </div>
  );
});

export default Home;

