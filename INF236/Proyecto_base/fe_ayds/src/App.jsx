import React, { useState } from 'react';
import Navbar from "./components/Navbar";
//Cambiar Home_mec por Home_jefe si se quiere probar la vista de jefe
import Home from "./views/Pages/Home_mec";
import Form  from "./views/Forms/Form_Login";

function App() {
  const [showForm, setShowForm] = useState(true);
  const [showHome, setShowHome] = useState(false);
  const [showNavbar, setShowNavbar] = useState(false);

  const handleIngresar = () => {
    setShowForm(false);
    setShowHome(true);
    setShowNavbar(true);
  };

  const handleCerrarSesion = () => {
    setShowForm(true);
    setShowHome(false);
    setShowNavbar(false);
  };

  return (
    <>
      {showForm ? (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
          <Form onIngresar={handleIngresar} />
        </div>
      ) : null}
      {showNavbar ? <Navbar handleCerrarSesion={handleCerrarSesion} /> : null}
      {showHome ? <Home /> : null}
    </>
  );
}

export default App;
