import { useState } from "react";
import { useDispatch } from "react-redux";
import { postTask } from "../redux/actionRedux/task";
import obsList from "./obsList";

const TaskDetails = ({ showTaskDetails, setShowTaskDetails, dlteTask, id }) => {
  
  const [description_final, setDescription_final] = useState("");
  const [description_obs, setDescription_obs] = useState("");
  const [showNewComponent, setShowNewComponent] = useState(false);
  const [componentLabel, setComponentLabel] = useState("");
  const [componentQuantity, setComponentQuantity] = useState("");
  const [showOtherComponents, setShowOtherComponents] = useState(true);
  const [showFinalObservation, setShowFinalObservation] = useState(false);
  const [showOriginalComponents, setShowOriginalComponents] = useState(false);
  const [showAddComponentButton, setShowAddComponentButton] = useState(true);
  const [showNewObservation, setShowNewObservation] = useState(false);

  const handleCloseTask = () => {
    //Modifica la ventana para que quede con la vista para cerrar la tarea
    setShowOtherComponents(false);
    setShowFinalObservation(true);
    setShowAddComponentButton(false);
    setShowNewObservation(false);
  };

  const handleFinish = () => {
    //Logica para terminar la tarea
    dlteTask(id);
    setShowTaskDetails(false);
  };

  const handleCancel = () => {
    //Logica para cancelar la modificacion
    setShowFinalObservation(false);
    setShowOtherComponents(true);
    setShowOriginalComponents(false);
    setShowAddComponentButton(true);
    setShowNewObservation(false);
  };

  const handleOpenComponent = () => {
    //Modifica la ventana para que quede con la vista para Agregar un componente
    setShowNewComponent(true);
    setShowOtherComponents(false);
    setShowNewObservation(false);
  };

  const handleAddComponent = () => {
    // Lógica para agregar el componente
    setShowNewComponent(false);
    setShowOtherComponents(true);
    setShowNewObservation(false);
  };

  const handleCancelComponent = () => {
    //Logica para cancelar la modificacion (usada para agregar componentes y agregar observacion)
    setShowNewComponent(false);
    setShowOtherComponents(true);
    setShowNewObservation(false);
  };

  const handleAddObservation = () => {
    ////Modifica la ventana para que quede con la vista para añadir una observacion
    setShowNewObservation(true);
    setShowOtherComponents(false);
    setShowNewComponent(false);
  };

  const handleAdd = () => {
    //Logica para añadir la observacion
    setShowFinalObservation(false);
    setShowOtherComponents(true);
    setShowOriginalComponents(false);
    setShowAddComponentButton(true);
    setShowNewObservation(false);
  };

  if (!showTaskDetails) {
    return null; // No mostrar TaskDetails si showTaskDetails es falso
  }

  return (
    <div className="w-full">

      {showOtherComponents && (
        <>
          <label className="w-full">Nombre de la tarea: </label> <br />

          <label className="w-full">Fecha de inicio: </label> <br />

          <label className="w-full">Maquinaria a trabajar: </label> <br />

          <label className="w-full">¿Es urgente?: </label> <br />

          <label className="w-full">Descripción de la tarea</label> <br />

          <label className="w-full">Trabajadores asignados</label> <br />

          <label className="w-full">Piezas utilizadas</label> <br />

          <label className="w-full">Observaciones</label> <br />
          <obsList /> <br />

          <button
            className="pointer bg-red hover:bg-red-300 text-white w-24 h-8 rounded"
            onClick={handleCloseTask}
          >
            Cerrar tarea
          </button>
          <button
            className="pointer hover:bg-black hover:text-white w-24 h-8 rounded"
            onClick={handleAddObservation}
          >
            Agregar observación
          </button>
        </>
      )}


      {showFinalObservation && (
        <>
          <label>Observación final:</label> <br />
          <textarea
            value={description_final}
            onChange={(e) => setDescription_final(e.target.value)}
            className="w-full"
          ></textarea>
          <br />
          <button
            className="pointer hover:bg-black hover:text-white w-24 h-8 rounded"
            onClick={handleFinish}
          >
            Terminar
          </button>
          <button
            className="pointer hover:bg-black hover:text-white w-24 h-8 rounded"
            onClick={handleCancel}
          >
            Cancelar
          </button>
        </>
      )}

      {showNewComponent ? (
        <div>
          <label>Componente:</label>
          <br />
          <select
            value={componentLabel}
            onChange={(e) => setComponentLabel(e.target.value)}
          >
            <option value="option1">Opción 1</option>
            <option value="option2">Opción 2</option>
            <option value="option3">Opción 3</option>
          </select>
          <br />

          <label>Cantidad:</label>
          <br />
          <input
            type="number"
            value={componentQuantity}
            onChange={(e) => setComponentQuantity(e.target.value)}
          />
          <br />

          <button
            className="pointer hover:bg-black hover:text-white w-24 h-8 rounded"
            onClick={handleAddComponent}
          >
            Agregar
          </button>

          <button
            className="pointer hover:bg-black hover:text-white w-24 h-8 rounded"
            onClick={handleCancelComponent}
          >
            Cancelar
          </button>
        </div>
      ) : (
        showAddComponentButton && (
          <button
            className="pointer hover:bg-black hover:text-white w-24 h-8 rounded"
            onClick={handleOpenComponent}
          >
            Agregar componente
          </button>
        )
      )}

      {showNewObservation && (
        <>
          <label>Nueva observación:</label> <br />
          <textarea
            value={description_obs}
            onChange={(e) => setDescription_obs(e.target.value)}
            className="w-full"
          ></textarea>
          <br />
          <button
            className="pointer hover:bg-black hover:text-white w-24 h-8 rounded"
            onClick={handleAdd}
          >
            Agregar
          </button>
          <button
            className="pointer hover:bg-black hover:text-white w-24 h-8 rounded"
            onClick={handleCancel}
          >
            Cancelar
          </button>
        </>
      )}
    </div>
  );
};

export default TaskDetails;

