import { useState } from "react";
import { useDispatch } from "react-redux";
import { postTask } from "../redux/actionRedux/task";
import SpinButton from "./spin";
import Dropdown from "./dropdown_maq";
import Spinspecial from "./spinspecial";
import UrgencyCheckbox from "./urgency";


const AddTask = ({ funcion }) => {
  const [description, setDescription] = useState("");
  const [title, setTitle] = useState("");
  
  const dispatch = useDispatch();

  const handleSubmit = (event) => {
    event.preventDefault();
    dispatch(postTask({ title: title, description }));
    funcion(false);
  };

  const handleTitle = (event) => {
    setTitle(event.target.value);
  };
  const handleDescription = (event) => {
    setDescription(event.target.value);
  };
  
  return (
    <div className="w-full">
      <form onSubmit={handleSubmit}>
        <label className="w-full">Titulo</label>
        <input className="w-full border" onChange={handleTitle} />
        <label className="w-full">Descripción de la tarea</label>
        <textarea className="w-full border" onChange={handleDescription} />
        <label className="w-full">Maquinaria a trabajar</label>
        <Dropdown />
        <label className="w-full">Mecanicos</label>
        <Spinspecial />
        <UrgencyCheckbox />

        <div className="flex justify-end pt-2">
          <button className="bg-black text-white rounded p-2" type="submit">
            Guardar
          </button>
        </div>
      </form>
    </div>
  );
};

export default AddTask;
