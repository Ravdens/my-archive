import { useState } from "react";
import { useDispatch } from "react-redux";
import { postTask } from "../redux/actionRedux/task";
import obsList from "./obsList";

const TaskDetails = ({ funcion }) => {
  const [description, setDescription] = useState("");
  const [title, setTitle] = useState("");
  
  const dispatch = useDispatch();

  const handleSubmit = (event) => {
    event.preventDefault();
    dispatch(postTask({ title: title, description }));
    funcion(false);
  };

  const handleTitle = (event) => {
    setTitle(event.target.value);
  };
  const handleDescription = (event) => {
    setDescription(event.target.value);
  };
  
  return (
    <div className="w-full">
        <label className="w-full">Nombre de la tarea: </label>

        <label className="w-full">Fecha de inicio: </label>

        <label className="w-full">Maquinaria a trabajar: </label>

        <label className="w-full">¿Es urgente?: </label>

        <label className="w-full">Descripción de la tarea</label>
        
        <label className="w-full">Trabajadores asignados</label>

        <label className="w-full">Piezas utilizadas</label>

        <label className="w-full">Observaciones</label>
        <obsList />
    </div>
  );
};

export default TaskDetails;

    