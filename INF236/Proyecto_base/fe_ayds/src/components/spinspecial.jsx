//Spin button para numero de mecanicos asignados y tantos menus desplegables (con mecanicos para trabajar) segun indique el Spin Button

import React, { useState } from 'react';

function SpinButton() {
  const [count, setCount] = useState(0);
  const [selectedOptions, setSelectedOptions] = useState([]);

  const handleCountChange = (event) => {
    setCount(parseInt(event.target.value));
    setSelectedOptions([]);
  };

  const handleOptionChange = (index, event) => {
    const updatedOptions = [...selectedOptions];
    updatedOptions[index] = event.target.value;
    setSelectedOptions(updatedOptions);
  };

  const renderDropdowns = () => {
    const dropdowns = [];
    for (let i = 0; i < count; i++) {
      dropdowns.push(
        <div key={i}>
          <select value={selectedOptions[i] || ''} onChange={(e) => handleOptionChange(i, e)}>
            <option value="">Selecciona una opción</option>
            <option value="opcion1">Opción 1</option>
            <option value="opcion2">Opción 2</option>
            <option value="opcion3">Opción 3</option>
          </select>
        </div>
      );
    }
    return dropdowns;
  };

  return (
    <div>
      <input type="number" min={0} value={count} onChange={handleCountChange} />
      {renderDropdowns()}
    </div>
  );
}

export default SpinButton;
