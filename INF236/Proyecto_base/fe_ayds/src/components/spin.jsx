//Spin button generico
import React, { useState } from 'react';

function SpinButton() {
  const [value, setValue] = useState(0);

  const handleIncrement = () => {
    setValue(value + 1);
  };

  const handleDecrement = () => {
    setValue(value - 1);
  };

  return (
    <div>
      <input type="number" min={0} value={value} onChange={(e) => setValue(parseInt(e.target.value))} />
      <button onClick={handleIncrement}>+</button>
      <button onClick={handleDecrement}>-</button>
    </div>
  );
}

export default SpinButton;
