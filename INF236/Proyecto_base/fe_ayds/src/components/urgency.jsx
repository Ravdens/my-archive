//Palomilla para marcar si una tarea es urgente o no

import React, { useState } from 'react';

function UrgencyCheckbox() {
  const [isUrgent, setIsUrgent] = useState(false);

  const handleCheckboxChange = (event) => {
    setIsUrgent(event.target.checked);
  };

  return (
    <div>
        <input type="checkbox" checked={isUrgent} onChange={handleCheckboxChange} />
        ¿Es urgente?
    </div>
  );
}

export default UrgencyCheckbox;
