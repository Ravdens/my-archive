import { useState } from "react";
import { useDispatch } from "react-redux";
import { postTask } from "../redux/actionRedux/task";
import SpinButton from "./spin";
import Dropdown from "./dropdown_maq";
import Spinspecial from "./spinspecial";
import UrgencyCheckbox from "./urgency";
//Ventana que muestra las incidencias

const AddIncidences = ({ funcion }) => {
  const [description, setDescription] = useState("");
  const [title, setTitle] = useState("");
  
  const dispatch = useDispatch();

  const handleSubmit = (event) => {
    event.preventDefault();
    dispatch(postTask({ title: title, description }));
    funcion(false);
  };

  const handleTitle = (event) => {
    setTitle(event.target.value);
  };
  const handleDescription = (event) => {
    setDescription(event.target.value);
  };
  
  
  return (
    <div className="w-full">
      <form onSubmit={handleSubmit}>
      <label className="w-full">Maquinaria: </label><Dropdown />
      </form>
    </div>
  );
};

export default AddIncidences;