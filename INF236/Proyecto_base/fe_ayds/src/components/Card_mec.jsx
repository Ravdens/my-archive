import { useState } from "react";
import Button from "./Button";
import Modal from "./Modal";
import FormTask from "../views/Forms/FormTask";
import ObsList from "./obsList";
import TaskDetails from "./TaskDetails_mec";

const Card = ({ data, dlteTask }) => {
  const { id, title, description } = data;
  const [valor, setValor] = useState(false);
  const [showTaskDetails, setShowTaskDetails] = useState(false);

  return (
    <div>
      <div className="flex justify-around h-16 shadow rounded bg-magnolia">
        <div className="flex items-center w-1/4 px-4">{id}</div>
        <div className="w-1/4 p-5">{title}</div>
        <div className="w-1/2 p-5">{description}</div>
        <div className="flex w-1/4 items-center justify-end p-7">
          <div className="px-3">

            <button
              className="pointer hover:bg-black hover:text-white w-24 h-8 rounded"
              onClick={() => setShowTaskDetails(true)}
            >
              Detalles
            </button>
          </div>
          <div className="px-3">
            <button
              className="pointer hover:bg-black hover:text-white w-24 h-8 rounded"
              onClick={() => setValor(true)}
            >
              Editar
            </button>
          </div>
          <div className="px-3">
            <button
              className="pointer bg-red hover:bg-red-300 text-white w-24 h-8 rounded"
              onClick={() => dlteTask(id)}
            >
              Eliminar
            </button>
          </div>
        </div>
      </div>
      <Modal isValidate={valor} title={title} id={id} funcion={setValor}>
        <FormTask
          id={id}
          observation={description}
          task={title}
          funcion={setValor}
        />
      </Modal>
      <Modal
        isValidate={showTaskDetails}
        title="Detalles de la tarea"
        funcion={setShowTaskDetails}
      >
        <TaskDetails
          showTaskDetails={showTaskDetails}
          setShowTaskDetails={setShowTaskDetails}
          dlteTask={dlteTask}
          id={id}
        />
      </Modal>
    </div>
  );
};

export default Card;