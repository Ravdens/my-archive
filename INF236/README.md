# Grupo 06

Este es el repositorio del *Grupo 6*, cuyos integrantes son:

* Sergio Ehlen Montero - 202130016-3
* Camilo Troncoso Hormazabal - 202130004-k
* Maximo Flores Suarez - 202130019-8
* **Tutor**: Alondra

## Wiki

Puede acceder a la Wiki mediante el siguiente [enlace](https://gitlab.inf.utfsm.cl/alaraya/inf236-2023-1-par001-grupo06/-/wikis/home)

## Videos

* [Video presentación cliente](https://youtu.be/zk1zjYb6IAw)
* [Video presentación avance 1](https://www.youtube.com/watch?v=nveGxBur1f0)
* [Video presentación avance 4](https://youtu.be/XvKyx23HNlU)

## Aspectos técnicos relevantes

_Todo aspecto relevante cuando para poder usar el proyecto o consideraciones del proyecto base a ser entregado_
