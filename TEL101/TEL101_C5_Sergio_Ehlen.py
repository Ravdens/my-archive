from matplotlib import pyplot as plt
from matplotlib.font_manager import FontProperties

def leeLista(L): #Funcion que me gusta usar para leer facilmente listas
    print("\n")
    for x in L:
        print(x)
    print("\n")

#Función del profe
def leer_datos():
    try:
        archivo=open("ventas_de_la_compania.csv")
    except OSError:
        print("Archivos: ventas_de_la_compania.csv no existe.")
        return []
    else:
        nombres=[]
        ut_mes = [] #utilidades por mes (1)
        num_prod_mes=[] #listas de cantidades mensuales por producto (3)   
        num_prod=[] #total vendidos, por producto (4)
        primera = True
        for linea in archivo:
            linea = linea.strip()
            if primera: #Extraer nombres de productos
                primera = False
                nombres = linea.split(",")
                nombres = nombres[1:len(nombres)-1]
                continue
            valores = linea.split(",")
            ut_mes.append(float(valores.pop())) #utilidad mensual, sacada
            valores = valores[1:] #elimina número de mes           
            if num_prod_mes == []: #Primer mes por producto
                for valor in valores:
                    v = float(valor)
                    num_prod_mes.append([v])
                    num_prod.append(v) #Primer valor del acumulado
            else:
                for i in range(len(valores)):
                    v = float(valores[i])
                    num_prod_mes[i].append(v)
                    num_prod[i] += v #Acumula cantidad
        archivo.close()

        return [nombres,ut_mes,num_prod_mes,num_prod]



bigL=leer_datos() #Big L es mi lista principal de datos
#[nombres,ut_mes,num_prod_mes,num_prod] Formato de bigL
#leeLista(bigL) #Para leer la lista
meses=list(range(1,13)) #meses solo es una lista con los enteros de 1 a 12
fig=plt.figure()
fig.subplots_adjust(hspace=0.25,wspace=0.2,left=0.1,right=0.95,bottom=0.1,top=0.87)
fig.suptitle("Infografía de Ventas PyCharm")
fontP = FontProperties()
fontP.set_size("small")

#Gráfico 1
ax1=fig.add_subplot(1,3,1)
ax1.plot(meses,bigL[1],"o--r", markerfacecolor="black",linewidth=2) #Creo la linea en el formato pedido
ax1.grid(linestyle="--") #Agrego el grid de fondo

#Nombres grafico 1
ax1.set_title("Utilidades Totales Mensuales")
ax1.set_ylabel("Utilidades")
ax1.set_xlabel("Mes")

#Gráfico 2
#Lineas es un diccionario para dar un color y forma especifica a la liena de cada producto
lineas={"crema facial":["o-","lightblue"],
    "limpieza facial":["*-","pink"] ,
    "pasta dientes":["v-","red"],
    "jabon":["p-","blue"] ,
    "shampoo" :["D-","green"],
    "balsamo":["2-","orange"]}

ax2=fig.add_subplot(1,3,2)
for nombre in bigL[0]:
    index=bigL[0].index(nombre)
    ax2.plot(meses,bigL[2][index],lineas[nombre][0],color=lineas[nombre][1],label=nombre,markerfacecolor="white")#Uso el diccionario para el código y color de cada linea
ax2.legend(prop=fontP)
ax2.grid(linestyle="--")

#Nombres gráfico 2
ax2.set_title("Unidades Vendidas por Producto (mensual)")
ax2.set_ylabel("Unidades")
ax2.set_xlabel("Mes")

#Gráfico 3
ax3=fig.add_subplot(1,3,3)
indmayor=bigL[3].index(max(bigL[3])) #Indice del mayor lo extraigo usando la funcion max() y la funcion index()
exp=[0]*len(bigL[0]) #Creo una lista de cero para explode
exp[indmayor]=0.1 #Remplazo el indice del dato mayor por un 0.1
ax3.pie(bigL[3],labels=bigL[0],autopct="%1.2f%%",shadow=True,startangle=90,explode=exp)

#Nombres gráfico 3
ax3.set_title("Porcentaje de unidades del total")

plt.show() #Mostrar gráfico