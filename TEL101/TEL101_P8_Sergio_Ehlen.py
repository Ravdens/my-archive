#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from tkinter import *

#Implemente las funciones acá:

def generateObjectDict():
    newDict={} #Diccionario nuevo
    try:
        file=open("detected_objects.txt","r") #Abro el archivo
    except FileNotFoundError:
        print("Archivo no encontrado")
        raise
    for linea in file:
        linea=linea.strip("\n")
        linea=linea.split(",") #Convierto en lista ordenada cada linea
        #La agrego al diccionario con el formato pedido y transformo los datos en enteros
        newDict[(int(linea[0]),int(linea[1]))]=[int(linea[2]),int(linea[3]),linea[4]]
    file.close()
    return newDict

def getMaxWH(D):
    maxX=0 #Inicialiso ambos maximos en 0
    maxY=0
    for key in D: #Uso un for para hacer comparaciones tipo bubble-sort para X e Y
        if key[0]+D[key][0]>maxX: #key[0]+D[key][0] me da el punto mas lejano de ese rectangulo en X
            maxX=key[0]+D[key][0] #Si es mayor al que ya está, remplazo
        if key[1]+D[key][1]>maxY: #Lo mismo para Y
            maxY=key[1]+D[key][1]
    return (maxX,maxY) #Retorno la tupla

#Código pre-generado (implemente funciones antes):
master = Tk()
master.title("Objetos Detectados")
D = generateObjectDict()
W, H = getMaxWH(D)
w = Canvas(master, width=W, height=H, bg="white")
w.pack()

#Implementar generación de rectángulos acá:

colores={"auto":"red","moto":"green","camion":"blue"}
#Uso un diccionario para asignar los colores a los vehiculos
for key in D: 
    vehiculo=D[key][2] #Saco el dato del vehiculo
    w.create_rectangle(key[0],key[1],key[0]+D[key][0],key[1]+D[key][1],outline=colores[vehiculo])
#Llamado de funcion con el formato y usando los diccionarios.



#Muestra la aplicación y espera por eventos:
mainloop()

