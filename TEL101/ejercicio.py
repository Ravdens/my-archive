precios = {"platano": 4,"manzana": 2,"naranja": 1.5,"pera": 3}
stock = {"platano": 6,"manzana": 0,"naranja": 32,"pera": 15}

def computarCuenta(lista):
    total=0
    try:
        for producto in lista:
            if producto in precios and stock[producto]>0:
                total+=int(precios[producto])
                stock[producto]-=1
        return total
    except TypeError:
        print("El tipo de dato ingresado es incorrecto")
        return 0

