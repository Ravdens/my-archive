#Control 3 - 2021
def leeInmuebles():   #40 puntos en total 
    base = [] #5 pts, estructura de la función, inicializaciones y retornos.
    try: #5 pts, manejo de try-except
        #Linux:
        f = open('BD-Inmuebles.csv', 'r', encoding='iso-8859-15') #5 pts, manejo de apertura y cierre
        #Windows
        #f=open("BD-Inmuebles.csv", 'r')

        #Almacena registros del tipo: [Provincia, Superficie, Precio Venta]
        # si corresponde a una venta
        #15 pts, manejo de flujo de lectura del archivo.
        first = True
        for line in f: #Lee una línea a la vez
            if first:
                first = False
                continue
            sp = line.split(",")
            if sp[3] == "Venta": #10 pts, extracción de información relevante desde el archivo
                base.append([sp[4],int(sp[5]),int(sp[6])])
        f.close()
    except OSError:
        print("Error: No se puede abrir el archivo para lectura: BD-Inmuebles.csv")
    return base

#Ordena información por provincia: provincia, num_ops, sum_venta, sum_superficie] y genera reporte
def generarReporteProvincias(archivo_reporte): #60 pts en total.
    #4 pts, estructura de la función.
    L = leeInmuebles() #4 pts de llamado de función existente
    #4 pts, definición de listas necesarias e inicializaciones
    provincias = []
    num_ops = []
    sum_venta = []
    sum_superficie = []
    
    #22 pts manejo de consolidación de información de provincias
    for registro in L: #Lee un registro a la vez
        if registro[0] in provincias: #Provincia ya registrada
            index = provincias.index(registro[0])
            num_ops[index] += 1         #Nueva operación
            sum_venta[index] += registro[2]      #Agrego ganancia
            sum_superficie[index] += registro[1] #Agrego Superficie
        else:
            provincias.append(registro[0])   #Nueva provincia
            num_ops.append(1)                       #Operación inicial
            sum_venta.append(registro[2])           #Ganancia inicial
            sum_superficie.append(registro[1])      #Superficie inicial
 
        for i in range(len(provincias)):
            provincias[i]
            int(num_ops[i])
            int(sum_venta[i])
            int(sum_superficie[i])

    return [provincias,num_ops,sum_venta,sum_superficie]

#Llamar función:
print(generarReporteProvincias("reporte.txt")) #5 pts, llamado a función de reporte


