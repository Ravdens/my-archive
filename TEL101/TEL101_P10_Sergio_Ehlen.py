from matplotlib import pyplot as plt

def leeInmuebles(): 
    base = []
    try: 
        f=open("BD-Inmuebles.csv", 'r')
        first = True
        for line in f:
            if first:
                first = False
                continue
            line = line.strip("\n")
            sp = line.split(",")
            if sp[3] == "Venta" and sp[8]!="": #Solo quité strings vacios
                base.append([sp[8],int(sp[5]),int(sp[6])]) #Y cambié el primer dato de la lista
        f.close()
    except OSError:
        print("Error: No se puede abrir el archivo para lectura: BD-Inmuebles.csv")
    return base

def generarListVend(): 
    L = leeInmuebles() 

    vendedores = [] #Cambié provincias por vendedores
    num_ops = []
    sum_venta = []
    sum_superficie = []

    for registro in L: 
        if registro[0] in vendedores: 
            index = vendedores.index(registro[0])
            num_ops[index] += 1   
            sum_venta[index] += registro[2]
            sum_superficie[index] += registro[1] 
        else:
            vendedores.append(registro[0]) 
            num_ops.append(1)                      
            sum_venta.append(registro[2])         
            sum_superficie.append(registro[1])    
 
    for i in range(len(vendedores)):
        int(num_ops[i])
        int(sum_venta[i])
        int(sum_superficie[i])
    #En vez de escribir el reporte me retorna una lista con listas de datos
    return [vendedores,num_ops,sum_venta,sum_superficie]

def leeLista(L): #Funcion que me gusta usar para leer facilmente listas
    print("\n")
    for x in L:
        print(x)
    print("\n")



#Main
print(leeInmuebles)
ListVend=generarListVend()
leeLista(ListVend) #Para acceso facil a la lista a usar
fig=plt.figure()

#Primer grafico de barra
ax1 = fig.add_subplot(2,2,1)
ax1.set_title("Operaciones por vendedor")
ax1.set_ylabel("Número de operaciones")
ax1.set_xlabel("Vendedor")
ax1.bar(ListVend[0],ListVend[1],tick_label=ListVend[0])

#Segundo grafico de barra
ax2 = fig.add_subplot(2,2,2)
ax2.set_title("Dinero ganado por vendedor")
ax2.set_ylabel("Ganancias")
ax2.set_xlabel("Vendedor")
ax2.bar(ListVend[0],ListVend[2],tick_label=ListVend[0],color="lightgreen")

#Gráfico de barra
ax3 = fig.add_subplot(2,1,2)
ax3.set_title("Superficie por vendedor")

ex=[] #Lista necesaria para alejar las parte de la torta
for name in ListVend[0]:
    ex.append(0.1)

ax3.pie(ListVend[3],labels=ListVend[0],autopct="%1.2f%%",shadow=True,startangle=180,explode=ex)

plt.show()