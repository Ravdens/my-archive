def dias(f):
    a = int(f[:4])
    m = int(f[4:6])
    d = int(f[6:])
    dd = (a-1)*365 + (m-10)*30 + d
    #print(dd)
    return dd

def dif(f1,f2):
    d1 = dias(f1)
    d2 = dias(f2)
    #print(d1,d2)
    r= abs(d1-d2)
    #print(r)
    return r

actual = input("Fecha actual: ")
cumple = input("Fecha de nacimiento: ")
d = dif(actual,cumple)
print(d)
a = d//365
m = (d-a*365)//30
d = d%30
print(a,' ',m,' ',d)
print("Usted tiene",a,"años, con",m,"meses y",d,"días")