#Sumatoria i=1 n=61 (i+3)


total=0 #El valor final
contador=1 #El valor de i


while contador<=61: #se detiene al llegar a 61
    print(f"i ahora mismo es: {contador}") #Decimos el valor de i
    total+=(contador+3) #Realizamos la expresión
    contador+=1 #Aumentamos i para la siguiente vuelta

print(f"El total de la sumatoria es {total}")



total=0 #El valor final

for i in range(1,62): #Usando for i in range 
    print(f"i ahora mismo es: {i}")
    total+=(i+3) #i toma cada valor en el rango del 1 al 62


print(f"El total de la sumatoria es {total}")