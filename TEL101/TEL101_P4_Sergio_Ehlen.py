'''
Created on Wed May 12 22:53:39 2021

@author: mzuniga

Instale pynput y random con pip: 
    pip install pynput
    pip install random
'''

from pynput import keyboard
from random import randint, shuffle, randrange
'''
variables globales:
'''
LaberintoA = []
posh=0
posw=0
pasos=0
posPP=[]
LaberintOculto=[]

'''
Funciones
'''

def EsconderLaberinto(laberintoC):  #Uso esta funcion para generar el laberinto *** que se le muestra al jugador.
    otrocontador=-1
    for i in laberintoC:        #Dentro de esta uso 2 iteradores FOR para acceder y validar cada casilla
        otrocontador+=1         #Los contadores me serviran para evitar alterar las de los bordes del laberinto
        if otrocontador==0 or otrocontador==(len(laberintoC)-1):
            continue
        contador=-1
        for x in i:
            contador+=1
            if contador==0:                 #Tengo todo un sistema de ifs diseñado para poder alterar la pared izquierda sin borrar la pared misma.
                if i[contador]=="+--":      #(esto no es necesario para la pared derecha pues está dentro de su propia celda)
                    i[contador]="+**"
                elif i[contador]=="|  ":
                    i[contador]="|**"
                elif i[contador]=="+  ":
                    i[contador]="+**"
                elif i[contador]=="|PP":
                    continue
                else:
                    i[contador]="***"

            elif contador==(len(i)-1):
                continue

            elif x==" PP" or x=="|PP":      #Excepción para no cubrir PP.
                continue
            else:
                i[contador]="***"           #Esconder celdas
    return laberintoC

def GenerarPP_TT(w,h,laberinto,PPTT,lado): #Esta funcion me dara las posiciones de PP y TT, entrando un string que puede ser "PP" o "TT"
    while True:                             #Estará en un While para probar con distintas posiciones hasta encontrar una que sea valida.
        if lado==0: #arriba                 #Tambien recibirá un valor entre [0-3] el cual indicara en cual lado del laberinto debe estar.
            posh=1
            posw=randint(0,w-1)

        elif lado==1: #abajo
            posh=2*(h-1)+1
            posw=randint(0,w-1)

        elif lado==2: #izquierda
            posh=2*(randint(0,h-1))+1
            posw=0

        elif lado==3: #derecha
            posh=(2*randint(0,h-1))+1
            posw=w-1

        if laberinto[posh][posw]=="   ":        #Se ubica PP o TT en una celda vacia
            laberinto[posh][posw]=(" "+PPTT)    #La suma de strings da " PP"
            return [posh,posw]

        elif laberinto[posh][posw]=="|  ":      #Se ubica PP o TT en una celda con una pared (|  )
            laberinto[posh][posw]=("|"+PPTT) 
            return [posh,posw]

def imprimirLaberinto(L):                   #Ipmrime el laberinto de forma ordenada
    s = ""
    for fila in L:
        for col in fila:
            s += col
        s += "\n"
    return s

def copiarLaberinto(laberintoE):            #Permite copiar el laberinto. Será necesario para tener una copia escondida y otra no del laberinto.
    L=[]                                    #Sobrescribir con un = no seviría pues solo me daría el puntero del laberinto y no el laberinto mismo.
    for i in laberintoE:
        newlist=i.copy()
        L.append(newlist)
    return L

def on_press(key):         #Funcion del profe, valida si la tecla entrada es valida o si es ESC para cerrar detección de teclado.
    if jugando==False:      #jugando será falso cuando el juego se haya terminado y por tanto se deshabilitan los controles
        return False
    if key == keyboard.Key.esc: #Si apreta Esc sale del juego
        return False  # Detiene listener de teclas
    try:
        k = key.char  # Teclas de un caracter
    except:
        k = key.name  # Teclas especiales
    if k in ['s', 'w', 'a', 'd']:  # Teclas de interés  
        Mover(k)
        return True #Si recibe True, el listener continúa

def Mover(k):           #La función principal del código, permite cambiar la posición de PP no solo como se muestra en el laberinto, pero ademas su posicion 
    global posPP        #registrada globalmente (En este caso llamada posPP)
    global LaberintoA
    global LaberintOculto
    posh=posPP[0]       #posPP[0] es la posición de PP en el eje x
    posw=posPP[1]       #mientras que posPP[1] es la posición de PP en el eje Y
    if LaberintoA[posh][posw]==" PP":   
        LaberintoA[posh][posw]="   "        #Esta corrida solo quita a PP del laberinto original, pues PP solo se moverá en el laberinto oculto
    elif LaberintoA[posh][posw]=="|PP":     #Y por tanto sigue quieto en el original.
        LaberintoA[posh][posw]="|  "

    #arriba
    if k=="w":                                              #Distintas variaciones de la funcion mover dependiendo de la dirección entrada
            if LaberintoA[posh-1][posw]=="+  ":             #Todas las funciones tienen distintas condiciónes para evitar excepciones.
                if LaberintoA[posh-2][posw]=="   ":
                    LaberintOculto[posh-2][posw]=" PP"      #Primero revisan la valides de la casilla donde se quiere mover y si es valida ponen a PP ahí
                elif LaberintOculto[posh-2][posw]=="|  ":   
                    LaberintOculto[posh-2][posw]="|PP"

                if LaberintOculto[posh][posw]==" PP":       #Luego todas corren este pedasito de codigo que solo quita a PP de la posición donde estaba
                    LaberintOculto[posh][posw]="   "
                elif LaberintOculto[posh][posw]=="|PP":
                    LaberintOculto[posh][posw]="|  "
                posPP[0]=posh-2                             #Se actualisa siempre la posición de PP registrada globalmente
                Despejar(posPP,LaberintOculto,LaberintoA)   #Y se corre la función despejar para dar visibilidad alrededor de PP

    #abajo  
    elif k=="s":
            if LaberintoA[posh+1][posw]=="+  ":
                if LaberintoA[posh+2][posw]=="   ":
                    LaberintOculto[posh+2][posw]=" PP"
                elif LaberintOculto[posh+2][posw]=="|  ":
                    LaberintOculto[posh+2][posw]="|PP"


                if LaberintOculto[posh][posw]==" PP":
                    LaberintOculto[posh][posw]="   "
                elif LaberintOculto[posh][posw]=="|PP":
                    LaberintOculto[posh][posw]="|  "
                posPP[0]=posh+2
                Despejar(posPP,LaberintOculto,LaberintoA)
            
    #izquierda
    elif k=="a":
            if LaberintoA[posh][posw]=="   ":
                if LaberintoA[posh][posw-1]=="   ":
                    LaberintOculto[posh][posw-1]=" PP"
                elif LaberintoA[posh][posw-1]=="|  ":
                    LaberintOculto[posh][posw-1]="|PP"

                if LaberintOculto[posh][posw]==" PP":
                    LaberintOculto[posh][posw]="   "
                elif LaberintOculto[posh][posw]=="|PP":
                    LaberintOculto[posh][posw]="|  "
                posPP[1]=posw-1
                Despejar(posPP,LaberintOculto,LaberintoA)

    #derecha
    elif k=="d":
            if posw+2!=len(LaberintoA[0]):
                if LaberintoA[posh][posw+1]=="   " or LaberintoA[posh][posw+1]==" TT":
                    LaberintOculto[posh][posw+1]=" PP"
                    if LaberintOculto[posh][posw]==" PP":
                            LaberintOculto[posh][posw]="   "
                    elif LaberintOculto[posh][posw]=="|PP":
                        
                            LaberintOculto[posh][posw]="|  "

                    posPP[1]=posw+1
                    Despejar(posPP,LaberintOculto,LaberintoA)

    if posPP==posTT:    #Este if revisará si la posPP(lista con las coordenadas de PP) es igual a la misma lista pero para TT (PP y TT estan en la misma posición)
        print(imprimirLaberinto(LaberintoA))    #Se imprime el laberinto sin ***
        print("¡Felicitaciones! ¡Ha completado el laberinto! Aprete ESC para continuar.")   #Se imprimen instrucciones al jugador.
        print("Si desea jugar denuevo, inserte nuevas proporciones de laberinto, si desea cerrar el juego, inserte 0")# Pongo ESC para que no se anoten letras en el siguiente panel
        global jugando  #Se llama a la variable global jugando para alterarla
        jugando=False   #Se altera el estado de jugando para avisar que ya no está jugando.
    else:
        print(imprimirLaberinto(LaberintOculto))  #Si la partida no ha terminado, se imprime el laberinto actualisado y sigue la partida.

def Despejar(posPP,laberintoculto,laberinto):
    posh=posPP[0]   #Separo denuevo posición de PP en dos variables para uso más facil.
    posw=posPP[1]   #siendo posh(posición en H) y posw(posición en W)

    #arriba
    if laberinto[posh+1][posw]=="+  ":                          #Se pone en IF si es que PP puede ver hacia arriba
        laberintoculto[posh+1][posw]=laberinto[posh+1][posw]    #Si ese es el caso se reemplazan valores de laberintoA a laberintoculto
        laberintoculto[posh+2][posw]=laberinto[posh+2][posw]    #Reemplazo dos valores porque cada celda tiene 2 espacios verticales.
    elif laberinto[posh+1][posw]=="+--":
        laberintoculto[posh+1][posw]=laberinto[posh+1][posw]    #Si no se puede ver arriba, entonces solo la pared se hace visible.

    #abajo
    if laberinto[posh-1][posw]=="+  ":                          #Lo mismo para mirar abajo.
        laberintoculto[posh-1][posw]=laberinto[posh-1][posw]
        laberintoculto[posh-2][posw]=laberinto[posh-2][posw]
    elif laberinto[posh-1][posw]=="+--":
        laberintoculto[posh-1][posw]=laberinto[posh-1][posw]

    #derecha
    if (posw+2)!=len(laberinto[0]):                            #Un if simple para evitar atravesar bordes del laberinto.
        if laberinto[posh][posw+1]=="   " or laberinto[posh][posw+1]==" TT":    #Siempre estará TT en los if para que ella tambien se pueda despejar
            laberintoculto[posh][posw+1]=laberinto[posh][posw+1]
        elif (laberinto[posh][posw+1]=="|  " and laberintoculto[posh][posw+1]!="|  ") or (laberinto[posh][posw+1]=="|TT"):
            laberintoculto[posh][posw+1]="|**"                 #Si hay una pared a la derecha entonces usamos un caractera especial para mostrar la pared pero no la
    #celda completa.
    
    #izquierda
    if laberinto[posh][posw-1]=="   " or laberinto[posh][posw-1]=="|  " or laberinto[posh][posw-1]==" TT" or laberinto[posh][posw-1]=="|TT":
        if (posw!=0) and (laberintoculto[posh][posw]!="|PP"):           #El chequeo para izquierda es mucho mas simple, pues las paredes siempre estarán dentro de la celda.
            laberintoculto[posh][posw-1]=laberinto[posh][posw-1]

    return laberintoculto     #Tras despejar se devuele el laberinto pero ahora con menos celdas asterisco.

def AJugar():               #Función entregada por el profe, incia el juego
    listener = keyboard.Listener(on_press=on_press)
    listener.start()  
    listener.join()  

'''
#Funciones de generación del laberinto: (No escritas ni alteradas por mi)
'''

def generarLaberinto(w = 16, h = 8):
    L = []
    v = []
    for i in range(h):
        v.append([0] * w) 
        L.append(["+--"] * w + ['+  '])
        L.append(["|  "] * w + ['|  '])  
    L.append(["+--"] * w + ['+  '])
    [v,L] = generarPaso(randrange(w), randrange(h), v, L)

    return L

def generarPaso(x, y, V, L):
        V[y][x] = 1
        d = [] #Almacena movimientos válidos
        if x > 0:
            d.append([x - 1, y])
        if y > 0:
            d.append([x, y - 1])
        if x < len(V[0]) - 1:
            d.append([x + 1, y])
        if y < len(V) - 1:
            d.append([x, y + 1])
        shuffle(d)
        for [xx, yy] in d:
            if V[yy][xx]: 
                continue
            if xx == x: #Movimiento vertical
                if yy < y: #Se mueve hacia arriba
                    L[(yy+1)*2][x] = "+  "
                else: #Se mueve hacia abajo
                    L[(yy)*2][x] = "+  "
            if yy == y: 
                if xx < x: #Se mueve a la izquierda
                    L[yy*2 + 1][xx+1] = "   "
                else: #Se mueve hacia la derecha
                    L[yy*2 + 1][xx] = "   "
            [V, L] = generarPaso(xx, yy, V, L)
        return [V,L]

'''
#Ciclo de control principal del juego:
'''

print("------------------------------------------------------------------------------------------------\n") #Una interfas muy simple al incio del juego para entregar
print("                                --+ Bienvenido a PyMaze +--\n")                                      #los controles y otros datos basicos.
print("                          Controles:    [W]        Salir: [Esc]")
print("                                     [A][S][D]\n")
print("   Su personaje: [PP] /Tambien conocido como Pepe, esta buscando a su hija Tete. \ ")
print("   Su objetivo: [TT]  \Tambien conocida como Tete, se ha perdido en el laberinto./\n")

while True:                                                                 #Dentro de este While corre el juego completo (permite jugar multiples veces)
    alto=int(input("Inserte ancho del laberinto: "))                       #Se pide alto del laberinto
    if alto==0:                                                             #Si alto o ancho es 0 se breakea el While y termina el juego
        print("Cerrando juego...\n")
        print("------------------------------------------------------------------------------------------------")
        break 
    ancho=int(input("Inserte alto del laberinto: "))                       #Se pide ancho del laberinto (Despues me di cuenta que usé las variables alreves.)
    if ancho==0:
        print("Cerrando juego...\n")
        print("------------------------------------------------------------------------------------------------")
        break

    elif alto<5 or ancho<5:                                                #Si el laberinto es muy pequeño solo se vuelve a el pedido de valores.
        print("Dimensiones invalidas (El laberinto debe ser de al menos 5 altura y 5 ancho)")
        continue

    else:
        jugando=True    #Jugando se pone como verdadero al empesar una partida (activa los comandos de teclado)
        LaberintoA=generarLaberinto(alto, ancho) #Se genera el laberinto y se guarda en la variable LaberintoA
        ladoLaberintoPP=randint(0,3)            #Se elije a que lado del laberinto estará PP
        posPP=GenerarPP_TT(alto,ancho,LaberintoA,"PP",ladoLaberintoPP)  #Se genera la posición de PP en el laberinto.
        if ladoLaberintoPP==0:      #Uso  un sets de IF y ELIF para voltear el lado, donde 0 es arriba, 1 es abajo, 2 es izquierda y 3 es derecha.
            ladoLaberintoTT=1
        elif ladoLaberintoPP==1:
            ladoLaberintoTT=0
        elif ladoLaberintoPP==2:
            ladoLaberintoTT=3
        elif ladoLaberintoPP==3:
            ladoLaberintoTT=2
        posTT=GenerarPP_TT(alto,ancho,LaberintoA,"TT",ladoLaberintoTT)  #Con el lado volteado se genera la posición de TT
        LaberintOculto=copiarLaberinto(LaberintoA)                      #Uso la funcion para copiar el laberintoA a una variable "LaberintOculto"
        LaberintOculto=EsconderLaberinto(LaberintOculto)                #Ahora que estan separadas, escondo en *** el LaberintOculto que se mostrara al jugador
        print(imprimirLaberinto(Despejar(posPP,LaberintOculto,LaberintoA))) #Se imprime el LaberintoOculto con la posición incial de PP y con aquello alrededor de el depejado.
        AJugar()    #Se empieza la partida. AJugar solo se romperá cuando posTT sea igual a posTT, y entonces se reiniciará el While True.