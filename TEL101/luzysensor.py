#importamos las respectivas bibliotecas
import RPi.GPIO as gpio
import time as t

#seleccionamos el modo a utilizar para los gpio de la raspi
gpio.setmode(gpio.BCM)

#asignamos las variables con el número según el gpio
pin=4
pin2=27
pin3=6
pin4=21
trigger=18
echo=24

#configuramos los respectivos gpios
gpio.setup(trigger, gpio.OUT)
gpio.setup(echo, gpio.IN)
gpio.setup(pin, gpio.OUT)
gpio.setup(pin2, gpio.OUT)
gpio.setup(pin3, gpio.OUT)
gpio.setup(pin4, gpio.OUT)
#configuramos ciertos gpio con modo apertura o salida en cada caso
gpio.output(trigger, False)
gpio.output(pin, False)
gpio.output(pin2, False)
gpio.output(pin3, False)
gpio.output(pin4, False)

#asignamos variable para el LIMITE de distancia
limit=200

#abrimos una escepción
try:
    print("sensor ultrasonica HC-SR04")

    #abrimos un bucle infinito
    while True:
        #activamos por decir asi la captura de objetos del sensor
        gpio.output(trigger, True)
        t.sleep(0.00001) #aquí espera dichos segundos y continúa
        #desactivamos la captura para no saturar la raspi
        gpio.output(trigger, False)

        #abrimos una variable de tiempo
        start=t.time()

        #abrimos dos bucles para la variable del sensor echo
        while gpio.input(echo)==0:
            start=t.time()
            
        while gpio.input(echo)==1:
            stop=t.time()

        #asignamos otras dos variables para ver el lapso de tiempo y calcular asi la distancia
        lapse=stop-start
        dist=(lapse*34300)/2

        #abrimos una condición para cuando la distancia sea menor al limite
        if dist<limit:
            print("¡cuidado! objeto a "+ str(dist) + "cm")
            gpio.output(pin, True)
            t.sleep(0.15)
            gpio.output(pin2, True)
            t.sleep(0.15)
            gpio.output(pin3, True)
            t.sleep(0.15)
            gpio.output(pin4, True)
        #si no se cumple la condición anterior continúa ejecutandose
        else:
            print("continua, no hay objetos.")
            gpio.output(pin, False)
            t.sleep(0.15)
            gpio.output(pin2, False)
            t.sleep(0.15)
            gpio.output(pin3, False)
            t.sleep(0.15)
            gpio.output(pin4, False)
        #el programa se detiene por 0.25 segundos para no saturar la raspi
        t.sleep(0.25)

#si se interrumpe presionando ctrl+c se detiene el programa
except KeyboardInterrupt:
    print("sensor en off")
    #se limpia la terminal
    gpio.cleanup()






