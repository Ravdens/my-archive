import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(21,GPIO.OUT)
GPIO.setup(20,GPIO.OUT)
while True:
        duracion=int(input("Duracion de motor: "))
        motor=int(input("Ingrese diraccion (1 o 2): "))
        if motor==1:
                GPIO.output(21,GPIO.HIGH)
                time.sleep(duracion)
                GPIO.output(21,GPIO.LOW)
        elif motor==2:
                GPIO.output(20,GPIO.HIGH)
                time.sleep(duracion)
                GPIO.output(20,GPIO.LOW)
