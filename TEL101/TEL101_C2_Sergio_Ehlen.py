from random import randrange

#Funciones escritas por el profe    :
def iniciarTablero():
    usados = [0]*9
    return usados
        
def ganador(U):
    #Horizontales:
    if U[0]==U[1]==U[2]:
        return U[0]
    elif U[3]==U[4]==U[5]:
        return U[3]
    elif U[6]==U[7]==U[8]:
        return U[6]
    #Verticales
    if U[0]==U[3]==U[6]:
        return U[0]
    elif U[1]==U[4]==U[7]:
        return U[1]
    elif U[2]==U[5]==U[8]:
        return U[2]
    #Diagonales:
    if U[0]==U[4]==U[8]:
        return U[0]
    elif U[2]==U[4]==U[6]:
        return U[2]
    #Nadie gana:
    return 0

def juegaPyGato(U):
    #Trata de ganar (primera jugada que cierra el juego):
    #Horizontal:
    for i in range(3):
        if U[i*3:i*3+3].count(2) == 2 and U[i*3:i*3+3].count(0) == 1:            
            U[U[i*3:i*3+3].index(0) + i*3] = 2
            return U
    #Vertical:
    for i in range(3):
        if U[i::3].count(2) == 2 and U[i::3].count(0) == 1:            
            U[U[i::3].index(0)*3+i] = 2
            return U
    #Diagonal:
    if U[0::4].count(2) == 2 and U[0::4].count(0) == 1:            
        U[U[0::4].index(0)*4] = 2
        return U
    if U[2:7:2].count(2) == 2 and U[2:7:2].count(0) == 1:            
        U[U[2:7:2].index(0)*2+2] = 2
        return U
                
    #Trata de no perder (primera jugada que bloquea al oponente):
    #Horizontal
    for i in range(3):
        if U[i*3:i*3+3].count(1) == 2 and U[i*3:i*3+3].count(0) == 1:            
            U[U[i*3:i*3+3].index(0) + i*3] = 2
            return U
    #Vertical:
    for i in range(3):
        if U[i::3].count(1) == 2 and U[i::3].count(0) == 1:            
            U[U[i::3].index(0)*3+i] = 2
            return U
    #Diagonal:
    if U[0::4].count(1) == 2 and U[0::4].count(0) == 1:            
        U[U[0::4].index(0)*4] = 2
        return U
    if U[2:7:2].count(1) == 2 and U[2:7:2].count(0) == 1:            
        U[U[2:7:2].index(0)*2+2] = 2
        return U

    #Juega aleatorio:
    libres = U.count(0)
    rnd = randrange(0, libres)
    count = 0
    for i in range(9): 
        if U[i] == 0:
            if count == rnd:
                U[i] = 2
                return U
            count += 1
            
#Funciones mías:
    
def mostrarTablero(usados):         #Mi funcion mostrar tablero
    slots=[1,2,3,4,5,6,7,8,9]       #Tiene todos los numeros para mostrar en pantalla
    for slotA in slots:             
        i1=slots.index(slotA)       #i1 nos dirá donde vamos en la lista de slots
        if usados[i1]==1:           #Si esta está jugada por el jugador, se cambia por una X
            slots[i1]="X"
        elif usados[i1]==2:         #Si esta está jugada por el  enemigo, se cambia por un O
            slots[i1]="O"
    print(f" {slots[0]} | {slots[1]} | {slots[2]}")   #Se imprimen los datos de la lista en el grid que se muestra por pantalla.
    print("---|---|---")
    print(f" {slots[3]} | {slots[4]} | {slots[5]}")
    print("---|---|---")
    print(f" {slots[6]} | {slots[7]} | {slots[8]}\n")

def jugadaValida(usados):           #La funcion que pide y valida jugadas
    while True:                     #Esta en in while para pedir otra jugada si la entrada es invalida
        jugada=int(input("Ingrese un número disponible en el tablero (o 0 para salir): "))
        jugada-=1                   #Se le resta 1 para que se alinie con los indices de la lista apropiados.
        if jugada>=0 and jugada<=9: #La jugada tiene que ser un numero valido
            if usados[jugada]==0:   #Ademas debe ser un 0 dentro de la lista usados
                return jugada       #En ese caso se termina el while y retorna la jugada.
            else:
                print("La jugada ingresada es inválida pues este espacio ya está ocupado. Intente nuevamente!") #Si no es cierto se dice que esta lista ya esta usada
        else:
            print("La jugada ingresada es inválida. Intente nuevamente!") #Solo se dice que es un espacio invalido

def procesaJugada(usados,jugada): #Esta función no necesita hacer mucho, solo reemplaza en los usados por un 1 
    usados[jugada]=1 
    return [ganador(usados),usados]  #Y luego llama a ganador para entregar los resultados actuales del juego.

#CICLO PRINCIPAL DE CONTROL DEL JUEGO:

print("Bienvenido a PyGato!!")
usados = iniciarTablero() #Se inicialisa el tablero y se guarda en usados
while True: #Loop principal del juego
    mostrarTablero(usados) #Se muestra el tablero al inicio de cada turno
    jugada = jugadaValida(usados) #Se intenta hasta conseguir una jugada valida
    if (jugada+1)==0: #Si esta jugada era 0, el cual al restarle 1 se convirtió en -1, entonces se cierra el juego
        print("Cerrando partida...")
        break
    else:
        results=procesaJugada(usados,jugada) #Se procesa la jugada recibida
        usados=results[1] #Se saca usados devuelta de la lista que nos entrega procesaJugada
        if results[0]==0: #Si este era 0, significa que no hay un ganador
            if 0 in usados: #Esto puede significar que sigue la partida, si es que hay espacios desocupados
                juegaPyGato(usados) #En cuyo caso juega el rival
                continue #Y sigue el juego
            else:
                print("||||||||||||||||||||||||")
                print("|||||No hay ganador|||||") #O hubo un empate en la partida
                print("||||||||||||||||||||||||\n")
                break
        elif results[0]==1: #Si los resultados me dieron una victoria, se breakea el loop y gana la partida
            print("------------------")
            print("-----Victoria!----")
            print("------------------\n")
            break
        elif results[0]==2: #Si los resultados me dieron una derrota, se breakea el loop y pierde la partida
            print("~~~~~~~~~~~~~~~~~~~~~~~~")
            print("~~~Ha sido derrotado~~~~")
            print("~~~~~~~~~~~~~~~~~~~~~~~~\n")
            break
    
        