while True:                                                         #Bucle inicial.
    numero=int(input("Ingrese un número mayor o igual a 10: "))     #Se pide variable.

    if numero<10 and numero!=0:                                     #Se ve el caso de que sea un numero invalido y repite pregunta.
        print("Error! (Número debe ser mayor o igual a 10)")
        continue                                                    #Se repite la pregunta

    elif numero==0:                                                 #Se ve el caso de el numero 0 (Cerrar programa).
        print("Código secreto activado, cerrando programa.")
        break                                                       #Se cierra el programa

    else:
        
        #numeros triangulares   
        print("~~~~~~~~~~~~~~~~~~~~~~")                             #Agregue estas cositas para que sea más fácil de leer. xd
        print("Numeros triangulares: ")
        triangulo=1                                                 #Se ingresan variables usadas en el calculo.
        nT=1 #Que wea hace esto? lol?
        while triangulo<numero:                                     #Se abre un bucle para ir calculando e imprimiendo los numeros que corresponden.
            triangulo=nT*(nT+1)//2                                  #La formula planteada en la guía.
            print(triangulo)
            nT+=1
        if triangulo==numero:
            print(f"El numero {numero} es triangular.")             #Si es igual al valor ingresado originalmente.


        #numeros pentagonales
        print("~~~~~~~~~~~~~~~~~~~~~~")
        print("Numeros pentagonales: ")
        pentagono=1                                                 #Lo mismo que para triangulo pero con distinta ecuacion y variables.
        nP=1
        while pentagono<numero:
            pentagono=nP*(3*nP-1)//2
            print(pentagono)
            nP+=1
        if pentagono==numero:
            print(f"El numero {numero} es pentagonal.")


        #numeros hexagonales
        print("~~~~~~~~~~~~~~~~~~~~~~")
        print("Numeros hexagonales: ")
        hexagono=1                                                 #Lo mismo que para triangulo pero con distinta ecuacion y variables, denuevo.
        nH=1
        while hexagono<numero:
            hexagono=nH*((2*nH)-1)
            print(hexagono)
            nH+=1
        if hexagono==numero:
            print(f"El numero {numero} es hexagonal.")

        
        #Caso de que no sea ninguno.
        if numero!=triangulo and numero!=pentagono and numero!=hexagono:   #Se ve que se cumplan toda las condiciones y se da el mensaje
            print(f"El numero {numero} no es triangular, pentagonal ni hexagonal.")