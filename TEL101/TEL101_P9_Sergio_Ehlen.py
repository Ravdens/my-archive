from matplotlib import pyplot as plt

def lee_zonas(archivo_zonas, archivo_subzonas):
    L = []
    en_zonas = True
    try:
        #Linux:
        f = open(archivo_zonas, 'r', encoding='iso-8859-15')
        #Windows
        #f=open(archivo_zonas, 'r')
        #Almacena en lista registros del tipo: (codigo_zona, Estado, Area, Subzonas)
        for line in f: #Lee una línea a la vez
            sp = line.split(":")
            L.append([sp[0], sp[1], int(sp[2]), []]) #inicialmente lista de subzonas vacia                
        f.close()
        en_zonas = False
        f = open(archivo_subzonas, 'r', encoding='iso-8859-15')
        #inserta subzonas con su ley
        for line in f: #Lee una línea a la vez
            sp = line.split(":")
            for registro_zona in L:
                if registro_zona[0] == sp[0]: #inserta subzona en registro correspondiente
                    registro_zona[3].append([sp[1],float(sp[2])])                 
        f.close()
    except OSError:
        print("Error: No se puede abrir el archivo para lectura:", 
              archivo_zonas if en_zonas else archivo_subzonas)
        return []

    return L

def ley_promedio(zona):
    szprom = 0.0
    if len(zona[3]) == 0:
        return 0
    for subzona in zona[3]:
        szprom += subzona[1]
    szprom /= len(zona[3])
    return szprom

def leeLista(lista): #Funcion que uso para leer más facil los datos que voy a usar
    for x in lista:
        print(x)


#Main
listaDlistas=lee_zonas("zonas-matplotlib.txt","subzonas-matplotlib.txt")
#print(listaDlistas)
leeLista(listaDlistas)

#Creación de ventana
fig, axs = plt.subplots(2, 2)
fig.suptitle("Zonas mineras")


#Gráficos de torta
nombreszonas=[]
Ejex1=[]
Ejex2=[]
for zona in listaDlistas:
    if zona[1]=="en explotacion":#Elijo solo las en explotacion
        nombreszonas.append(zona[0])#Agrego los nombres que corresponden
        Ejex1.append(zona[2])#Agrego aquí el area
        Ejex2.append(ley_promedio(zona))#Agego aquí la ley promedio
axs[0,0].pie(Ejex1,labels=nombreszonas,autopct="%1.2f%%",shadow=True,startangle=180)
axs[0,1].pie(Ejex2,labels=nombreszonas,autopct="%1.2f%%",shadow=True,startangle=180)

#Grafico de barra
zonas=[[],[]]
for zona in listaDlistas:
    zonas[0].append(zona[0])
    zonas[1].append(ley_promedio(zona))
axs[1,0].bar(zonas[0],zonas[1],color="orange",tick_label=zonas[0])
axs[1,0].set_ylabel("Ley")

#Gráfico de lineas
names = ['Mínima', 'Intermedia', 'Máxima'] #Agrego los nombres que voy a poner abajo
for zona in listaDlistas:
    subzonas=zona[3]
    newL=[]
    for subzona in subzonas: 
        newL.append(subzona[1])#Agrego los datos que necesito a una nueva lista
    axs[1, 1].plot(names,sorted(newL),label=zona[0])#Ordeno y ploteo los datos
    axs[1, 1].plot(sorted(newL),"ro")#Hago nuevos plot encima de puntos para enfocar puntos de cambio
axs[1, 1].legend()
axs[1,1].set_ylabel("Ley")

#Genero los titulos
axs[0, 0].set_title("Zonas en explotación, por área")
axs[0, 1].set_title("Zonas en explotación, por ley")
axs[1, 0].set_title("Zonas, por ley")
axs[1, 1].set_title("Subzonas, de menor a mayor ley")
plt.show() #Muestro el plot