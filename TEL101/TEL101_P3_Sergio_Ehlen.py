
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

def gaussiana_multivariada(pos, mu, Sigma):
    n = mu.shape[0]
    Sigma_det = np.linalg.det(Sigma)
    Sigma_inv = Sigma
    if Sigma_det == 0: 
        print("Advertencia!: Matriz de covarianza singular genera caso degenerado.")
        Sigma_det = 1
    else:
        Sigma_inv = np.linalg.inv(Sigma)
    N = np.sqrt((2*np.pi)**n * np.abs(Sigma_det))
    fac = np.einsum('...k,kl,...l->...', pos-mu, Sigma_inv, pos-mu) 
    
    return [np.exp(-fac / 2) / N, N]        #Por cierto, el codigo me tira unos errores al correr la funcion por esta linea, la linea 42 y la 44, pero imprime
                                            #correctamente asique no voy a cambiarle nada, porque ademas esto ya venía así. El linter además me los marca como error
def plot_gaussian(lmu, lSigma):             #y no se como se supone que se arreglen.
    mu = np.array(lmu)
    Sigma = np.array(lSigma)
    N = 400
    K1 = np.sqrt(lSigma[0][0])*2.5
    K2 = np.sqrt(lSigma[1][1])*2.5
    X = np.linspace(-K1+lmu[0], K1+lmu[0], N)
    Y = np.linspace(-K2+lmu[1], K2+lmu[1], N)
    X, Y = np.meshgrid(X, Y)


    pos = np.empty(X.shape + (2,))
    pos[:, :, 0] = X
    pos[:, :, 1] = Y

    [Z, NN] = gaussiana_multivariada(pos, mu, Sigma)


    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot_surface(X, Y, Z, rstride=3, cstride=3, linewidth=1, antialiased=True,
                cmap=cm.viridis)

    ax.contourf(X, Y, Z, zdir='z', offset=-1/NN, cmap=cm.viridis)

    ax.set_xlim(-K1+lmu[0], K1+lmu[0])
    ax.set_ylim(-K2+lmu[1], K2+lmu[1])
    ax.set_zlim(-1/NN,1/NN)
    ax.set_zticks(np.linspace(0,1/NN,5))

    plt.show()

'''
Interfaz inicial y promedios(medias)--------------------------------------------------------------------------------------------------------------------------------
'''

while True:
    lista1=[]    #Listas para los 2 grupos de datos.
    lista2=[]
    
    cant=int(input("¿Cuantos números desea ingresar? (Ingrese 0 para cerrar programa.): ")) #cant siendo la cantidad de datos por lista
    
    if cant==0:
        print("Cerrando programa...") #Para cerrar programa
        break
    
    contador=1
    total1=0
    for i in range(cant):   #Para pedir los numeros ingresados por el usuario.
        num1 = int(input(f"Ingrese número {contador} de la serie 1: "))
        total1+=num1        #Ir sumando los datos para sacar mas facilmente el promedio.
        lista1.append(num1) #Ir agregando los datos a la lista 1.
        contador+=1         #Para mostrar en pantalla cuantos datos lleva.
        
    contador=1
    total2=0                #Lo mismo pero para el segundo grupo de datos.
    for i in range(cant):
        num2 = int(input(f"Ingrese número {contador} de la serie 2: "))
        total2+=num2
        lista2.append(num2)
        contador+=1

    promedio1=total1/cant   #Se calculan y muestran los promedios (o media arirtmetica) de ambos grupos de datos.
    promedio2=total2/cant
    VmeD=[promedio1,promedio2]
    print(f"Vector de medias: {VmeD}")


    '''
    Calculo de variansa y desviación:------------------------------------------------------------------------------------------------------------------------------
    '''
    variansa1=0     #Establesco desde el principio las variables que voy a querer calcular para poder ir sumandoles sin tener que definir dentro del for
    variansa2=0
    covariansa=0

    contador=0      #Utiliso contador para ir cambiando de posición dentro de la lista
    for i in lista1:
        numero=lista1[contador]
        numero-=promedio1
        numero=numero**2    #Uso este proceso para tomar cada numero de la lista, restarle el promedio, sacarle el cuadrado, y agregarlo a la variable variansa.
        variansa1+=numero
        contador+=1
    variansa1=variansa1/(cant-1)    #Luego se divide en cantidad -1 para terminar el calculo de varianza.
    desviación1=variansa1**(1/2)    #Se saca la rais y se anota en una nueva variable de desviacion estandar.

    contador=0
    for i in lista2:            #El mismo proceso pero para la lista 2
        numero=lista2[contador]
        numero-=promedio2
        numero=numero**2
        variansa2+=numero
        contador+=1
    variansa2=variansa2/(cant-1)
    desviación2=variansa2**(1/2)

    contador=0
    for i in range(cant):           
        numero1=lista1[contador]    #Realiso  un proceso parecido a las variansas pero en vez de multiplicar al cuadrado
        numero2=lista2[contador]    #multiplico el valor de cada lista por el del mismo indice en la otra lista.
        numero1-=promedio1
        numero2-=promedio2
        covariansa+=(numero1*numero2)
        contador+=1
    covariansa=covariansa/(cant-1)  

    listaA=[variansa1,covariansa]
    listaB=[covariansa,variansa2]
    mDCov=[listaA,listaB]                   #Se crean las listas a imprimir para que se vea mas ordenado y se imprime.
    print(f"Matriz de covarianza: {mDCov}")
    print(f"Desviación estandar serie1: {desviación1}\nDesviación estandar serie2: {desviación2}")


    '''
    Sistema para ordenar las listas de menor a mayor (por un sistema parecido a bubble-sort que logré hacer). (Será necesario para encontrar la mediana.)------------
    '''

    i=0
    pos=0           #Contador para medir la posición
    menor=lista1[0] #Se define el menor inicialmente como el primer número de la lista (lo sea o no).
    listemp=[]      #Se define una lista temporal donde se van a ir poniendo los numeros por orden.
    while True:     #Se itera este while por cada número que se pasa de lista.
        for i in range(1,len(lista1)):          #Segundo iterador, usado para comparaciones.              
            if menor>lista1[i]:                 #Si el numero encontrado es mayor al menor actual, se reemplaza.
                menor=lista1[i]
                pos=i              #Se actualiza la posición.
        add=lista1.pop(pos)        
        listemp.append(add)        #Uso la variable add para hacer la transferencia de la lista inicial a la lista temporal.
        menor=lista1[0]            #Se reinicia el número menor.
        pos=0                      #Se reinicia la posición.
        if len(lista1)==1:         #Se ve el caso de que solo quede 1 valor en la lista 1.
            add=lista1.pop(0)
            listemp.append(add)    #Se hace la transferencia faltante y se cierra el while.
            break
    lista1=listemp                 #Se reemplaza la lista original por la lista temporal.
  
    #Se repite el proceso pero para la lista 2 (Lo hubiera hecho una función pero no se usar las funciones por completo y no se si están permitidas realmente.)
    
    i=0
    pos=0   
    menor=lista2[0] 
    listemp=[]  
    while True:     
        for i in range(1,len(lista2)):                     
            if menor>lista2[i]:                
                menor=lista2[i]
                pos=i             
        add=lista2.pop(pos)        
        listemp.append(add)     
        menor=lista2[0]          
        pos=0                    
        if len(lista2)==1:       
            add=lista2.pop(0)
            listemp.append(add)    
            break
    lista2=listemp              

    #Para sacar la mediana de ambos grupos de datos.
    if cant%2==0:
        semimediana=cant/2                        #Si es par entonces se hace un proceso donde se calculan los dos indices del medio, sacando un indice que no existe
        semiMedianaMayor=int(semimediana+0.5)       #pero esta a 0,5 de los indices que buscamos, y sumandole y restandole 0,5 para encontrar los indices del medio.
        semiMedianaMenor=int(semimediana-0.5)
        semiMedianaMayor=lista1[(semiMedianaMayor)]
        semiMedianaMenor=lista1[(semiMedianaMenor)]
        mediana1=(semiMedianaMenor+semiMedianaMayor)/2
    else:
        posMediana=int(cant/2)                    #Este proceso se salta si es impar.
        mediana1=lista1[posMediana]

    #Lo mismo pero para la lista 2.
    if cant%2==0:
        semimediana=cant/2
        semiMedianaMayor=int(semimediana+0.5)
        semiMedianaMenor=int(semimediana-0.5)
        semiMedianaMayor=lista2[(semiMedianaMayor)]
        semiMedianaMenor=lista2[(semiMedianaMenor)]
        mediana2=(semiMedianaMenor+semiMedianaMayor)/2
    else:
        posMediana=int(cant/2)
        mediana2=lista2[posMediana]

    print(f"Mediana serie 1: {mediana1}")     #Se imprimen por pantalla las medianas.
    print(f"Mediana serie 2: {mediana2}")

    '''
    Llamado de funciones------------------------------------------------------------------------------------------------------------------------------------------
    '''
    plot_gaussian(VmeD,mDCov) #Solo entro mis listas VmeD (El vector de medias) y mDCov (Matris de covariansa) a la función ya escrita por el profe.