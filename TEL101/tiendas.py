def tienda_mas_cercana(nombre_archivo_tiendas , x , y):
    file = open(nombre_archivo_tiendas,"r")
    lista_locales=[]
    for linea in file:
    #linea = "IWIbella;Centro,1,1:Norte,3,10"
        linea = linea.strip().split(";")
    #linea = [ "Iwibella" , "Centro,1,1:Norte,3,10" ]
        nombre_cadena = linea[0]
        locales = linea[1].split(":")
    #locales = [ "Centro,1,1" , "Norte,3,10" ]
        for local in locales:
            local=local.split(",")
        #local = [ "Centro" , "1" , "1" ]
            distx = float(local[1])-x
            disty = float(local[2])-y
            distancia = (distx*distx + disty*disty)**(0.5)

            #nombre_cadena + " " + local = "IWIbella Norte"
            lista_locales.append((distancia,nombre_cadena+" "+local[0]))
    file.close()

    lista_locales.sort()
    print(lista_locales)
    return lista_locales[0][1]

#tienda_mas_cercana("tiendas.txt, 10, 5")


def analizar_productos(nombre_archivo_tiendas):
    file = open(nombre_archivo_tiendas,"r")
    dict_productos={}
    for linea in  file:
        linea=linea.strip().split(";")
        file_cadena = open(linea[0]+".txt","r")

        for producto in file_cadena:
            producto = producto.strip().split(";")
            prod_name = producto[0]
                      #stock
            if float(producto[2])>0:
                if prod_name not in dict_productos:
                    dict_productos[prod_name]=[]
                #                                   Precio     cadena
                dict_productos[prod_name].append((float(producto[1]),linea[0]))

        dict_productos[prod_name].sort()
        file_cadena.close()
    file.close()

    cont = 0
    for producto in dict_productos:
        new_file=open(producto+".txt","w")
        lista = dict_productos[producto]
        total = 0
        i = 0
        for precio,cadena in lista:
            #IWIbella: $43000
            new_file.write(cadena+": &"+str(precio)+"\n")
            total+=precio
            i+=1

        promedio = total/i
        new_file.write("Precio promedio para "+producto+": $"+promedio)
        new_file.close()
        cont+=1

    return cont
        

