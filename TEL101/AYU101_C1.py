cEI=3                #Cantidades de juegos incial
cCDR=12
cSMCW=9
cMS=7
while True:                                                                              #Loop completo de la maquina, nunca debería romperse.
    opt2=2                                            
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")   #Se presenta solo una vez por usuario usuario.
    print("¡Bienvenido a Tel Mart!")
    print("Los juegos disponibles ahora mismo son:")
    total=0
    while True:

        if opt2==0:                     #Si se elijió terminar la compra al final del proceso, entonces el loop se termina aqui.
            break

        print(f"  (1) Epidemic Inc. (Corona Edition): $10,990 ({cEI} unidades disponibles.)") #Loop completo de la maquina, nunca debería romperse.
        print(f"  (2) Coffin Dance Revolution: $5500 ({cCDR} unidades disponibles.)")
        print(f"  (3) Stacy Malibu Civil War: $7500 ({cSMCW} unidades disponibles.)")
        print(f"  (4) Mechón Simulator 2020: $15990 ({cMS} unidades disponibles.)\n")
        opt=int(input("¿Que juego desea comprar?: "))
        cant=int(input("¿Cuantas copias desea comprar?: "))


        while True:     #Se chekea la disponibilidad de los juegos y luego se suman sus costos al total.   
                                                         
            if (opt==1 and cant>cEI) or (opt==2 and cant>cCDR) or (opt==3 and cant>cSMCW) or (opt==4 and cant>cMS):
                print("No hay suficientes unidades, ingrese cantidad denuevo.")

            else:
                if opt==1:
                    total+=cant*10990
                    cEI-=cant
                elif opt==2:
                    total+=cant*5500
                    cCDR-=cant
                elif opt==3:
                    total+=cant*7500
                    cSMCW-=cant
                elif opt==4:
                    total+=cant*15990
                    cMS-=cant

                print("¿Desea agregar otro juego?\n") #Se usa opt2 para ver si se quiere cerrar la sesión
                opt2=int(input("(1=Si,0=No): "))

                if opt2==1:
                    print("Que juego desea agregar?\n") #Se cierra este loop y se vuelve al loop anterior (Agregar juegos)
                    break

                elif opt2>0 or opt2<0:                      #Se ve que la opción sea una opción valida
                    print("opción no existente")

                else:                                       #Se cierra el loop y se cerrará el otro loop al ser la opt2==0
                    print(f"Su total es {total} Gracias por comprar en Tel Mart!")
                    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n")
                    break