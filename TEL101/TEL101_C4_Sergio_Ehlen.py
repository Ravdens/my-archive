def CreateDict(filename):
    try:                        
        file=open(filename,"r") #Trato de leer el archivo
    except FileNotFoundError:
        print("El archivo no pudo ser encontrado") #Si no hay archivo retorn diccionari ovacio
        return {}

    newDict={} #Creo un nuevo dicccionario
    for linea in file:
        linea=linea.strip("\n")
        linea=linea.split(",") #Transformo cada linea en una lista usable

        if linea[0] in newDict: #Si el nombre ya está en el diccionario
            newDict[linea[0]].append(float(linea[1]))#Agrego el flotante a la lista que lo acompaña
        else:
            newDict[linea[0]]=[float(linea[1])] #Si no está el nombre, crea una lista nueva
    file.close() #Cierro archivo
    return newDict #Retorna el diccionario 

def CreateMetricas(dict1):
    newDict={}
    for key in dict1:

        total=0
        for num in dict1[key]:
            total+=num
        promedio=total/len(dict1[key]) #Saco el promedio con una sumatoria

        totalCuadratico=0
        for num in dict1[key]:
            totalCuadratico+=(num-promedio)**2 #Saco la desviacion con una sumatoria distinta que usa el promedio
        desviacion=(totalCuadratico/(len(dict1[key])-1))**(0.5) #Hago el resto del proceso (dividir y sacar raiz)

        newDict[key]=[promedio,desviacion,min(dict1[key]),max(dict1[key])] #Agrego al diccionario nuevo los valores requeridos
    return newDict #Retorno newDict

def GenerarReporte(dict,newFileName):
    try:
        file=open(newFileName,"w") #Intento crear el archivo
    except:
        print("No se pudro crear el archivo, intente otro nombre.") #Si encuentra cualquier excepcion pide cambiar el nombre
        return False #Retorna false

    for key in dict: #Uso este string extremadamente grande para ir agregando al archivo
        file.write(f"{key}:\n\tMedia: {dict[key][0]}\n\tDesviacion estandar: {dict[key][1]}\n\tMinimo: {dict[key][2]}\n\tMaximo: {dict[key][3]}\n")

    return True #Retorna true si todo funcionó



#Llamado a funciones
filename="NuevoArchivo" #dejo una variable para cambiar el nombre facilmente
dict=CreateMetricas(CreateDict("arrow_marks.csv")) #Creo el diccionario en dict
if GenerarReporte(dict,filename)==True: #Si la funcion terminó sin problemas
    print("Archivo creado! :D")  #Imprime
