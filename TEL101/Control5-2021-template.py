from matplotlib import pyplot as plt
import random

def leer_datos():
    try:
        archivo=open("ventas_de_la_compania.csv")
    except OSError:
        print("Archivos: " + archivo + " no existe.")
        return []
    else:
        nombres=[]
        ut_mes = [] #utilidades por mes (1)
        num_prod_mes=[] #listas de cantidades mensuales por producto (3)   
        num_prod=[] #total vendidos, por producto (4)
        primera = True
        for linea in archivo:
            linea = linea.strip()
            if primera: #Extraer nombres de productos
                primera = False
                nombres = linea.split(",")
                nombres = nombres[1:len(nombres)-1]
                continue
            valores = linea.split(",")
            ut_mes.append(float(valores.pop())) #utilidad mensual, sacada
            valores = valores[1:] #elimina número de mes           
            if num_prod_mes == []: #Primer mes por producto
                for valor in valores:
                    v = float(valor)
                    num_prod_mes.append([v])
                    num_prod.append(v) #Primer valor del acumulado
            else:
                for i in range(len(valores)):
                    v = float(valores[i])
                    num_prod_mes[i].append(v)
                    num_prod[i] += v #Acumula cantidad
        archivo.close()

        return [nombres,ut_mes,num_prod_mes,num_prod]
