import speech_recognition as sr
from time import sleep

def main():
    print("\n\n-----------------------------------")
    print("Bienvenido al setup de toaster.py")
    print("Primero verificaremos si settings.txt ya existe...")
    sleep(3)
    try:
        file=open("settings.txt","r")
    except FileNotFoundError:
        print("\nsettings.txt no encontrado...\n\tCreando nuevo settings.txt")
    else:
        file.close()
        print("\nYa existe un archivo settings.txt, desea sobrescribirlo?")
        while True:
            try:
                entrada=str(input("(Y/N): "))
                if entrada in ["Y","N"]:
                    if entrada in ["N","n","no","No"]:
                        print("\nOk: Cerrando setup")
                        return
                    elif entrada in ["Y","y","yes","Yes"]:
                        print("Sobrecribiendo settings.txt...\n")
                        break
                else:
                    print("Opcion invalida")
            except ValueError:
                print("Oops, esa entrada no es aceptable, intente denuevo...\n")
    try:
        file=open("settings.txt","w")
    except:
        print("oh oh, algo salio mal con la creacion del archivo, revisa que tengas espacio disponible e intentalo denuevo...")
        return
    finally:
        print("\nMicrofonos disponibles:")
        micros=sr.Microphone.list_microphone_names()
        count=0
        for micro in micros:
            print([count,micro])
            count+=1
        while True:
            try:
                opt=int(input("\n\tElija microfono a usar (número asociado): "))
                if opt <= count and opt>=0:
                    print("Ok, elijiendo este microfono como default!")
                    file.write("micIndex:"+str(opt))
                    break
                else:
                    raise ValueError
            except:
                print("Oh oh, esa opcion no es valida, intentelo denuevo")


        print("\nPaquetes de voz (toaster.py viene con solo 1 paquete predeterminado, si usted instaló otro en la carpeta voice2 elijalo aquí)")
        while True:
            try:
                opt=int(input("Elija voz (Voice1/Voice2): "))
                if opt=="Voice1":
                    print("Voz 1 elejida!")
                    file.write("voice:"+str(opt))
                    break
                elif opt=="Voice2":
                    print("Voz 2 elejida!")
                    file.write("voice:"+str(opt))
                    break
                else:
                    print("Opcion invalida, intente denuevo")
            except:
                print("Oh oh, esa opcion no es valida, intentelo denuevo")


        print("\nPuertos Gpio. Elija que puerto GPIO quiere usar para salida de señal (no todos son validos)")
        print("Usando los puertos por número de board (1-40)")

        while True:
            try:
                opt=int(input("Pin de salida: "))
                if opt in [1,2,4,6,9,14,17,20,25,30,34,39]:
                    print("Este pin no permite GPIO (Solo tierra o voltaje). Elija un pin válido.")
                elif opt>=1 and opt<=40:
                    print("Excelente! Pin elejido correctamente!")
                    file.write("pin:"+str(opt))
                    break
            except:
                print("Oh oh! Opción invalida. Intente denuevo.")

    file.close()
    print("\n\nFelicitasiones! El setup está completo")
    print("Si quieres cambiar alguna opcion, corre este programa denuevo y sobrescribe settings.txt")
    print("toaster.py ahora esta listo para usar!")
    print("---------------------------------------------------------")
    return



try:
    main()
except AttributeError:
    print("\nOops! Una de las dependencias no fué encontrada o no fué instalada correctamente.")
    print("Por favor consulte README.md para más información.\n")