dA = 23                                            #Uso A como "actual"
mA = 4
aA = 2021
flag=True
aEB=False
while flag==True:                                    #Solicitar fecha hasta que se cumpla: 0.5 pt
    aE=int(input("Ingrese año: "))                   #Uso E como "entrado"
    mE=int(input("Ingrese mes: "))
    dE=int(input("Ingrese día: ")) 
    if aE <1000:                                     #Validación de año: 0.5 pt
        print("Año ingresado es invalido (Debe ser mayor a 1000")
    else:
        if mE<=0 or mE>=13:                          #Validación de mes: 1 pt
            print("Mes ingresado es inválido (Debe ser valor entre [1,12]")
        else:
            if mE==2:                                #Validación de días febrero: 0.5 pt
                if aE%4==0:
                    aEB=True                         #aEB siendo ¿Es el año entrado biciesto?
                elif aE%100==0:
                    aEB=False
                elif aEB%400==0:
                    aEB=True
                else:
                    aEB=False

                if aEB==True:                  #El año es biciesto
                    if dE>=30 or dE<=0:
                        print("Día ingresado es inválido (Debe de ser un valor entre [0,29] para el mes ingresado).")
                    else:
                        flag=False             #Se cierra y se pasa a la fase de imprimir
                else:                          #El año no es biciesto      
                    if dE>=29 or dE<=0:
                        print("Día ingresado es inváldio (Debe de ser un valor entre [0,28] para el mes ingresado).")
                    else:
                        flag=False
            else:                                    #Validación de dia, meses distintos de febrero: 1 pt
                if mE==1 or mE==3 or mE==5 or mE==7 or mE==8 or mE==10 or mE==12:    #Meses de 31 días
                    if dE>=32 or dE<=0:
                        print("Día ingresado es inváldio (Debe de ser un valor entre [0,31] para el mes ingresado).")
                    else:
                        flag=False
                else:
                    if dE>=31 or dE<=0:
                        print("Día ingresado es inváldio (Debe de ser un valor entre [0,30] para el mes ingresado).")
                    else:
                        flag=False
fEM=True                               #fEM siendo Fecha Entrada Es Mayor. Se revisará si el año es mayor, luego el mes, y luego el día si es necesario.
if aE>aA:
    fEM=True
elif aE==aA:
    if mE>mA:
        fEM=True
    elif mE==mA:
        if dE>dA:
            fEM=True
        if dE<=dA:
            fEM=False
    else:
        fEM=False
else: fEM=False

#Está todo listo para mostrar, entonces mostrar la fecha con formato correcto y terminar: 0.5 pt

if fEM==False:
    print(f"La fecha de {dE}/{mE}/{aE} es válida y menor o igual a 23/04/2021.")
else:
    print(f"La fecha de {dE}/{mE}/{aE} es válida y mayor a 23/04/2021.")