import matplotlib.pyplot as plt
import numpy as np
import sys

filename=sys.argv[1]

#Generar lista de datos (Leer archivo)
DotDict={} #Para los puntos
GraphList=[[],[]] #Para el gráfico principal
file=open(filename,"r")
for linea in file:
    linea=linea.strip("\n")
    linea=linea.split(" ")
    
    if float(linea[3])>=0:

        #Diccionario (Puntos)
        try:
            DotDict[linea[3]].append(float(linea[0]))
        except:
            DotDict[linea[3]]=[float(linea[0])]
        
        #Lista (Grafico principal)
        GraphList[0].append(float(linea[10]))
        GraphList[1].append(float(linea[0]))
file.close()


def readDict(dict):
    print("\n")
    for key in dict:
        print([key,dict[key]])
    print("\n")

def Leelista(lista):
    print("\n")
    for x in lista:
        print(x)
    print("\n")

fig=plt.figure() #Generar figura
plt.grid(linestyle="--") #Generar grid

#Ticks eje X y eje Y
plt.yticks(np.arange(min(GraphList[0]), max(GraphList[0]), 1000)) #min y max de datos en el eje X que importan
plt.xticks(np.arange(min(GraphList[1]), max(GraphList[1]), 10000))  #min y max de datos en el eje Y que importan
plt.plot(GraphList[1],GraphList[0],"k-") #Graficar el grafico principal



#Graficar puntos
#Estilo de los puntos
sign={"0":"ro",  #0 Rojo
"1":"bo",        #1 Azul
"2":"go",        #2 Verde
"3":"co",        #3 Cyan
"4":"mo",        #4 Magenta
"5":"yo",        #5 Amarillo
"6":"ko"}        #6 Negro

#Opciones puntos
tamanho=4               #Tamaño puntos
height0=float((-8634))  #Altura puntos inicial
heightdif=0             #Distancia entre puntos (En altura)

#Graficar puntos
for key in DotDict:
    plt.plot(DotDict[key],[height0]*(len(DotDict[key])),sign[key],markersize=tamanho)
    height0+=heightdif


plt.show()