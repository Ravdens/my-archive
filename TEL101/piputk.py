from random import randint
import tkinter as tk


intentos = 3

def victoria():
    global intentos
    intentos=3
    #Mostrar ventana de victoria
    #Poner 2 botones

    #Boton 1 llama a openw_dificultad()

    #Withdraw la ventana que creaste


def derrota():
    global intentos
    intentos=3
    #Mostrar ventana de derrota
    #Poner 2 botones

    #Boton 1 llama a openw_dificultad()

    #Withdraw la ventana que creaste


#Ventana de juego
def jugar(rango):

    numerito = randint(1,rango)

    #Window config
    w_juego = tk.Toplevel()
    w_juego.geometry("500x500")
    w_juego.resizable(False,False)
    w_juego.config(background = "white")
    texto = tk.Label(w_juego,text = "Ingrese un número del 1 al "+str(rango), font = ("Times new Roman", 25), bg = "white", fg = "black", width = "20", height = "2")
    texto.pack(padx=5,pady=5,ipadx=5,ipady=5,fill=tk.X)

    #Cajita
    cajaentrada = tk.Entry(w_juego,width=30)
    cajaentrada.pack()

    def probar():
        global intentos
        valor = int(cajaentrada.get())
        
        if valor == numerito:
            w_juego.withdraw()
            victoria()
            return

        else:
            intentos = intentos - 1
            texto = tk.Label(w_juego,text = "MALO", font = ("Times new Roman", 40), bg = "white", fg = "red", width = "20", height = "2")
            texto.pack(padx=5,pady=5,ipadx=5,ipady=5,fill=tk.X)
            cajaentrada.delete(0,tk.END)

            if intentos<1:
                w_juego.withdraw()
                derrota()

            return

        



    btn5 = tk.Button(w_juego, text = "Probar", command = probar)
    btn5.pack()
    btn5.config(bg = "green", fg = "black")
    btn5.place(x = 230, y = 250)




#Ventana de selección de dificultad
def openw_dificultad():

    #Window config
    w_inicio.withdraw()
    w_dificultad = tk.Toplevel()
    w_dificultad.geometry("500x500")
    w_dificultad.resizable(False,False)
    w_dificultad.config(background = "white")
    texto = tk.Label(w_dificultad,text = "Seleccione el nivel de dificultad", font = ("Times new Roman", 25), bg = "white", fg = "black", width = "20", height = "2")
    texto.pack(padx=5,pady=5,ipadx=5,ipady=5,fill=tk.X)

    def facil():
        w_dificultad.withdraw()
        jugar(3)

    def mediano():
        w_dificultad.withdraw()
        jugar(5)

    def dificil():
        w_dificultad.withdraw()
        jugar(10)

    
    #Botones
    btn2 = tk.Button(w_dificultad, text = "Fácil", command = facil)
    btn2.pack()
    btn2.config(bg = "green", fg = "black")
    btn2.place(x = 230, y = 150)

    btn3 = tk.Button(w_dificultad, text = "Medio", command = mediano)
    btn3.pack()
    btn3.config(bg = "yellow", fg = "black")
    btn3.place(x = 230, y = 250)

    btn4 = tk.Button(w_dificultad, text = "Difícil", command = dificil)
    btn4.pack()
    btn4.config(bg = "red", fg = "black")
    btn4.place(x = 230, y = 350)




#Ventana de saludo
w_inicio = tk.Tk ()
rango = 0

w_inicio.title("¡Adivina el número!")
w_inicio.geometry("500x500")
w_inicio.resizable(False,False)


w_inicio.config(background = "#FFD8D7")
main_title = tk.Label(text = "¡ADIVINA EL NÚMERO!", font = ("Times new Roman", 25), bg = "#FF9995", fg = "#7A2D2A", width = "20", height = "2")
main_title.place(x = 80, y = 200)

btn1 = tk.Button(w_inicio, text = "Jugar", command = openw_dificultad)
btn1.pack()
btn1.config(bg = "#FF9995", fg = "#7A2D2A")
btn1.place(x = 240, y = 300)

w_inicio.mainloop()