from os import error

def genera_Ventas(file): #La primera funcion pedida en el ejercicio
    archivo=open(file,"r") #Abre el archivo
    listaza=[] #Genera una lista a retornar
    for transaccion in archivo: #Por cada linea del archivo que llamare transacciones
        transaccion=transaccion.split(",") #Separa en una lista usando las comas
        if transaccion[3]=="Venta": #Si el la transaccion es una venta
            listaza.append([transaccion[4],transaccion[6],transaccion[5]]) #Agrega los datos que nos importan de esa transaccion
    archivo.close() #Cierra el archivo
    return listaza #Retorna los datos
    


def resumen_Regional(newFileName,file): #Segunda funcion, pide el nombre del nuevo archivo y el archivo anterior
    try:
        ventasData=genera_Ventas(file) #Intento abrir el arhivo entregado
    except FileNotFoundError:
        print("El archivo con los datos no se ha encontrado.") #Si no se encuentra devuelve error
        raise FileNotFoundError
    try:
        newFile=open(newFileName,"w") #Intento crear el archivo nuevo
    except OSError:
        print("Hubo un problema al intentar escribir el archivo nuevo.") #Si el nombre no se puede usar devuelve eror
        raise OSError

    listaGirona=["Girona",0,0,0] #Genero la lista con los datos de la primera region
    for venta in ventasData:
        if venta[0]==listaGirona[0]: #Por cada venta que haya sido en la region de Girona
            listaGirona[1]+=int(venta[1]) #Sumo las ganancias
            listaGirona[2]+=int(venta[2]) #La superficie
            listaGirona[3]+=1 #Y aumento un contador de ventas

    listaTarragona=["Tarragona",0,0,0] #Lo mismo para cada otra región
    for venta in ventasData:
        if venta[0]==listaTarragona[0]:
            listaTarragona[1]+=int(venta[1])
            listaTarragona[2]+=int(venta[2])
            listaTarragona[3]+=1

    listaLleida=["Lleida",0,0,0]
    for venta in ventasData:
        if venta[0]==listaLleida[0]:
            listaLleida[1]+=int(venta[1])
            listaLleida[2]+=int(venta[2])
            listaLleida[3]+=1

    listaBarcelona=["Barcelona",0,0,0]
    for venta in ventasData:
        if venta[0]==listaBarcelona[0]:
            listaBarcelona[1]+=int(venta[1])
            listaBarcelona[2]+=int(venta[2])
            listaBarcelona[3]+=1

    listotal=[listaGirona,listaTarragona,listaLleida,listaBarcelona] #Genero una lista con todas las regiones
    newFile.write("Reporte de operaciones Inmobiliarias:\n") #Agrego el primer texto
    for provincia in listotal: #Por cada region genero una seccion del archivo final usando el formato de la lista de cada region.
        newFile.write(f"{provincia[0]}:\n\t-Numero de operaciones: {provincia[3]}\n\t- Ganancias: {provincia[1]}\n\t- Superficie total: {provincia[2]}\n")
    newFile.close() #Cierro el archivo
    return newFileName #Devuelvo el nombre con el que quedó el archivo.


#Cree un main para el llamado de las funciones tambien
print("---------------------------------------")    
print("Resumidor de ventas de inmobiliarios:")
while True: #Intento hasta que el programa funcione correctamente.
    try:
        newfile=resumen_Regional(input("Ingrese nombre del archivo que quiera crear: "),"BD-Inmuebles.csv") #Pido un nombre y trato de meterlo a la función resumen_regional.
        print(f"Exito, su nuevo archivo fué escrito como {newfile}, gracias por utilizar el programa! :D") #Si eso se hace bien, imprimo un gracias y cierro el main
        break
    except:
        print("Intente denuevo...") #Si ocurre una excepcion, continuo el loop y pido otro nombre.