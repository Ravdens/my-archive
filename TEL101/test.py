#Imports
import RPi.GPIO as GPIO
import time
from google.cloud import dialogflow_v2
import os
from random import randint
from google.api_core.exceptions import InvalidArgument
from playsound import playsound

#Inicializacion de GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(21,GPIO.OUT)

#Informacion de archivos y seción
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = 'gentle-pier-316115-f53cb14cbc91.json'
DIALOGFLOW_PROJECT_ID = 'gentle-pier-316115'
DIALOGFLOW_LANGUAGE_CODE = 'es'
SESSION_ID = 'me'
voz="Voice1"
flag=True

#Generar diccionario

try:
    file=open("audioNames.txt","r")
    audios={}
    for line in file:
        line=line.strip("\n")
        line=line.split(":")
        audios[line[2]]=[line[0],line[1]]

except FileNotFoundError:
    print("No se encontró audioNames.txt.")
    flag=False

#Loop principal de funcionamiento
while flag==True:

    mensajeALeer = str(input("\n\tTexto: "))
    session_client = dialogflow_v2.SessionsClient()
    session = session_client.session_path(DIALOGFLOW_PROJECT_ID, SESSION_ID)
    text_input = dialogflow_v2.types.TextInput(text=mensajeALeer, language_code=DIALOGFLOW_LANGUAGE_CODE)
    query_input = dialogflow_v2.types.QueryInput(text=text_input)
    try:
        response = session_client.detect_intent(session=session, query_input=query_input)
    except InvalidArgument:
        print("\t\nArgumento inválido, intente denuevo\n")

    #Parametros opcionales para se mostrados en pantalla

    #print("Query text:", response.query_result.query_text)
    #print("Detected intent:", response.query_result.intent.display_name)
    #print("Detected intent confidence:", response.query_result.intent_detection_confidence)
    #print("Fulfillment text:", response.query_result.fulfillment_text)

    print("\t\n", response.query_result.fulfillment_text ,"\n")
    respuesta=audios[response.query_result.fulfillment_text]
    DirRespuesta="AudioFiles/"+voz+"/"+respuesta[0]+"/"+respuesta[1]
    print(DirRespuesta)
    playsound(DirRespuesta)

    if response.query_result.intent.display_name in ["Pan tostado con saludo","Quiero pan tostado (buenos modales)"]:
        print("Tostando...")
        GPIO.output(21,GPIO.HIGH)
        time.sleep(60)
        GPIO.output(21,GPIO.LOW)
        print("Tostada lista.")
        flag=False
    elif response.query_result.intent.display_name=="Quiero pan tostado (malos modales)":
        print("Tostando...")
        aob=randint(0,1)
        if aob==0:
            duracion=30
        elif aob==1:
            duracion=120
        GPIO.output(21,GPIO.HIGH)
        time.sleep(duracion)
        GPIO.output(21,GPIO.LOW)
        print("Tostada lista.")
        flag=False
