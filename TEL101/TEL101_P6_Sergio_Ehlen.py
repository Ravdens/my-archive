def perfect_int():
    while True:
        try:
            num=int(input("Ingrese un entero: "))
            print(num)
            return(num)
        except ValueError:
            print("Tipo de dato invalido, Intente denuevo")

def perfect_float():
    while True:
        try:
            num=float(input("Ingrese un entero: "))
            print(num)
            return(num)
        except ValueError:
            print("Tipo de dato invalido, Intente denuevo")

def tasas(L):
    count=-1
    newL=[]
    while count<len(L)-1:
        try:
            newL.append(L[count+1]/L[count+2]) #Intento agregar los valores a la nueva lista
            count+=2
        except ZeroDivisionError:
            print("Existe una division por 0, intente con otra lista")
            return [] #Retorna lista vacía
        except IndexError:
            if L==[]:
                print("Error, la lista entregada está vacia.")
            elif len(L)%2!=0:
                print("Eror, la lista contiene una cantidad de valores impar.")
            return[]
    return newL


def reparar(L):
    contador=0
    errores=0 #Me dice si hay errores 
    while contador<len(L):
        try:
            error="a" #error me dice cual es el valor problematico
            a=L[contador]
            a/1 #Pruebo si a es un numero
            error="b" 
            b=L[contador+1]
            b/1 #Pruebo si b es un numero
            a/b #Pruebo si el dividendo es 0
            errores-=1 #Si resta un error si la division se hiso sin problemas
        except ZeroDivisionError:
            print(f"ZeroDivisionError en L{contador+1}: {contador+1}")
            L[contador+1]=0.001 #reemplazo la division por 0
        except IndexError:
            print("IndexError, el largo de la lista es impar.")
            L.append(1) #Agrego un uno para evitar imparidad
        except TypeError:
            if error=="a": #Si el error es en el dividendo
                print(f"TypeError en L[{contador}]: {L[contador]}")
                L[contador]=perfect_int()
            elif error=="b": #Si es en el divisor
                print(f"TypeError en L[{contador+1}]: {L[contador+1]}")
                L[contador+1]=perfect_int()
        finally:
            contador+=2 #Se suma dos al contador para ir chequeando los valores de a 2
            errores+=1 #Se agrega 1 error siempre.
    if errores==0: #Si al final del while los errores quedaron en 0 entonces no hubo errores
        print("La lista no requiere reparación.")
    return L

'''
Listas puestas por el profe en el control (para probar llamado de funciones)

F1 =["quince" , 20 , 12.5 , ".1" , "01"]
F2 =[10, 0, 5, 2, 22, 11]
F3 =[10, 2.5, 5, 2, 22]
F4 =[10.3, 2.5, 5, 2, 22, 11]
F5 =[10, 2.5, 5, 2, 22, 11]
print(reparar(F1))

'''