
palabra1="patata"   #Implemento las variables iniciales (se pueden cambiar intentos y la palabra.)
descubiertas1=[]
intentos1=5
jugando=True        #Jugando servirá para deicrme que la persona está jugando y no ha terminado la partida.

def ocultarPalabra(palabra,descubiertas):   #Esta función ocultará la palabra
    Letraspalabraoculta=[]
    str1=""
    for i in palabra:   #Chequeo cada letra de la palabra
        if i in descubiertas: #La comparo con cada letra en descubiertas
            Letraspalabraoculta.append(i) #Si ya estan descubiertas, las agrego a la palabra
        else:
            Letraspalabraoculta.append("*") #Si no está descubierta, se pone un asterisco
    for i in Letraspalabraoculta:
        str1+=i #La lista de letras que cree las sumo al string que voy a entregar devuelta (la palabra oculta)
    return str1


def evaluarLetra(letra,palabra,descubiertas): #Validación
    if (letra in palabra and letra!=descubiertas): #Se chequea que esta sea valida, estando en la palabra y no en las descubiertas
        return True
    else:
        return False

print("\nBIENVENIDO AL JUEGO DEL MINI-AHORCADO") #Se inicia el juego
print("(Tenga en cuenta que el juego solo recibe letras minusculas)")
while jugando==True: #El loop principal del juego
    if intentos1==0: #Se chequea si hay intentos todavíá
        print("Lo siento! Has muerto ahorcado D:")
        print(f'La palabra era "{palabra1}"')
        break   #Si no lo hay se rompe el ciclo de juego antes de pedir mas letras

    else: #Se permite jugar si hay intentos aun
        palabraoculta=ocultarPalabra(palabra1,descubiertas1) #Se genera la palabra oculta
        if palabraoculta==palabra1: #Se chequea que la palabra no esté ya completa
            jugando=False #Si lo está, se cierra el loop de juego
        else:
            print(f"\nAhora mismo tiene {intentos1} intentos.")
            print(f"Palabra: {palabraoculta}") 
            while True: #Si no, se abre otro while para pedir letras validas
                letra=input("\nIngrese una letra: ") #Se pide una letra
                if (letra.isalpha()==True and len(letra)==1): #Si este es tan solo 1 letra, se procede a otro ifelse
                    if evaluarLetra(letra,palabra1,descubiertas1)==True:    #Se evalua la letra, si esta es valida se agrega a las despejadas
                        descubiertas1.append(letra)
                    else:
                        intentos1-=1 #Si la letra metida es valida, pero no está en la palabra, se resta un intento y se vuelve al while prinicpal de juego
                    break   
                else: #Si lo entregado no es una sola letra entonces solo se repite el while mas pequeño y se pide otra letra
                    continue

if intentos1!=0: #Al cerrar el loop principal de juego, se revisa que hayan quedado intentos validos, y se imprime la pantalla de victoria
    print(f"Palabra: {palabra1}")
    print("Felicitaciones! Descubriste la palabra! :D")