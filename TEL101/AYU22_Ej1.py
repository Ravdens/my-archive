
def datos(archivo1,archivo2):
    A1=open(archivo1,"r")#Alumnos.csv
    A2=open(archivo2,"r")#Notas.csv
    listaza=[]
    for nombre in A1:
        nombre=nombre.strip("\n")
        linea1=nombre.split(",")
        linea2=A2.readline()
        linea2=linea2.strip("\n")
        linea2=linea2.split(":")
        datoAlumno=[linea1,linea2]
        listaza.append(datoAlumno)
    return listaza



def promedio_total(alumnos, archivo):
    file=open(archivo,"w")
    for alumno in alumnos:
        notas=alumno[1]
        total=0
        contador=0
        for nota in notas:
            total+=float(nota)
            contador+=1
        promedio=round(total/contador , 1)
        if float(promedio)>=4.5:
            file.writelines([str(alumno[0][0])," ",str(alumno[0][1])," ", str(alumno[0][2]),",",str(promedio)," Aprobado\n"])
        else:
            file.writelines([str(alumno[0][0])," ",str(alumno[0][1])," ", str(alumno[0][2]),",",str(promedio)," Reprobado\n"])
    file.close()



def promedio_personal(nombre,alumnos):
    total=0 
    matches=0
    listaza=[]
    for alumno in alumnos:
        if nombre in alumno[0]:
            matches+=1
            cantNotas=len(alumno[1])

            for nota in alumno[1]:
                total+=float(nota)
            
            promedio=round(total/cantNotas , 1)
            try:
                if promedio>=4.5:
                    listaza.append(alumno[0],promedio)
                    print(f"El almuno {alumno[0][0]} {alumno[0][1]} {alumno[0][2]} está aprobando con promedio {promedio}")
                else:
                    raise ValueError
            except ValueError:
                print(f"El almuno {alumno[0][0]} {alumno[0][1]} {alumno[0][2]} está reprobando con promedio {promedio}")
    
    if matches>0:
        return listaza
    else:
        print("No se encontró ningun alumno")
        return False


#Funcion que me gusta meter a mi código con menues xddd
def option(n):
    while True:
        try:
            opt=int(input("Opcion: "))
            if opt<=n and opt>0:
                return opt
            else:
                print("Opción invalida, intente denuevo.")
                continue
        except:
            print("Input invalido, trate denuevo.")
            continue



print("Bienvenido a sacador de promedios:")
alumnos=datos("alumnos.csv","notas.csv")
while True:
    print("---------------------------------------------------------")
    print("Opcion 1: Promedio total de alumnos en un archivo")
    print("Opcion 2: Busqueda y entrega personal de las notas de un o unos alumnos segun el nombre o el apellido paterno o materno")
    print("Opcion 3: Salir")
    print("---------------------------------------------------------")
    opt=option(3)
    if opt==1:
        nombre=str(input("Ingrese nombre del archivo: "))
        promedio_total(alumnos,nombre)
    elif opt==2:
        nombre=str(input("Ingrese nombre a buscar: "))
        promedio_personal(nombre,alumnos)
    elif opt==3:
        print("Gracias por utilizar el programa, cerrando...")
        break
