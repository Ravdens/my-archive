def AgregarArtista(cancionero,artista): 
    albumsdelartista=[]                                                     #Se hace altiro una lista para agregarla al mismo tiempo
    cancionero.append(artista)
    cancionero.append(albumsdelartista)

def AgregarAlbum(cancionero,artista,album):
    cancionesdelalbum=[]
    posArtista=cancionero.index(artista)                                    #La lista de albums del artista siempre estará 1 posicion despues del artista mismo.
    pre_album=cancionero.pop(posArtista+1)                                  #Uso pop y append para sacar la lista del artista, agregar lo necesario, y devolverla.
    pre_album.append(album)
    pre_album.append(cancionesdelalbum)
    cancionero.insert(posArtista+1,pre_album)

def AgregarCancion(cancionero,artista,album,cancion):
    posAlbumsDeArtista=(cancionero.index(artista))+1
    posAlbumbuscado=(cancionero[posAlbumsDeArtista].index(album))+1         #Tuve que usar hartas variables aquí y está mas feo de lo que debería pero lo único que hace
    pre_album=cancionero[posAlbumsDeArtista].pop(posAlbumbuscado)           #es encontrar la lista de canciones que hay dentro de la lista de albumes y agregarla la cancion
    pre_album.append(cancion)
    cancionero[posAlbumsDeArtista].insert(posAlbumbuscado,pre_album)

def DiscosArtistas(cancionero,artista):
    print(f"EL artista de quien desea conocer es: {artista}")
    Albums=cancionero.index(artista)+1
    CantAlbums=len(cancionero[Albums])/2                                    #Basado en la cantidad de albums me facilito el proceso y cambio el texto entregado al usuario.

    if CantAlbums<1:
        print("Este artista actualmente no tiene albums.")

    elif CantAlbums==1:
        print(f"Su album es:\n{cancionero[Albums][0]}")                     #Para este caso es super simple entregar las canciones
        print(f"Sus canciones son:\n{cancionero[Albums][1]}")

    else:
        print("Sus albums son:")                                            #Para el caso de una album con multiples albums abro un while el cual a travs de un contador
        contador=0                                                          #me va a imprimir la mitad de los elemento (los cuales son los nombres de album) y la otra mitad
        while contador<=CantAlbums:                                         #que son los albumes en si de forma separada.
            print(cancionero[Albums][contador])
            contador+=1
            if len(cancionero[Albums][contador])==0:
                print("Este album está vacio.")
            else:
                print(f"Sus canciones son:\n{cancionero[Albums][contador]}")
            contador+=1



'''
    
                                  Estas llamadas a funcion solo las hise yo de prueba.

cancioneroG=[]

AgregarArtista(cancioneroG,"LuchoJara")

AgregarAlbum(cancioneroG,"LuchoJara","AlbumA")

AgregarAlbum(cancioneroG,"LuchoJara","AlbumB")

AgregarCancion(cancioneroG,"LuchoJara","AlbumA","CancionA")

AgregarCancion(cancioneroG,"LuchoJara","AlbumA","CancionB")

DiscosArtistas(cancioneroG,"LuchoJara")

'''