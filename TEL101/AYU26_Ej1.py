#Imports
import matplotlib.pyplot as plt
import numpy as np

#Funciones
def readFile(filename):
    try:
        file=open(filename,"r")

        newList=[[],[]]
        flag=True
        for line in file:

            if flag==True:
                flag=False
                continue

            line=line.strip("\n")
            line=line.split(",")
            newList[0].append(int(line[0]))
            newList[1].append(float(line[1]))
        file.close()
        return newList

    except FileNotFoundError:
        print("El archivo que busca no se encontró")
        return []
  
def generarGrafico(lista):

    x=np.array(lista[0])
    y=np.array(lista[1])

    fig=plt.figure(figsize=(13,6))
    plt.axis([1950, 2000, -50, 70])

    plt.ylabel("Temperatura")
    plt.xlabel("Años")
    plt.title("Media de temperatura a través de los años")

    plt.bar(x,y,width=1.2,color="lightblue")
    plt.show()

  
#Main
funcion1=readFile("temperaturas.txt")
generarGrafico(funcion1)