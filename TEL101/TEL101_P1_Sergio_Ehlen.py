
print("Ingrese una secuencia de 4 números.")
flag = True
while flag == True:                                             #Se inicia while para repetir el codigo.
    num1=int(input("Ingrese el primer numero: "))
    num2=int(input("Ingrese el segundo numero: "))
    num3=int(input("Ingrese el tercer numero: "))
    num4=int(input("Ingrese el cuarto numero: "))
    if num1==0 and num2==0 and num3==0 and num4==0:             #Caso que todos los numeros son 0.
        print("Esta no es una secuencia valida, ingrese otra secuencia.")
    elif num4-num3==num3-num2 and num2-num1==num3-num2:         #Se revisa la diferencia de los numeros para ver si es aritmetica.
        print("La secuencia es de progreción Aritmética.")   
        dif=num2-num1                                           #Se calcula la diferencia
        print(f"La diferencia comun es {dif}.")
        sig=num4+dif                                            #Se calcula el siguiente número.
        print(f"El siguiente número de la secuencia es {sig}.")
    elif num4%num1==0 and num3%num1==0 and num2%num1==0:        #Se revisa diferencia entre los numeros para ver si es geometrica
        print("La secuencia es de progreción Geométrica.")
        div=num2//num1                                          #Se calcula la diferencia multiplicatiba entre ellos
        print(f"La tasa comun es {div}.")
        sig=num4*div                                            #Se multiplica para ver el siguiente número
        print(f"El siguiente número de la secuencia es {sig}.")
    else:
        print("La secuencia no es ni PA ni PG.")                #Si no es ninguno de los dos