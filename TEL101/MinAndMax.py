while True:
    nom = float(input("Nominal: "))
    tol = int(input("Tolerancia: "))

    minimum = nom - (tol/100) * nom

    maximum = nom + (tol/100) * nom

    print(f"min: {minimum} , max: {maximum}")