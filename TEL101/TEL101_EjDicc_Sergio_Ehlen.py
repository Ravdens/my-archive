def getSpotifyInfo(filename): #sporifyData.csv
    try:
        file=open(filename,"r") #Intento abrir el archivo
    except FileNotFoundError:
        print("El archivo no se ha encontrado.") #Si no está se da error
        raise FileNotFoundError
    newDict={}
    for genre in file:
        genre=genre.strip("\n")
        genre=genre.split(",")
        newDict[genre[0]]=[genre[1],genre[2],genre[3]] #Se agregan al diccionario en el orden predeterminado
    file.close()
    return newDict
    
def getGenresData(infoDict,genres,data):
    listaNew=[]
    if data=="bailable": #Los index me dicen que tipo de dato voy a ingresar en la lista
        index=0 
    elif data=="duracion":
        index=1
    elif data=="popularidad":
        index=2
    else:
        print("Tipo de dato solicitado no existe.") #Si no era ninguno de los otros datos se tira error
        raise ValueError
    for genero in genres:
        try:
            listaNew.append(infoDict[genero][index]) #Se van agregando usando el index de antes
        except KeyError:
            print("Uno de los generos ingresados no existe") #Si hay key error el genero no existe
            raise KeyError
    return (genres,listaNew) #Se genera la tupla altiro en el return

def searchGenres(infoDict,search):
    matches=[] #lista con todos los encontrados
    letrasSearch=list(search) #Hago una lista con las letras que me dieron
    for genre in infoDict:
        letrasGenre=list(genre) #Hago una lista de cada genero
        sonIguales=True #Digo para empesar que son iguales
        for index in range(len(letrasSearch)):
            try:
                if letrasGenre[index]!=letrasSearch[index]: #Comparo cada letra de la busqueda en orden
                    sonIguales=False #Si no son iguales se cambia a falso
            except IndexError:
                sonIguales=False #Si es muy corto el nombre del genero tambien se cambia a falso
        if sonIguales==True:
            matches.append(genre) #Si sonIguales sigue verdad se agrega a los matches
    return matches #Se devuelven los matches

def printGenresData(infoDict,reporte):
    print("Ingrese que dato desea buscar:")
    print("'Bailable (b)'\n'Duracion' (d)\n'Popularidad' (p)")
    while True:
        try:
            respuesta=str(input("Acción: ")) #Se intenta pedir el dato a conseguir
            if respuesta in ["b","d","p"]: #Si es uno de los validos
                break #Se deja de intentar
            elif respuesta=="": #Si es string vacio
                print("Abortando creación de archivo...") #se cierra la funcion completa
                return
            else:
                print("String ingresado no es correcto, intente denuevo: ") #Si el string no srive
        except:
            print("Ingreso no es apropiado, intente denuevo.") #Lo ingresado causa error, se intenta deneuvo.
            continue

    #while True:            Loop para pedir generos




    try:
        newFile=open(reporte,"w") #Intento abrir el nuevo archivo
    except FileNotFoundError:
        print("El nombre de archivo entregado no es valido.") #Si me tira error lo raiseo
        raise FileNotFoundError

    newFile.close()




#print(getGenresData(getSpotifyInfo("spotifyData.csv"),["korean pop","hip hop"],"duracion"))
#print(searchGenres(getSpotifyInfo("spotifyData.csv"),"ja"))