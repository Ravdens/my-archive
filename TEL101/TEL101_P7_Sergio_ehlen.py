def lee_zonas(archivo_zonas,archivo_subzonas):
    try:                                    
        fzon=open("zonas.txt","r")          #Intento abrir ambos archivos por si existen
        fsubzon=open("subzonas.txt","r")
    except:
        print("Archivos ingresados no se han encontrado... :(") #Error no se encuentran
        return #Se termina la función
    else:
        fsubzon.close() #Si no ubo error serro lo que no necesito y estaba solo de prueba
    listasa=[] #Listasa es la lista que voy a devolver
    for line in fzon: #fzon es file_zonas
        fsubzon=open("subzonas.txt","r") #Abro cada vez el file_subzonas para reinicar puntero
        newL=[] #Creo una nueva linnea
        line=line.strip("\n") #Quito los newlinne
        l=line.split(":") #Quito los :
        newL.append(l) #Agrego la zona a la lista a entregar
        subzonas=[] #Creo una lista de subzonas que agregar
        for subline in fsubzon:
            z1=line[:2] #Veo el indice de cada subzona
            z2=subline[:2]
            if z1==z2: #Si tienen el mismo indice la agrego
                subline=subline.strip("\n")
                subline=subline.split(":")
                subzonas.append(subline)
        newL.append(subzonas)
        listasa.append(newL) #Agrego la nueva linea a la lista final
    fzon.close() #cierro los archivos
    fsubzon.close() 
    return listasa #Devuelvo la lista

def ley_promedio(zona):
    subzonas=zona[1] #encuentro las subzonas
    total=0 #inicio el total en 0
    for subzona in subzonas:
        total+=float(subzona[2]) #Saco las leyes y las sumo al total
    total/=len(subzonas) #las divido
    return total #retorno el total

def create_file():
    lista=(lee_zonas("zonas.txt","subzonas.txt")) #Llamdo a la primera funcion para leer los archivos
    while True: #Repito si por cualquier motivo el nombre es invalido
        try:
            nombre=input("Ingrese nombre de archivo a generar: ")
            nombre+=".txt"
            new=open(nombre,"w")
            break
        except:
            print("Nombre ingresado no es un nombre valido")
    new.write("Ley promedio por zona: \n") #Inicio el archivo con la primera frase
    new.close()
    new=open(nombre,"a") #Lo abro ahora para ir agregando con append
    for zona in lista: #Agrego parte por parte lo que me pide el formato
        new.write("-")
        new.write(zona[0][0])
        new.write(": ")
        new.write(str(ley_promedio(zona)))
        new.write("\n")
    new.close() #Cierro el archivo


create_file() #Llamado de funciones.