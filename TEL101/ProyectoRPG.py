from pynput import keyboard
from time import sleep
from random import randint

def on_press(key):
    if key == keyboard.Key.esc:
        return False  
    try:
        k = key.char  
    except:
        k = key.name  
    if k in ['a', 'd', 'w',"s"]: 
        ControlarMenu(k)
        return True

snake=["_:","L","/","--"]
enemigo=snake
hpJugador=200
hpEnemigo=200
opcion=0
status="Combat"

def ControlarMenu(k):
    global status
    status="Combat"
    global opcion
    if k=="w" and opcion==0:
        status="bow"
    elif k=="w" and opcion==1:
        status="sword"
    elif k=="w" and opcion==2:
        status="heal"
    elif k=="w" and opcion==3:
        status="run"
    elif k=="w" and opcion==4:
        status="item"
    elif k=="d" and opcion==4:
        status="calm"
    elif k=="d":
        opcion+=1
        status="calm"
    elif k=="a" and opcion==0:
        status="calm"         
    elif k=="a":
        opcion-=1
        status="calm"
    elif k=="s":
        status="calm"
    printMenu(status)

def printMenu(status):
        if status=="calm":
            PrintJugador()
            if opcion==0:
                print("('[Bow]')---[Sword]---[Heal]---[Run]---[Item]")
            elif opcion==1:
                print("[Bow]---('[Sword]')---[Heal]---[Run]---[Item]")
            elif opcion==2:
                print("[Bow]---[Sword]---('[Heal]')---[Run]---[Item]")
            elif opcion==3:
                print("[Bow]---[Sword]---[Heal]---('[Run]')---[Item]")
            elif opcion==4:
                print(print("[Bow]---[Sword]---[Heal]---[Run]---('[Item]')"))
        elif status=="bow":
            Print_Shoot()
        elif status=="sword":
            Print_Slash()
        elif status=="heal":
            Print_Heal()
        elif status=="run":
            Print_Run()
        elif status=="item":
            return

def Print_Shoot():
    global hpEnemigo
    print(f"\n\n\n\n\n\n\n\n\n\n    {enemigo[0]}                        /| o")
    print(f"    {enemigo[1]}                      <-+-+/k| ")
    print(f"    {enemigo[2]}                         \| /| ")
    print(f"   {enemigo[3]}")
    sleep(0.5)
    print(f"    {enemigo[0]}                        /| o")
    print(f"    {enemigo[1]}              <-----<   | |/k| ")
    print(f"    {enemigo[2]}                         \| /| ")
    print(f"   {enemigo[3]}")
    sleep(0.5)
    print(f"    {enemigo[0]}                        /| o")
    print(f"    {enemigo[1]}   <-----<              | |/k| ")
    print(f"    {enemigo[2]}                         \| /| ")
    print(f"   {enemigo[3]}")
    sleep(0.5)

    
    if randint(0,10)>=3:
        if randint(0,10)==10:
            print(f"    {enemigo[0]}                        /| o")
            print(f"  <-{enemigo[1]}---<   [CRIT!]          | |/k| ")
            print(f"    {enemigo[2]}                         \| /| ")
            print(f"   {enemigo[3]}")
            hpEnemigo-=150
        else:
            print(f"    {enemigo[0]}                        /| o")
            print(f"  <-{enemigo[1]}---<    hit!            | |/k| ")
            print(f"    {enemigo[2]}                         \| /| ")
            print(f"   {enemigo[3]}")
            hpEnemigo-=50
    else:
            print(f"    {enemigo[0]}                        /| o")
            print(f"--< {enemigo[1]}        miss...         | |/k| ")
            print(f"    {enemigo[2]}                         \| /| ")
            print(f"   {enemigo[3]}")
    sleep(1)      
    ControlarMenu("a")
    
def Print_Slash():
    global hpEnemigo
    global hpJugador
    print(f"\n\n\n\n\n\n\n\n\n\n    {enemigo[0]}                         |  o")
    print(f"    {enemigo[1]}                          +--k| ")
    print(f"    {enemigo[2]}                            /| ")
    print(f"   {enemigo[3]}")
    sleep(0.5)
    print(f"    {enemigo[0]}                       \   o")
    print(f"    {enemigo[1]}                          x-k ")
    print(f"    {enemigo[2]}                            \ \ ")
    print(f"   {enemigo[3]}")
    sleep(0.5)
    if randint(0,10)>=2:
        if randint(0,10)>=(9):
            print(f"----------------------------------------------")
            print(f"   *{enemigo[0]} o                         ")
            print(f" ---{enemigo[1]}x-k\   |Crítical hit!|")
            print(f"   *{enemigo[2]} / |  ")
            print(f"----------------------------------------------")
            hpEnemigo-=150
        else:
            print(f"    {enemigo[0]}*\  o")
            print(f"    {enemigo[1]}   \=k\  hit!")
            print(f"    {enemigo[2]}     \ \   ")
            print(f"    {enemigo[3]}")
            hpEnemigo-=100
    else:
        print(f"    {enemigo[0]})  /o")
        print(f"    {enemigo[1]} ) /=k|  [BLOCK] ")
        print(f"    {enemigo[2]} )  / |   ")
        print(f"    {enemigo[3]})")
        hpEnemigo-=10
        hpJugador-=20

    sleep(1)
    ControlarMenu("a")

def Print_Heal():
    global hpJugador
    print(f"\n\n\n\n\n\n\n\n\n\n     {enemigo[0]}                       \ o /")
    print(f"    {enemigo[1]}                           k ")
    print(f"    {enemigo[2]}                          /| ")
    print(f"   {enemigo[3]}")
    sleep(0.5)
    print(f"                             *      *")
    print(f"    {enemigo[0]}                        \ o /")
    print(f"    {enemigo[1]}                           k ")
    print(f"    {enemigo[2]}                          /| ")
    print(f"   {enemigo[3]}")
    sleep(0.5)
    print(f"                             *+200HP*")
    print(f"    {enemigo[0]}                        \ o /")
    print(f"    {enemigo[1]}                           k ")
    print(f"    {enemigo[2]}                          /| ")
    print(f"   {enemigo[3]}")
    hpJugador+=200
    sleep(1)
    ControlarMenu("a")

def Print_Run():
    print("\n\n\n\n\n\n")
    print(".")
    sleep(0.3)
    print(".")
    sleep(0.3)
    print(".")
    sleep(0.3)  
    print("...")
    sleep(1)
    if randint(0,10)>=6:

        print("|||||||||||||||||||||||||||||||||")
        print("|||||||||||||||||||||||||||||||||")
        print("|||||||ESCAPED SUCCESFULLY|||||||")
        print("|||||||||||||||||||||||||||||||||")
        print("|||||||||||||||||||||||||||||||||")
    else:
        print("=================================")
        print("=================================")
        print("======= But it failed... ========")
        print("=================================")
        print("=================================")
        sleep(1)
        ControlarMenu("a")

def PrintJugador():
    global hpEnemigo
    global hpJugador

    print(f"\n\n\n\n\n\n\n\n\n\n  Hp:{hpEnemigo}                     Hp:{hpJugador}")
    print(f"    {enemigo[0]}                          o")
    print(f"    {enemigo[1]}                          /k| ")
    print(f"    {enemigo[2]}                          /| ")
    print(f"   {enemigo[3]}")


ControlarMenu("a")
listener = keyboard.Listener(on_press=on_press)
listener.start()  
listener.join()