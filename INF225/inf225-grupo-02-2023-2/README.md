# Grupo 02

Este es el repositorio del *Grupo 2*, cuyos integrantes son:

* Sergio Ehlen Montero - 202130016-3
* Camilo Troncoso Hormazabal - 202130004-k
* Maximo Flores Suarez - 202130019-8
* **Tutor**: Paula

## Wiki

Puede acceder a la Wiki mediante el siguiente [enlace](https://gitlab.com/Ravdens/inf225-grupo-02-2023-2/-/wikis/home)

## Videos

* [Video presentación cliente](https://youtu.be/NI3GSDZrnHYURL)
* [Video presentación avance 1](https://youtu.be/HtEZ-BanD-M)
* [Video presentación avance 2](https://youtu.be/I8-6KdUZLRk)
* [Video presentación resultados finales](https://youtu.be/5aPygSyCxB8)


## Aspectos técnicos relevantes

El proyecto se puede levantar utilizando en la carpeta raíz los comandos:

> docker compose build

> docker compose up

y bajar con

> docker compose down

Lo mismo aplica a los contenedores individuales en sus respectivas carpetas.

Se escucha en los siguientes puertos:
 - FE: 3000
 - API_Composition 4000
 - API_1: 4001
 - API_2: 4002
 - API_3: 4003
 - mySql: 3306

#### Notas:

- En general el front-end puede tomar 1 o 2 minutos en incializar. 
- Las APIs necesitan ser incializadas despues de la base de datos. Si esto falla, se pueden reiniciar manualmente
