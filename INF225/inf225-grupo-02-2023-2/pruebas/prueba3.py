import requests as rq
import json
api = "http://localhost:4000/"
passes= False

#Parametros
nombre = "Bakery Bakeshop"
foto = "/../../tiendas/2.jpg"
owner_id = 3


#Query
body = {
    "tienda_name":nombre,
    "foto":foto,
    "owner_id":3,
}
resp_1 = rq.post(api+"tienda/",json=body)
resp_2 = rq.get(api+"tienda/")

#Registro
jsonfile = json.dumps(resp_2.json(),indent=4)
with open("prueba3_results.json", "w") as outfile:
    outfile.write(jsonfile)

#Validacion
with open('prueba3_expected.json', 'r') as openfile:
    expected = json.load(openfile)

if resp_2.json() == expected:
    passes = True
else:
    passes = False

print("Prueba 2: crear tienda queda registrada")
print("Input:")
print(body)
print(f"Output:")
print(resp_2.json())
print(f"Pasa: {passes}")