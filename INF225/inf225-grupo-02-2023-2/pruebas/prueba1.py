import requests as rq
import json
api = "http://localhost:4000/"
passes= False

#Parametros
username = "xxjimmyxx"
password = "123"

#Query
body = {
    "user_name":username,
    "password":password,
}
resp = rq.post(api+"user/login/",json=body)


#Registro
jsonfile = json.dumps(resp.json(),indent=4)
with open("prueba1_results.json", "w") as outfile:
    outfile.write(jsonfile)


#Validación
with open('prueba1_expected.json', 'r') as openfile:
    expected = json.load(openfile)

if resp.json() == expected:
    passes = True
else:
    passes = False

print("Prueba 1: login correcto")
print("Input:")
print(body)
print("Output: ")
print(resp.json())
print(f"Pasa: {passes}")