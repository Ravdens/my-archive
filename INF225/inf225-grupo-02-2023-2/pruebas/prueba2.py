import requests as rq
import json
api = "http://localhost:4000/"
passes= False

#Parametros
nombre = "Piñata halloween"
foto = "/../../productos/2.jpg"
costo = 15000
descripcion = "Piñata decorativa para eventos de halloween"
tienda_name = "Veysa manualidades"
tienda_id = 3


#Query
body = {
    "name":nombre,
    "foto":foto,
    "costo":costo,
    "description":descripcion,
    "tienda_name":tienda_name,
    "tienda_id":tienda_id, 
}
resp_1 = rq.post(api+"producto/",json=body)
resp_2 = rq.get(api+"producto/")

#Registro
jsonfile = json.dumps(resp_2.json(),indent=4)
with open("prueba2_results.json", "w") as outfile:
    outfile.write(jsonfile)

#Validacion
with open('prueba2_expected.json', 'r') as openfile:
    expected = json.load(openfile)

if resp_2.json() == expected:
    passes = True
else:
    passes = False

print("Prueba 2: crear producto queda registrado")
print("Input:")
print(body)
print(f"Output:")
print(resp_2.json())
print(f"Pasa: {passes}")