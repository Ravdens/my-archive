const express = require('express');
const app = express();
require('dotenv').config();
const routes = require('./src/routes/index');

app.use(express.json());
app.use(routes);
require("./routes")(app);


app.listen(process.env.PORT_API, () => {
    console.log("\n > API Composition up")
    console.log(' > Server running on port 4000\n');
});


module.exports = app;