const producto = require("./src/routes/producto.js");
const categoria = require("./src/routes/categoria.js");
const prodXCat = require("./src/routes/prodXCat.js");
const tienda = require("./src/routes/tienda.js");
const tipo = require("./src/routes/tipo.js");
const login = require("./src/routes/login.js");
const userType = require("./src/routes/userType.js");
const mensaje = require("./src/routes/mensaje.js")
const ind = require("./src/routes/index.js")

module.exports = (app) =>{
    app.use(tienda);
    app.use(tipo);
    app.use(login);
    app.use(producto);
    app.use(categoria);
    app.use(prodXCat);
    app.use(mensaje);
    app.use(userType);
    app.use(ind);
};