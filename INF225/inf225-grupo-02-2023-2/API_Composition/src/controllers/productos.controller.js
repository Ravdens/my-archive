require('dotenv').config();

const getProductos = async (req, res) => {
  console.log(process.env.API2+'producto/')
  const response = await fetch(process.env.API2+"producto/",{
    method: "GET"
  })
  const result = await response.json() 
  //console.log(result);
  res.json(result).status(200);
};

const createProducto = async (req, res) => {
  const response = await fetch(process.env.API2+"producto/",{
    method: "POST",
    headers:{
      "Content-type":"application/json",
    },
    body: JSON.stringify(req.body),
  });
  const result = await response.json();
  res.json(result).status(200);
};

const getProducto = async (req, res) => {
  console.log(process.env.API2+'producto/')
  const {id} = req.params;
  const response = await fetch(process.env.API2+"producto/"+id+"/",{
    method: "GET"
  })
  const result = await response.json() 
  //console.log(result);
  res.json(result).status(200);
};

const getProductosByTienda = async (req, res) => {
  console.log(process.env.API2+'producto/')
  const {id} = req.params;
  const response = await fetch(process.env.API2+"producto/tienda/"+id+"/",{
    method: "GET"
  })
  const result = await response.json() 
  //console.log(result);
  res.json(result).status(200);
};



module.exports = { getProductos,createProducto,getProducto,getProductosByTienda};