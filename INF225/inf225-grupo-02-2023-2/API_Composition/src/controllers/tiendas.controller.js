require('dotenv').config();

const getTiendas = async (req, res) => {
  console.log(process.env.API1+'tienda/')
  const response = await fetch(process.env.API1+"tienda/",{
    method: "GET"
  })
  const result = await response.json() 
  //console.log(result);
  res.json(result).status(200);
};

const getTienda = async (req, res) => {
  const {id} = req.params;
  console.log(process.env.API1+'tienda/')
  const response = await fetch(process.env.API1+"tienda/"+id+"/",{
    method: "GET"
  })
  const result = await response.json() 
  //console.log(result);
  res.json(result).status(200);
};


const createTienda = async (req, res) => {
  const response = await fetch(process.env.API1+"tienda/",{
    method: "POST",
    headers:{
      "Content-type":"application/json",
    },
    body: JSON.stringify(req.body),
  });
  const result = await response.json();
  res.json(result).status(200);
};

module.exports = { getTiendas,getTienda,createTienda};
