require('dotenv').config();


const createMensaje = async (req, res) => {
  const response = await fetch(process.env.API3+"mensaje/",{
    method: "POST",
    headers:{
      "Content-type":"application/json",
    },
    body: JSON.stringify(req.body),
  });
  const result = await response.json();
  res.json(result).status(200);
};


const getMensajes = async (req, res) => {
  console.log(process.env.API3+'mensaje/')
  const response = await fetch(process.env.API3+"mensaje/",{
    method: "GET"
  })
  const result = await response.json() 
  res.json(result).status(200);
};

const getMensajesForId = async(req,res)=>{
    const {id} = req.params;
    const response = await fetch(process.env.API3+"mensaje/"+id+"/",{
      method: "GET"
    })
    const result = await response.json()
    res.json(result).status(200);
};


module.exports = { createMensaje, getMensajes, getMensajesForId };
