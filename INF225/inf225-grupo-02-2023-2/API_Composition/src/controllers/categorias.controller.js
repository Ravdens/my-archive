require('dotenv').config();

const createCategoria = async (req, res) => {
  const response = await fetch(process.env.API2+"categoria/",{
    method: "POST",
    headers:{
      "Content-type":"application/json",
    },
    body: JSON.stringify(req.body),
  });
  const result = await response.json();
  res.json(result).status(200);
};

const getCategoria = async (req, res) => {
  console.log(process.env.API2+'categoria/')
  const response = await fetch(process.env.API2+"categoria/",{
    method: "GET"
  })
  const result = await response.json() 
  console.log(result);
  res.json(result).status(200);
};

module.exports = { createCategoria ,getCategoria };
