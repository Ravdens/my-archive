const express = require("express");

const router = express.Router();

const {
  getTipo,
  createTipo,
} = require("../controllers/tipos.controller.js");

router.get("/tipo", getTipo);
router.post("/tipo", createTipo);

module.exports = router;
