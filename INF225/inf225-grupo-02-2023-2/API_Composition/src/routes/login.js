const express = require("express");
const { getUsers, getUser,loginUser , createUser} = require("../controllers/users.controller.js");

const router = express.Router();

router.get("/user", getUsers);
router.get("/user/:id",getUser);
router.post("/user/login", loginUser);
router.post("/user/",createUser)

module.exports = router;
