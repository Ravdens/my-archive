import reflex as rx
from FE.state import State

def mensaje_modal()->rx.modal:
    return rx.modal(
        rx.modal_overlay(
            rx.modal_content(
                rx.modal_header("Nuevo mensaje a "+State.writing_to),
                rx.modal_body(
                    rx.text_area(value=State.current_mensaje,on_change=State.set_current_mensaje,),
                ),
                rx.modal_footer(
                    rx.hstack(
                        rx.button("Enviar", on_click=State.enviar_mensaje),
                        rx.button("Cancelar",on_click=State.close_mensaje),
                    ),
                ),
            ),
        ),
        bg="white",
        close_on_esc=True,
        is_centered=True,
        is_open=State.bool_mensajes_modal,
        close_on_overlay_click=True
    )