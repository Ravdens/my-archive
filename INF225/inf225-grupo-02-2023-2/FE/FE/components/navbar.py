import reflex as rx
from FE.state import State

def navbar()->rx.box:
    return rx.box(
        rx.hstack(
            rx.hstack(
                rx.button(
                    rx.image(src="/favicon.ico",
                    height=50,width=50,border_radius="5px",padding="1",bg="white"),
                    on_click=rx.redirect("/"),variant="unstyled"
                ),
                rx.breadcrumb(
                    rx.breadcrumb_item(
                        rx.heading("Tienda colaborativa", 
                        size="sm",color="white",bg="green",padding="2",),
                    ),
                ),
                spacing="5",
                padding_left="2",
            ),
            rx.spacer(),

            rx.cond(
                State.logged_in,
                rx.hstack(
                    rx.button(
                        rx.icon(tag="bell",color="white",bg="green",size="md"),
                        on_click=rx.redirect("/mensajes"),variant="unstyled",bg="green",
                    ),
                    rx.heading(State.login_u_name,size="md",color="white"),
                    rx.menu(
                        rx.menu_button(
                            rx.avatar(name=str(State.current_usuario["user_name"]),size="md",color="white"),
                            rx.box(),
                        ),
                        rx.menu_list(  
                            rx.cond(
                                State.usuario_is_emprendedor,
                                rx.menu_item("Mi tienda",on_click=rx.redirect("/tienda/2"))
                            ),
                            rx.menu_item("Ayuda"),
                            rx.menu_item("Opciones"),
                            rx.menu_item("Cerrar sesión",on_click=State.log_out)
                        ),
                    ),
                    spacing="5",
                    padding_right="2",
                ),
                rx.hstack(
                    rx.button("Iniciar sesión",on_click=State.open_login()),
                    rx.menu(
                        rx.menu_button(
                            rx.avatar(name="?",size="md",color="white"),
                            rx.box(),
                        ),
                        rx.menu_list(
                            rx.menu_item("Ayuda"),
                            rx.menu_item("Opciones"),
                        ),
                    ),
                    spacing="5",
                    padding_right="2",
                ),           
            
            ),

        spacing="380"
        ),
    height="55",
    bg="green",
    backdrop_filter="auto",
    position="sticky",
    top="0",
    z_index="5",
    )