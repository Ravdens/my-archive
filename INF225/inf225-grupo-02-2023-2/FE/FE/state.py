import reflex as rx
from random import choice,randint
import requests as rq
from .models.Producto import Producto


container = True
if container:
    api ="http://host.docker.internal:4000/"
else:
    api = "http://localhost:4000/"


class State(rx.State):

    page_color_theme: str = choice(["lightblue","lightgreen","purple","tomato","orange","yellow"])
    
    current_p: dict = {}
    current_u: dict = {}
    current_t: dict = {}
    
    productos_t: list[dict] = []
    productos_home: list[dict] = []
    productos_home_class: list[Producto] = []
    categorias: list[str] = []
    home_is_empty: bool = False
    filtro: str= ""
    
    login_u_name: str = ""
    login_pword: str = ""
    bool_login_modal: bool = False
    logged_in: bool = False
    usuario_is_emprendedor: bool = False
    
    all_mensajes: list[dict]= []
    bool_mensajes_modal: bool = False
    destinatario: dict = {}
    writing_to: str = ""
    current_mensaje: str = ""

    #Busqueda productos
    def filtrar_productos(self):
        cat_id = 0
        qcat = rq.get(api+"categoria/").json()
        for cat in qcat:
            if cat["nameCategory"]==self.filtro:
                cat_id = cat["id"]
                break

        self.home_is_empty=False
        if self.filtro=="":
            self.productos_home=rq.get(api+"producto/").json()
        
        else:
            self.productos_home = rq.get(api+"producto/categoria/"+str(cat_id)+"/").json()
            
            if len(self.productos_home)==0:
                self.home_is_empty=True

# Esta seccion esta aqui temporalmente es solo para que funcione redirijir
        self.productos_home_class = []
        for p in self.productos_home:
            print(p)
            prod_i = Producto()
            prod_i.create(p["id"],p["id"],p["foto"],p["costo"],p["name"],p["description"],p["tienda_name"],"No implementado aun",p["tienda_id"])
            self.productos_home_class.append(prod_i)

    #Login
    def open_login(self):
        self.bool_login_modal = True
    def close_login(self):
        self.bool_login_modal = False

    def try_login(self):
        print("Intentando: "+self.login_u_name+" / "+self.login_pword)
        req = {
            "user_name":self.login_u_name,
            "password":self.login_pword,
        }
        resp = rq.post(api+"user/login",json=req).json()

        if resp["validated"]==True:
            self.logged_in = True
            self.current_u = rq.get(api+"user/"+str(resp["id"])).json()
            self.usuario_is_emprendedor = (self.current_u["tipo_id"]=="2")
            self.close_login()
            print("Se ha logeado el usuario:",self.current_u["user_name"],"de tipo:",self.current_u["tipo_id"])

    def log_out(self):
        self.current_u = {}
        self.login_u_name = ""
        self.login_pword= ""
        self.usuario_is_emprendedor = False
        self.logged_in = False
    

    #Mensajes
    def open_mensaje(self):
        #Esto esta hardecodeado temporalmente
        self.destinatario = "XXXX"
        self.writing_to = "XXXX"
        self.bool_mensajes_modal = True
    def open_mensaje_2(self):
        #Esto esta hardecodeado temporalmente
        self.destinatario = "XXXX"
        self.writing_to = "XXXX"
        self.bool_mensajes_modal = True
    def close_mensaje(self):
        self.bool_mensajes_modal = False
    def enviar_mensaje(self):
        mnsaje = {}
        #mnsaje.create(self.current_u.u_id,self.destinatario.u_id,
        #self.current_u.u_name,self.destinatario.u_name,self.current_mensaje)
        #self.all_mensajes.append(mnsaje)
        self.bool_mensajes_modal = False
        self.current_mensaje = ""
        
    #Cargado de paginas        
    def load_producto(self):
        #nota: args es un diccionario con los parametros en la URL 
        #ej: /productos/[product_id] -> args={ "product_id" : n }
        for prod in self.productos_home:
            print(prod)
        args = self.get_query_params()
        key = int(args.get("producto_id"))
        self.current_p=rq.get(api+"producto/"+str(key)).json()
        self.page_color_theme = choice(["lightblue","lightgreen","purple","tomato","orange","yellow"])

    def load_tienda(self):
        args = self.get_query_params()
        key = int(args.get("tienda_id"))
        self.current_t=rq.get(api+"tienda/"+str(key)).json()
        self.productos_t=rq.get(api+"/producto/tienda"+str(self.current_t["id"])).json()
        self.page_color_theme = choice(["lightblue","lightgreen","purple","tomato","orange","yellow"])

    def load_home(self):
        print("Loading home")
        self.categorias = []
        qcat = rq.get(api+"categoria/").json()
        for cat in qcat:
            self.categorias.append(cat["nameCategory"])
        self.productos_home=rq.get(api+"producto/").json()

    def load_mensajes(self):
        self.all_mensajes = rq.get(api+"mensaje/").json()

    # Variables dinamicas
    @rx.var
    def current_producto(self)->dict:
        return self.current_p
    @rx.var
    def current_usuario(self)->dict:
        return self.current_u
    @rx.var
    def current_tienda(self)->dict:
        return self.current_t
    @rx.var
    def current_destinatario(self)->dict:
        return self.destinatario