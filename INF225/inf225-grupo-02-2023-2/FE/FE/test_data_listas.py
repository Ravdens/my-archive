
#Esto es para generar listas de productos y usuarios falsas. Al integrar la base de datos
#y APIs esto se hara con queries

from .models.Mensaje import Mensaje
from .models.Producto import Producto
from .models.Tienda import Tienda
from .models.Usuario import Usuario
from random import randint,choice


global L_productos
L_productos = []
global L_usuario
L_usuario = []
global L_tiendas
L_tiendas = []
global L_mensajes
L_mensajes = []


def make_test_data():
    for i in range(0,49):
        pi = Producto()
        p_id = randint(1,4)
        foto = "/../../productos/"+str(p_id)+".jpg"
        costo = "$"+str(randint(1,80)*1000)
        if(p_id == 1):
            nombre = "Peluche goomy"
            descripcion ="Descripción: Un bonito peluche de Goomy hecho de mocos"
            tienda = "Tortas el Jimmy"
            tienda_id = 0
            categorias = ["Manualidades"]
        elif(p_id == 2):
            nombre = "Piñata halloween"
            descripcion ="Descripción: Piñata decorativa para eventos de halloween"
            tienda = "Veysa manualidades"
            tienda_id = 1
            categorias = ["Manualidades"]
        elif(p_id == 3):
            nombre = "Decoración torta pascua"
            descripcion ="Descripción: Decoración para poner sobre torta con tematica de pascua"
            tienda = "Veysa manualidades"
            tienda_id = 1
            categorias = ["Manualidades","Pascua"]
        elif(p_id == 4):
            nombre = "Cupcakes cumpleaños"
            descripcion ="Descripción: Cupcakes personalizados para celebraciones"
            tienda = "BakeryBakeshop"
            tienda_id = 2
            categorias = ["Comida"]
        pi.create(i,p_id,foto,costo,nombre,descripcion,tienda,categorias,tienda_id)
        L_productos.append(pi) 
    

    usuarios =[(0,"huevito123","123","1"),
    (1,"xxjimmyxx","123","2"),
    (2,"juanito.peres","123","2"),
    (3,"sapo.sepo","123","3"),
    (4,"Elvis_Presley","321","1"),
    ]
    for tup in usuarios:
        ui = Usuario()
        ui.create(tup[0],tup[1],tup[2],tup[3])
        L_usuario.append(ui)


    #Agregar tienda 0 porque no existe
    t = Tienda()
    for producto in L_productos:
        if (producto.tienda_id==0 and producto.p_id==1):
            t.create(0,"xxjimmyxx",[producto],"Tortas el Jimmy","/../../tiendas/"+str(producto.tienda_id)+".jpg")
            break
    L_tiendas.append(t)
    t = Tienda()
    L_2 = []
    for producto in L_productos:
        if (producto.tienda_id==1 and producto.p_id==2):
            L_2.append(producto)
            break
    for producto in L_productos:
        if (producto.tienda_id==1 and producto.p_id==3):
            L_2.append(producto)
            t.create(1,"veysa.manualidades",L_2,"Veysa manualidades","/../../tiendas/"+str(producto.tienda_id)+".jpg")
            break   
    L_tiendas.append(t)
    t = Tienda()
    for producto in L_productos:
        if (producto.tienda_id==2 and producto.p_id==4):
            t.create(2,"juanito.peres",[producto],"BakeryBakeshop","/../../tiendas/"+str(producto.tienda_id)+".jpg")
            break
    L_tiendas.append(t)

    #Solo supondremos que son todos los del juanito.peres
    mensajes=[
        "Hola, queria preguntar por los cupcakes. ¿Entregan a Honduras?",
        "¿Los cupcakes pueden ser sin maní? soy alergico al mani",
        "Me gustaría saber si pueden enviar los cupcake por correo",
        "¿Que tan largo puede ser el texto en los cupcake? No se si quepa mi nombre"
    ]
    l = [0,1,3,4]
    for txt in mensajes:
        m = Mensaje()
        i = choice(l)
        l.remove(i)
        m.create(i,3,L_usuario[i].u_name,"juanito.peres",txt)
        L_mensajes.append(m)
