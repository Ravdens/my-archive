from random import randint
from reflex import Base

class Producto(Base):
    #Defaults
    l_id: int = -1
    #Esto solo es l_id en string ya que un iterador
    #en variable de estado no puede hacer la conversion
    l_id_str: str = "null"
    p_id: int = -1
    foto :str = "null"
    nombre: str = ""
    descripcion: str = ""
    tienda: str = ""
    tienda_id: int = -1
    #Lo mismo que l_id_str
    tienda_id_str: str = "null"
    categorias: list[str] = []
    costo: str = ""

    #Crear data de prueba, esto despues se hara por queries
    def create(self,new_id,p_id,foto,costo,nombre,descripcion,tienda,categoria,tienda_id):
        self.l_id = new_id
        self.l_id_str = str(self.l_id)
        self.p_id=p_id
        self.costo=costo
        self.foto=foto
        self.nombre=nombre
        self.descripcion=descripcion
        self.tienda=tienda
        self.tienda_id=tienda_id
        self.tienda_id_str=str(tienda_id)
        self.categorias=categoria

    
