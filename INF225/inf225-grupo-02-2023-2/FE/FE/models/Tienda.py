from reflex import Base

class Tienda(Base):
    tienda_id : int = "null"
    nombre: str = "null"
    owner : str = "null"
    productos : list = []
    foto: str = "null"

    def create(self,tienda_id,owner,productos,nombre,foto):
        self.tienda_id=tienda_id
        self.nombre = nombre
        self.owner=owner
        self.productos=productos
        self.foto=foto