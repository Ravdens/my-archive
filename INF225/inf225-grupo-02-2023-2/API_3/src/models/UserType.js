const { DataTypes } = require("sequelize");
const sequelize = require("../db.js");
const User = require("./Users.js");

const UserType = sequelize.define(
  "UsersType",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    nameCategory: {
      type: DataTypes.STRING,
    },
  },
  { timestamps: false }
);

/** 
UserType.hasMany(User, {
  foreignKey: "tipo_id",
  sourceKey: "id",
});

User.belongsTo(UserType, {
  foreignKey: "id",
  targetId: "tipo_id",
});
*/

module.exports = UserType;