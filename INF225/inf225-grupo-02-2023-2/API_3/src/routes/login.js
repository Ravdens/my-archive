const express = require("express");
const { getUsers, loginUser , createUser,getUser} = require("../controllers/users.controller.js");

const router = express.Router();

router.get("/user", getUsers);
router.get("/user/:id",getUser);
router.post("/user/login", loginUser);
router.post("/user/",createUser)

module.exports = router;
