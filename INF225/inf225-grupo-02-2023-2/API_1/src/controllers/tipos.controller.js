const Tipo = require("../models/Tipo.js");

const createTipo = async (req, res) => {
  const { nameCategory } = req.body;

  if (!nameCategory) {
    res.status(400);
    return;
  }
  const tiendaTipo = await Tipo.create({
    nameCategory,
  });
  res.json(tiendaTipo).status(200);
};

const getTipo = async (req, res) => {
  const categories = Tipo.findAll();
  res.json(categories);
};

module.exports = { createTipo, getTipo };
