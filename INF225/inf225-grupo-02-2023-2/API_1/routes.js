const tienda = require("./src/routes/tienda.js");
const tipo = require("./src/routes/tipo.js");
const ind = require("./src/routes/index.js")

module.exports = (app) =>{
    app.use(tienda);
    app.use(tipo);
    app.use(ind);
};