const { DataTypes } = require("sequelize");
const sequelize = require("../db.js");

const Producto = sequelize.define(
  "Productos",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: {
      type: DataTypes.STRING,
    },
    foto: {
      type: DataTypes.STRING,
    },
    costo: {
      type: DataTypes.INTEGER,
    },
    description: {
      type: DataTypes.STRING,
    },
    tienda_name:{ //Esto está aquí temporalmente y debe ser removido al integrar la API de composición
      type: DataTypes.STRING,
    },
    tienda_id: {
        type: DataTypes.INTEGER,
    },
  },
  {
    timestamps: false,
  }
);

module.exports = Producto;
