const producto = require("./src/routes/producto.js");
const categoria = require("./src/routes/categoria.js");
const categoriaXproducto = require("./src/routes/prodXCat.js");
const ind = require("./src/routes/index.js")

module.exports = (app) =>{
    app.use(producto);
    app.use(categoria);
    app.use(categoriaXproducto);
    app.use(ind);
};